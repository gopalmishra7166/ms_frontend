import { MerchantKey } from './MerchantKey';
import {LoginData} from './loginData';

export class ProductMapping {
    productName: string;
    country: string;
    measurementPoints;
    merchantKey: MerchantKey;
}
