export class AddressInfoV2{
  country: string;
  state: string;
  city: string;
  street_name: string;
  zip: number;

}
