import {MerchantAdress} from './merchant-adress';

export class MerchantInfo {
    email_id: string;
    password = 'old';
    companyName: string;
    contactPersonName: string;
    location: string;
    country_code2: string;
    area_code: string;
    mobile_num: string;
    logo: string;
    landline_num: string;
    profilePic: string;
    street_name: string;
    pincode: string;

    city: string;
    state: string;
    country: string;
    designationOfContactPerson: string;
    emailOfContactPerson: string;
    csp_id: string;
    altMobileOfContactPerson: string;
    landlineOfContactPerson: string;
    // contactPersonName;
    address: MerchantAdress[];

    // Contact Person Information
    //  contactPersonName;
    //  mobile_num;
    //  designationOfContactPerson;
    //  emailOfContactPerson;
    //  altEmailOfContactPerson;
    //  altMobileOfContactPerson;
    //  profilePic="NA";
    //  landlineOfContactPerson;
    industriesType;

}
