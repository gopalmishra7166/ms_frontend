import { OfficeAddress } from './officeAddress';


export class CustomerService {
    firstName:      string;
    middleName:     string;
    lastName:       string;
    emailId:        string;
    mobile_number_country_code:     string;
    mobile_number_area_code:     string;
    mobile_num:     string;
    office_contact_number: string;

  }
  