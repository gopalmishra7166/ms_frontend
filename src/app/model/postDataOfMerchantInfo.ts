import { MerchantAdress } from './merchant-adress';
import { AddressInfo } from 'dgram';

export class postDataOfMerchantInfo {
	email_id: string;
     companyName: string;
	 logo: string;
	 location: string;
	 country_code: number;
	 area_code: number;
	 landline_num: number;
	 industriesType: string;
	address: MerchantAdress[];
	landMark: string;
	country: string;
	city: string;
	state: string;
	zip: string;
	contactPersonMobile: string;
	contactPersonAltMobile: string;
	phoneNoOfContactPerson: string;


	// Contact Person InformationconpanyContactNo
	 contactPersonName: string;
	 mobile_num: string;
	 designationOfContactPerson: string;
	 conpanyContactNo: string;
	 emailOfContactPerson: string;
	 csp_id: string;
	 altMobileOfContactPerson:  string;
	 profilePic: string;
	 landlineOfContactPerson: string;
}
