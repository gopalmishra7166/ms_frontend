export class OfficeAddress {
 
    state:          string;
    country:        string;
    city_name:           string;
    pin_code:            string;
    house_num:       string;
    building_name:  string;
    street_name:    string;
  }