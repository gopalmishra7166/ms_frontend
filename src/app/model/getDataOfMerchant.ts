export class getDataOfMerchant{

	 companyName: string;
	 logo: string;
	 industriesType: string;
	 Country: string;
	 ConpanyContactNo: string;
	 state: string;
	 landMark: string;
	 zip: string;
	 city: string;
	 contactPersonName: string;
	 designationOfContactPerson: string;
	 emailOfContactPerson: string;
	 altEmailOfContactPerson: string;
	 phoneNoOfContactPerson: string;
	 profilePic: string;
	 contactPersonMobile: string;
	 contactPersonAltMobile: string;
}