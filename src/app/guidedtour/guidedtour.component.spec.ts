import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidedtourComponent } from './guidedtour.component';

describe('GuidedtourComponent', () => {
  let component: GuidedtourComponent;
  let fixture: ComponentFixture<GuidedtourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidedtourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidedtourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
