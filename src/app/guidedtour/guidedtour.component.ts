import { Component, OnInit } from '@angular/core';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-guidedtour',
  templateUrl: './guidedtour.component.html',
  styleUrls: ['./guidedtour.component.css']
})
export class GuidedtourComponent implements OnInit {

  constructor(private readonly joyrideService: JoyrideService) { }

  ngOnInit() {
  }

  onClick() {
    this.joyrideService.startTour(
      { steps: ['firstStep', 'secondStep','thirdStep','forthStep']} // Your steps order
    );
  }

  onNext(id){
    let el = document.getElementById(id);
    el.scrollIntoView({
      behavior: 'smooth' 
    });
  }

  

}
