import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-i-frame-communicator',
  templateUrl: './i-frame-communicator.component.html',
  styleUrls: ['./i-frame-communicator.component.css']
})
export class IFrameCommunicatorComponent implements OnInit {

  constructor() { }

  callParentFunction(str) {
    // console.log(window.parent.parent['AuthorizeNetIFrame'])
    // console.log(window.parent.parent['CommunicationHandler'])
    if (str && str.length > 0
      && window.parent
      && window.parent.parent
      && window.parent.parent['CommunicationHandler']
      && window.parent.parent['CommunicationHandler'].onReceiveCommunication) {
      // Errors indicate a mismatch in domain between the page containing the iframe and this page.
      window.parent.parent['CommunicationHandler'].onReceiveCommunication(str);
    }
  }

  receiveMessage(event) {
    // console.log(event)
    if (event && event.data) {
      this.callParentFunction(event.data);
    }
  }

  ngOnInit() {
    if (window.addEventListener) {
      window.addEventListener("message", this.receiveMessage.bind(this), false);
    } else if (window['attachEvent']) {
      window['attachEvent']("onmessage", this.receiveMessage.bind(this));
    }

    if (window.location.hash && window.location.hash.length > 1) {
      this.callParentFunction(window.location.hash.substring(1));
    }
  }
}