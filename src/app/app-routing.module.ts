import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule,Routes } from '@angular/router';
import {HomePageComponent} from './home-page/home-page.component';
import {CompanyProfileComponent} from './company-profile/company-profile.component';
import {NavbarComponent} from './navbar/navbar.component';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {TermsConditionComponent} from './terms-condition/terms-condition.component';
import {FooterComponent} from './footer/footer.component';
import {MerchantAccesscodeComponent} from './merchant-accesscode/merchant-accesscode.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {ApparelDetailComponent} from './apparel-detail/apparel-detail.component';
import { AccessCodeS2fComponent } from './access-code-s2f/access-code-s2f.component';
import { ApparelComponent } from './apparel/apparel.component';
import { AddapparelComponent } from './addapparel/addapparel.component';
import { GraphComponent } from './graph/graph.component';
import { ReportsComponent } from './reports/reports.component';
import { PaymentComponent } from './payment/payment.component';
import {SubscriptionComponent} from './subscription/subscription.component';
import { AuthGuard } from './auth.guard';
import {ResourcesComponent} from './resources/resources.component';
import { UsersComponent } from './users/users.component';
import { ApparelS2fComponent } from './apparel-s2f/apparel-s2f.component';
import { UserComponent } from './user/user.component';
import { MeasurementguideComponent } from './measurementguide/measurementguide.component';
import { SupportComponent } from './support/support.component';
import { PaymentconfirmationComponent } from './paymentconfirmation/paymentconfirmation.component';
import { UpgradeplangmComponent } from './upgradeplangm/upgradeplangm.component';
import { UpgradeplansfComponent } from './upgradeplansf/upgradeplansf.component';
import { GuidedtourComponent } from './guidedtour/guidedtour.component';
import { SizechartComponent } from './sizechart/sizechart.component';
import { Guidedtours2fComponent } from './guidedtours2f/guidedtours2f.component';
import { UsermysizeComponent } from './usermysize/usermysize.component';
import { SizechartmysizeComponent } from './sizechartmysize/sizechartmysize.component';
import { MysizelistingComponent } from './mysizelisting/mysizelisting.component';
import { UserquicksizeComponent } from './userquicksize/userquicksize.component';
import { GuidedtourqsComponent } from './guidedtourqs/guidedtourqs.component';
import { SizechartquicksizeComponent } from './sizechartquicksize/sizechartquicksize.component';
import { GuidedtourmsComponent } from './guidedtourms/guidedtourms.component';
import { CoupanhistoryComponent } from './coupanhistory/coupanhistory.component';
import { TestingComponent } from './testing/testing.component';
import {IFrameCommunicatorComponent} from './i-frame-communicator/i-frame-communicator.component';
import { DemoComponent } from './demo/demo.component';

const routes: Routes = [
  {path: 'payment', component: PaymentComponent,canActivate:[AuthGuard]},
  {path: 'footer', component: FooterComponent},
  {path: '', redirectTo: 'home', pathMatch : 'full'},
  {path: 'home', component: HomePageComponent},
  {path: 'graph', component: GraphComponent,canActivate:[AuthGuard]},
  {path: 'front-header', component: NavbarComponent},
  {path: 'apparel/:id', component: ApparelComponent,canActivate:[AuthGuard]},
  {path: 'access-code-s2f', component: AccessCodeS2fComponent,canActivate:[AuthGuard]},
  {path: 'add-apparel/:id', component: AddapparelComponent,canActivate:[AuthGuard]},
  {path: 'apparel-detail', component: ApparelDetailComponent,canActivate:[AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {path: 'company-profile', component: CompanyProfileComponent,canActivate:[AuthGuard]},
  {path: 'privacy-policy', component: PrivacyPolicyComponent,canActivate:[AuthGuard]},
  {path: 'terms-and-conditions', component: TermsConditionComponent,canActivate:[AuthGuard]},
  {path: 'merchant-acceess-code', component: MerchantAccesscodeComponent,canActivate:[AuthGuard]},
  {path: 'reports', component: ReportsComponent,canActivate:[AuthGuard]},
  {path:'subscribe',component:SubscriptionComponent,canActivate:[AuthGuard]},
  {path:'resources',component:ResourcesComponent,canActivate:[AuthGuard]},
  {path:'users/:id',component:UsersComponent,canActivate:[AuthGuard]},
  {path:'user/:id',component:UserComponent,canActivate:[AuthGuard]},
  {path:'apparel-s2f/:id',component:ApparelS2fComponent,canActivate:[AuthGuard]},
  {path:'measurementguide',component:MeasurementguideComponent,canActivate:[AuthGuard]},
  {path:'paymentconfirmation',component:PaymentconfirmationComponent},
  {path:'support',component:SupportComponent,canActivate:[AuthGuard]},
  {path:'upgradeplangm',component:UpgradeplangmComponent,canActivate:[AuthGuard]},
  {path:'upgradeplansf',component:UpgradeplansfComponent,canActivate:[AuthGuard]},
  {path:'guidedtour',component:GuidedtourComponent,canActivate:[AuthGuard]},
  {path:'guidedtours2f',component:Guidedtours2fComponent,canActivate:[AuthGuard]},
  {path:'sizechart',component:SizechartComponent,canActivate:[AuthGuard]},
  {path:'usermysize',component:UsermysizeComponent,canActivate:[AuthGuard]},
  {path:'sizechartmysize',component:SizechartmysizeComponent,canActivate:[AuthGuard]},
  {path:'mysizelisting',component:MysizelistingComponent,canActivate:[AuthGuard]},
  {path:'userquicksize',component:UserquicksizeComponent,canActivate:[AuthGuard]},
  {path:'guidedtourqs',component:GuidedtourqsComponent,canActivate:[AuthGuard]},
  {path:'sizechartquicksize',component:SizechartquicksizeComponent,canActivate:[AuthGuard]},
  {path:'guidedtourms',component:GuidedtourmsComponent,canActivate:[AuthGuard]},
  {path:'coupanhistory',component:CoupanhistoryComponent,canActivate:[AuthGuard]},
  {path:'testing',component:TestingComponent},
  {path:'checkrecomandation',component:DemoComponent},
  { path: 'iFrameCommunicator', component: IFrameCommunicatorComponent },
  {path: '**', component: LoginComponent }
];

@NgModule({
  imports: [
    CommonModule,
    // RouterModule.forRoot(routes),
    RouterModule.forRoot(routes,{useHash:true})
  ],
  declarations: []
})
export class AppRoutingModule { }