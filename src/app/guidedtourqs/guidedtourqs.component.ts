import { Component, OnInit } from '@angular/core';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-guidedtourqs',
  templateUrl: './guidedtourqs.component.html',
  styleUrls: ['./guidedtourqs.component.css']
})
export class GuidedtourqsComponent implements OnInit {

  constructor(private readonly joyrideService: JoyrideService) { }

  ngOnInit() {
  }


  onClick() {
    this.joyrideService.startTour(
      { steps: ['firstStep', 'secondStep','thirdStep','forthStep']} // Your steps order
    );
  }

}
