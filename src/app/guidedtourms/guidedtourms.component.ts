import { Component, OnInit } from '@angular/core';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-guidedtourms',
  templateUrl: './guidedtourms.component.html',
  styleUrls: ['./guidedtourms.component.css']
})
export class GuidedtourmsComponent implements OnInit {

  constructor(private joyrideService:JoyrideService) { }

  ngOnInit() {
  }

  onClick() {
    this.joyrideService.startTour(
      { steps: ['firstStep']} // Your steps order
    );
  }

}
