import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ActivatedRoute } from '@angular/router';
import { PagerService } from '../services/pagerService';
const Swal = require('sweetalert2');
import {ToastrService} from 'ngx-toastr';
declare var $:any;
var counter=0;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  usersData=[];
  detailData=[];
  routeParam='';
  email;
  token;
  imgsrc:any;
  title;
  isChecked=false;
  gender;
  processId;
  searchKey="userName";
  searchText="";
  userCount=0;
  pager: any = {};
  pagedItems: any[];
  showLoader=false;
  status: any;
  expired=false;
  content="";

  approval;

  constructor(private service:MsServices,private toast: ToastrService,private rout:ActivatedRoute,private pagerService:PagerService) { }

  ngOnInit() 
  {
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Users";
    var param = this.rout.snapshot.paramMap.get("id");
    if(param=='GetMeasured')
    {
        this.routeParam="GET_MEASURED"
        // this.isGetmeasured = true;
    }
    else
    {
        this.routeParam="SIZE2FIT"
        // this.isGetmeasured = false;
    }

    var d=JSON.parse(localStorage.getItem('data'));
    this.email=d['emailId'];
    this.token=d['token'];

    
    if (d['product'].GET_MEASURED.paymentStatus=='INACTIVE') 
    {
      if(d['product'].GET_MEASURED.remainingDays < 0)
      {
        this.expired = false;
      }
      else
      {
        this.expired = true;
      }
    }
    if(this.expired==false)
    {
      this.getUserList(1);
    }
  }

  getUserList(page)
  {
    this.showLoader=true;
    var obj={"emailId":this.email,"token":this.token,"productName":this.routeParam,"pageNo":page.toString()}
    this.service.getUserList(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1" && response.data)
      {
        this.userCount=response.count
        this.usersData=response.data;
        this.pager = this.pagerService.getPager(this.userCount, page);
      }
      else
      {
        this.toast.warning(response.message, 'Message', {timeOut: 2000,}); 
      }
    })
  }

  setPage(page: number) 
  {
      this.pager = this.pagerService.getPager(this.userCount,page);
      this.pagedItems = this.usersData.slice(this.pager.startIndex, this.pager.endIndex + 1);
      this.getUserList(page);
  }

  processIdData(data)
  {
    this.showLoader=true;
    this.gender=data.gender;
    this.processId=data.processId;

    var obj={"processId":data.processId,"token":this.token,"productName":this.routeParam,"emailId":this.email,"pageNo":"1"}
    this.service.processIdData(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        this.detailData=response.data;
        $('#dataModal').modal('show')
;      }
      else
      {
        this.toast.warning(response.message, 'Message', {timeOut: 2000,}); 
      }
    })
  }

  getStatus(e,d)
  {
    this.status=e.target.value;
    this.processId=d.processId;
    if(this.status=='REJECT'){
      $('#aproveModal').modal('show');
    }
    else{
      this.changeAproval();
    }
    
  }

  changeAproval()
  { 
    this.showLoader=true;
      var obj={processId:this.processId,token:this.token,emailId:this.email,status:this.status}
      this.service.changeAproval(obj).subscribe(response=>{
        this.showLoader=false;
        if(response.code=="1")
        {
          this.toast.success(response.message, 'Message', {timeOut: 2000,}); 
          this.getUserList(1);
        }
        else
        {
          this.toast.warning(response.message, 'Message', {timeOut: 2000,}); 
        }
      })
  }

  changeAprovalWithnotification(content)
  {
    this.showLoader=true;
      var obj={processId:this.processId,token:this.token,emailId:this.email,status:this.status,textBody:content}
      this.service.changeAproval(obj).subscribe(response=>{
        this.showLoader=false;
        if(response.code=="1")
        {
          $('#aproveModal').modal('hide');
          this.toast.success(response.message, 'Message', {timeOut: 2000,}); 
          this.getUserList(1);
        }
        else
        {
          this.toast.warning(response.message, 'Message', {timeOut: 2000,}); 
        }
      })
  }


  searchUser()
  {
    this.showLoader=true;
    this.usersData=[];
    var obj={"token":this.token,"emailId":this.email,"productName":this.routeParam,"searchKey":this.searchKey,"searchValue":this.searchText,"pageNo":1};
    this.service.searchUserList(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        this.usersData=response.data;
      }
      else
      {
        this.toast.warning(response.message, 'Message', {timeOut: 2000,}); 
      }
    })
  }

  clear()
  {
    this.getUserList(1);
  }

  switchValue(e)
  {
      if(e.target.checked)
      {
          this.isChecked=true;
      }
      else
      {
          this.isChecked=false;
      }
  }

  download(data)
  {
    this.showLoader=true;
    let result = data.measurement.map(a => a.displayName);
    var obj={"gender":this.gender,"emailId":this.email,"productName":this.routeParam,"apparelId":data.apparelId,"brandName":data.brandName,"measurementPoint":result,"processId":this.processId,"token":this.token}
    this.service.getUserPdf(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        window.open(response.s3urlpdf);
      }
      else
      {
        this.toast.warning(response.message, 'Message', {timeOut: 2000,}); 
      }
    })
  }

  viewImages(val,title)
  {
    this.title=title
    this.imgsrc=val;
    $('#viewImages').modal('show');
  }

  rotate()
  {
      counter += 90;
      $('.img').css('transform', 'rotate(' + counter + 'deg)')
  }

  addClass()
  {
      document.getElementById('headerShow').style.zIndex='1';
      document.getElementById('outerMain').style.zIndex="9999"; 
      document.getElementById('aproveModal').style.background='rgba(0, 0, 0, 0.61)';
      document.getElementById('dataModal').style.background='rgba(0, 0, 0, 0.61)';
      document.getElementById('imageApproal').style.background='rgba(0, 0, 0, 0.61)';
  }

  closeModal()
  {
      document.getElementById('headerShow').style.zIndex='99999';
      document.getElementById('outerMain').style.zIndex="0"; 
  }

  imageViewApproval()
  {
    var obj={token:this.token,emailId:this.email,productName:this.routeParam,imageStatus:"Agree"}
     if(this.approval)
     {
       this.service.imageViewApproval(obj).subscribe(response=>{
         if(response.code=='1')
         {
             this.getUserList(1);
             $('#imageApproal').modal('hide');
         }
         else{
          this.toast.warning(response.message, 'Message', {timeOut: 2000,}); 
         }
       })
     }
  }

}
