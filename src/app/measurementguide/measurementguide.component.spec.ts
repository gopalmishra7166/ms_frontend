import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementguideComponent } from './measurementguide.component';

describe('MeasurementguideComponent', () => {
  let component: MeasurementguideComponent;
  let fixture: ComponentFixture<MeasurementguideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasurementguideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasurementguideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
