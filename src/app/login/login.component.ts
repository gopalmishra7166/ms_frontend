import {LoginData} from './../model/loginData';
import { Title }     from '@angular/platform-browser';
import {MerchantDetails} from './../model/merchantdetails';

import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';
import {MsServices} from './../services/ms-services';

const Swal = require('sweetalert2');
import {Router} from '@angular/router';
import {MissingInfo} from '../model/MissingInfo';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {Http} from '@angular/http';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    showLoader=false;
    planType: string;
    productName: string;

    constructor(private titleService: Title,private toast: ToastrService, private spinner: NgxSpinnerService, private router: Router, private serv: MsServices, private cookieService: CookieService, private http: Http) 
    {
       
    }
    isLogin=true;
    isforgotpassword=false;
    mdata: MissingInfo;
    subscribedProductStatus: any[];
    err: string = undefined;
    userValue: LoginData;
    merchantDetails: MerchantDetails;
    form = new FormGroup({
        mail: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', Validators.required)
    });
    remainingDay: any;
    responseLogin: any;
    publicIP: any;
    physicalIp: any;
    userCheck: any;
    isOtp=false;
    email='';
    otp='';

    getUrl() {
        return 'url(\'assets/img/background5.jpg\')';
    }

    ngOnInit() 
    {
        if(this.serv.isLoggedIn())
        {
            this.router.navigate(['/graph'])
        }

        this.titleService.setTitle( "Mirrorsize" );
        this.http.get('https://jsonip.com/').subscribe(data => {
            this.publicIP = data;
            const body = this.publicIP._body;
            this.physicalIp = JSON.parse(body).ip;
        });

        if(localStorage.getItem('useremail'))
        {
            this.form.patchValue({mail:localStorage.getItem('useremail')});
        }
        else{
            this.form.patchValue({mail:''});
        }
    
    }

    send() 
    {
        this.showLoader=true;
        var obj={"emailId":this.form.value.mail,"password":this.form.value.password,"physicalIp":this.physicalIp}
        this.serv.loginUser(obj).subscribe(res => {
          
            this.showLoader=false;
        
            if(res.code=="1")
            {
            this.responseLogin = res;

                if (this.responseLogin.emailStatus =='InActive' || this.responseLogin.emailStatus =='INACTIVE') 
                {
                    localStorage.setItem('verified','false'); 
                    this.isLogin=false;
                    this.router.navigate(['/signup']);
                    localStorage.setItem('userID',this.form.value.mail); 
                    localStorage.setItem('userId',this.form.value.mail);
                    localStorage.setItem('accountId',this.responseLogin['product'].accountId);

                    if(this.responseLogin['product'].GET_MEASURED)
                    {
                        this.productName = 'GET_MEASURED';
                        this.planType = this.responseLogin['product'].GET_MEASURED.planType;
                    }
                    else 
                    {
                        this.productName = 'SIZE2FIT';
                        this.planType = this.responseLogin['product'].GET_MEASURED.planType;
                    }
                    sessionStorage.setItem('productName', this.productName);
                }
                else if (this.responseLogin.accountStatus == 'Inactive' || this.responseLogin.accountStatus == 'INACTIVE') {
                    this.router.navigate(['/signup']);
                    localStorage.setItem('userID',this.form.value.mail); 
                    localStorage.setItem('userId',this.form.value.mail);
                    localStorage.setItem('accountId',this.responseLogin['product'].accountId);
                    localStorage.setItem('verified','true'); 
                    
                    if(this.responseLogin['product'].GET_MEASURED)
                    {
                        this.productName = 'GET_MEASURED';
                        this.planType = this.responseLogin['product'].GET_MEASURED.planType;
                    }
                    else 
                    {
                        this.productName = 'SIZE2FIT';
                        this.planType = this.responseLogin['product'].GET_MEASURED.planType;
                    }

                    sessionStorage.setItem('productName', this.productName);
                }
                
                else
                {
                   this.router.navigate(['/graph']);
                   localStorage.setItem('data',JSON.stringify(this.responseLogin)); 
                }
            }
            else
            {
                this.toast.warning('Invalid email or password', 'Error', {
                    timeOut: 1200,
                }); 
            }
        });
    }

    switchform(){
        console.log(this.isLogin);
        console.log(this.isforgotpassword)
        this.isLogin=!this.isLogin;
        this.isforgotpassword=!this.isforgotpassword;
    }

    onSubmit() {
        this.showLoader = true;
        this.form.value.mail;
        this.serv.recoverPassword({ emailId: this.form.value.mail }).subscribe(res => {

            if (res.code == "1") {
                this.showLoader = false;
                Swal('success', res.message, 'success');
            }
            else {
                Swal('Oops !', res.message, 'error');
            }
        });
    }

    // -----for  signup sending it to home page for  selection product--------//
    signUp() {
        this.toast.error('Please select the product first then signup.', 'Select product', { timeOut: 2000,});
        this.router.navigate(['/home']);
    }


    verifyOtp()
    {
        var obj={"email_id":this.email,"otp":this.otp}
        this.serv.verifyEmail(obj).subscribe(response=>{
            if(response.code=="1")
            {
                this.isLogin=true;
            }
            else
            {
                Swal('Oops!',response.message,'error');
            }
        })
    }

    sendOtp()
    {
        var obj={"emailId":this.email,"token":""}
        this.serv.sendOtp(obj).subscribe(response=>{
            if(response.code=="1")
            {
                this.isOtp=true;
            }
            else
            {
                Swal('Oops!',response.message,'error');
            }
        }) 
    }

    rememberMe(e)
    {
        if(e.target.checked)
        {
            localStorage.setItem('useremail',this.form.value.mail);
        }
        else
        {
            localStorage.removeItem('useremail');
        }
    }
    
}

