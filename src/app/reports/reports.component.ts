import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { IMyDpOptions } from 'mydatepicker';
const Swal = require('sweetalert2');


@Component({
    selector: 'app-reports',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

    public myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'yyyy-mm-dd',
        disableSince: { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() + 1 }
    };

    SIZE2FIT=false;
    GET_MEASURED=false;
    MYSIZE=false;
    QUICKSIZE=false;
    
    constructor(private service: MsServices, private toastr: ToastrService, private spinner: NgxSpinnerService) {}

    now = new Date();
    startdate: any;
    enddate: any;
    email: any;
    token: any;
    showLoader=false;
    productName="GET_MEASURED";
    month="jan"
    
    ngOnInit() 
    {
      
        var d=JSON.parse(localStorage.getItem('data'));
        this.token=d['token'];
        this.email=d['emailId'];
        if (d['product'].GET_MEASURED) {
            this.GET_MEASURED = true;
        }
        if(d['product'].SIZE2FIT) {
            this.SIZE2FIT = true;
        }
        if (d['product'].GET_MEASURED) {
            this.GET_MEASURED = true;
        }
        if(d['product'].MYSIZE) {
            this.MYSIZE = true;
        }
        if(d['product'].QUICKSIZE) {
            this.QUICKSIZE = true;
        }
     
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Clicks Report";
    }

    dowmnloadYourPdf() 
    {
        this.showLoader=true;
        var data={emailId:this.email,token:this.token,month:this.month,productName:this.productName}
        this.service.billingPdfGeneration(data).subscribe(response=>{
            this.showLoader=false;
            if(response.code=="1")
            {
                window.open(response.s3urlpdf, '_blank');
            }
            else
            {
                Swal('Oops !',response.message,'error')
            }
        })
    }
}
