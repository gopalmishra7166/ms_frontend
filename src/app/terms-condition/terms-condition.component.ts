import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
import {CookieService} from 'ngx-cookie-service';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { MerchantDetails } from './../model/merchantdetails';
import { MsServices } from '../services/ms-services';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-terms-condition',
  templateUrl: './terms-condition.component.html',
  styleUrls: ['./terms-condition.component.css']
})
export class TermsConditionComponent implements OnInit {

    now = new Date();

  merchantDetails: MerchantDetails;
  uid: string;
  name: string;
  minNUmber: string;
  constructor(private route: Router, private ser: MsServices, private cookieService: CookieService, public _sanitizer: DomSanitizer) {
    // -----------------------
    this.merchantDetails = new MerchantDetails();
    
    }

  ngOnInit() 
  {
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Terms & Conditions";
  }

}
