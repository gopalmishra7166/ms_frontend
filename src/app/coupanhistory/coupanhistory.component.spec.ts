import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoupanhistoryComponent } from './coupanhistory.component';

describe('CoupanhistoryComponent', () => {
  let component: CoupanhistoryComponent;
  let fixture: ComponentFixture<CoupanhistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoupanhistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoupanhistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
