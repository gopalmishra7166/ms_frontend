import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { PagerService } from '../services/pagerService';
import { ActivatedRoute, Router } from '@angular/router';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-coupanhistory',
  templateUrl: './coupanhistory.component.html',
  styleUrls: ['./coupanhistory.component.css']
})
export class CoupanhistoryComponent implements OnInit {

  rewardData=[];
  email='';
  token='';
  showLoader=false;
  searchKey="merchant_name"
  count=0;
  pager: any = {};
  pagedItems: any[];
  searchText="";
  expired=false;

  constructor(private router:Router,private service:MsServices,private pagerService:PagerService) { }

  ngOnInit() 
  {
    var d = JSON.parse(localStorage.getItem('data'));
    this.email = d['emailId'];
    this.token = d['token'];
    if (d['product'].MYSIZE.paymentStatus == 'INACTIVE') {
      if(d['product'].MYSIZE.remainingDays < 0)
      {
        this.expired = false;
      }
      else
      {
        this.expired = true;
      }
    }
    if(this.expired==false)
    {
      this.getrewarddata(); 
    }
  }
  gotoSubscription()
  { 
    localStorage.setItem('subscribeProduct','MYSIZE');
    this.router.navigate(['/subscribe'])
  }

  getrewarddata() 
  {
    this.showLoader = true;
    var obj = {
      "emailId": this.email,
      "token": this.token,
    }
    this.service.getrewarddata(obj).subscribe(response => {
      this.showLoader = false;
      if (response.code == "1") 
      {
        this.rewardData = response.data; 
        this.count=this.rewardData.length;
        this.setPage(1);
      }
      else if(response.code == "2") 
      {
        
      }
      else
      {
        // Swal('error', response.message, 'error');
      }
    })
  }

  setPage(page: number) 
  {
      this.pager = this.pagerService.getPager(this.count,page);
      this.pagedItems = this.rewardData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  changeStatus(id,e)
  {
    var obj={token:this.token,status:e.target.value,rewardId:id}
    this.service.changeStatus(obj).subscribe(response=>{

      if(response.code=="1")
      {
        Swal('success', response.message, 'success');
        this.getrewarddata();
      }
      else
      {
        Swal('error', response.message, 'error');
        this.getrewarddata();
      }
    })
  }


  searchCoupon()
  {
    this.count=0;
    this.rewardData=[]
    var obj={"token":this.token,"key":this.searchKey,"value":this.searchText}
    this.service.serachCoupon(obj).subscribe(response=>{
      if(response.code=="1")
      {
        this.rewardData=response.data;
        this.count=response.record_count;
      }
      else
      {
        Swal('Oops!',response.message,'info')
        this.count=0;
      }
    })
  }

  clear()
  {
    this.getrewarddata();
    this.searchText="";

  }

}
