import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradeplansfComponent } from './upgradeplansf.component';

describe('UpgradeplansfComponent', () => {
  let component: UpgradeplansfComponent;
  let fixture: ComponentFixture<UpgradeplansfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradeplansfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradeplansfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
