import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApparelS2fComponent } from './apparel-s2f.component';

describe('ApparelS2fComponent', () => {
  let component: ApparelS2fComponent;
  let fixture: ComponentFixture<ApparelS2fComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApparelS2fComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApparelS2fComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
