import {MsServices} from '../services/ms-services';
import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormControl, Validators, NgForm} from '@angular/forms';
import {PagerService} from '../services/pagerService'
const Swal = require('sweetalert2');
import {Router, ActivatedRoute} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ToastrService} from 'ngx-toastr';
import {DomSanitizer} from '@angular/platform-browser';
import { observable } from 'rxjs';

declare var $:any;


@Component({
  selector: 'app-addapparel',
  templateUrl: './addapparel.component.html',
  styleUrls: ['./addapparel.component.css']
})
export class AddapparelComponent implements OnInit {

 
  imageUrl="";
  keyVal='';
  pager: any = {};
  pagedItems: any[];
  showLoader=false;
  isSizeChart=false;
  addsize=false;
  show=true;
  storeCount=0;
  email: any;
  token: any;
  mid: any;
  dummyData:any
  sizeArray=[];
  country = ['China', 'Euro', 'India', 'Japan', 'Russia', 'US', 'UK','Other'];
  genderType = '';
  status = true;
  apparels = [];
  finalPoints={};
  gender='';
  selectedType="body-fixed";
  tolrance=[];
  priorit=[];  
 
  selectedItems = [];

  routeParam='';
  size='';
  measurement={};
  finalData=[];
  allValues=[];
  isOther=false;
  mn=0;
  mx=0
  al=0;
  tol=0;
  pri=0;
  apparelId;
  brandName;
  countrySize;
  isEdit=false;
  apparelForm:FormGroup;

  constructor(private router:Router,public rout:ActivatedRoute,public pagerService:PagerService,private toastr: ToastrService, private service: MsServices, private route: Router, private cookieService: CookieService, public _sanitizer: DomSanitizer) 
  {

    this.apparelForm = new FormGroup({
        brandName: new FormControl('', ),
        country: new FormControl('', ),
        gender: new FormControl('', ),
        apparelCategory: new FormControl(''),
        countryName:new FormControl('')
    });
  
  }
  
  
  ngOnInit() 
  {
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Add apparel";
      var param= this.rout.snapshot.paramMap.get("id");

      if(param=='GET_MEASURED')
      {
        this.isEdit=false;
        localStorage.removeItem('editMeasurement');
      }
      if(param=='GetMeasured' || param=='GET_MEASURED')
      {
          this.routeParam="GET_MEASURED";
       
      }
      else{
          this.routeParam="SIZE2FIT"
      }
     
      var d=JSON.parse(localStorage.getItem('data'));
      this.email=d['emailId'];
      this.token=d['token'];
      var dd=JSON.parse(localStorage.getItem('editMeasurement'));
      if(dd)
      {
          var chartType=JSON.parse(dd[0].sizeChart);
          this.selectedType=chartType.sizeChartType;
          this.editData();
      }

      $(".block__pic").imagezoomsl({
        loadinggif: '',
        loadopacity: 0,
    });

    }

    ngAfterViewInit()
    {
        var editTab = <HTMLTableElement>document.getElementById('myTable');
        var tTable=<HTMLTableElement>document.getElementById('TolrenceTable');

        if(tTable)
        {
            var tCells;
            var tRows=tTable.rows;

            for(var i=1;i<tRows.length;i++)
            {
                tCells=tRows[i].cells;

            for(var j=0;j<tCells.length;j++)
            {
                tCells[1].children[0].value=this.tolrance[this.tol];
                tCells[2].children[0].selectedOptions[0].innerHTML=this.priorit[this.tol];
            }

            this.tol++;
        }
        
        }

        if(editTab)
        {
            var rows=editTab.rows;
            var cell;
            var min;
            var max;
            var minArray=[];
            var maxArray=[];
            var allowance;
            var divs;
            var array=[];
          
            for(var i=0;i<this.allValues.length;i++)
            {
                if(this.allValues[i].key.match(/min/g))
                {
                    minArray.push(this.allValues[i].value);
                }
                else if(this.allValues[i].key.match(/max/g))
                {
                    maxArray.push(this.allValues[i].value)
                }
                else
                {
                    array.push(this.allValues[i].value)
                }
            }

            for(var i=1;i<rows.length;i++)
            {  
                cell=rows[i].cells;

                if(this.selectedType=='body-range')
                {

                    min=cell[2].children;
                    max=cell[3].children;
            
                    for(var k=0;k<min.length;k++)
                    {    
                    
                        min[k].children[0].value=minArray[this.mn++];
                        max[k].children[0].value=maxArray[this.mx++];
            
                    }
                }

                else if(this.selectedType=='ready-range')
                {
            
                   
                    min=cell[2].children;
                    max=cell[3].children;
                    allowance=cell[4].children;
                  
                    for(var k=0;k<min.length;k++)
                    {    
                        min[k].children[0].value=minArray[this.mn++];
                        max[k].children[0].value=maxArray[this.mx++];
                        allowance[k].children[0].value=maxArray[this.al++]
                    }
                    
                }
                
                else if(this.selectedType=='ready-fixed')
                {
                
                    divs=cell[2].children;
                    allowance=cell[3].children;
                
                    for(var k=0;k<divs.length;k++)
                    {    
                        divs[k].children[0].value=array[this.mn++]
                        allowance[k].children[0].value=array[this.al++]
                    }

                }
                else
                {
                    divs=cell[2].children;
                    for(var k=0;k<divs.length;k++)
                    {
                        divs[k].children[0].value=array[this.mn++];
                   }
                }
            }
            this.mn=0;
            this.mx=0;
            this.al=0;
        }                
    }

    onChecked(key,value,checked) 
    {    
        if(checked)
        {
            this.selectedItems.push({key:key,value:value});
         
        }
        else
        {
          
            var myArray = this.selectedItems.filter(function(obj,i) 
            {
                return obj.value != value;
            });
            
            this.selectedItems = [];
            this.selectedItems=myArray;
          
    }

    this.finalPoints = {};
    for (var i=0; i<this.selectedItems.length; i++) {
        this.finalPoints[this.selectedItems[i].key] = this.selectedItems[i].value;
    }

}

    onCheck(key,value,e) 
    {   

        if(e.target.checked)
        {
            this.selectedItems.push({key:key,value:value});
            for(var i=0;i<this.sizeArray.length;i++)
            {
                this.sizeArray[i].points=this.selectedItems;   
            }
        }
        else
        {
            var myArray = this.selectedItems.filter(function(obj) 
            {
                return obj.value != value;
            });
            
            this.selectedItems = [];
            this.selectedItems=myArray;
            
            for(var i=0;i<this.sizeArray.length;i++)
            {
                var myArr = this.sizeArray[i].points.filter(function(obj) 
                {
                    return obj.value != value;
                });
                
                this.sizeArray[i].points=[]
                this.sizeArray[i].points=myArr;
            }

        }

    this.finalPoints = {};
    for (var i=0; i<this.selectedItems.length; i++) {
        this.finalPoints[this.selectedItems[i].key] = this.selectedItems[i].value;
    }

}

  editData()
  {
    this.isEdit=true;
    this.sizeArray=[]
    var chartData={};
    this.selectedItems=[];
    var d=JSON.parse(localStorage.getItem('editMeasurement'));
    
      if(d)
      {
        
        var _self=this;
        this.apparelId=d[0].apparelId;
        this.brandName=d[0].brandName;
        this.countrySize=d[0].countrySize;
        this.gender=d[0].gender
       
        var chartType=JSON.parse(d[0].sizeChart);
        
        this.isSizeChart=true;
        this.addsize=true;
        this.show=false;

        this.selectedItems = Object.keys(d[0].measurementPoint).map(function(key) {
            return ({"key":(key),"value":d[0].measurementPoint[key]});
          });
        
          var arr=[];
          var key=[];
          chartData=chartType.apparel;

            var objNameArray = [];
            var allValues = [];
            var usedKeys = {};

            for(var k in chartData)
            {
                objNameArray.push(k);
            }

            for(var i = 0; i < objNameArray.length; ++i)
            {
             
                if(!chartData[objNameArray[i]]) 
                {
                    continue;
                }
                var currentObj = chartData[objNameArray[i]];
            
                for(var j in currentObj)
                {       
                    this.allValues.push({key:j,value:currentObj[j]});
                }   
            }

          this.sizeArray=Object.keys(chartData).map(function(key,i){
            
           arr=Object.keys(chartData[key]).map(function(k){
             
               return (chartData[key][k])
           })
            return ({size:(key),points:_self.selectedItems,measurement:chartType.apparel[key]});
          })

          for(k in chartType.tolerence)
          {
              this.tolrance.push(chartType.tolerence[k])
          }

          for(k in chartType.priority)
          {
              this.priorit.push(chartType.priority[k])
          }
         
        }
      else
      {
        this.isSizeChart=false;
        this.addsize=false;
        this.show=true;
      }

      this.finalPoints = {};
      for (var i=0; i<this.selectedItems.length; i++) {
          this.finalPoints[this.selectedItems[i].key] = this.selectedItems[i].value;
      }

      this.getMeasurementPoints();

  }


  submit()
  {
   
      if(this.apparelForm.value.brandName=="")
      {
        Swal('Message',"Please enter brand name",'info')
       
      }
      else if(this.apparelForm.value.gender=="")
      {
        Swal('Message',"Please select gender",'info')
      }
      else if(this.apparelForm.value.country=="")
      {
        Swal('Message',"Please select country",'info') 
      }
      else if(this.apparelForm.value.apparelCategory=="")
      {
        Swal('Message',"Please select apparel category",'info') 
      }
      else
      {
        this.getMeasurementPoints();     
        this.gender=this.apparelForm.value.gender
      }
  }

  getMeasurementPoints()
  {
      var gender='';
      this.showLoader=true;
      if(this.isEdit)
      {
          gender=this.gender;
      }
      else{
          gender=this.apparelForm.value.gender;
      }
      var obj={"gender":gender,"token":this.token,"productName":this.routeParam,"emailId":this.email}
      this.service.getMeasurementPoints(obj).subscribe(response=>{
          this.showLoader=false;
          if(response.code=="1")
          {
              this.dummyData=response.data
          }
          else
          {
              Swal('error',response.message,'error');
          }
      })
  }


  
  


  mapValue(data2) {
      return data2;
  }

  getPoints()
  {
    var size={}
    var finalObj={};
    var arr=[];
    var oCells;
    var tCells;
    var divs;
    var allowance;
    var obj={};
    var tll=[];
    var prr=[];
    var oTable = <HTMLTableElement>document.getElementById('myTable');
    var tTable=<HTMLTableElement>document.getElementById('TolrenceTable');
    var data={};
    var tRows=tTable.rows;
    var rowLength = oTable.rows;
    var obj={}
    var tol={};
    var priority={};
    var min;
    var max;

    for(var i=0;i<tRows.length;i++)
    {
        if(i==0)
        {
            continue;
        }
        else
        {
          
                tCells=tRows[i].cells;
                for(var j=0;j<tCells.length;j++)
                {
                    tol[tCells[0].innerHTML]=tCells[1].children[0].value;
                    priority[tCells[0].innerHTML]=tCells[2].children[0].selectedOptions[0].innerHTML
                }
         
        }
    }

    for (var i = 1; i < rowLength.length; i++)
    {          

            oCells=rowLength[i].cells;

            for(var j=i;j>=i;j--)
            {
                 if(this.selectedType=='body-range')
                 {   
                    obj={};  
                    min=oCells[2].children;
                    max=oCells[3].children;
                    for(var k=0;k<min.length;k++)
                    {
                        obj[min[k].children[0].placeholder+"_min"]=min[k].children[0].value;
                        obj[max[k].children[0].placeholder+"_max"]=max[k].children[0].value;
                        size[oCells[1].innerHTML]=obj;
                    }
                    
                 }

                 else if(this.selectedType=='ready-range')
                 {
                    obj={};  
                    min=oCells[2].children;
                    max=oCells[3].children;
                    allowance=oCells[4].children;

                    for(var k=0;k<allowance.length;k++)
                     {
                        obj[min[k].children[0].placeholder+"_min"]=min[k].children[0].value;
                        obj[max[k].children[0].placeholder+"_max"]=max[k].children[0].value;
                        obj[allowance[k].children[0].placeholder+"_allowance"]=allowance[k].children[0].value;
                        size[oCells[1].innerHTML]=obj;
                     
                    }

                 }

                 else if(this.selectedType=='ready-fixed')
                 {
                    obj={};  
                    divs=oCells[2].children;
                    allowance=oCells[3].children;

                    for(var k=0;k<allowance.length;k++)
                    {
                       obj[divs[k].children[0].placeholder]=divs[k].children[0].value;
                       obj[allowance[k].children[0].placeholder+"_allowance"]=allowance[k].children[0].value;
                       size[oCells[1].innerHTML]=obj;
                   }
                 }
                 else
                 {
                    obj={};  
                    divs=oCells[2].children;
                     for(var k=0;k<divs.length;k++)
                     {
                      obj[divs[k].children[0].placeholder]=divs[k].children[0].value;
                      size[oCells[1].innerHTML]=obj;
                    }

                }                
        }
    }

            var dupe = false;
            var keys = Object.keys(priority);
        
            for(var i=0;i<keys.length;i++){
            for(var j=i+1;j<keys.length;j++){
            if(priority[keys[i]] === priority[keys[j]]){
                dupe = true;
                break;
            }
            }
            if(dupe)
            { 
                Swal('Oops !','Priority can not be duplicate','info');
                break; 
            }
            else
            {         
                this.finalData.push({"sizeChartType":this.selectedType,"apparel":size,"priority":priority,"tolerence":tol})   
            }
        }
        

    if(this.isEdit)
    {
        if(dupe)
        {
            data={};
        }
        else{

            data={
                'token': this.token,
                'emailId': this.email,
                'apparelId':this.apparelId,
                'gender':this.gender,
                'brandName':this.brandName,
                'countrySize':this.countrySize,
                'productName':this.routeParam,
                "measurementPointData":this.finalPoints,
                "sizeChartData":this.finalData[0],
                "action":"update"
            }

            this.addApprel(data);
        }
    }
    else
    {
        if(dupe)
        {

        }
        else{

        data={
                'token': this.token,
                'emailId': this.email,
                'apparelId': this.apparelForm.value.apparelCategory,
                'gender': this.apparelForm.value.gender,
                'brandName': this.apparelForm.value.brandName,
                'countrySize': this.apparelForm.value.country,
                'productName':this.routeParam,
                "measurementPointData":this.finalPoints,
                "sizeChartData":this.finalData[0]
            }

            this.addApprel(data);
        }
    }
      
      
  }
 
  addApparel(val)
  {
    
    var data={}
    if(this.isOther)
    {
       this.countrySize=this.apparelForm.value.countryName
    }
    else
    {
       this.countrySize=this.apparelForm.value.country
    }
   if(val=='GET_MEASURED') 
   {
    if(this.selectedItems.length>0)
    {
      data={
          'token': this.token,
          'emailId': this.email,
          'apparelId': this.apparelForm.value.apparelCategory,
          'gender': this.apparelForm.value.gender,
          'brandName': this.apparelForm.value.brandName,
          'countrySize':this.countrySize,
          'productName':this.routeParam,
          "measurementPointData":this.finalPoints,
      }

      this.addApprel(data)
    }
    }
    else
    {
       this.addsize=true;
       this.show=false; 
       this.isSizeChart=true; 
    }
  } 


addApprel(data)
{
  
    this.service.addApparel(data).subscribe(res => 
    {
        if(res.code==1)
        {
            this.apparels = res.data;
            if(this.routeParam=="GET_MEASURED")
            {
                this.router.navigate(['/apparel/'+this.routeParam]);
                this.isEdit=false;
            }
            else
            {
                this.router.navigate(['/apparel-s2f/'+this.routeParam]);
            }
        }
        else{
            Swal.fire('Oops !',res.message,'error');
        }
    });
}

getType(e)
{
    this.selectedType=e.target.value;
}

  addSize()
  {
    if(this.size=='' || this.size==undefined)
    {
        Swal('Oops !','Please enter a size','info');
    }
    else
    {
        this.sizeArray.push({size:this.size,points:this.selectedItems});
        this.size='';
    }
  }

  next()
  {
      this.isSizeChart=true;
      this.addsize=false;
      this.show=false;
  }

  showModal()
  {
     
    $(".block__pic").imagezoomsl({
        zoomrange: [4,4],
        stepzoom: 0.5,
        cursorshade:true,
    });
     
  }    

  getImage(val,gen,keyval)
  {
      this.keyVal=keyval;
      this.imageUrl="https://s3.ap-south-1.amazonaws.com/commonms/"+gen.toLowerCase()+"_large_icon/"+val+".png";
      $('#viewImage').modal('show');
  } 

  numberOnly(event) 
  {
    //   const charCode = (event.which) ? event.which : event.keyCode;
    //   if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    //     return false;
    //   }
    //   return true;
    // }
  }
    isSelected(data,k)
    {
        for(var i=0;i<this.selectedItems.length;i++)
        {
            if(this.selectedItems[i].key==data.key){
                return true
            }
            else{
                $('#check'+k).prop('checked',false)
            }
        }
    }

    getCountry(e)
    {
        if(e.target.value=='Other')
        {
            this.isOther=true;
            this.countrySize=this.apparelForm.value.countryName;
        }
        else
        {
            this.isOther=false;
        }
    }
}