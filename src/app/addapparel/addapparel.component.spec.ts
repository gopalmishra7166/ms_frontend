import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddapparelComponent } from './addapparel.component';

describe('AddapparelComponent', () => {
  let component: AddapparelComponent;
  let fixture: ComponentFixture<AddapparelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddapparelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddapparelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
