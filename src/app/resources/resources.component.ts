import { Component, OnInit } from '@angular/core';
import {MsServices} from '../services/ms-services';
import { Router } from '@angular/router';
declare var $:any;
const Swal = require('sweetalert2');

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css']
})
export class ResourcesComponent implements OnInit {

  merchantId='';
  apikey='';
  pass='';
  planDetailGM:any
  planDetailS2F:any
  planDetailsQS:any
  expired: boolean;
  gm: boolean;
  sf: boolean;
  sfexpired: boolean;
  gmexpired: boolean;
  GET_MEASURED=false;
  SIZE2FIT=false;
  qs: boolean;
  QUICKSIZE=false;
  qsexpired: boolean;
  localData:any;
  constructor(public service:MsServices,private router:Router) { }

  ngOnInit() 
  {
    var d=JSON.parse(localStorage.getItem('data'));
    if(d)
    {
      this.merchantId=d['emailId'];
      this.localData=d['product'];
    }
    
    if(d['product'].GET_MEASURED && d['product'].GET_MEASURED.paymentStatus=='ACTIVE')
    {
      this.planDetailGM=d['product'].GET_MEASURED;
      this.GET_MEASURED=true;
      this.gm=true;
      this.sf=false;
      this.qs=false;
      console.log(this.GET_MEASURED);
    }
    if(d['product'].SIZE2FIT && d['product'].SIZE2FIT.paymentStatus=='ACTIVE')
    {
      this.planDetailS2F=d['product'].SIZE2FIT; 
      this.SIZE2FIT=true;
      this.gm=false;
      this.sf=true;
      this.qs=false;
    }
    if(d['product'].QUICKSIZE && d['product'].QUICKSIZE.paymentStatus=='ACTIVE')
    {
      this.planDetailsQS=d['product'].QUICKSIZE; 
      this.QUICKSIZE=true;
      this.gm=false;
      this.sf=false;
      this.qs=true;
      this.apikey=d['product'].QUICKSIZE.apiKey
    }
    // if(d['apiKey'])
    // {
    //   this.apikey=d['apiKey'];
    // }

    if(d['product'].GET_MEASURED && d['product'].GET_MEASURED.paymentStatus=='ACTIVE'){
      this.GET_MEASURED=true;
      this.gm=true;
      this.sf=false;
      this.qs=false;
      this.apikey=d['product'].GET_MEASURED.apiKey
    }
    else if(d['product'].SIZE2FIT && d['product'].SIZE2FIT.paymentStatus=='ACTIVE'){
      this.SIZE2FIT=true;
      this.gm=false;
      this.sf=true;
      this.qs=false;
      this.apikey=d['product'].SIZE2FIT.apiKey
    }
    else if(d['product'].QUICKSIZE && d['product'].QUICKSIZE.paymentStatus=='ACTIVE'){
      this.QUICKSIZE=true;
      this.gm=false;
      this.sf=false;
      this.qs=true;
    }

    if (d['product'].SIZE2FIT){
      if (d['product'].SIZE2FIT.planType == 'freeTrial' || d['product'].SIZE2FIT.planType == 'FREETRIAL') {
        if(d['product'].SIZE2FIT.remainingDays < 0)
        {
          this.sfexpired = true;
        }
        else
        {
          this.sfexpired = false;
        }
      }
    }
    
    if (d['product'].GET_MEASURED){
      if (d['product'].GET_MEASURED.planType == 'freeTrial' || d['product'].GET_MEASURED.planType == 'FREETRIAL') {
        if(d['product'].GET_MEASURED.remainingDays < 0)
        {
          this.gmexpired = true;
        }
        else
        {
          this.gmexpired = false;
        }
      }
    }

    if (d['product'].QUICKSIZE){
      if (d['product'].QUICKSIZE.planType == 'freeTrial' || d['product'].QUICKSIZE.planType == 'FREETRIAL') {
        if(d['product'].QUICKSIZE.remainingDays < 0)
        {
          this.qsexpired = true;
        }
        else
        {
          this.qsexpired = false;
        }
      }
    }
    
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Developer Resources";
  }

  view(val)
  {
    if(val=='android')
    {
      window.open('https://s3.ap-south-1.amazonaws.com/commonms/developers/documents/android_sdk_developer_document.pdf','_blank')
    }
    else if(val=='ios')
    {
      window.open('https://s3.ap-south-1.amazonaws.com/commonms/developers/documents/ios_sdk_developer_document.pdf','_blank')
    }
    else if(val=='getms')
    {
      window.open('https://s3.ap-south-1.amazonaws.com/commonms/developers/documents/getmeasured_webapis_developer_document.pdf','_blank')
    }
    else if(val=='getqs')
    {
      window.open('https://s3.ap-south-1.amazonaws.com/commonms/developers/documents/quicksize_webapis_developer_document.pdf','_blank')
    }
    else
    {
      window.open('https://s3.ap-south-1.amazonaws.com/commonms/developers/documents/size2fit_webapis_developer_document.pdf','_blank')
    }
  }


  key(val)
  {
    if(val=='gm'){
      this.gm=true;
      this.sf=false;
      this.qs=false;
    }
    else if(val=='sf'){
      this.gm=false;
      this.qs=false;
      this.sf=true;
    }
    else if(val=='qs')
    {
      this.qs=true;
      this.gm=false;
      this.sf=false;
    }
  }

  getKey()
  {
    if(this.pass=="" || this.pass==undefined)
    {
      Swal('info','please enter current password','info')
    }
    else{
      this.service.getApiKey({email_id:this.merchantId,password:this.pass}).subscribe(response=>{
        if(response.code=="1")
        {
           this.apikey=response.data;
           $('#apikeyModal').modal('hide');
           localStorage.setItem('key',this.apikey);
        }  
        else
        {
          Swal('Oops!',response.message,'error')
        }
      })
    }
  }

  gotoSubscription()
  { 
    localStorage.setItem('subscribeProduct','QUICKSIZE');
    this.router.navigate(['/subscribe'])
  }
  

}
