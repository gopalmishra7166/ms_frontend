import { Component, OnInit } from '@angular/core';
import { JoyrideService } from 'ngx-joyride';

@Component({
  selector: 'app-guidedtours2f',
  templateUrl: './guidedtours2f.component.html',
  styleUrls: ['./guidedtours2f.component.css']
})
export class Guidedtours2fComponent implements OnInit {

  constructor(private readonly joyrideService: JoyrideService) { }

  ngOnInit() {
  }


  onClick() {
    this.joyrideService.startTour(
      { steps: ['firstStep', 'secondStep','thirdStep','forthStep']} // Your steps order
    );
  }

}
