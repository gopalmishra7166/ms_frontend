import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import * as S3 from 'aws-sdk/clients/s3';
import { FormGroup, FormControl, Validators, FormBuilder, ɵangular_packages_forms_forms_f } from '@angular/forms';
import {IMyDpOptions} from 'mydatepicker';
import { ActivatedRoute, Router } from '@angular/router';
declare var $: any;
const Swal = require('sweetalert2');


@Component({
  selector: 'app-mysizelisting',
  templateUrl: './mysizelisting.component.html',
  styleUrls: ['./mysizelisting.component.css']
})
export class MysizelistingComponent implements OnInit {

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
};

  rewardForm: FormGroup;
  logo: string;
  backgroundImg: string;
  showLoader = false;
  timestamp: number;
  accountId: string;
  data: any;
  email: any;
  token: any;
  image: string;
  merchantName = '';
  voucher = '';
  redirectLink = '';
  validityDay = '';
  tagLine = '';
  ifEdit = false;
  path: string;
  rewardData:any;
  data1: File;
  image1: string;
  logoImage:any
  banner:any
  isLogo=false;
  isBackground=false;
  day:any='';
  expired=false;

  constructor(private router:Router,private service: MsServices) {
      this.rewardForm = new FormGroup({
      voucher: new FormControl('', Validators.required),
      redirectLink: new FormControl(''),
      validityDay: new FormControl(null, Validators.required),
      merchantName: new FormControl('', Validators.required),
      tagLine: new FormControl(''),
      description: new FormControl(''),
      rewardTitle:new FormControl('', Validators.required),
    })

    this.setDate();
  }

  ngOnInit() {
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "List your brand";
    var d = JSON.parse(localStorage.getItem('data'));
    this.email = d['emailId'];
    this.token = d['token'];
    this.accountId = d['accountId'];
    this.path='';
    
    this.rewardData={
      voucher:'',
      redirectLink:'',
      validityDay:'',
      merchantName:'',
      tagLine:'',
      description:'',
      rewardTitle:'',
      logo:'',
      backgroundImg:''
    };

    this.setDate();
    console.log(this.rewardForm.value.validityDay);

    if (d['product'].MYSIZE.paymentStatus == 'INACTIVE') {
      if(d['product'].MYSIZE.remainingDays < 0)
      {
        this.expired = false;
      }
      else
      {
        this.expired = true;
      }
    }
  }

  gotoSubscription()
  { 
    localStorage.setItem('subscribeProduct','MYSIZE');
    this.router.navigate(['/subscribe'])
  }

  setDate(): void {
    // Set today date using the patchValue function
    let date = new Date();
    this.rewardForm.patchValue({validityDay: {
    date: {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()}
    }});
}

onDateChanged(e)
{
  this.rewardForm.patchValue({validityDay:e.formatted});
  this.day=e.formatted;
}


  changeListener($event): void {
    this.isLogo=true;
    this.isBackground=false;
    this.readThis($event.target);
  }

  changeListener1($event): void {
    this.isBackground=true;
    this.isLogo=false
    this.readThis1($event.target);
  }

  getrewarddata() {
    this.showLoader = true;
    var obj = {
      "emailId": this.email,
      "token": this.token,
    }
    this.service.getrewarddata(obj).subscribe(response => {
      if (response.code == "1") {
        this.showLoader = false;
        this.rewardData = response.data;
        this.rewardForm.patchValue({ voucher: this.rewardData.voucher });
        this.rewardForm.patchValue({ redirectLink: this.rewardData.redirectLink });
        this.rewardForm.patchValue({ validityDay: this.rewardData.validityDay });
        this.rewardForm.patchValue({ merchantName: this.rewardData.merchantName });
        this.rewardForm.patchValue({ tagLine: this.rewardData.tagLine });
        this.rewardForm.patchValue({ description: this.rewardData.description });
        this.rewardForm.patchValue({ rewardTitle: this.rewardData.rewardTitle });
        console.log(this.rewardData);
      }
      else if(response.code =="2") {
        this.showLoader = false;
        this.ifEdit = true;
        this.rewardForm.patchValue({ voucher: '' });
        this.rewardForm.patchValue({ redirectLink:'' });
        this.rewardForm.patchValue({ validityDay: '' });
        this.rewardForm.patchValue({ merchantName: '' });
        this.rewardForm.patchValue({ tagLine: '' });
        this.rewardForm.patchValue({ description: '' });
        this.rewardForm.patchValue({ rewardTitle: '' });
      }
      else  {
        this.showLoader = false;
        Swal('error', response.message, 'error');
      }
    })
    console.log("Success");
  }



  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
        this.logoImage = myReader.result;
    }

    myReader.readAsDataURL(file);

    this.data = file;
    this.image = file.name;
    this.uploadToS3(this.image,this.data);
   
  }

  readThis1(inputValue: any): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
        this.banner = myReader.result;
    }

    myReader.readAsDataURL(file);
    this.data1 = file;
    this.image1 = file.name;
    this.uploadToS3(this.image1,this.data1);
   
  }

  saveReward() {
    this.showLoader = true;
    var obj = {
      "emailId": this.email,
      "token": this.token,
      "voucher": this.rewardForm.value.voucher,
      "redirectLink": this.rewardForm.value.redirectLink,
      "validityDay": this.rewardForm.value.validityDay.formatted,
      "merchantName": this.rewardForm.value.merchantName,
      "tagLine": this.rewardForm.value.tagLine,
      "logo": this.logo,
      "description":this.rewardForm.value.description,
      "rewardTitle":this.rewardForm.value.rewardTitle,
      "backgroundImg":this.backgroundImg
    }

    this.service.setreward(obj).subscribe(response => {
      if (response.code == "1") {
        this.showLoader = false;
        Swal('Success', response.message, 'success');
        this.data = '';
        this.getrewarddata();
        this.ifEdit = !this.ifEdit;
      }
      else {
        Swal('error', response.message, 'error');
      }
    })
  }

  uploadToS3(img,data) {
    this.showLoader = true;
    const bucket = new S3(
    {
      accessKeyId: 'AKIAIFXOEPN7XBYCUXBA',
      secretAccessKey: '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',
      region: 'us-east-1'
    }
  );

    this.timestamp = Date.now();

    const params = {
      Bucket: 'miiror-vijay',
      Key: 'merchantuploads/' + this.accountId + '/logo/' + this.timestamp+img,
      Body: data,
    };

    var self = this;
    bucket.putObject(params).on('httpUploadProgress', function (evt) {
      this.pv = (evt.loaded * 100) / evt.total
    }).send(function (err, data) {
      if (!err) {
        self.showLoader = false;
        self.path =  'https://miiror-vijay.s3.amazonaws.com/merchantuploads/' + self.accountId + '/logo/' + self.timestamp+img;
        if(self.isLogo)
        {
          self.logo=self.path;
        }
        else
        {
          self.backgroundImg=self.path;
        }
      } else {
        Swal('error', 'Error occured while uploading', 'error');
      }
    });  
  }

  addClass()
  {
      document.getElementById('headerShow').style.zIndex='1';
      document.getElementById('outerMain').style.zIndex="9999"; 
      document.getElementById('viewImage').style.background='rgba(0, 0, 0, 0.61)';
      document.getElementById('updatePoints').style.background='rgba(0, 0, 0, 0.61)';
      
  }

  closeModal()
  {
      document.getElementById('headerShow').style.zIndex='99999';
      document.getElementById('outerMain').style.zIndex="0"; 
  }
  
  resizeTextarea (id) {
    var a = document.getElementById(id);
    a.style.height = 'auto';
    a.style.height = a.scrollHeight+'px';
  
}
}