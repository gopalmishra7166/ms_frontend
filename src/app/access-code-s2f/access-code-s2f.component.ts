import {ProductData} from './../model/productData';
import {postDataOfMerchantInfo} from './../model/postDataOfMerchantInfo';
import {MerchantInfo} from './../model/merchant-info';
import {MerchantAdress} from './../model/merchant-adress';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Route} from '@angular/compiler/src/core';
import {Component, OnInit} from '@angular/core';
import $ from 'jquery';
import {MsServices} from './../services/ms-services';
import {CookieService} from 'ngx-cookie-service';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import {MerchantDetails} from './../model/merchantdetails';

const Swal = require('sweetalert2');

@Component({
    selector: 'app-access-code-s2f',
    templateUrl: './access-code-s2f.component.html',
    styleUrls: ['./access-code-s2f.component.css']
})
export class AccessCodeS2fComponent implements OnInit {
    now = new Date();
    accessCode: string = undefined;
    imageSrc: any = '/assets/img/logo-preview.jpg';
    new_logo: string;
    files: FileList;
    minNUmber: string;
    filestring: string;
    show = false;
    sh = true;
    merchantDetails: MerchantDetails;
    updateMerchantInfo: postDataOfMerchantInfo;
    updateMerchantAddress: MerchantAdress;
    getAcceessCodeDetail;
    email: any;
    token: any;
    mid: any;
    uid: string;
    name: string;
    fileType;
    prdDiv = false;
    pass='';
    showLoader=false;
    expired=false;
    
    ngOnInit() 
    {
        var d=JSON.parse(localStorage.getItem('data'));
        this.email=d['emailId'];
        this.token=d['token'];
        this.name=d['companyName']
        this.mid=d['mid'];
        this.pass=d['product'].SIZE2FIT.accessCode;

        if (d['product'].SIZE2FIT.paymentStatus == 'INACTIVE') {
            
            if(d['product'].SIZE2FIT.remainingDays < 0)
            {
              this.expired = false;
            }
            else
            {
              this.expired = true;
            }
          }
          if(this.expired==false)
          {
            this.getAccessCodeHistory();
          }
    
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="User Login Details";

    }

    constructor(private route: Router, private ser: MsServices, private cookieService: CookieService, public _sanitizer: DomSanitizer) 
    {
       
        this.updateMerchantInfo = new postDataOfMerchantInfo();
        this.updateMerchantAddress = new MerchantAdress();
        var d=JSON.parse(localStorage.getItem('data'));
        this.email=d['emailId'];
    }

    getAccessCodeHistory()
    {   
     var obj={emailId:this.email,token:this.token,productName:'SIZE2FIT'}
     this.ser.getAccessCodeHistory(obj).subscribe(response=>{
         if(response.code=="1")
         {
            this.getAcceessCodeDetail=response.data;
         }
         else{
             Swal('error',response.message,'error')
         }
     })
    }

    updateAccessCode() 
    {
        var obj={"token":this.token,"emailId":this.email,"productName":'SIZE2FIT',}
            Swal({
                
                title: 'Are you sure, want to update?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: '',
                cancelButtonColor: '',
                confirmButtonText: 'Yes',
                allowOutsideClick: false,

            }).then((result) => {

                if (result.value) 
                {
                    this.showLoader=true;
                    this.ser.updateAccessCode(obj).subscribe(res => 
                    {
                        this.showLoader=false;
                        if(res.code=="1")
                        {
                            this.pass = res.accessCode;
                              this.ser.getAccessCodeHistory(obj).subscribe(response=>{
                           
                              if(response.code=="1")
                              {
                                  this.getAcceessCodeDetail=response.data;
                              }
                              else{
                                  Swal('error',response.message,'error')
                              }
                          });
                        }
                        else
                        {
                            Swal('error',res.message,'error')  
                        }
                    });
                }
            });
        }
        
}

