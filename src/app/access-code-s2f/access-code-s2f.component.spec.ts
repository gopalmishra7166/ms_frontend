import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessCodeS2fComponent } from './access-code-s2f.component';

describe('AccessCodeS2fComponent', () => {
  let component: AccessCodeS2fComponent;
  let fixture: ComponentFixture<AccessCodeS2fComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessCodeS2fComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessCodeS2fComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
