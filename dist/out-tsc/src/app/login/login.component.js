var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Title } from '@angular/platform-browser';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { MsServices } from './../services/ms-services';
var Swal = require('sweetalert2');
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Http } from '@angular/http';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(titleService, toast, spinner, router, serv, cookieService, http) {
        this.titleService = titleService;
        this.toast = toast;
        this.spinner = spinner;
        this.router = router;
        this.serv = serv;
        this.cookieService = cookieService;
        this.http = http;
        this.showLoader = false;
        this.isLogin = true;
        this.isforgotpassword = false;
        this.err = undefined;
        this.form = new FormGroup({
            mail: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required)
        });
        this.isOtp = false;
        this.email = '';
        this.otp = '';
    }
    LoginComponent.prototype.getUrl = function () {
        return 'url(\'assets/img/background5.jpg\')';
    };
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.serv.isLoggedIn()) {
            this.router.navigate(['/graph']);
        }
        this.titleService.setTitle("Mirrorsize");
        this.http.get('https://jsonip.com/').subscribe(function (data) {
            _this.publicIP = data;
            var body = _this.publicIP._body;
            _this.physicalIp = JSON.parse(body).ip;
        });
        if (localStorage.getItem('useremail')) {
            this.form.patchValue({ mail: localStorage.getItem('useremail') });
        }
        else {
            this.form.patchValue({ mail: '' });
        }
    };
    LoginComponent.prototype.send = function () {
        var _this = this;
        this.showLoader = true;
        var obj = { "emailId": this.form.value.mail, "password": this.form.value.password, "physicalIp": this.physicalIp };
        this.serv.loginUser(obj).subscribe(function (res) {
            _this.showLoader = false;
            if (res.code == "1") {
                _this.responseLogin = res;
                if (_this.responseLogin.emailStatus == 'InActive' || _this.responseLogin.emailStatus == 'INACTIVE') {
                    localStorage.setItem('verified', 'false');
                    _this.isLogin = false;
                    _this.router.navigate(['/signup']);
                    localStorage.setItem('userID', _this.form.value.mail);
                    localStorage.setItem('userId', _this.form.value.mail);
                    localStorage.setItem('accountId', _this.responseLogin['product'].accountId);
                    if (_this.responseLogin['product'].GET_MEASURED) {
                        _this.productName = 'GET_MEASURED';
                        _this.planType = _this.responseLogin['product'].GET_MEASURED.planType;
                    }
                    else {
                        _this.productName = 'SIZE2FIT';
                        _this.planType = _this.responseLogin['product'].GET_MEASURED.planType;
                    }
                    sessionStorage.setItem('productName', _this.productName);
                }
                else if (_this.responseLogin.accountStatus == 'Inactive' || _this.responseLogin.accountStatus == 'INACTIVE') {
                    _this.router.navigate(['/signup']);
                    localStorage.setItem('userID', _this.form.value.mail);
                    localStorage.setItem('userId', _this.form.value.mail);
                    localStorage.setItem('accountId', _this.responseLogin['product'].accountId);
                    localStorage.setItem('verified', 'true');
                    if (_this.responseLogin['product'].GET_MEASURED) {
                        _this.productName = 'GET_MEASURED';
                        _this.planType = _this.responseLogin['product'].GET_MEASURED.planType;
                    }
                    else {
                        _this.productName = 'SIZE2FIT';
                        _this.planType = _this.responseLogin['product'].GET_MEASURED.planType;
                    }
                    sessionStorage.setItem('productName', _this.productName);
                }
                else {
                    _this.router.navigate(['/graph']);
                    localStorage.setItem('data', JSON.stringify(_this.responseLogin));
                }
            }
            else {
                _this.toast.warning('Invalid email or password', 'Error', {
                    timeOut: 1200,
                });
            }
        });
    };
    LoginComponent.prototype.switchform = function () {
        console.log(this.isLogin);
        console.log(this.isforgotpassword);
        this.isLogin = !this.isLogin;
        this.isforgotpassword = !this.isforgotpassword;
    };
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        this.showLoader = true;
        this.form.value.mail;
        this.serv.recoverPassword({ emailId: this.form.value.mail }).subscribe(function (res) {
            if (res.code == "1") {
                _this.showLoader = false;
                Swal('success', res.message, 'success');
            }
            else {
                Swal('Oops !', res.message, 'error');
            }
        });
    };
    // -----for  signup sending it to home page for  selection product--------//
    LoginComponent.prototype.signUp = function () {
        this.toast.error('Please select the product first then signup.', 'Select product', { timeOut: 2000, });
        this.router.navigate(['/home']);
    };
    LoginComponent.prototype.verifyOtp = function () {
        var _this = this;
        var obj = { "email_id": this.email, "otp": this.otp };
        this.serv.verifyEmail(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.isLogin = true;
            }
            else {
                Swal('Oops!', response.message, 'error');
            }
        });
    };
    LoginComponent.prototype.sendOtp = function () {
        var _this = this;
        var obj = { "emailId": this.email, "token": "" };
        this.serv.sendOtp(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.isOtp = true;
            }
            else {
                Swal('Oops!', response.message, 'error');
            }
        });
    };
    LoginComponent.prototype.rememberMe = function (e) {
        if (e.target.checked) {
            localStorage.setItem('useremail', this.form.value.mail);
        }
        else {
            localStorage.removeItem('useremail');
        }
    };
    LoginComponent = __decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        }),
        __metadata("design:paramtypes", [Title, ToastrService, NgxSpinnerService, Router, MsServices, CookieService, Http])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map