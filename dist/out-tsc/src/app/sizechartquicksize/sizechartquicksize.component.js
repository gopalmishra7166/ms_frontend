var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
import * as S3 from 'aws-sdk/clients/s3';
var Swal = require('sweetalert2');
var SizechartquicksizeComponent = /** @class */ (function () {
    function SizechartquicksizeComponent(service) {
        this.service = service;
        this.muid = '';
        this.mid = '';
        this.upflag = false;
        this.pv = 0;
        this.showLoader = false;
        this.files = [];
        this.sizechartType = 'READY';
    }
    SizechartquicksizeComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.accountId = d['accountId'];
        if (d['product'].QUICKSIZE.planType == 'PAID') {
            this.paid = true;
            this.getfiles();
        }
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Upload size chart";
    };
    SizechartquicksizeComponent.prototype.uploadToS3 = function () {
        this.showLoader = true;
        var contentType = this.data.type;
        var bucket = new S3({
            accessKeyId: 'AKIAIFXOEPN7XBYCUXBA',
            secretAccessKey: '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',
            region: 'us-east-1'
        });
        this.timestamp = Date.now();
        var params = {
            Bucket: 'miiror-vijay',
            Key: 'merchantuploads/' + this.accountId + '/sizecharts/' + this.timestamp + '.zip',
            Body: this.data,
            ContentType: contentType,
        };
        var self = this;
        bucket.putObject(params).on('httpUploadProgress', function (evt) {
            this.pv = (evt.loaded * 100) / evt.total;
            // console.log("Uploaded :: " + (evt.loaded * 100) / evt.total+'%');
        }).send(function (err, data) {
            if (!err) {
                self.showLoader = false;
                self.s3fileupload();
                self.getfiles();
            }
            else {
                console.log(err);
            }
        });
    };
    SizechartquicksizeComponent.prototype.getfiles = function () {
        var _this = this;
        var obj = { "emailId": this.email, "token": this.token, "productName": "QUICKSIZE", "fileType": "sizechart" };
        this.service.getfiles(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.filedata = response.data;
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    SizechartquicksizeComponent.prototype.downloadfile = function (response) {
        window.open(response.filePath, '_blank');
    };
    SizechartquicksizeComponent.prototype.deletefile = function (d) {
        var _this = this;
        var obj = { "emailId": this.email, "token": this.token, "id": d.id };
        this.service.removefile(obj).subscribe(function (response) {
            if (response.code == "1") {
                Swal('Success', response.message, 'success');
                _this.getfiles();
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    SizechartquicksizeComponent.prototype.s3fileupload = function () {
        var _this = this;
        this.showLoader = true;
        var path = 'https://miiror-vijay.s3.amazonaws.com/merchantuploads/' + this.accountId + '/sizecharts/' + this.timestamp + '.zip';
        var obj = {
            "emailId": this.email,
            "token": this.token,
            "productName": "QUICKSIZE",
            "filePath": path,
            "fileType": "sizechart",
            "description": {
                "sizechartType": this.sizechartType
            }
        };
        this.service.fileUpload(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.showLoader = false;
                Swal('Success', response.message, 'success');
                _this.getfiles();
                _this.createTicket(path);
                _this.data = '';
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
        console.log("Success");
    };
    SizechartquicksizeComponent.prototype.createTicket = function (path) {
        var _this = this;
        this.showLoader = true;
        var obj = {
            emailId: this.email,
            token: this.token,
            content: "Review and deploy size chart on the mirrorsize platform, sizechart path " + path,
            subject: "New Size chart Upload request"
        };
        this.service.raiseTicket(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                Swal('success', response.message, 'success');
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    SizechartquicksizeComponent.prototype.dropped = function (files) {
        var _this = this;
        this.files = files;
        for (var _i = 0, files_1 = files; _i < files_1.length; _i++) {
            var droppedFile = files_1[_i];
            // Is it a file?
            if (droppedFile.fileEntry.isFile) {
                var fileEntry = droppedFile.fileEntry;
                fileEntry.file(function (file) {
                    _this.data = file;
                });
            }
            else {
                var fileEntry = droppedFile.fileEntry;
                console.log(droppedFile.relativePath, fileEntry);
            }
        }
    };
    SizechartquicksizeComponent.prototype.fileOver = function (event) {
        console.log(event);
    };
    SizechartquicksizeComponent.prototype.fileLeave = function (event) {
        console.log(event);
    };
    SizechartquicksizeComponent = __decorate([
        Component({
            selector: 'app-sizechartquicksize',
            templateUrl: './sizechartquicksize.component.html',
            styleUrls: ['./sizechartquicksize.component.css']
        }),
        __metadata("design:paramtypes", [MsServices])
    ], SizechartquicksizeComponent);
    return SizechartquicksizeComponent;
}());
export { SizechartquicksizeComponent };
//# sourceMappingURL=sizechartquicksize.component.js.map