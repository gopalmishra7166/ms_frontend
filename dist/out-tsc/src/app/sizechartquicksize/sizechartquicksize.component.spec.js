import { async, TestBed } from '@angular/core/testing';
import { SizechartquicksizeComponent } from './sizechartquicksize.component';
describe('SizechartquicksizeComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [SizechartquicksizeComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(SizechartquicksizeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=sizechartquicksize.component.spec.js.map