import { async, TestBed } from '@angular/core/testing';
import { GuidedtourmsComponent } from './guidedtourms.component';
describe('GuidedtourmsComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [GuidedtourmsComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(GuidedtourmsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=guidedtourms.component.spec.js.map