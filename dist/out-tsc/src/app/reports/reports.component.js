var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
var Swal = require('sweetalert2');
var ReportsComponent = /** @class */ (function () {
    function ReportsComponent(service, toastr, spinner) {
        this.service = service;
        this.toastr = toastr;
        this.spinner = spinner;
        this.myDatePickerOptions = {
            dateFormat: 'yyyy-mm-dd',
            disableSince: { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() + 1 }
        };
        this.SIZE2FIT = false;
        this.GET_MEASURED = false;
        this.MYSIZE = false;
        this.QUICKSIZE = false;
        this.now = new Date();
        this.showLoader = false;
        this.productName = "get_measured";
        this.month = "jan";
    }
    ReportsComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.token = d['token'];
        this.email = d['emailId'];
        if (d['product'].GET_MEASURED) {
            this.GET_MEASURED = true;
        }
        if (d['product'].SIZE2FIT) {
            this.SIZE2FIT = true;
        }
        if (d['product'].GET_MEASURED) {
            this.GET_MEASURED = true;
        }
        if (d['product'].MYSIZE) {
            this.MYSIZE = true;
        }
        if (d['product'].QUICKSIZE) {
            this.QUICKSIZE = true;
        }
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Clicks Report";
    };
    ReportsComponent.prototype.dowmnloadYourPdf = function () {
        var _this = this;
        this.showLoader = true;
        var data = { emailId: this.email, token: this.token, month: this.month, productName: this.productName };
        this.service.billingPdfGeneration(data).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                window.open(response.s3urlpdf, '_blank');
            }
            else {
                Swal('Oops !', response.message, 'error');
            }
        });
    };
    ReportsComponent = __decorate([
        Component({
            selector: 'app-reports',
            templateUrl: './reports.component.html',
            styleUrls: ['./reports.component.css']
        }),
        __metadata("design:paramtypes", [MsServices, ToastrService, NgxSpinnerService])
    ], ReportsComponent);
    return ReportsComponent;
}());
export { ReportsComponent };
//# sourceMappingURL=reports.component.js.map