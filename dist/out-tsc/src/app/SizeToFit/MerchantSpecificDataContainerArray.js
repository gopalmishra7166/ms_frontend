var MerchantSpecificDataContainerArray = /** @class */ (function () {
    function MerchantSpecificDataContainerArray() {
        // xl, m
        /* name:string = "";*/
        /*  show: boolean;
          showForm: boolean;
          hide: boolean;
        */ this.allowances = [];
        /* tolerance: any[] = [];
     */
        // measurementPoints:string[];
        // size 38,36
        this.enteredSize = [];
        this.min = [];
        // pri
        /*  priorities: any[] = [];*/
        this.max = [];
    }
    return MerchantSpecificDataContainerArray;
}());
export { MerchantSpecificDataContainerArray };
// package com.ms.stf.model;
//
// public class MerchantSpecificDataContainerArray {
//
//     private String sizePoint;
//     private String measurementPoints[];
//     private String Sizes[];
//     private String priorities[];
//     private String minMaxValue[];
//
//
// }
//
//# sourceMappingURL=MerchantSpecificDataContainerArray.js.map