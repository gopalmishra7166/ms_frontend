var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
var Swal = require('sweetalert2');
var PaymentComponent = /** @class */ (function () {
    function PaymentComponent(service, ser, toastr, spinner) {
        this.service = service;
        this.ser = ser;
        this.toastr = toastr;
        this.spinner = spinner;
        this.now = new Date();
    }
    PaymentComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.getplanhistorydata();
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Payment";
    };
    PaymentComponent.prototype.getplanhistorydata = function () {
        var _this = this;
        this.showLoader = true;
        var obj = { emailId: this.email, token: this.token };
        this.ser.getpaymenthistorydata(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.getpaymenthistoryactive = response.data.activeHistory;
                _this.getpaymenthistoryexpired = response.data.expiredHistory;
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    PaymentComponent.prototype.pay = function (i) {
        var _this = this;
        var d = JSON.parse(localStorage.getItem('data'));
        var data = {
            "getHostedPaymentPageRequest": {
                "merchantAuthentication": {
                    "name": "9863nDF2VGfG",
                    "transactionKey": "4Vv4T8h8Z5vc9Wjn"
                },
                "transactionRequest": {
                    "transactionType": "authCaptureTransaction",
                    "amount": i.paidAmount,
                    "customer": {
                        "email": d['emailId']
                    },
                    "billTo": {
                        "firstName": d['contactPerson'].cpName,
                        "lastName": '',
                        "company": d['companyProfile'].name,
                        "address": d['companyProfile'].companyAddress,
                        "city": d['companyProfile'].city,
                        "state": d['companyProfile'].state,
                        "zip": d['companyProfile'].pinCode,
                        "country": d['companyProfile'].country,
                    }
                },
                "hostedPaymentSettings": {
                    "setting": [
                        {
                            "settingName": "hostedPaymentReturnOptions",
                            "settingValue": "{\"showReceipt\": true, \"url\": \"https://services.mirrorsize.com/#/paymentconfirmation\", \"urlText\": \"Continue\", \"cancelUrl\": \"https://services.mirrorsize.com/#/graph\", \"cancelUrlText\": \"Cancel\"}"
                        },
                        {
                            "settingName": "hostedPaymentButtonOptions",
                            "settingValue": "{\"text\": \"Pay\"}"
                        },
                        {
                            "settingName": "hostedPaymentStyleOptions",
                            "settingValue": "{\"bgColor\": \"blue\"}"
                        },
                        {
                            "settingName": "hostedPaymentPaymentOptions",
                            "settingValue": "{\"cardCodeRequired\": true, \"showCreditCard\": true, \"showBankAccount\": true}"
                        },
                        {
                            "settingName": "hostedPaymentSecurityOptions",
                            "settingValue": "{\"captcha\": false}"
                        },
                        {
                            "settingName": "hostedPaymentShippingAddressOptions",
                            "settingValue": "{\"show\": false, \"required\": false}"
                        },
                        {
                            "settingName": "hostedPaymentBillingAddressOptions",
                            "settingValue": "{\"show\": true, \"required\": false}"
                        },
                        {
                            "settingName": "hostedPaymentCustomerOptions",
                            "settingValue": "{\"showEmail\": false, \"requiredEmail\": false, \"addPaymentProfile\": true}"
                        },
                        {
                            "settingName": "hostedPaymentOrderOptions",
                            "settingValue": "{\"show\": true, \"merchantName\": \"Mirrorsize US Inc\"}"
                        },
                        {
                            "settingName": "hostedPaymentIFrameCommunicatorUrl",
                            "settingValue": "{\"url\": \"http://services.mirrorsize.com\"}"
                        }
                    ]
                }
            }
        };
        this.showLoader = true;
        this.service.makePayment(data).subscribe(function (response) {
            _this.showLoader = false;
            if (response.messages.resultCode == 'Ok') {
                _this.authorizetoken = response.token;
                document.getElementById('headerShow').style.zIndex = '1';
                document.getElementById('outerMain').style.zIndex = "9999";
                document.getElementById('paymentmodal').style.background = 'rgba(0, 0, 0, 0.35)';
                $('#paymentmodal').modal('show');
                _this.loadScript();
            }
            else {
                Swal('Oops!', response.messages.message.text, 'error');
            }
        });
        console.log("amount Due");
    };
    PaymentComponent.prototype.loadScript = function () {
        var isFound = false;
        var scripts = document.getElementsByTagName("script");
        for (var i = 0; i < scripts.length; ++i) {
            if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("AcceptUI")) {
                isFound = true;
            }
        }
        if (!isFound) {
            var dynamicScripts = ["https://js.authorize.net/v3/AcceptUI.js"];
            for (var i = 0; i < dynamicScripts.length; i++) {
                var node = document.createElement('script');
                node.src = dynamicScripts[i];
                node.type = 'text/javascript';
                node.async = false;
                node.charset = 'utf-8';
                document.getElementsByTagName('head')[0].appendChild(node);
            }
        }
    };
    PaymentComponent = __decorate([
        Component({
            selector: 'app-payment',
            templateUrl: './payment.component.html',
            styleUrls: ['./payment.component.css']
        }),
        __metadata("design:paramtypes", [MsServices, MsServices, ToastrService, NgxSpinnerService])
    ], PaymentComponent);
    return PaymentComponent;
}());
export { PaymentComponent };
//# sourceMappingURL=payment.component.js.map