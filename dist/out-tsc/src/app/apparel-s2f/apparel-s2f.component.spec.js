import { async, TestBed } from '@angular/core/testing';
import { ApparelS2fComponent } from './apparel-s2f.component';
describe('ApparelS2fComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ApparelS2fComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ApparelS2fComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=apparel-s2f.component.spec.js.map