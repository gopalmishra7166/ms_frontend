import { async, TestBed } from '@angular/core/testing';
import { IFrameCommunicatorComponent } from './i-frame-communicator.component';
describe('IFrameCommunicatorComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [IFrameCommunicatorComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(IFrameCommunicatorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=i-frame-communicator.component.spec.js.map