var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
var IFrameCommunicatorComponent = /** @class */ (function () {
    function IFrameCommunicatorComponent() {
    }
    IFrameCommunicatorComponent.prototype.callParentFunction = function (str) {
        if (str && str.length > 0
            && window.parent
            && window.parent.parent
            && window.parent.parent['AuthorizeNetIFrame']
            && window.parent.parent['AuthorizeNetIFrame'].onReceiveCommunication) {
            // Errors indicate a mismatch in domain between the page containing the iframe and this page.
            window.parent.parent['AuthorizeNetIFrame'].onReceiveCommunication(str);
        }
    };
    IFrameCommunicatorComponent.prototype.receiveMessage = function (event) {
        if (event && event.data) {
            this.callParentFunction(event.data);
        }
    };
    IFrameCommunicatorComponent.prototype.ngOnInit = function () {
        if (window.addEventListener) {
            window.addEventListener("message", this.receiveMessage, false);
        }
        else if (window['attachEvent']) {
            window['attachEvent']("onmessage", this.receiveMessage);
        }
        if (window.location.hash && window.location.hash.length > 1) {
            this.callParentFunction(window.location.hash.substring(1));
        }
    };
    IFrameCommunicatorComponent = __decorate([
        Component({
            selector: 'app-i-frame-communicator',
            templateUrl: './i-frame-communicator.component.html',
            styleUrls: ['./i-frame-communicator.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], IFrameCommunicatorComponent);
    return IFrameCommunicatorComponent;
}());
export { IFrameCommunicatorComponent };
//# sourceMappingURL=i-frame-communicator.component.js.map