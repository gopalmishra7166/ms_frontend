import { async, TestBed } from '@angular/core/testing';
import { PaymentconfirmationComponent } from './paymentconfirmation.component';
describe('PaymentconfirmationComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [PaymentconfirmationComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(PaymentconfirmationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=paymentconfirmation.component.spec.js.map