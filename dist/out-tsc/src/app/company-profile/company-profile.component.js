var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
var Swal = require('sweetalert2');
import { Router } from '@angular/router';
import { MsServices } from '../services/ms-services';
import { CookieService } from 'ngx-cookie-service';
import { PagerService } from '../services/pagerService';
var CompanyProfileComponent = /** @class */ (function () {
    function CompanyProfileComponent(router, service, cookieService, pagerService) {
        this.router = router;
        this.service = service;
        this.cookieService = cookieService;
        this.pagerService = pagerService;
        this.subUsres = [];
        this.userName = '';
        this.subUser = 'subuser';
        this.designation = '';
        this.countriesList = [];
        this.states = [];
        this.industries = [];
        this.isEdit = false;
        this.cName = '';
        this.cType = '';
        this.state = '';
        this.city = '';
        this.mobile = '';
        this.address = '';
        this.country = '';
        this.pincode = '';
        this.landmark = '';
        this.logo = '';
        this.industryType = '';
        this.altMobileOfContactPerson = "";
        this.contactPersonName = "";
        this.emailOfContactPerson = "";
        this.mobile_num = "";
        this.profilePic = "";
        this.status = "1";
        this.oldPass = '';
        this.newPass = '';
        this.confirmPass = '';
        this.planData = [];
        this.productType = '';
        this.userCountry = '0';
        this.userState = '0';
        this.userCity = '';
        this.userEmail = '';
        this.userPass = '';
        this.userMobile = '';
        this.userPincode = '';
        this.checkEmail = true;
        this.mid = '';
        this.activityLogs = [];
        this.count = 0;
        this.pager = {};
        this.showLoader = false;
        this.state_name = '';
        this.planDetailMS = false;
        this.planDetailQS = false;
        this.pGM = false;
        this.pS2F = false;
        this.pMS = false;
        this.pQS = false;
        this.industries =
            [
                { 'Description': 'Accounts' },
                { 'Description': 'Airlines/Aviation' },
                { 'Description': 'Alternative Dispute Resolution' },
                { 'Code': 125, 'Groups': 'hlth', 'Description': 'Alternative Medicine' },
                { 'Code': 127, 'Groups': 'art, med', 'Description': 'Animation' },
                { 'Code': 19, 'Groups': 'good', 'Description': 'Apparel & Fashion' },
                { 'Code': 50, 'Groups': 'cons', 'Description': 'Architecture & Planning' },
                { 'Code': 111, 'Groups': 'art, med, rec', 'Description': 'Arts and Crafts' },
                { 'Code': 53, 'Groups': 'man', 'Description': 'Automotive' },
                { 'Code': 52, 'Groups': 'gov, man', 'Description': 'Aviation & Aerospace' },
                { 'Code': 41, 'Groups': 'fin', 'Description': 'Banking' },
                { 'Code': 12, 'Groups': 'gov, hlth, tech', 'Description': 'Biotechnology' },
                { 'Code': 36, 'Groups': 'med, rec', 'Description': 'Broadcast Media' },
                { 'Code': 49, 'Groups': 'cons', 'Description': 'Building Materials' },
                { 'Code': 138, 'Groups': 'corp, man', 'Description': 'Business Supplies and Equipment' },
                { 'Code': 129, 'Groups': 'fin', 'Description': 'Capital Markets' },
                { 'Code': 54, 'Groups': 'man', 'Description': 'Chemicals' },
                { 'Code': 90, 'Groups': 'org, serv', 'Description': 'Civic & Social Organization' },
                { 'Code': 51, 'Groups': 'cons, gov', 'Description': 'Civil Engineering' },
                { 'Code': 128, 'Groups': 'cons, corp, fin', 'Description': 'Commercial Real Estate' },
                { 'Code': 118, 'Groups': 'tech', 'Description': 'Computer & Network Security' },
                { 'Code': 109, 'Groups': 'med, rec', 'Description': 'Computer Games' },
                { 'Code': 3, 'Groups': 'tech', 'Description': 'Computer Hardware' },
                { 'Code': 5, 'Groups': 'tech', 'Description': 'Computer Networking' },
                { 'Code': 4, 'Groups': 'tech', 'Description': 'Computer Software' },
                { 'Code': 48, 'Groups': 'cons', 'Description': 'Construction' },
                { 'Code': 24, 'Groups': 'good, man', 'Description': 'Consumer Electronics' },
                { 'Code': 25, 'Groups': 'good, man', 'Description': 'Consumer Goods' },
                { 'Code': 91, 'Groups': 'org, serv', 'Description': 'Consumer Services' },
                { 'Code': 18, 'Groups': 'good', 'Description': 'Cosmetics' },
                { 'Code': 65, 'Groups': 'agr', 'Description': 'Dairy' },
                { 'Code': 1, 'Groups': 'gov, tech', 'Description': 'Defense & Space' },
                { 'Code': 99, 'Groups': 'art, med', 'Description': 'Design' },
                { 'Code': '', 'Groups': 'ecom', 'Description': 'Ecommerce' },
                { 'Code': 69, 'Groups': 'edu', 'Description': 'Education Management' },
                { 'Code': 132, 'Groups': 'edu, org', 'Description': 'E-Learning' },
                { 'Code': 112, 'Groups': 'good, man', 'Description': 'Electrical/Electronic Manufacturing' },
                { 'Code': 28, 'Groups': 'med, rec', 'Description': 'Entertainment' },
                { 'Code': 86, 'Groups': 'org, serv', 'Description': 'Environmental Services' },
                { 'Code': 110, 'Groups': 'corp, rec, serv', 'Description': 'Events Services' },
                { 'Code': 76, 'Groups': 'gov', 'Description': 'Executive Office' },
                { 'Code': 122, 'Groups': 'corp, serv', 'Description': 'Facilities Services' },
                { 'Code': 63, 'Groups': 'agr', 'Description': 'Farming' },
                { 'Code': 43, 'Groups': 'fin', 'Description': 'Financial Services' },
                { 'Code': 38, 'Groups': 'art, med, rec', 'Description': 'Fine Art' },
                { 'Code': 66, 'Groups': 'agr', 'Description': 'Fishery' },
                { 'Code': 34, 'Groups': 'rec, serv', 'Description': 'Food & Beverages' },
                { 'Code': 23, 'Groups': 'good, man, serv', 'Description': 'Food Production' },
                { 'Code': 101, 'Groups': 'org', 'Description': 'Fund-Raising' },
                { 'Code': 26, 'Groups': 'good, man', 'Description': 'Furniture' },
                { 'Code': 29, 'Groups': 'rec', 'Description': 'Gambling & Casinos' },
                { 'Code': 145, 'Groups': 'cons, man', 'Description': 'Glass, Ceramics & Concrete' },
                { 'Code': 75, 'Groups': 'gov', 'Description': 'Government Administration' },
                { 'Code': 148, 'Groups': 'gov', 'Description': 'Government Relations' },
                { 'Code': 140, 'Groups': 'art, med', 'Description': 'Graphic Design' },
                { 'Code': 124, 'Groups': 'hlth, rec', 'Description': 'Health, Wellness and Fitness' },
                { 'Code': 68, 'Groups': 'edu', 'Description': 'Higher Education' },
                { 'Code': 14, 'Groups': 'hlth', 'Description': 'Hospital & Health Care' },
                { 'Code': 31, 'Groups': 'rec, serv, tran', 'Description': 'Hospitality' },
                { 'Code': 137, 'Groups': 'corp', 'Description': 'Human Resources' },
                { 'Code': 134, 'Groups': 'corp, good, tran', 'Description': 'Import and Export' },
                { 'Code': 88, 'Groups': 'org, serv', 'Description': 'Individual & Family Services' },
                { 'Code': 147, 'Groups': 'cons, man', 'Description': 'Industrial Automation' },
                { 'Code': 84, 'Groups': 'med, serv', 'Description': 'Information Services' },
                { 'Code': 96, 'Groups': 'tech', 'Description': 'Information Technology and Services' },
                { 'Code': 42, 'Groups': 'fin', 'Description': 'Insurance' },
                { 'Code': 74, 'Groups': 'gov', 'Description': 'International Affairs' },
                { 'Code': 141, 'Groups': 'gov, org, tran', 'Description': 'International Trade and Development' },
                { 'Code': 6, 'Groups': 'tech', 'Description': 'Internet' },
                { 'Code': 45, 'Groups': 'fin', 'Description': 'Investment Banking' },
                { 'Code': 46, 'Groups': 'fin', 'Description': 'Investment Management' },
                { 'Code': 73, 'Groups': 'gov, leg', 'Description': 'Judiciary' },
                { 'Code': 77, 'Groups': 'gov, leg', 'Description': 'Law Enforcement' },
                { 'Code': 9, 'Groups': 'leg', 'Description': 'Law Practice' },
                { 'Code': 10, 'Groups': 'leg', 'Description': 'Legal Services' },
                { 'Code': 72, 'Groups': 'gov, leg', 'Description': 'Legislative Office' },
                { 'Code': 30, 'Groups': 'rec, serv, tran', 'Description': 'Leisure, Travel & Tourism' },
                { 'Code': 85, 'Groups': 'med, rec, serv', 'Description': 'Libraries' },
                { 'Code': 116, 'Groups': 'corp, tran', 'Description': 'Logistics and Supply Chain' },
                { 'Code': 143, 'Groups': 'good', 'Description': 'Luxury Goods & Jewelry' },
                { 'Code': 55, 'Groups': 'man', 'Description': 'Machinery' },
                { 'Code': 11, 'Groups': 'corp', 'Description': 'Management Consulting' },
                { 'Code': 95, 'Groups': 'tran', 'Description': 'Maritime' },
                { 'Code': 97, 'Groups': 'corp', 'Description': 'Market Research' },
                { 'Code': 80, 'Groups': 'corp, med', 'Description': 'Marketing and Advertising' },
                { 'Code': 135, 'Groups': 'cons, gov, man', 'Description': 'Mechanical or Industrial Engineering' },
                { 'Code': 126, 'Groups': 'med, rec', 'Description': 'Media Production' },
                { 'Code': 17, 'Groups': 'hlth', 'Description': 'Medical Devices' },
                { 'Code': 13, 'Groups': 'hlth', 'Description': 'Medical Practice' },
                { 'Code': 139, 'Groups': 'hlth', 'Description': 'Mental Health Care' },
                { 'Code': 71, 'Groups': 'gov', 'Description': 'Military' },
                { 'Code': 56, 'Groups': 'man', 'Description': 'Mining & Metals' },
                { 'Code': 35, 'Groups': 'art, med, rec', 'Description': 'Motion Pictures and Film' },
                { 'Code': 37, 'Groups': 'art, med, rec', 'Description': 'Museums and Institutions' },
                { 'Code': 115, 'Groups': 'art, rec', 'Description': 'Music' },
                { 'Code': 114, 'Groups': 'gov, man, tech', 'Description': 'Nanotechnology' },
                { 'Code': 81, 'Groups': 'med, rec', 'Description': 'Newspapers' },
                { 'Code': 100, 'Groups': 'org', 'Description': 'Non-Profit Organization Management' },
                { 'Code': 57, 'Groups': 'man', 'Description': 'Oil & Energy' },
                { 'Code': 113, 'Groups': 'med', 'Description': 'Online Media' },
                { 'Code': 123, 'Groups': 'corp', 'Description': 'Outsourcing/Offshoring' },
                { 'Code': 87, 'Groups': 'serv, tran', 'Description': 'Package/Freight Delivery' },
                { 'Code': 146, 'Groups': 'good, man', 'Description': 'Packaging and Containers' },
                { 'Code': 61, 'Groups': 'man', 'Description': 'Paper & Forest Products' },
                { 'Code': 39, 'Groups': 'art, med, rec', 'Description': 'Performing Arts' },
                { 'Code': 15, 'Groups': 'hlth, tech', 'Description': 'Pharmaceuticals' },
                { 'Code': 131, 'Groups': 'org', 'Description': 'Philanthropy' },
                { 'Code': 136, 'Groups': 'art, med, rec', 'Description': 'Photography' },
                { 'Code': 117, 'Groups': 'man', 'Description': 'Plastics' },
                { 'Code': 107, 'Groups': 'gov, org', 'Description': 'Political Organization' },
                { 'Code': 67, 'Groups': 'edu', 'Description': 'Primary/Secondary Education' },
                { 'Code': 83, 'Groups': 'med, rec', 'Description': 'Printing' },
                { 'Code': 105, 'Groups': 'corp', 'Description': 'Professional Training & Coaching' },
                { 'Code': 102, 'Groups': 'corp, org', 'Description': 'Program Development' },
                { 'Code': 79, 'Groups': 'gov', 'Description': 'Public Policy' },
                { 'Code': 98, 'Groups': 'corp', 'Description': 'Public Relations and Communications' },
                { 'Code': 78, 'Groups': 'gov', 'Description': 'Public Safety' },
                { 'Code': 82, 'Groups': 'med, rec', 'Description': 'Publishing' },
                { 'Code': 62, 'Groups': 'man', 'Description': 'Railroad Manufacture' },
                { 'Code': 64, 'Groups': 'agr', 'Description': 'Ranching' },
                { 'Code': 44, 'Groups': 'cons, fin, good', 'Description': 'Real Estate' },
                { 'Code': 40, 'Groups': 'rec, serv', 'Description': 'Recreational Facilities and Services' },
                { 'Code': 89, 'Groups': 'org, serv', 'Description': 'Religious Institutions' },
                { 'Code': 144, 'Groups': 'gov, man, org', 'Description': 'Renewables & Environment' },
                { 'Code': 70, 'Groups': 'edu, gov', 'Description': 'Research' },
                { 'Code': 32, 'Groups': 'rec, serv', 'Description': 'Restaurants' },
                { 'Code': 27, 'Groups': 'good, man', 'Description': 'Retail' },
                { 'Code': 121, 'Groups': 'corp, org, serv', 'Description': 'Security and Investigations' },
                { 'Code': 7, 'Groups': 'tech', 'Description': 'Semiconductors' },
                { 'Code': 58, 'Groups': 'man', 'Description': 'Shipbuilding' },
                { 'Code': 20, 'Groups': 'good, rec', 'Description': 'Sporting Goods' },
                { 'Code': 33, 'Groups': 'rec', 'Description': 'Sports' },
                { 'Code': 104, 'Groups': 'corp', 'Description': 'Staffing and Recruiting' },
                { 'Code': 22, 'Groups': 'good', 'Description': 'Supermarkets' },
                { 'Code': 8, 'Groups': 'gov, tech', 'Description': 'Telecommunications' },
                { 'Code': 60, 'Groups': 'man', 'Description': 'Textiles' },
                { 'Code': 130, 'Groups': 'gov, org', 'Description': 'Think Tanks' },
                { 'Code': 21, 'Groups': 'good', 'Description': 'Tobacco' },
                { 'Code': 108, 'Groups': 'corp, gov, serv', 'Description': 'Translation and Localization' },
                { 'Code': 92, 'Groups': 'tran', 'Description': 'Transportation/Trucking/Railroad' },
                { 'Code': 59, 'Groups': 'man', 'Description': 'Utilities' },
                { 'Code': 106, 'Groups': 'fin, tech', 'Description': 'Venture Capital & Private Equity' },
                { 'Code': 16, 'Groups': 'hlth', 'Description': 'Veterinary' },
                { 'Code': 93, 'Groups': 'tran', 'Description': 'Warehousing' },
                { 'Code': 133, 'Groups': 'good', 'Description': 'Wholesale' },
                { 'Code': 142, 'Groups': 'good, man, rec', 'Description': 'Wine and Spirits' },
                { 'Code': 119, 'Groups': 'tech', 'Description': 'Wireless' },
                { 'Code': 103, 'Groups': 'art, med, rec', 'Description': 'Writing and Editing' }
            ];
    }
    CompanyProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.states = this.service.countriesData;
        var d = JSON.parse(localStorage.getItem('data'));
        this.token = d['token'];
        this.email = d['emailId'];
        this.companyProfileData = d['companyProfile'];
        this.country = this.companyProfileData.country;
        this.contactPersonProile = d['contactPerson'];
        this.logo = d['companyLogo'];
        this.role = d['role'];
        if (this.role == "subuser") {
            this.subuserid = d['subuserId'];
        }
        if (d['product'].GET_MEASURED) {
            this.planDetailGM = d['product'].GET_MEASURED;
        }
        if (d['product'].SIZE2FIT) {
            this.planDetailS2F = d['product'].SIZE2FIT;
        }
        if (d['product'].MYSIZE) {
            this.planDetailMS = d['product'].MYSIZE;
        }
        if (d['product'].QUICKSIZE) {
            this.planDetailQS = d['product'].QUICKSIZE;
        }
        if (d['product'].GET_MEASURED) {
            this.pGM = true;
        }
        else if (d['product'].SIZE2FIT) {
            this.pS2F = true;
        }
        else if (d['product'].MYSIZE) {
            this.pMS = true;
        }
        else if (d['product'].QUICKSIZE) {
            this.pQS = true;
        }
        console.log(this.pGM, this.pS2F, this.pMS, this.pQS);
        console.log(this.planDetailGM, this.planDetailS2F, this.planDetailMS, this.planDetailQS);
        this.getAllCountries();
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Profile";
        this.states = this.service.AllStates.filter(function (item) { return item.short == _this.companyProfileData['country']; })[0].states;
    };
    CompanyProfileComponent.prototype.selectProduct = function (p) {
        if (p == 'gm') {
            this.pGM = true;
            this.pS2F = false;
            this.pMS = false;
            this.pQS = false;
        }
        if (p == 's2f') {
            this.pGM = false;
            this.pS2F = true;
            this.pMS = false;
            this.pQS = false;
        }
        if (p == 'ms') {
            this.pGM = false;
            this.pS2F = false;
            this.pMS = true;
            this.pQS = false;
        }
        if (p == 'qs') {
            this.pGM = false;
            this.pS2F = false;
            this.pMS = false;
            this.pQS = true;
        }
    };
    CompanyProfileComponent.prototype.getAllCountries = function () {
        var _this = this;
        this.service.getAllCountries().subscribe(function (response) {
            if (response) {
                _this.countriesList = response;
            }
        });
    };
    CompanyProfileComponent.prototype.getCountryCode = function (e) {
        this.companyProfileData.country = e.target.value;
        this.states = this.service.AllStates.filter(function (item) { return item.short == e.target.value; })[0].states;
    };
    CompanyProfileComponent.prototype.getStateName = function (e) {
        this.companyProfileData.state = e.target.value;
    };
    CompanyProfileComponent.prototype.changeIndustryType = function (e) {
        this.companyProfileData.industryType = e.target.value;
    };
    CompanyProfileComponent.prototype.changeListener = function ($event) {
        this.readThis($event.target);
    };
    CompanyProfileComponent.prototype.readThis = function (inputValue) {
        var _this = this;
        var file = inputValue.files[0];
        var myReader = new FileReader();
        myReader.onloadend = function (e) {
            _this.profileLogo = myReader.result;
        };
        myReader.readAsDataURL(file);
        // this.companyProfileForm.patchValue({logo:this.image})
    };
    CompanyProfileComponent.prototype.updateProfile = function (val) {
        var _this = this;
        this.showLoader = true;
        var d = JSON.parse(localStorage.getItem('data'));
        var obj = {};
        if (val == 'contactperson') {
            obj =
                {
                    emailId: this.email,
                    token: this.token,
                    contactPerson: {
                        cpName: this.contactPersonProile.cpName,
                        cpMobileNo: this.contactPersonProile.cpMobileNo,
                        cpEmailId: this.contactPersonProile.cpEmailId,
                        cpDesignation: this.contactPersonProile.cpDesignation
                    },
                    companyProfile: this.companyProfileData,
                    logo: this.logo,
                    profilePic: this.profileLogo
                };
        }
        else {
            obj =
                {
                    emailId: this.email,
                    token: this.token,
                    contactPerson: this.contactPersonProile,
                    companyProfile: {
                        country: this.companyProfileData.country,
                        landlineNo: this.companyProfileData.landlineNo,
                        industryType: this.companyProfileData.industryType,
                        state: this.companyProfileData.state,
                        pinCode: this.companyProfileData.pinCode,
                        city: this.companyProfileData.city,
                        companyAddress: this.companyProfileData.companyAddress
                    },
                    logo: this.profileLogo,
                    profilePic: this.companyProfileData.cpProfilePic
                };
        }
        this.service.updateProfile(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                document.getElementById("uploadCaptureInputFile").value = "";
                _this.contactPersonProile = {};
                _this.companyProfileData = {};
                Swal('success', 'Profile Updated Successfully', 'success');
                _this.isEdit = false;
                d['companyProfile'] = response.companyProfile;
                d['contactPerson'] = response.contactPerson;
                _this.contactPersonProile = response.contactPerson;
                _this.companyProfileData = response.companyProfile;
                _this.logo = response.companyProfile.companyLogo;
                localStorage.setItem('data', JSON.stringify(d));
            }
            else {
                Swal('Oops !', response.message, 'error');
            }
        });
    };
    CompanyProfileComponent.prototype.getState = function () {
        return "delhi";
    };
    CompanyProfileComponent.prototype.changePassword = function () {
        var _this = this;
        if (this.role == "subuser") {
            var obj = {
                emailId: this.subuserid,
                oldPassword: this.oldPass,
                password: this.newPass,
                token: this.token,
                role: this.role
            };
        }
        else {
            var obj = {
                emailId: this.email,
                oldPassword: this.oldPass,
                password: this.newPass,
                token: this.token,
                role: this.role
            };
        }
        if (this.newPass == this.confirmPass) {
            this.service.changePassword(obj).subscribe(function (response) {
                if (response.code == "1") {
                    Swal('success', 'Password changed successfully', 'success');
                    _this.cookieService.deleteAll();
                    sessionStorage.clear();
                    localStorage.clear();
                    _this.router.navigate(['/']);
                }
                else {
                    Swal('Oops!', response.message, 'error');
                }
            });
        }
        else {
            Swal('Oops!', 'old password and confirm password did not matched !', 'error');
        }
    };
    CompanyProfileComponent.prototype.viewAgreement = function (data) {
        window.open('https://s3.amazonaws.com/miiror-vijay/images/public/document/' + data, "_blank");
    };
    CompanyProfileComponent.prototype.subscribe = function (val) {
        $('#subscribeNow').modal('show');
    };
    CompanyProfileComponent.prototype.buyNow = function (val) {
        localStorage.setItem('buy', val);
        $('#subscribeNow').modal('hide');
        this.router.navigate(['/subscribe']);
    };
    CompanyProfileComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    CompanyProfileComponent.prototype.getSubUsers = function () {
        var _this = this;
        var obj = { "token": this.token, "emailId": this.email };
        this.service.getSubUsers(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.subUsres = response.data;
            }
            else {
                Swal('Oops !', response.message, 'error');
            }
        });
    };
    CompanyProfileComponent.prototype.createSubUser = function () {
        var _this = this;
        var obj = {
            "name": this.userName,
            "emailId": this.userEmail,
            "password": this.userPass,
            "parent": this.email,
            "designation": this.designation,
            "role": this.subUser,
            "token": this.token
        };
        this.service.createSubUser(obj).subscribe(function (response) {
            if (response.code == "1") {
                Swal('success', response.message, 'success');
                _this.getSubUsers();
                $('#addUser').modal('hide');
            }
            else {
                Swal('Oops!', response.message, 'error');
            }
        });
    };
    CompanyProfileComponent.prototype.ValidateEmail = function (e) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e.target.value)) {
            this.checkEmail = true;
        }
        else {
            this.checkEmail = false;
            return (false);
        }
    };
    CompanyProfileComponent.prototype.updateAccountStatus = function (email, val) {
        var _this = this;
        Swal({
            title: 'Sure, You want to' + " " + val + " " + email,
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '',
            cancelButtonColor: '',
            confirmButtonText: 'Yes',
            allowOutsideClick: false,
        }).then(function (result) {
            if (result.value) {
                _this.service.updateAccountStatus({ "emailId": email, "token": _this.token }).subscribe(function (response) {
                    if (response.code == "1") {
                        Swal('success', response.messsage, 'success');
                        _this.getSubUsers();
                    }
                    else {
                        Swal('Oops!', response.message, 'error');
                    }
                });
            }
        });
    };
    CompanyProfileComponent.prototype.getActivityLogs = function (page) {
        var _this = this;
        var obj = { "emailId": this.email, "token": this.token, "pageNo": page };
        this.service.getActivityLogs(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.count = response.count;
                _this.activityLogs = response.data;
                _this.pager = _this.pagerService.getPager(_this.count, page);
            }
            else {
                Swal('Oops !', response.message, 'error');
            }
        });
    };
    CompanyProfileComponent.prototype.setPage = function (page) {
        this.pager = this.pagerService.getPager(this.count, page);
        this.activityLogs = this.activityLogs.slice(this.pager.startIndex, this.pager.endIndex + 1);
        this.getActivityLogs(page);
    };
    CompanyProfileComponent.prototype.addClass = function () {
        document.getElementById('headerShow').style.zIndex = '1';
        document.getElementById('outerMain').style.zIndex = "9999";
        document.getElementById('addUser').style.background = 'rgba(0, 0, 0, 0.61)';
    };
    CompanyProfileComponent.prototype.closeModal = function () {
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
    };
    CompanyProfileComponent.prototype.getAgreement = function (val) {
        var d = JSON.parse(localStorage.getItem('data'));
        if (d['product'].GET_MEASURED) {
            this.pdfSrc = d['product'].GET_MEASURED.pdfSrcpdfSrc;
            console.log(this.pdfSrc);
        }
        if (d['product'].SIZE2FIT) {
            this.pdfSrc = d['product'].SIZE2FIT.pdfSrcpdfSrc;
        }
        if (d['product'].MYSIZE) {
            this.pdfSrc = d['product'].MYSIZE.pdfSrcpdfSrc;
        }
        if (d['product'].QUICKSIZE) {
            this.pdfSrc = d['product'].QUICKSIZE.pdfSrcpdfSrc;
        }
    };
    CompanyProfileComponent = __decorate([
        Component({
            selector: 'app-company-profile',
            templateUrl: './company-profile.component.html',
            styleUrls: ['./company-profile.component.css']
        }),
        __metadata("design:paramtypes", [Router, MsServices, CookieService, PagerService])
    ], CompanyProfileComponent);
    return CompanyProfileComponent;
}());
export { CompanyProfileComponent };
//# sourceMappingURL=company-profile.component.js.map