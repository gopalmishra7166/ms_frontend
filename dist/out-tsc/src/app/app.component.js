var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewEncapsulation } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MsServices } from './services/ms-services';
import { CookieService } from 'ngx-cookie-service';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
var Swal = require('sweetalert2');
var AppComponent = /** @class */ (function () {
    function AppComponent(route, titleService, cookieService, toastr, spinner, msservice, http) {
        this.route = route;
        this.titleService = titleService;
        this.cookieService = cookieService;
        this.toastr = toastr;
        this.spinner = spinner;
        this.msservice = msservice;
        this.http = http;
        this.navbarhideshow = false;
        this.logoImage = "";
        this.isLogo = false;
        this.companyName = '';
        this.product_type = '';
        this.uid = '';
        this.isGetMeasured = false;
        this.isSize2Fit = false;
        this.isMySize = false;
        this.isQuickSize = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (JSON.parse(localStorage.getItem('data'))) {
            var d = JSON.parse(localStorage.getItem('data'));
            this.email = d['emailId'];
            this.token = d['token'];
            this.name = d['companyName'];
            this.role = d['role'];
        }
        this.http.get('https://jsonip.com/').subscribe(function (data) {
            _this.publicIP = data;
            var body = _this.publicIP._body;
            _this.physicalIp = JSON.parse(body).ip;
        });
        this.loadScript();
    };
    AppComponent.prototype.ngOnChanges = function () {
    };
    AppComponent.prototype.ngDoCheck = function () {
        if (localStorage.getItem('data')) {
            var d = JSON.parse(localStorage.getItem('data'));
            this.companyName = d['companyName'];
            this.logoImage = d['companyLogo'];
            if (d['role'] == 'master') {
                this.uid = d['emailId'];
            }
            else {
                this.uid = d['subuserId'];
            }
            if (d['product'].SIZE2FIT) {
                this.isSize2Fit = true;
                document.getElementById('s2f0').style.display = 'block';
                document.getElementById('s2f0').style.position = 'absolute';
                document.getElementById('s2f0').style.marginTop = '-37px';
                document.getElementById('s2f0').style.marginLeft = '135px';
                document.getElementById('s2f1').style.display = 'none';
            }
            else {
                document.getElementById('s2f1').style.display = 'block';
                document.getElementById('s2f1').style.position = 'absolute';
                document.getElementById('s2f1').style.marginTop = '-30px';
                document.getElementById('s2f1').style.marginLeft = '120px';
                document.getElementById('s2f0').style.display = 'none';
            }
            if (d['product'].GET_MEASURED) {
                this.isGetMeasured = true;
                document.getElementById('gm0').style.display = 'block';
                document.getElementById('gm0').style.position = 'absolute';
                document.getElementById('gm0').style.marginTop = '-37px';
                document.getElementById('gm0').style.marginLeft = '135px';
                document.getElementById('gm1').style.display = 'none';
            }
            else {
                document.getElementById('gm1').style.display = 'block';
                document.getElementById('gm1').style.position = 'absolute';
                document.getElementById('gm1').style.marginTop = '-30px';
                document.getElementById('gm1').style.marginLeft = '120px';
                document.getElementById('gm0').style.display = 'none';
            }
            if (d['product'].MYSIZE) {
                this.isMySize = true;
                document.getElementById('msize0').style.display = 'block';
                document.getElementById('Msize-show0').style.display = 'block';
                document.getElementById('Msize-show1').style.display = 'block';
                document.getElementById('msize0').style.position = 'absolute';
                document.getElementById('msize0').style.marginTop = '-37px';
                document.getElementById('msize0').style.marginLeft = '135px';
                document.getElementById('msize1').style.display = 'none';
            }
            else {
                document.getElementById('msize1').style.display = 'block';
                document.getElementById('msize1').style.position = 'absolute';
                document.getElementById('msize1').style.marginTop = '-30px';
                document.getElementById('msize1').style.marginLeft = '120px';
                document.getElementById('msize0').style.display = 'none';
                document.getElementById('Msize-show0').style.display = 'none';
                document.getElementById('Msize-show1').style.display = 'none';
            }
            if (d['product'].QUICKSIZE) {
                this.isQuickSize = true;
                document.getElementById('Qsize-show0').style.display = 'block';
                document.getElementById('Qsize-show1').style.display = 'block';
                document.getElementById('qsize0').style.display = 'block';
                document.getElementById('qsize0').style.position = 'absolute';
                document.getElementById('qsize0').style.marginTop = '-37px';
                document.getElementById('qsize0').style.marginLeft = '135px';
                document.getElementById('qsize1').style.display = 'none';
            }
            else {
                document.getElementById('qsize1').style.display = 'block';
                document.getElementById('qsize1').style.position = 'absolute';
                document.getElementById('qsize1').style.marginTop = '-30px';
                document.getElementById('qsize1').style.marginLeft = '120px';
                document.getElementById('qsize0').style.display = 'none';
                document.getElementById('Qsize-show0').style.display = 'none';
                document.getElementById('Qsize-show1').style.display = 'none';
            }
        }
    };
    // for navigation on change
    AppComponent.prototype.ngAfterContentInit = function () {
    };
    AppComponent.prototype.ngAfterViewInit = function () {
    };
    AppComponent.prototype.showSuccess = function () {
        this.toastr.success('Hello world!', 'Toastr fun!');
    };
    AppComponent.prototype.logout = function () {
        var _this = this;
        if (this.role == "subuser") {
            this.logoutUserPoint = {
                'emailId': this.uid,
                'token': this.token,
                'role': this.role,
                'physicalIp': this.physicalIp
            };
        }
        else {
            this.logoutUserPoint = {
                'emailId': this.email,
                'token': this.token,
                'role': this.role,
                'physicalIp': this.physicalIp
            };
        }
        Swal({
            title: 'Sure, You want to logout?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '',
            cancelButtonColor: '',
            confirmButtonText: 'Yes',
            allowOutsideClick: false,
        }).then(function (result) {
            if (result.value) {
                _this.msservice.logoutUser(_this.logoutUserPoint).subscribe(function (res) {
                    _this.logoutResponse = res;
                });
                _this.cookieService.deleteAll();
                sessionStorage.clear();
                localStorage.clear();
                _this.toastr.success('successfull signed out', 'Signed out', {
                    timeOut: 600,
                });
                _this.route.navigate(['/']);
                _this.msservice.statechange.next(false);
            }
            else {
                _this.toastr.warning('Not signed out', 'Canceled', {
                    timeOut: 800,
                });
            }
        });
    };
    // display()
    // {
    //     var x = document.getElementById("drpContent");
    //     if (x.style.display == "none") 
    //     {
    //         x.style.display = "block";
    //     } 
    //     else 
    //     {
    //         if (x.style.display == "block") 
    //         {
    //             x.style.display = "none";
    //         } 
    //         x.style.display = "none";
    //     }
    // }
    AppComponent.prototype.notification = function () {
        var x = document.getElementById("showNotification");
        if (x.style.display === "none") {
            x.style.display = "block";
        }
        else {
            x.style.display = "none";
        }
    };
    AppComponent.prototype.products = function () {
        var x = document.getElementById("showProducts");
        if (x.style.display === "none") {
            x.style.display = "block";
        }
        else {
            x.style.display = "none";
        }
    };
    AppComponent.prototype.productSelect = function (p) {
        if (p == 'gm') {
            localStorage.setItem('subscribeProduct', 'GET_MEASURED');
            this.route.navigate(['/subscribe']);
        }
        if (p == 'sf') {
            localStorage.setItem('subscribeProduct', 'SIZE2FIT');
            this.route.navigate(['/subscribe']);
        }
        if (p == 'ms') {
            localStorage.setItem('subscribeProduct', 'MYSIZE');
            this.route.navigate(['/subscribe']);
        }
        if (p == 'qs') {
            localStorage.setItem('subscribeProduct', 'QUICKSIZE');
            this.route.navigate(['/subscribe']);
        }
    };
    AppComponent.prototype.loadScript = function () {
        var isFound = false;
        var ANS_customer_id = "d1896866-4687-448a-877a-4afe81585f55";
        var scripts = document.getElementsByTagName("script");
        for (var i = 0; i < scripts.length; ++i) {
            if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("anetseal")) {
                isFound = true;
            }
        }
        if (!isFound) {
            var dynamicScripts = ["//verify.authorize.net:443/anetseal/seal.js/"];
            for (var i = 0; i < dynamicScripts.length; i++) {
                var node = document.createElement('script');
                node.src = dynamicScripts[i];
                node.id = ANS_customer_id;
                node.type = 'text/javascript';
                node.async = false;
                node.charset = 'utf-8';
                document.getElementsByTagName('head')[0].appendChild(node);
            }
        }
    };
    AppComponent = __decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css'],
            encapsulation: ViewEncapsulation.None
        }),
        __metadata("design:paramtypes", [Router, Title, CookieService,
            ToastrService, NgxSpinnerService, MsServices, Http])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map