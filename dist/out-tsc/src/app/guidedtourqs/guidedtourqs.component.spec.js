import { async, TestBed } from '@angular/core/testing';
import { GuidedtourqsComponent } from './guidedtourqs.component';
describe('GuidedtourqsComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [GuidedtourqsComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(GuidedtourqsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=guidedtourqs.component.spec.js.map