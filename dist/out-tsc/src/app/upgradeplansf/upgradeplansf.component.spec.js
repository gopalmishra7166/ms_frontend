import { async, TestBed } from '@angular/core/testing';
import { UpgradeplansfComponent } from './upgradeplansf.component';
describe('UpgradeplansfComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [UpgradeplansfComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(UpgradeplansfComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=upgradeplansf.component.spec.js.map