var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { JoyrideService } from 'ngx-joyride';
var Guidedtours2fComponent = /** @class */ (function () {
    function Guidedtours2fComponent(joyrideService) {
        this.joyrideService = joyrideService;
    }
    Guidedtours2fComponent.prototype.ngOnInit = function () {
    };
    Guidedtours2fComponent.prototype.onClick = function () {
        this.joyrideService.startTour({ steps: ['firstStep', 'secondStep', 'thirdStep', 'forthStep'] } // Your steps order
        );
    };
    Guidedtours2fComponent = __decorate([
        Component({
            selector: 'app-guidedtours2f',
            templateUrl: './guidedtours2f.component.html',
            styleUrls: ['./guidedtours2f.component.css']
        }),
        __metadata("design:paramtypes", [JoyrideService])
    ], Guidedtours2fComponent);
    return Guidedtours2fComponent;
}());
export { Guidedtours2fComponent };
//# sourceMappingURL=guidedtours2f.component.js.map