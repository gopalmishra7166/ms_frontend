import { async, TestBed } from '@angular/core/testing';
import { Guidedtours2fComponent } from './guidedtours2f.component';
describe('Guidedtours2fComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [Guidedtours2fComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(Guidedtours2fComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=guidedtours2f.component.spec.js.map