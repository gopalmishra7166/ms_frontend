var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MsServices } from '../services/ms-services';
var Swal = require('sweetalert2');
var TestingComponent = /** @class */ (function () {
    function TestingComponent(service) {
        this.service = service;
        this.mForm = new FormGroup({
            height: new FormControl(''),
            weight: new FormControl(''),
            age: new FormControl(''),
            gender: new FormControl('')
        });
    }
    TestingComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.merchantId = d['accountId'];
    };
    TestingComponent.prototype.selectSize = function (id) {
        if (id == 's1') {
            document.getElementById(id).style.background = '#011b2f';
            document.getElementById(id).style.color = '#fff';
            document.getElementById('s2').style.background = '#fff';
            document.getElementById('s2').style.color = '#333';
            document.getElementById('s3').style.background = '#fff';
            document.getElementById('s3').style.color = '#333';
            document.getElementById('s4').style.background = '#fff';
            document.getElementById('s4').style.color = '#333';
            document.getElementById('s5').style.background = '#fff';
            document.getElementById('s5').style.color = '#333';
        }
        else if (id == 's2') {
            document.getElementById(id).style.background = '#011b2f';
            document.getElementById(id).style.color = '#fff';
            document.getElementById('s1').style.background = '#fff';
            document.getElementById('s1').style.color = '#333';
            document.getElementById('s3').style.background = '#fff';
            document.getElementById('s3').style.color = '#333';
            document.getElementById('s4').style.background = '#fff';
            document.getElementById('s4').style.color = '#333';
            document.getElementById('s5').style.background = '#fff';
            document.getElementById('s5').style.color = '#333';
        }
        else if (id == 's3') {
            document.getElementById(id).style.background = '#011b2f';
            document.getElementById(id).style.color = '#fff';
            document.getElementById('s2').style.background = '#fff';
            document.getElementById('s2').style.color = '#333';
            document.getElementById('s1').style.background = '#fff';
            document.getElementById('s1').style.color = '#333';
            document.getElementById('s4').style.background = '#fff';
            document.getElementById('s4').style.color = '#333';
            document.getElementById('s5').style.background = '#fff';
            document.getElementById('s5').style.color = '#333';
        }
        else if (id == 's4') {
            document.getElementById(id).style.background = '#011b2f';
            document.getElementById(id).style.color = '#fff';
            document.getElementById('s2').style.background = '#fff';
            document.getElementById('s2').style.color = '#333';
            document.getElementById('s3').style.background = '#fff';
            document.getElementById('s3').style.color = '#333';
            document.getElementById('s1').style.background = '#fff';
            document.getElementById('s1').style.color = '#333';
            document.getElementById('s5').style.background = '#fff';
            document.getElementById('s5').style.color = '#333';
        }
        else if (id == 's5') {
            document.getElementById(id).style.background = '#011b2f';
            document.getElementById(id).style.color = '#fff';
            document.getElementById('s1').style.background = '#fff';
            document.getElementById('s1').style.color = '#333';
            document.getElementById('s3').style.background = '#fff';
            document.getElementById('s3').style.color = '#333';
            document.getElementById('s4').style.background = '#fff';
            document.getElementById('s4').style.color = '#333';
            document.getElementById('s2').style.background = '#fff';
            document.getElementById('s2').style.color = '#333';
        }
    };
    TestingComponent.prototype.getMeasurementQuickSize = function () {
        var obj = {
            "userId": this.email,
            "height": this.mForm.value.height,
            "weight": this.mForm.value.weight,
            "age": this.mForm.value.age,
            "gender": this.mForm.value.gender,
            "userType": "merchant",
            "merchantId": this.merchantId,
            "productName": "QUICKSIZE",
            "stomachShape": "normal",
            "apparelName": "shirt",
            "fitType": "reguler"
        };
        this.service.getMeasurementQuickSize(obj).subscribe(function (response) {
            if (response.code == "1") {
            }
            else {
                Swal('Oops!', response.message, 'info');
            }
        });
    };
    TestingComponent = __decorate([
        Component({
            selector: 'app-testing',
            templateUrl: './testing.component.html',
            styleUrls: ['./testing.component.css']
        }),
        __metadata("design:paramtypes", [MsServices])
    ], TestingComponent);
    return TestingComponent;
}());
export { TestingComponent };
//# sourceMappingURL=testing.component.js.map