var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { PagerService } from '../services/pagerService';
var Swal = require('sweetalert2');
var CoupanhistoryComponent = /** @class */ (function () {
    function CoupanhistoryComponent(service, pagerService) {
        this.service = service;
        this.pagerService = pagerService;
        this.rewardData = [];
        this.email = '';
        this.token = '';
        this.showLoader = false;
        this.searchKey = "brandName";
        this.count = 0;
        this.pager = {};
    }
    CoupanhistoryComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.getrewarddata();
    };
    CoupanhistoryComponent.prototype.getrewarddata = function () {
        var _this = this;
        this.showLoader = true;
        var obj = {
            "emailId": this.email,
            "token": this.token,
        };
        this.service.getrewarddata(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.rewardData = response.data;
                _this.count = _this.rewardData.length;
                _this.setPage(1);
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    CoupanhistoryComponent.prototype.setPage = function (page) {
        this.pager = this.pagerService.getPager(this.count, page);
        this.pagedItems = this.rewardData.slice(this.pager.startIndex, this.pager.endIndex + 1);
    };
    CoupanhistoryComponent.prototype.searchUser = function () {
    };
    CoupanhistoryComponent.prototype.clear = function () {
    };
    CoupanhistoryComponent = __decorate([
        Component({
            selector: 'app-coupanhistory',
            templateUrl: './coupanhistory.component.html',
            styleUrls: ['./coupanhistory.component.css']
        }),
        __metadata("design:paramtypes", [MsServices, PagerService])
    ], CoupanhistoryComponent);
    return CoupanhistoryComponent;
}());
export { CoupanhistoryComponent };
//# sourceMappingURL=coupanhistory.component.js.map