var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { postDataOfMerchantInfo } from './../model/postDataOfMerchantInfo';
import { MerchantAdress } from './../model/merchant-adress';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { MsServices } from './../services/ms-services';
import { CookieService } from 'ngx-cookie-service';
import { DomSanitizer } from '@angular/platform-browser';
import { MerchantDetails } from './../model/merchantdetails';
var Swal = require('sweetalert2');
var MerchantAccesscodeComponent = /** @class */ (function () {
    function MerchantAccesscodeComponent(route, ser, cookieService, _sanitizer) {
        this.route = route;
        this.ser = ser;
        this.cookieService = cookieService;
        this._sanitizer = _sanitizer;
        this.now = new Date();
        this.accessCode = undefined;
        this.imageSrc = '/assets/img/logo-preview.jpg';
        this.show = false;
        this.sh = true;
        this.prdDiv = false;
        this.showLoader = false;
        this.expired = false;
        this.merchantDetails = new MerchantDetails();
        this.updateMerchantInfo = new postDataOfMerchantInfo();
        this.updateMerchantAddress = new MerchantAdress();
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
    }
    MerchantAccesscodeComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.name = d['companyName'];
        this.accessCode = d['product'].GET_MEASURED.accessCode;
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="User Login Details";
        if (d['product'].GET_MEASURED.planType == 'freeTrial' || d['product'].GET_MEASURED.planType == 'FREETRIAL') {
            console.log(d['product'].GET_MEASURED.remainingDays);
            if (d['product'].GET_MEASURED.remainingDays < 0) {
                this.expired = true;
            }
            else {
                this.expired = false;
            }
        }
        if (this.expired == false) {
            this.getAccessCodeHistory();
        }
    };
    MerchantAccesscodeComponent.prototype.getAccessCodeHistory = function () {
        var _this = this;
        var obj = { emailId: this.email, token: this.token, productName: 'GET_MEASURED' };
        this.ser.getAccessCodeHistory(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.getAcceessCodeDetail = response.data;
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    MerchantAccesscodeComponent.prototype.updateAccessCode = function () {
        var _this = this;
        this.showLoader = true;
        var obj = { "token": this.token, "accessCode": this.accessCode, "emailId": this.email, "productName": 'GET_MEASURED', };
        Swal({
            title: 'Are you sure, want to update?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '',
            cancelButtonColor: '',
            confirmButtonText: 'Yes',
            allowOutsideClick: false,
        }).then(function (result) {
            if (result.value) {
                _this.ser.updateAccessCode(obj).subscribe(function (res) {
                    _this.showLoader = false;
                    if (res.code == "1") {
                        _this.accessCode = res.accessCode;
                        _this.ser.getAccessCodeHistory(obj).subscribe(function (response) {
                            if (response.code == "1") {
                                _this.getAcceessCodeDetail = response.data;
                            }
                            else {
                                Swal('error', response.message, 'error');
                            }
                        });
                    }
                    else {
                        Swal('error', res.message, 'error');
                    }
                });
            }
            else {
                _this.showLoader = false;
            }
        });
    };
    MerchantAccesscodeComponent = __decorate([
        Component({
            selector: 'app-merchant-accesscode',
            templateUrl: './merchant-accesscode.component.html',
            styleUrls: ['./merchant-accesscode.component.css']
        }),
        __metadata("design:paramtypes", [Router, MsServices, CookieService, DomSanitizer])
    ], MerchantAccesscodeComponent);
    return MerchantAccesscodeComponent;
}());
export { MerchantAccesscodeComponent };
//# sourceMappingURL=merchant-accesscode.component.js.map