import { async, TestBed } from '@angular/core/testing';
import { MerchantAccesscodeComponent } from './merchant-accesscode.component';
describe('MerchantAccesscodeComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [MerchantAccesscodeComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(MerchantAccesscodeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=merchant-accesscode.component.spec.js.map