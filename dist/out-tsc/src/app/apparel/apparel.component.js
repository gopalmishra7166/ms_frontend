var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Router, ActivatedRoute } from '@angular/router';
import { Component, EventEmitter, Output } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { CookieService } from 'ngx-cookie-service';
// import {DomSanitizer} from '@angular/platform-browser';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { PagerService } from '../services/pagerService';
import * as jspdf from 'jspdf';
var Swal = require('sweetalert2');
import { saveAs } from 'file-saver';
import { NgxSpinnerService } from 'ngx-spinner';
var ApparelComponent = /** @class */ (function () {
    function ApparelComponent(rout, pagerService, spinner, config, modalService, toastr, route, cookieService, service, _sanitizer) {
        this.rout = rout;
        this.pagerService = pagerService;
        this.spinner = spinner;
        this.modalService = modalService;
        this.toastr = toastr;
        this.route = route;
        this.cookieService = cookieService;
        this.service = service;
        this._sanitizer = _sanitizer;
        this.selectedItems = [];
        this.imageUrl = '';
        this.key = '';
        this.pager = {};
        this.showLoader = false;
        this.storeCount = 0;
        this.keyValue = "apparelName";
        this.isSearch = false;
        this.editData = [];
        this.finalPoints = {};
        this.result = [];
        this.newArry = [];
        this.expired = false;
        this.now = new Date();
        this.hello = [];
        this.products = [];
        this.selectedProducts = [];
        this.dummyData = [];
        this.checked = new EventEmitter();
        config.backdrop = 'static';
        config.keyboard = false;
    }
    ApparelComponent.prototype.setClickedRow = function (i) {
        if (i.measurementPoints === this.products) {
        }
        else {
        }
    };
    ApparelComponent.prototype.mybtn = function (measured) {
        // measured points comming
        this.hello = measured.measurementPoints;
        this.country = measured.country;
        this.selectedMeasuredUpdate = { merchantKey: __assign({}, measured.merchantKey), measurementPoints: measured.measurementPoints.slice() };
        this.missing = this.products.filter(function (item) { return measured.measurementPoints.indexOf(item.products) < 0; });
    };
    ApparelComponent.prototype.ngOnInit = function () {
        var param = this.rout.snapshot.paramMap.get("id");
        if (param == 'GetMeasured' || param == 'GET_MEASURED') {
            this.routeParam = "GET_MEASURED";
        }
        else {
            this.routeParam = "SIZE2FIT";
        }
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.name = d['companyName'];
        this.mid = d['mid'];
        if (d['product'].GET_MEASURED.planType == 'freeTrial' || d['product'].GET_MEASURED.planType == 'FREETRIAL') {
            console.log(d['product'].GET_MEASURED.remainingDays);
            if (d['product'].GET_MEASURED.remainingDays < 0) {
                this.expired = true;
            }
            else {
                this.expired = false;
            }
        }
        if (this.expired == false) {
            this.getData(1);
        }
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Apparels";
    };
    ApparelComponent.prototype.getData = function (page) {
        var _this = this;
        this.data = [];
        this.showLoader = true;
        var data = { "emailId": this.email, "token": this.token, "productName": this.routeParam, "pageNo": page };
        this.service.getApparel(data).subscribe(function (res) {
            _this.showLoader = false;
            if (res.code == 1 && res.data) {
                _this.storeCount = res.count;
                _this.data = res.data;
                _this.pager = _this.pagerService.getPager(_this.storeCount, page);
                _this.data.map(function (item) {
                    item.measurementPoint = JSON.parse(item.measurementPoint);
                });
            }
            else {
                Swal.fire("Oops!", res.message, 'info');
            }
        });
    };
    ApparelComponent.prototype.setPage = function (page) {
        this.pager = this.pagerService.getPager(this.storeCount, page);
        this.pagedItems = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
        this.getData(page);
    };
    ApparelComponent.prototype.onChecked = function (app, checked) {
        if (checked) {
            this.result.push({ key: app.key, value: app.displayName });
        }
        else {
            var myArray = this.result.filter(function (obj) {
                return obj.value !== app.displayName;
            });
            this.result = [];
            this.result = myArray;
        }
        this.finalPoints = {};
        for (var i = 0; i < this.result.length; i++) {
            this.finalPoints[this.result[i].key] = this.result[i].value;
        }
    };
    ApparelComponent.prototype.mapValue = function (data2) {
        return data2;
    };
    ApparelComponent.prototype.updateMeasurement = function () {
        var _this = this;
        var obj = {
            "emailId": this.email,
            "token": this.token,
            "action": 'update',
            "brandName": this.measurementData.brandName,
            "gender": this.measurementData.gender,
            "countrySize": this.measurementData.coutry,
            "apparelId": this.measurementData.apparelId,
            "productName": this.routeParam,
            "measurementPointData": this.finalPoints
        };
        console.log(this.finalPoints);
        this.service.addApparel(obj).subscribe(function (response) {
            if (response.code == "1") {
                Swal('Success', response.message, 'success');
                _this.getData(1);
                _this.newArry = [];
                $('#updatePoints').modal('hide');
            }
            else {
                Swal('Oops!', response.message, 'info');
            }
        });
    };
    ApparelComponent.prototype.editMeasurement = function (measured) {
        var _this = this;
        this.measurementData = measured;
        var measurement = measured.measurementPoint;
        this.result = Object.keys(measurement).map(function (key) {
            return ({ "key": (key), "value": measurement[key] });
        });
        this.gender = measured.gender;
        var obj = { "gender": this.gender, "token": this.token, "productName": this.routeParam, "emailId": this.email };
        this.service.getMeasurementPoints(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.editData = response.data;
                $('#updatePoints').modal('show');
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    ApparelComponent.prototype.searchApparel = function () {
        var _this = this;
        this.data = [];
        if (this.searchText == "") {
            Swal('info', 'please enter brand name or apparel name', 'info');
        }
        else {
            var obj = { token: this.token, emailId: this.email, productName: this.routeParam, "searchKey": this.keyValue, "searchValue": this.searchText.trim(), "pageNo": "1" };
            this.service.searchApparel(obj).subscribe(function (response) {
                if (response.code == "1") {
                    _this.data = response.data;
                    // console.log(this.data[0].measurementPoints)
                    _this.data.map(function (item) {
                        item.measurementPoint = JSON.parse(item.measurementPoint);
                    });
                }
                else {
                    Swal('Error', response.message, 'error');
                }
            });
        }
    };
    ApparelComponent.prototype.isSelected = function (data, k) {
        for (var i = 0; i < this.result.length; i++) {
            if (this.result[i].key == data.key) {
                return true;
            }
            else {
                $('#check' + k).prop('checked', false);
            }
        }
    };
    ApparelComponent.prototype.onChange = function (value, checked) {
        if (checked) {
            this.hello.push(value);
        }
        else {
            var index = this.hello.indexOf(value);
            this.hello.splice(index, 1);
        }
    };
    ApparelComponent.prototype.delete = function (measured) {
        var _this = this;
        Swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this data!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            _this.fordeleteConfirm = result.value;
            if (result.value == true) {
                var data = { "emailId": measured.merchantId, "brandName": measured.brandName, "apparelId": measured.apparelId, "gender": measured.gender, "productName": measured.productName, "token": _this.token };
                _this.service.deleteMerchantInfo(data).subscribe(function (res) {
                    if (res.code == 1) {
                        _this.getData(1);
                        _this.toastr.success(res.message, 'Success', { timeOut: 1100, });
                    }
                    else {
                        _this.toastr.warning(res.message, 'Error', { timeOut: 1100, });
                    }
                });
            }
            else {
                _this.toastr.warning('Not deleted', 'Failed', { timeOut: 1100, });
            }
        });
    };
    ApparelComponent.prototype.dynamicMeasurementPoints = function (measured) {
        var _this = this;
        this.showLoader = true;
        this.products = [];
        var onAddApparel = {
            'merchantID': measured.merchantKey.merchatID,
            'apparelName': measured.merchantKey.apparelName,
            'gender': measured.merchantKey.gender,
            'brandName': measured.merchantKey.brandName,
            'country': measured.country,
            'productname': 'SIZE2FIT',
        };
        this.service.measurementPoints(onAddApparel).subscribe(function (res) {
            _this.showLoader = false;
            if (res.code == 1) {
                _this.products = res.data;
            }
            else {
                Swal.fire('Oops!', res.message, 'error');
            }
        });
    };
    ApparelComponent.prototype.downloadTable = function () {
        var _this = this;
        var jsonForPdf = {
            'merchantID': this.email,
            'productname': 'size2fit',
            'token': this.cookieService.get('token'),
        };
        this.service.forSize2FitPdfSizes(jsonForPdf).subscribe(function (res) {
            _this.responseForDate = res;
            if (_this.responseForDate.status === 'FAILURE') {
                _this.toastr.error('Date not exist', 'Date is invalid', {
                    timeOut: 1200,
                });
            }
            else {
                _this.service.getDownload(_this.responseForDate.message).subscribe(function (respose) {
                    _this.downloadCompletePDF(respose);
                });
            }
        });
    };
    ApparelComponent.prototype.downloadCompletePDF = function (res) {
        var doc = new jspdf();
        var reader = new FileReader();
        reader.addEventListener('loadend', function () {
        });
        saveAs(res, this.email + '.pdf');
        this.spinner.hide();
        reader.readAsArrayBuffer(res);
        var file = new Blob([res], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(res);
        var down11 = window.open(fileURL, '_blank');
        down11.print();
    };
    ApparelComponent.prototype.clear = function () {
        this.getData(1);
    };
    ApparelComponent.prototype.getImage = function (val, gen, keyval) {
        this.key = keyval;
        this.imageUrl = "https://s3.ap-south-1.amazonaws.com/commonms/" + gen.toLowerCase() + "_large_icon/" + val.toLowerCase() + ".png";
        $('#viewImage').modal('show');
    };
    // showModal(val) 
    // {
    //     if (val == 'hover') 
    //     {
    //         $(".block__pic").imagezoomsl({
    //             zoomrange: [5, 5]
    //         });
    //     }
    //     else {
    //         document.getElementById('blockPik').classList.remove('magnifier');
    //     }
    // }
    ApparelComponent.prototype.addClass = function () {
        document.getElementById('headerShow').style.zIndex = '1';
        document.getElementById('outerMain').style.zIndex = "9999";
        document.getElementById('viewDescription').style.background = 'rgba(0, 0, 0, 0.61)';
    };
    ApparelComponent.prototype.closeModal = function () {
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
    };
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], ApparelComponent.prototype, "checked", void 0);
    ApparelComponent = __decorate([
        Component({
            selector: 'app-apparel',
            templateUrl: './apparel.component.html',
            styleUrls: ['./apparel.component.css']
        }),
        __metadata("design:paramtypes", [ActivatedRoute, PagerService, NgxSpinnerService,
            NgbModalConfig, NgbModal, ToastrService, Router,
            CookieService, MsServices, DomSanitizer])
    ], ApparelComponent);
    return ApparelComponent;
}());
export { ApparelComponent };
//# sourceMappingURL=apparel.component.js.map