var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { DomSanitizer } from '@angular/platform-browser';
import { MerchantDetails } from './../model/merchantdetails';
import { MsServices } from '../services/ms-services';
var Swal = require('sweetalert2');
var TermsConditionComponent = /** @class */ (function () {
    function TermsConditionComponent(route, ser, cookieService, _sanitizer) {
        this.route = route;
        this.ser = ser;
        this.cookieService = cookieService;
        this._sanitizer = _sanitizer;
        this.now = new Date();
        // -----------------------
        this.merchantDetails = new MerchantDetails();
    }
    TermsConditionComponent.prototype.ngOnInit = function () {
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Terms & Conditions";
    };
    TermsConditionComponent = __decorate([
        Component({
            selector: 'app-terms-condition',
            templateUrl: './terms-condition.component.html',
            styleUrls: ['./terms-condition.component.css']
        }),
        __metadata("design:paramtypes", [Router, MsServices, CookieService, DomSanitizer])
    ], TermsConditionComponent);
    return TermsConditionComponent;
}());
export { TermsConditionComponent };
//# sourceMappingURL=terms-condition.component.js.map