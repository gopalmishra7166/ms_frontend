var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
var Swal = require('sweetalert2');
var MeasurementguideComponent = /** @class */ (function () {
    function MeasurementguideComponent(service) {
        this.service = service;
        this.gender = 'male';
        this.data = [];
        this.showLoader = false;
    }
    MeasurementguideComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.token = d['token'];
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Measurement Guide";
        this.getMeasurementGuide();
    };
    MeasurementguideComponent.prototype.getMeasurementGuide = function () {
        var _this = this;
        this.showLoader = true;
        var obj = { gender: this.gender, token: this.token };
        this.service.getMesurementGuide(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.data = response.data;
            }
            else
                Swal('Oops !', response.message, 'error');
        });
    };
    MeasurementguideComponent.prototype.getGraph = function (val) {
        this.gender = val;
        this.getMeasurementGuide();
    };
    MeasurementguideComponent = __decorate([
        Component({
            selector: 'app-measurementguide',
            templateUrl: './measurementguide.component.html',
            styleUrls: ['./measurementguide.component.css']
        }),
        __metadata("design:paramtypes", [MsServices])
    ], MeasurementguideComponent);
    return MeasurementguideComponent;
}());
export { MeasurementguideComponent };
//# sourceMappingURL=measurementguide.component.js.map