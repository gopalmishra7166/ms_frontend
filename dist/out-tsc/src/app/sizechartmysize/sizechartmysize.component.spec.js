import { async, TestBed } from '@angular/core/testing';
import { SizechartmysizeComponent } from './sizechartmysize.component';
describe('SizechartmysizeComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [SizechartmysizeComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(SizechartmysizeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=sizechartmysize.component.spec.js.map