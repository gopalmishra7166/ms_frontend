var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ActivatedRoute } from '@angular/router';
import { PagerService } from '../services/pagerService';
var Swal = require('sweetalert2');
var counter = 0;
var UserComponent = /** @class */ (function () {
    function UserComponent(service, rout, pagerService) {
        this.service = service;
        this.rout = rout;
        this.pagerService = pagerService;
        this.usersData = [];
        this.detailData = [];
        this.routeParam = '';
        this.isChecked = false;
        this.searchKey = "userName";
        this.searchText = "";
        this.userCount = 0;
        this.pager = {};
        this.showLoader = false;
        this.expired = false;
    }
    UserComponent.prototype.ngOnInit = function () {
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Users";
        var param = this.rout.snapshot.paramMap.get("id");
        if (param == 'GetMeasured') {
            this.routeParam = "GET_MEASURED";
        }
        else {
            this.routeParam = "SIZE2FIT";
        }
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        if (d['product'].SIZE2FIT.planType == 'freeTrial' || d['product'].SIZE2FIT.planType == 'FREETRIAL') {
            if (d['product'].SIZE2FIT.remainingDays < 0) {
                this.expired = true;
            }
            else {
                this.expired = false;
            }
        }
        if (this.expired == false) {
            this.getUserList(1);
        }
    };
    UserComponent.prototype.getUserList = function (page) {
        var _this = this;
        this.showLoader = true;
        var obj = { "emailId": this.email, "token": this.token, "productName": this.routeParam, "pageNo": page.toString() };
        this.service.getUserList(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.userCount = response.count;
                _this.usersData = response.data;
                _this.pager = _this.pagerService.getPager(_this.userCount, page);
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    UserComponent.prototype.setPage = function (page) {
        this.pager = this.pagerService.getPager(this.userCount, page);
        this.pagedItems = this.usersData.slice(this.pager.startIndex, this.pager.endIndex + 1);
        this.getUserList(page);
    };
    UserComponent.prototype.processIdData = function (data) {
        var _this = this;
        this.showLoader = true;
        this.gender = data.gender;
        this.processId = data.processId;
        var obj = { "processId": data.processId, "token": this.token, "productName": this.routeParam, "emailId": this.email, "pageNo": "1" };
        this.service.processIdData(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.detailData = response.data;
                $('#dataModal').modal('show');
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    UserComponent.prototype.searchUser = function () {
        var _this = this;
        this.showLoader = true;
        this.usersData = [];
        var obj = { "token": this.token, "emailId": this.email, "productName": this.routeParam, "searchKey": this.searchKey, "searchValue": this.searchText, "pageNo": 1 };
        this.service.searchUserList(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.usersData = response.data;
            }
            else {
                Swal("Error", response.message, 'erro');
            }
        });
    };
    UserComponent.prototype.clear = function () {
        this.getUserList(1);
    };
    UserComponent.prototype.switchValue = function (e) {
        if (e.target.checked) {
            this.isChecked = true;
        }
        else {
            this.isChecked = false;
        }
    };
    UserComponent.prototype.download = function (data) {
        var _this = this;
        this.showLoader = true;
        var result = data.measurement.map(function (a) { return a.displayName; });
        var obj = { "gender": this.gender, "emailId": this.email, "productName": this.routeParam, "apparelId": data.apparelId, "brandName": data.brandName, "measurementPoint": result, "processId": this.processId, "token": this.token };
        this.service.getUserPdf(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                window.open(response.s3urlpdf, '_blank');
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    UserComponent.prototype.viewImages = function (val, title) {
        this.title = title;
        this.imgsrc = val;
        $('#viewImages').modal('show');
    };
    UserComponent.prototype.rotate = function () {
        counter += 90;
        $('.img').css('transform', 'rotate(' + counter + 'deg)');
    };
    UserComponent.prototype.addClass = function () {
        document.getElementById('headerShow').style.zIndex = '1';
        document.getElementById('outerMain').style.zIndex = "9999";
        document.getElementById('dataModal').style.background = 'rgba(0, 0, 0, 0.61)';
    };
    UserComponent.prototype.closeModal = function () {
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
    };
    UserComponent = __decorate([
        Component({
            selector: 'app-user',
            templateUrl: './user.component.html',
            styleUrls: ['./user.component.css']
        }),
        __metadata("design:paramtypes", [MsServices, ActivatedRoute, PagerService])
    ], UserComponent);
    return UserComponent;
}());
export { UserComponent };
//# sourceMappingURL=user.component.js.map