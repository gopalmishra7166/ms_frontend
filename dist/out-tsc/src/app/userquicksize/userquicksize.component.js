var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ActivatedRoute } from '@angular/router';
import { PagerService } from '../services/pagerService';
var Swal = require('sweetalert2');
import { ToastrService } from 'ngx-toastr';
var UserquicksizeComponent = /** @class */ (function () {
    function UserquicksizeComponent(service, toast, rout, pagerService) {
        this.service = service;
        this.toast = toast;
        this.rout = rout;
        this.pagerService = pagerService;
        this.showLoader = false;
        this.expired = false;
        this.usersData = [];
        this.searchKey = "email_id";
        this.searchText = "";
        this.userCount = 0;
        this.pager = {};
        this.productName = '';
    }
    UserquicksizeComponent.prototype.ngOnInit = function () {
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Users";
        var param = this.rout.snapshot.paramMap.get("id");
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.productName = 'QUICKSIZE';
        if (d['product'].QUICKSIZE.planType == 'FREETRIAL') {
            console.log(d['product'].QUICKSIZE.remainingDays);
            if (d['product'].QUICKSIZE.remainingDays < 0) {
                this.expired = true;
            }
            else {
                this.expired = false;
            }
        }
        if (this.expired == false) {
            this.getUserListQuicksize(1);
        }
    };
    UserquicksizeComponent.prototype.getUserListQuicksize = function (page) {
        var _this = this;
        this.showLoader = true;
        var obj = { "emailId": this.email, "token": this.token, "productName": this.productName, "pageNo": page.toString() };
        this.service.getUserList(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.userCount = response.count;
                _this.usersData = response.data;
                _this.pager = _this.pagerService.getPager(_this.userCount, page);
            }
            else {
                _this.toast.warning(response.message, 'Message', { timeOut: 2000, });
            }
        });
    };
    UserquicksizeComponent.prototype.searchUser = function () {
        var _this = this;
        this.showLoader = true;
        this.usersData = [];
        var obj = { "token": this.token, "emailId": this.email, "productName": 'QUICKSIZE', "key": this.searchKey, "value": this.searchText };
        this.service.searchuserlistqs(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.usersData = response.data;
                _this.userCount = response.record_count;
            }
            else {
                Swal("Error", response.message, 'erro');
            }
        });
    };
    UserquicksizeComponent.prototype.clear = function () {
        this.getUserListQuicksize(1);
    };
    UserquicksizeComponent = __decorate([
        Component({
            selector: 'app-userquicksize',
            templateUrl: './userquicksize.component.html',
            styleUrls: ['./userquicksize.component.css']
        }),
        __metadata("design:paramtypes", [MsServices, ToastrService, ActivatedRoute, PagerService])
    ], UserquicksizeComponent);
    return UserquicksizeComponent;
}());
export { UserquicksizeComponent };
//# sourceMappingURL=userquicksize.component.js.map