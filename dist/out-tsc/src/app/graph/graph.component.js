var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { MsServices } from '../services/ms-services';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
var Swal = require('sweetalert2');
var GraphComponent = /** @class */ (function () {
    function GraphComponent(route, cookie, Service, titleService) {
        this.route = route;
        this.cookie = cookie;
        this.Service = Service;
        this.titleService = titleService;
        this.selectedVal = "last30days";
        this.now = new Date();
        // for pie chart
        this.width = 600;
        this.height = 400;
        this.type = 'pie3d';
        this.dataFormat = 'json';
        this.displayName = "GetMeasured";
        this.product_type = '';
        this.graphType = "Get_Measured";
        this.graph1 = false;
        this.totalAccount = '';
        this.totalVisitor = '';
        this.approval = '';
        this.visitorsData = [];
        this.genderData = [];
        this.isQuickSize = false;
        this.titleService.setTitle("Dashboard");
        this.title = 'List of user visited our website';
    }
    GraphComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.token = d['token'];
        this.uid = d['emailId'];
        if (d['product'].GET_MEASURED) {
            this.isGetMeasured = true;
        }
        if (d['product'].SIZE2FIT) {
            this.isSize2Fit = true;
        }
        if (d['product'].MYSIZE) {
            this.isMySize = true;
        }
        if (d['product'].QUICKSIZE) {
            this.isQuickSize = true;
        }
        if (d['product'].GET_MEASURED) {
            this.graphType = 'GET_MEASURED';
        }
        else if (d['product'].SIZE2FIT) {
            this.graphType = 'SIZE2FIT';
        }
        else if (d['product'].MYSIZE) {
            this.graphType = 'MYSIZE';
        }
        else if (d['product'].QUICKSIZE) {
            this.graphType = 'QUICKSIZE';
        }
        this.getGraphData(this.graphType);
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Dashboard";
    };
    GraphComponent.prototype.getGraph = function (val) {
        this.graphType = val;
        this.getGraphData(this.graphType);
    };
    GraphComponent.prototype.getChart = function (val) {
        this.selectedVal = val.target.value;
        this.getGraphData(this.graphType);
    };
    GraphComponent.prototype.getGraphData = function (productName) {
        var _this = this;
        this.genderData = [];
        this.visitorsData = [];
        var obj = { emailId: this.uid, token: this.token, productName: productName };
        this.Service.graphData(obj).subscribe(function (response) {
            if (response.code == 1) {
                var d = response.data.last30days.visitors;
                for (var i = 0; i < d.date.length; i++) {
                    _this.visitorsData.push({ date: d.date[i], count: d.count[i] });
                }
                _this.genderData.push({ Gender: "Male", count: response.data.last30days.genderWise.male }, { Gender: "Female", count: response.data.last30days.genderWise.female });
                _this.genderWise(_this.genderData);
                _this.visitors(_this.visitorsData);
                _this.totalAccount = response.data.accounts.subuserAccounts;
                _this.totalVisitor = response.data.last30days.totalVisitor;
                _this.approval = response.data.mpPendingApproval;
                _this.apparelCount = response.data.apparelCount;
                _this.pendingTicketCount = response.data.pendingTicketCount;
            }
            else {
                Swal.fire('Oops !', response.message, 'error');
            }
        });
    };
    GraphComponent.prototype.visitors = function (data) {
        var chart = AmCharts.makeChart("chartdiv1", {
            "hideCredits": true,
            "type": "serial",
            "theme": "none",
            "dataProvider": data,
            "depth3D": 20,
            "angle": 30,
            "titles": [{
                    "text": "Monthly Visitors Count"
                }],
            "valueAxes": [{
                    "gridColor": "#FFFFFF",
                    "gridAlpha": 0.2,
                    "dashLength": 0
                }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                    "balloonText": "[[date]]: <b>[[count]]</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "count"
                }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 20
            },
            "export": {
                "enabled": true
            }
        });
    };
    GraphComponent.prototype.genderWise = function (data) {
        var chart = AmCharts.makeChart("chartdiv2", {
            "hideCredits": true,
            "type": "pie",
            "theme": "none",
            "dataProvider": data,
            "titleField": "Gender",
            "valueField": "count",
            "labelRadius": 5,
            "titles": [{
                    "text": "Gender Wise Count"
                }],
            "radius": "42%",
            "innerRadius": "60%",
            "labelText": "[[Gender]]",
            "export": {
                "enabled": true
            }
        });
    };
    GraphComponent = __decorate([
        Component({
            selector: 'app-graph',
            templateUrl: './graph.component.html',
            styleUrls: ['./graph.component.css']
        }),
        __metadata("design:paramtypes", [Router, CookieService, MsServices, Title])
    ], GraphComponent);
    return GraphComponent;
}());
export { GraphComponent };
//# sourceMappingURL=graph.component.js.map