var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
var Swal = require('sweetalert2');
var ResourcesComponent = /** @class */ (function () {
    function ResourcesComponent(service) {
        this.service = service;
        this.merchantId = '';
        this.apikey = '';
        this.pass = '';
        this.GET_MEASURED = false;
        this.SIZE2FIT = false;
        this.QUICKSIZE = false;
    }
    ResourcesComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.merchantId = d['emailId'];
        if (d['product'].GET_MEASURED) {
            this.planDetailGM = d['product'].GET_MEASURED;
        }
        if (d['product'].SIZE2FIT) {
            this.planDetailS2F = d['product'].SIZE2FIT;
        }
        if (d['apiKey']) {
            this.apikey = d['apiKey'];
        }
        if (d['product'].GET_MEASURED) {
            this.GET_MEASURED = true;
            this.gm = true;
            this.sf = false;
            this.qs = false;
        }
        else if (d['product'].SIZE2FIT) {
            this.SIZE2FIT = true;
            this.gm = false;
            this.sf = true;
            this.qs = false;
        }
        else if (d['product'].QUICKSIZE) {
            this.QUICKSIZE = true;
            this.gm = false;
            this.sf = false;
            this.qs = true;
        }
        if (d['product'].SIZE2FIT) {
            if (d['product'].SIZE2FIT.planType == 'freeTrial' || d['product'].SIZE2FIT.planType == 'FREETRIAL') {
                if (d['product'].SIZE2FIT.remainingDays < 0) {
                    this.sfexpired = true;
                }
                else {
                    this.sfexpired = false;
                }
            }
        }
        if (d['product'].GET_MEASURED) {
            if (d['product'].GET_MEASURED.planType == 'freeTrial' || d['product'].GET_MEASURED.planType == 'FREETRIAL') {
                if (d['product'].GET_MEASURED.remainingDays < 0) {
                    this.gmexpired = true;
                }
                else {
                    this.gmexpired = false;
                }
            }
        }
        if (d['product'].QUICKSIZE) {
            if (d['product'].QUICKSIZE.planType == 'freeTrial' || d['product'].QUICKSIZE.planType == 'FREETRIAL') {
                if (d['product'].QUICKSIZE.remainingDays < 0) {
                    this.qsexpired = true;
                }
                else {
                    this.qsexpired = false;
                }
            }
        }
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Developer Resources";
    };
    ResourcesComponent.prototype.view = function (val) {
        if (val == 'android') {
            window.open('https://s3.ap-south-1.amazonaws.com/commonms/developers/documents/android_sdk_developer_document.pdf', '_blank');
        }
        else if (val == 'ios') {
            window.open('https://s3.ap-south-1.amazonaws.com/commonms/developers/documents/ios_sdk_developer_document.pdf', '_blank');
        }
        else if (val == 'getms') {
            window.open('https://s3.ap-south-1.amazonaws.com/commonms/developers/documents/getmeasured_webapis_developer_document.pdf', '_blank');
        }
        else {
            window.open('https://s3.ap-south-1.amazonaws.com/commonms/developers/documents/size2fit_webapis_developer_document.pdf', '_blank');
        }
    };
    ResourcesComponent.prototype.key = function (val) {
        if (val == 'gm') {
            this.gm = true;
            this.sf = false;
        }
        else if (val == 'sf') {
            this.gm = false;
            this.sf = true;
        }
    };
    ResourcesComponent.prototype.getKey = function () {
        var _this = this;
        if (this.pass == "" || this.pass == undefined) {
            Swal('info', 'please enter current password', 'info');
        }
        else {
            this.service.getApiKey({ email_id: this.merchantId, password: this.pass }).subscribe(function (response) {
                if (response.code == "1") {
                    _this.apikey = response.data;
                    $('#apikeyModal').modal('hide');
                    localStorage.setItem('key', _this.apikey);
                }
                else {
                    Swal('Oops!', response.message, 'error');
                }
            });
        }
    };
    ResourcesComponent = __decorate([
        Component({
            selector: 'app-resources',
            templateUrl: './resources.component.html',
            styleUrls: ['./resources.component.css']
        }),
        __metadata("design:paramtypes", [MsServices])
    ], ResourcesComponent);
    return ResourcesComponent;
}());
export { ResourcesComponent };
//# sourceMappingURL=resources.component.js.map