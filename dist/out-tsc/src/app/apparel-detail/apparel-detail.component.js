var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Pernt_MerchantSpecificDataContainerArray } from '../SizeToFit/Pernt_MerchantSpecificDataContainerArray';
import { MerchantSizeChartCompositKey } from '../SizeToFit/MerchantSizeChartCompositKey';
import { MerchantSpecificDataContainerArray } from '../SizeToFit/MerchantSpecificDataContainerArray';
import { ToastrService } from 'ngx-toastr';
var Swal = require('sweetalert2');
// for side bar you need to add dependencies dom sanitizer and router and jquery with merchannt model
var ApparelDetailComponent = /** @class */ (function () {
    function ApparelDetailComponent(_sanitizer, toastr, route, service, cookieService, cdref) {
        this._sanitizer = _sanitizer;
        this.toastr = toastr;
        this.route = route;
        this.service = service;
        this.cookieService = cookieService;
        this.cdref = cdref;
        this.now = new Date();
        this.BodyFixed = true;
        this.BodyRange = false;
        this.ReadyRange = false;
        this.ReadyFixed = false;
        this.nonbody = true;
        this.body = true;
        this.show = true;
        this.showAllowances = false;
        this.merchantSpecificData = [];
        this.cm = true;
        this.inch = false;
        this.f = 1;
        this.msg = 'centimeters(CM)';
        this.user = [];
        this.enable = true;
        this.data = false;
        this.arrSize = 0;
        this.inArrSize = [];
        this.testing = [];
        this.measurementPoints = [];
        this.cloned = [];
        this.permission = [[]]; // for size or min value
        this.tolerence = [[]]; // for tolerence
        this.allowances = [[]]; // for allowances
        this.permissible = [[]]; // for max value
        this.minSize = [[]];
        this.permit = [[]];
        this.trial = [];
        this.perTrial = [];
        this.checked = [];
        this.typeArr = [];
        this.sizeChartType = '';
        this.selectedChartType = '';
        this.uid = this.cookieService.get('uid');
        this.name = this.cookieService.get('name');
        this.service.statechange.next(true);
        this.apparelDetail = {
            'brandName': this.cookieService.get('brandName'),
            'gender': this.cookieService.get('gender'),
            'merchatID': this.cookieService.get('email'),
            'apparelName': this.cookieService.get('apparelName'),
        };
    }
    // ------New implementation as on 16/2/2019-------//
    // -----This will check whether priority is 0 or not-------//
    ApparelDetailComponent.prototype.contains = function (arr, element) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] === element) {
                return true;
            }
        }
        return false;
    }; // ----Endeds
    // for hidding three forms accourding toi requirement """""Utkarsh""""""
    ApparelDetailComponent.prototype.toggle = function () {
        if (this.showAllowances) {
            /*alert("on toggle")*/
            this.showAllowances = false;
            this.maxmin = true;
        }
        else if (this.maxmin) {
            /*alert("in else ifs")*/
            this.maxmin = false;
            this.body = false;
        }
        else {
            /*alert("in else of toggle")*/
            this.body = true;
            this.maxmin = true;
        }
    };
    // for  hidding  three form accourding to requirement
    ApparelDetailComponent.prototype.toggle1 = function () {
        if (!this.body) {
            this.body = true;
            this.showAllowances = true;
            /* alert(showAllowancesclicking")*/
        }
        else if (this.maxmin) {
            this.maxmin = false;
            this.showAllowances = true;
            /* alert("in else if of toggle 2")*/
        }
        else {
            this.maxmin = false;
            this.showAllowances = false;
            this.body = false;
            /*alert("in else")*/
        }
    };
    ApparelDetailComponent.prototype.delete = function (size) {
        this.deleteData = {
            'brandName': this.cookieService.get('brandName'),
            'gender': this.cookieService.get('gender'),
            'merchatID': this.cookieService.get('email'),
            'apparelName': this.cookieService.get('apparelName'),
            'country': this.cookieService.get('country'),
            'size': size.type1,
        };
    };
    // -------Checking whether  the size is blank------//
    ApparelDetailComponent.prototype.isCherries = function (fruit) {
        return fruit === '';
    };
    ApparelDetailComponent.prototype.permiTest = function () {
        var _this = this;
        this.arrayForTrueFalse = [];
        this.parentDetail = new Pernt_MerchantSpecificDataContainerArray();
        this.mearchantClass = new MerchantSizeChartCompositKey();
        this.mearchantClass.apparelName = this.cookieService.get('apparelName');
        this.mearchantClass.brandName = this.cookieService.get('brandName');
        this.mearchantClass.gender = this.cookieService.get('gender');
        this.mearchantClass.merchatID = this.email;
        this.mearchantClass.contry = this.cookieService.get('country');
        this.parentDetail.sizeChartType = this.sizeChartType;
        this.parentDetail.compositeKey = this.mearchantClass;
        for (var i = 0; i < this.arrSize; i++) {
            this.merchantSpecificData[i] = new MerchantSpecificDataContainerArray();
        }
        for (var i = 0; i < this.arrSize; i++) {
            for (var j = 0; j < this.inArrSize[i]; j++) {
                /*this.merchantSpecificData[i].priorities = new Array(this.inArrSize[i]).fill('');*/
                this.merchantSpecificData[i].enteredSize = new Array(this.inArrSize[i]).fill('');
                this.merchantSpecificData[i].min = new Array(this.inArrSize[i]).fill('');
                this.merchantSpecificData[i].max = new Array(this.inArrSize[i]).fill(''); /*
                this.merchantSpecificData[i].tolerance = new Array(this.inArrSize[i]).fill('');*/
                this.merchantSpecificData[i].allowances = new Array(this.inArrSize[i]).fill('');
            }
        }
        for (var i = 0; i < this.arrSize; i++) {
            for (var j = 0; j < this.inArrSize[i]; j++) {
                this.merchantSpecificData[i].enteredSize[j] = (this.permission[i][j]);
                this.merchantSpecificData[i].min[j] = (this.minSize[i][j]);
                this.merchantSpecificData[i].max[j] = (this.permissible[i][j]);
                this.merchantSpecificData[i].allowances[j] = (this.allowances[i][j]);
                this.merchantSpecificData[i].selectedSize = this.arr[i].type1; //
                this.parentDetail.measurementPOintsName = this.arr[i].value1;
                this.parentDetail.priorities.push(this.permit[i][j]);
                this.parentDetail.tolerance.push(this.tolerence[i][j]);
            }
        }
        if (this.cm === true) {
            this.parentDetail.unit = 1;
        }
        else {
            (this.parentDetail.unit = 2);
        }
        this.parentDetail.child_merchantKey = this.merchantSpecificData;
        /* const valueCheckForAllowancesOne = [];*/
        var valueCheckForAllowancesZero = [];
        var valueCheckForAllowancesNegative = [];
        var valueCheckForPriorityZero = [];
        var valueCheckForPriorityNegative = [];
        // -----Checking for priority and tolerence-----//
        if (this.parentDetail.priorities.findIndex(this.isCherries) === 0 || this.parentDetail.priorities.findIndex(this.isCherries) === 1 || this.parentDetail.tolerance.findIndex(this.isCherries) === 0 || this.parentDetail.tolerance.findIndex(this.isCherries) === 1) {
            /* this.valueCheckForBlankAllowances = true;*/
            /*              || a.max.findIndex(this.isCherries) === 0 || a.max.findIndex(this.isCherries) === 1*/
            valueCheckForPriorityZero.push(this.parentDetail.priorities.findIndex(this.isCherries));
        }
        else {
            valueCheckForPriorityNegative.push(this.parentDetail.priorities.findIndex(this.isCherries));
        }
        this.service.apparelDetailsSave(this.parentDetail).subscribe(function (res) {
            _this.finalResponse = res;
            _this.messageResponseForJson = _this.finalResponse.message;
            if (_this.finalResponse.code == 1) {
                _this.toastr.success('Size chart save successfully', 'Success', {
                    timeOut: 1100,
                });
                _this.route.navigate(['/apparel-table']);
                _this.service.forBlankApiInPriority(_this.apparelDetail).subscribe(function (response) {
                });
            }
            else if (_this.finalResponse.message === 'size contain zero') {
                if (_this.radioThreeButtonClick === true) {
                    _this.toastr.warning('Minimum cant be zero', 'Minimum Zero', {
                        timeOut: 1200,
                    });
                }
                else {
                    _this.toastr.warning('Size cant be zero', 'Size Zero', {
                        timeOut: 1200,
                    });
                }
            }
            else if (_this.finalResponse.message === 'max value contain zero') {
                _this.toastr.warning('Max value cant be zero', 'Zero', {
                    timeOut: 1000,
                });
            }
            else {
                _this.toastr.warning('Priority could not be same, please fill different priorities', 'Priorities same', {
                    timeOut: 1300,
                });
            }
        });
    };
    ApparelDetailComponent.prototype.cmClick = function () {
        this.inch = false;
        this.cm = true;
        this.msg = '(cm)';
        this.f = 1;
    };
    ApparelDetailComponent.prototype.inchClick = function () {
        this.inch = true;
        this.cm = false;
        this.msg = ' (inch)';
        this.f = 2.54;
    };
    ApparelDetailComponent.prototype.isEmptyObject = function (onCall) {
        return (onCall && (Object.keys(onCall).length === 0));
    };
    ApparelDetailComponent.prototype.hello = function () {
        var data = JSON.parse(localStorage.getItem('apparelData'));
        var user = data;
        this.onCall = user;
        var newObjTest = {};
        if (this.isEmptyObject(this.onCall) === false) {
            this.arr = Object.keys(user).map(function (key) { return ({ type1: key, value1: user[key] }); });
            if (this.arr[0].value1 !== 'FAILURE') {
                this.maxLengthForPriority = this.arr[0].value1.length;
                this.type2 = this.arr[0].type1;
                this.value2 = this.arr[0].value1;
                this.typeArr.push(this.arr[0].type1);
                this.arrSize = this.arr.length;
                for (var i = 0; i < this.arrSize; i++) {
                    this.inArrSize[i] = this.arr[i].value1.length;
                }
                this.permission = new Array(this.arrSize).fill(''); // --size
                this.permit = new Array(this.arrSize).fill(''); // priority
                this.minSize = new Array(this.arrSize).fill('');
                this.permissible = new Array(this.arrSize).fill(''); // max value
                this.tolerence = new Array(this.arrSize).fill(''); // tolerence
                this.allowances = new Array(this.arrSize).fill(''); // tolerence
                for (var j = 0; j < this.arrSize; j++) {
                    this.permission[j] = new Array(this.inArrSize[j]).fill('');
                } // for max size
                for (var k = 0; k < this.arrSize; k++) {
                    this.permit[k] = new Array(this.inArrSize[k]).fill('');
                } // for priority
                for (var m = 0; m < this.arrSize; m++) {
                    this.permissible[m] = new Array(this.inArrSize[m]).fill('');
                } // for min size value
                for (var m = 0; m < this.arrSize; m++) {
                    this.minSize[m] = new Array(this.inArrSize[m]).fill('');
                } // for min size value
                for (var n = 0; n < this.arrSize; n++) {
                    this.tolerence[n] = new Array(this.inArrSize[n]).fill('');
                }
                for (var o = 0; o < this.arrSize; o++) {
                    this.allowances[o] = new Array(this.inArrSize[o]).fill('');
                } // for tolerence calculating on index so that value will not repeat
                this.data = true;
                this.cdref.detectChanges();
            }
        }
    };
    ApparelDetailComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.selectedChartType = localStorage.getItem('chartType');
        this.sizeChartType = this.selectedChartType;
        if (this.sizeChartType == 'bodyFixed') {
            this.BodyFixed = true;
            this.BodyRange = false;
            this.ReadyRange = false;
            this.ReadyFixed = false;
        }
        else if (this.sizeChartType == 'bodyRange') {
            this.BodyFixed = false;
            this.BodyRange = true;
            this.ReadyRange = false;
            this.ReadyFixed = false;
        }
        else if (this.sizeChartType == 'readyRange') {
            this.BodyFixed = false;
            this.BodyRange = false;
            this.ReadyRange = true;
            this.ReadyFixed = false;
        }
        else if (this.sizeChartType == 'readyFixed') {
            this.BodyFixed = false;
            this.BodyRange = false;
            this.ReadyRange = false;
            this.ReadyFixed = true;
        }
        else {
            this.sizeChartType = "bodyFixed";
            this.BodyFixed = true;
        }
        this.hello();
        this.apparelDetail = {
            'brandName': this.cookieService.get('brandName'),
            'gender': this.cookieService.get('gender'),
            'merchatID': this.cookieService.get('email'),
            'apparelName': this.cookieService.get('apparelName'),
            'country': this.cookieService.get('country'),
        };
    };
    ApparelDetailComponent.prototype.ngOnChanges = function () {
    };
    ApparelDetailComponent.prototype.bodyType = function (e) {
        var val = e.target.value;
        this.sizeChartType = val;
        if (val == 'bodyFixed') {
            this.BodyFixed = true;
            this.BodyRange = false;
            this.ReadyRange = false;
            this.ReadyFixed = false;
        }
        else if (val == 'bodyRange') {
            this.BodyRange = true;
            this.BodyFixed = false;
            this.ReadyRange = false;
            this.ReadyFixed = false;
        }
        else if (val == 'readyRange') {
            this.ReadyRange = true;
            this.BodyFixed = false;
            this.BodyRange = false;
            this.ReadyFixed = false;
        }
        else if (val == 'readyFixed') {
            this.ReadyRange = false;
            this.BodyFixed = false;
            this.BodyRange = false;
            this.ReadyFixed = true;
        }
    };
    ApparelDetailComponent = __decorate([
        Component({
            selector: 'app-apparel-detail',
            templateUrl: './apparel-detail.component.html',
            styleUrls: ['./apparel-detail.component.css'],
            changeDetection: ChangeDetectionStrategy.OnPush,
        }),
        __metadata("design:paramtypes", [DomSanitizer, ToastrService, Router, MsServices, CookieService, ChangeDetectorRef])
    ], ApparelDetailComponent);
    return ApparelDetailComponent;
}());
export { ApparelDetailComponent };
//# sourceMappingURL=apparel-detail.component.js.map