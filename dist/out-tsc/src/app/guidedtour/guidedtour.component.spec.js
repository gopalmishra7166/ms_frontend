import { async, TestBed } from '@angular/core/testing';
import { GuidedtourComponent } from './guidedtour.component';
describe('GuidedtourComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [GuidedtourComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(GuidedtourComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=guidedtour.component.spec.js.map