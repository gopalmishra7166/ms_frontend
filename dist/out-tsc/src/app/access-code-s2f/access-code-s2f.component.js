var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { postDataOfMerchantInfo } from './../model/postDataOfMerchantInfo';
import { MerchantAdress } from './../model/merchant-adress';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { MsServices } from './../services/ms-services';
import { CookieService } from 'ngx-cookie-service';
import { DomSanitizer } from '@angular/platform-browser';
var Swal = require('sweetalert2');
var AccessCodeS2fComponent = /** @class */ (function () {
    function AccessCodeS2fComponent(route, ser, cookieService, _sanitizer) {
        this.route = route;
        this.ser = ser;
        this.cookieService = cookieService;
        this._sanitizer = _sanitizer;
        this.now = new Date();
        this.accessCode = undefined;
        this.imageSrc = '/assets/img/logo-preview.jpg';
        this.show = false;
        this.sh = true;
        this.prdDiv = false;
        this.pass = '';
        this.showLoader = false;
        this.expired = false;
        this.updateMerchantInfo = new postDataOfMerchantInfo();
        this.updateMerchantAddress = new MerchantAdress();
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
    }
    AccessCodeS2fComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.name = d['companyName'];
        this.mid = d['mid'];
        this.pass = d['product'].SIZE2FIT.accessCode;
        if (d['product'].SIZE2FIT.planType == 'freeTrial' || d['product'].SIZE2FIT.planType == 'FREETRIAL') {
            console.log(d['product'].SIZE2FIT.remainingDays);
            if (d['product'].SIZE2FIT.remainingDays < 0) {
                this.expired = true;
            }
            else {
                this.expired = false;
            }
        }
        if (this.expired == false) {
            this.getAccessCodeHistory();
        }
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="User Login Details";
    };
    AccessCodeS2fComponent.prototype.getAccessCodeHistory = function () {
        var _this = this;
        var obj = { emailId: this.email, token: this.token, productName: 'SIZE2FIT' };
        this.ser.getAccessCodeHistory(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.getAcceessCodeDetail = response.data;
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    AccessCodeS2fComponent.prototype.updateAccessCode = function () {
        var _this = this;
        var obj = { "token": this.token, "emailId": this.email, "productName": 'SIZE2FIT', };
        Swal({
            title: 'Are you sure, want to update?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '',
            cancelButtonColor: '',
            confirmButtonText: 'Yes',
            allowOutsideClick: false,
        }).then(function (result) {
            if (result.value) {
                _this.showLoader = true;
                _this.ser.updateAccessCode(obj).subscribe(function (res) {
                    _this.showLoader = false;
                    if (res.code == "1") {
                        _this.pass = res.accessCode;
                        _this.ser.getAccessCodeHistory(obj).subscribe(function (response) {
                            if (response.code == "1") {
                                _this.getAcceessCodeDetail = response.data;
                            }
                            else {
                                Swal('error', response.message, 'error');
                            }
                        });
                    }
                    else {
                        Swal('error', res.message, 'error');
                    }
                });
            }
        });
    };
    AccessCodeS2fComponent = __decorate([
        Component({
            selector: 'app-access-code-s2f',
            templateUrl: './access-code-s2f.component.html',
            styleUrls: ['./access-code-s2f.component.css']
        }),
        __metadata("design:paramtypes", [Router, MsServices, CookieService, DomSanitizer])
    ], AccessCodeS2fComponent);
    return AccessCodeS2fComponent;
}());
export { AccessCodeS2fComponent };
//# sourceMappingURL=access-code-s2f.component.js.map