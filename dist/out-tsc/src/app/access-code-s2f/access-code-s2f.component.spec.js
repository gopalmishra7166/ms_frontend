import { async, TestBed } from '@angular/core/testing';
import { AccessCodeS2fComponent } from './access-code-s2f.component';
describe('AccessCodeS2fComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [AccessCodeS2fComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(AccessCodeS2fComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=access-code-s2f.component.spec.js.map