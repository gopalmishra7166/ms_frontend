var URL_api = /** @class */ (function () {
    function URL_api() {
    }
    // public static host = 'http://localhost:8080';
    URL_api.host = 'http://192.168.0.128:8090'; // tenda
    // public static host = 'http://192.168.1.7:8090'; // mirrorsize
    // public static host = 'http://192.168.1.21:8080';
    // public static host = 'https://api.services.mirrorsize.com';
    // public static host = 'http://localhost:8090';
    // ------for admin disabling the users--20/02/2019---//
    URL_api.merchantDisable = URL_api.host + '/api/UserManagementController/delete';
    // ---For total number of clicks and failed click as on18/02/2019-----//
    URL_api.totalClicksForGraph = URL_api.host + '/api/GraphController/getNUmberOfClick';
    // ---For total number of clicks and failed click as on18/02/2019-----//
    URL_api.onlineApiKeyGeneration = URL_api.host + '/api/merchantController/getnerateApiKey';
    // ---For total number of clicks and failed click as on18/02/2019-----//
    URL_api.gettingAllMerchant = URL_api.host + '/api/UserManagementController/getAll';
    // ------after filling clear the whole page value in datatbase-------
    URL_api.blankApiForData = URL_api.host + '/api/brandReadyToMeasureController/getsimplejsonRemove/';
    // -------billing pdf as on 13/feb/2019-------//
    URL_api.billingPdf = URL_api.host + '/api/pdfController/billingPdf';
    // for graph clicks
    URL_api.graphDataForClicks = URL_api.host + '/api/GraphController/getClickGraph';
    // for merchant info
    URL_api.merchantInfo = URL_api.host + '/api/merchant/info/';
    // for graph
    URL_api.graphData = URL_api.host + '/api/GraphController/getgraph';
    // ----only for recommendation on size to fit-------//
    URL_api.size2FitRecommendaion = URL_api.host + '/api/RecommendationController/getRecommendationPagination';
    // for logout the user
    URL_api.logout = URL_api.host + '/api/merchant/logout';
    // -----for all kind of store and online measurement of size 2 fit and get measured-------//
    URL_api.allKindOfData = URL_api.host + '/api/widgetControlle/getdynamictype/?mid=';
    // --------all kind of data ended-----//
    // for updating sizes on table for rendering
    URL_api.updateSizesSize2Fit = URL_api.host + '/api/MMPIController/updateSize';
    // for  table generation apparel detail
    URL_api.apparelTable = URL_api.host + '/api/MerchantSpecificSize2FitController/getSize2fit';
    URL_api.storeSizeToFit = URL_api.host + '/api/widgetControlle/getmyCustomer/';
    // check duration
    URL_api.freeTrial = URL_api.host + '/api/merchantCtrl/checkduration?merchantid=';
    // navigation response api
    URL_api.navigation = URL_api.host + '/api/merchantCtrl/getmerchantType?merchantid=';
    URL_api.apparelDetailSave = URL_api.host + '/api/MerchantSpecificSize2FitController/s2fdata';
    URL_api.getSize = URL_api.host + '/api/merchantCommonController/token/getbrandsize?type=';
    URL_api.sizeUpdate = URL_api.host + '/api/merchantCommonController/jwheiury8o73/save';
    URL_api.apparelCompleteDeatil = URL_api.host + '/api/brandReadyToMeasureController/getdata/';
    URL_api.apparelDetail = URL_api.host + '/api/brandReadyToMeasureController/getsimplejson/';
    URL_api.sizeChart = URL_api.host + '/api/brandReadyToMeasureController/merchantsize/';
    // testing with local server
    URL_api.recommendation = URL_api.host + '/api/merchantController/myproduct';
    URL_api.deleteMerchant = URL_api.host + '/api/data/removeapparel/';
    URL_api.storeUserWithOneZero = URL_api.host + '/api/storeOnlineuserPagenation/';
    URL_api.getMerchantDetails = URL_api.host + '/api/merchant/';
    URL_api.resetPassword = URL_api.host + '/api/reset/merchant/';
    URL_api.getCspDetails = URL_api.host + '/api/csp/csp@mirrorsize.com/merchant';
    // Downloading pdf on click
    URL_api.downloadFile = URL_api.host + '/api/measurementController/getpdf/?';
    // getMeasured API
    URL_api.getMissingInfo = URL_api.host + '/api/merchant/getMissingInfo';
    URL_api.loginUser = URL_api.host + '/api/merchant/login/';
    URL_api.signupUser = URL_api.host + '/api/merchant/signup/';
    URL_api.apiDetails = URL_api.host + '/api/merchant/get-measured/generate/';
    URL_api.updatMerchant = URL_api.host + '/api/merchant/update/';
    URL_api.productMapping = URL_api.host + '/api/data/addapparel/';
    URL_api.contactUs = URL_api.host + '/api/contactus/save/';
    URL_api.getMeasured = URL_api.host + '/api/merchant/getmeasured/mapped/';
    // for data division for size to fit and get measured
    URL_api.divisionSize = URL_api.host + '/mmpicontroller/getbyTypePagination/';
    URL_api.getSizeToFitDetail = URL_api.host + '/api/MMPIController/withsize/';
    URL_api.getMeasuredUpdate = URL_api.host + '/api/merchant/getmeasured/mapped/update/';
    // -----for  getting measuremenet point accourding  to gender-----//
    URL_api.allMeasurementPoints = URL_api.host + '/api/merchantCtrl/pointsNamewithImage';
    // ------ for getting edit tolerence & points --------//
    URL_api.editAllTolerence = URL_api.host + '/api/MerchantSpecificSize2FitController/getJsonForEdit';
    // ------posting edited tolerences  and other value---------//
    URL_api.postUpdateAllTolerence = URL_api.host + '/api/MerchantSpecificSize2FitController/updatesizeChart';
    // -----for  displaying filtered data in getmeasured store--------//
    URL_api.gmMeasurementPointsWithBrand = URL_api.host + '/api/merchantCtrl/gmpointsWithBrandPagination';
    // --------for pdf accourding to specific date-----------//
    URL_api.pdfAccourdingToDate = URL_api.host + '/api/pdfController/makeMyuserPdf';
    // ---for download specific pdf store gm------//
    URL_api.pdfStoreGMDownload = URL_api.host + '/api/pdfController/limitedpointPdf';
    // -------download complete table for specific s2f sizechart measurement points with brand---------//
    URL_api.pdfS2fCompletlyDownload = URL_api.host + '/api/pdfController/size2fitpdf';
    // -------download complete table for specific getmeasured mapped apparel measurement points with brand---------//
    URL_api.pdfGmCompletlyDownload = URL_api.host + '/api/pdfController/get_measured';
    // ------notificationForFreeTrialAndBuyNow-------------
    URL_api.notificationForFreeTrialAndBuyNow = URL_api.host + '/api/notificationController/save';
    // ------------purchase save -----------//
    URL_api.purchaseYear = URL_api.host + '/api/merchantController/savePayment';
    // -------For checking size to fit product map or not---------//
    URL_api.checkForProductMapSize2Fit = URL_api.host + '/api/MMPIController/checkAvailability';
    // --------------For removing s2f sizes 7/2/2019---------//
    URL_api.removeSizeSize2Fit = URL_api.host + '/api/MerchantSpecificSize2FitController/deleteSizes';
    return URL_api;
}());
export { URL_api };
//# sourceMappingURL=url_api.js.map