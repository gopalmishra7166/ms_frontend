import { async, TestBed } from '@angular/core/testing';
import { UpgradeplangmComponent } from './upgradeplangm.component';
describe('UpgradeplangmComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [UpgradeplangmComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(UpgradeplangmComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=upgradeplangm.component.spec.js.map