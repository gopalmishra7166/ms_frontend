var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
import * as S3 from 'aws-sdk/clients/s3';
import { FormGroup, FormControl, Validators } from '@angular/forms';
var Swal = require('sweetalert2');
var MysizelistingComponent = /** @class */ (function () {
    function MysizelistingComponent(service) {
        this.service = service;
        this.showLoader = false;
        this.merchantName = '';
        this.voucher = '';
        this.redirectLink = '';
        this.validityDay = '';
        this.tagLine = '';
        this.ifEdit = false;
        this.isLogo = false;
        this.isBackground = false;
        this.rewardForm = new FormGroup({
            voucher: new FormControl('', Validators.required),
            redirectLink: new FormControl('', Validators.required),
            validityDay: new FormControl('', Validators.required),
            merchantName: new FormControl('', Validators.required),
            tagLine: new FormControl(''),
            description: new FormControl('', Validators.required),
            rewardTitle: new FormControl('', Validators.required),
        });
    }
    MysizelistingComponent.prototype.ngOnInit = function () {
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "List your brand";
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.accountId = d['accountId'];
        this.path = '';
        this.rewardData = {
            voucher: '',
            redirectLink: '',
            validityDay: '',
            merchantName: '',
            tagLine: '',
            description: '',
            rewardTitle: '',
            logo: '',
            backgroundImg: ''
        };
        // this.getrewarddata();
        console.log(this.rewardData);
    };
    MysizelistingComponent.prototype.changeListener = function ($event) {
        this.isLogo = true;
        this.isBackground = false;
        this.readThis($event.target);
    };
    MysizelistingComponent.prototype.changeListener1 = function ($event) {
        this.isBackground = true;
        this.isLogo = false;
        this.readThis1($event.target);
    };
    MysizelistingComponent.prototype.getrewarddata = function () {
        var _this = this;
        this.showLoader = true;
        var obj = {
            "emailId": this.email,
            "token": this.token,
        };
        this.service.getrewarddata(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.showLoader = false;
                _this.rewardData = response.data;
                _this.rewardForm.patchValue({ voucher: _this.rewardData.voucher });
                _this.rewardForm.patchValue({ redirectLink: _this.rewardData.redirectLink });
                _this.rewardForm.patchValue({ validityDay: _this.rewardData.validityDay });
                _this.rewardForm.patchValue({ merchantName: _this.rewardData.merchantName });
                _this.rewardForm.patchValue({ tagLine: _this.rewardData.tagLine });
                _this.rewardForm.patchValue({ description: _this.rewardData.description });
                _this.rewardForm.patchValue({ rewardTitle: _this.rewardData.rewardTitle });
                console.log(_this.rewardData);
            }
            else if (response.code == "2") {
                _this.showLoader = false;
                _this.ifEdit = true;
                _this.rewardForm.patchValue({ voucher: '' });
                _this.rewardForm.patchValue({ redirectLink: '' });
                _this.rewardForm.patchValue({ validityDay: '' });
                _this.rewardForm.patchValue({ merchantName: '' });
                _this.rewardForm.patchValue({ tagLine: '' });
                _this.rewardForm.patchValue({ description: '' });
                _this.rewardForm.patchValue({ rewardTitle: '' });
            }
            else {
                _this.showLoader = false;
                Swal('error', response.message, 'error');
            }
        });
        console.log("Success");
    };
    MysizelistingComponent.prototype.readThis = function (inputValue) {
        var _this = this;
        var file = inputValue.files[0];
        var myReader = new FileReader();
        myReader.onloadend = function (e) {
            _this.logoImage = myReader.result;
        };
        myReader.readAsDataURL(file);
        this.data = file;
        this.image = file.name;
        this.uploadToS3(this.image, this.data);
    };
    MysizelistingComponent.prototype.readThis1 = function (inputValue) {
        var _this = this;
        var file = inputValue.files[0];
        var myReader = new FileReader();
        myReader.onloadend = function (e) {
            _this.banner = myReader.result;
        };
        myReader.readAsDataURL(file);
        this.data1 = file;
        this.image1 = file.name;
        this.uploadToS3(this.image1, this.data1);
    };
    MysizelistingComponent.prototype.saveReward = function () {
        var _this = this;
        this.showLoader = true;
        var obj = {
            "emailId": this.email,
            "token": this.token,
            "voucher": this.rewardForm.value.voucher,
            "redirectLink": this.rewardForm.value.redirectLink,
            "validityDay": this.rewardForm.value.validityDay,
            "merchantName": this.rewardForm.value.merchantName,
            "tagLine": this.rewardForm.value.tagLine,
            "logo": this.logo,
            "description": this.rewardForm.value.description,
            "rewardTitle": this.rewardForm.value.rewardTitle,
            "backgroundImg": this.backgroundImg
        };
        this.service.setreward(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.showLoader = false;
                Swal('Success', response.message, 'success');
                _this.data = '';
                _this.getrewarddata();
                _this.ifEdit = !_this.ifEdit;
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    MysizelistingComponent.prototype.uploadToS3 = function (img, data) {
        this.showLoader = true;
        var bucket = new S3({
            accessKeyId: 'AKIAIFXOEPN7XBYCUXBA',
            secretAccessKey: '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',
            region: 'us-east-1'
        });
        this.timestamp = Date.now();
        var params = {
            Bucket: 'miiror-vijay',
            Key: 'merchantuploads/' + this.accountId + '/logo/' + this.timestamp + '.' + img.split('.'),
            Body: data,
        };
        var self = this;
        bucket.putObject(params).on('httpUploadProgress', function (evt) {
            this.pv = (evt.loaded * 100) / evt.total;
        }).send(function (err, data) {
            if (!err) {
                self.showLoader = false;
                self.path = 'https://miiror-vijay.s3.amazonaws.com/merchantuploads/' + self.accountId + '/logo/' + self.timestamp + '-' + img;
                if (self.isLogo) {
                    self.logo = self.path;
                }
                else {
                    self.backgroundImg = self.path;
                }
            }
            else {
                Swal('error', 'Error occured while uploading', 'error');
            }
        });
    };
    MysizelistingComponent.prototype.addClass = function () {
        document.getElementById('headerShow').style.zIndex = '1';
        document.getElementById('outerMain').style.zIndex = "9999";
        document.getElementById('viewImage').style.background = 'rgba(0, 0, 0, 0.61)';
        document.getElementById('updatePoints').style.background = 'rgba(0, 0, 0, 0.61)';
    };
    MysizelistingComponent.prototype.closeModal = function () {
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
    };
    MysizelistingComponent = __decorate([
        Component({
            selector: 'app-mysizelisting',
            templateUrl: './mysizelisting.component.html',
            styleUrls: ['./mysizelisting.component.css']
        }),
        __metadata("design:paramtypes", [MsServices])
    ], MysizelistingComponent);
    return MysizelistingComponent;
}());
export { MysizelistingComponent };
//# sourceMappingURL=mysizelisting.component.js.map