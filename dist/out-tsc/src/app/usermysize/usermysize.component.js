var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ActivatedRoute } from '@angular/router';
import { PagerService } from '../services/pagerService';
var Swal = require('sweetalert2');
import { ToastrService } from 'ngx-toastr';
var UsermysizeComponent = /** @class */ (function () {
    function UsermysizeComponent(service, toast, rout, pagerService) {
        this.service = service;
        this.toast = toast;
        this.rout = rout;
        this.pagerService = pagerService;
        this.showLoader = false;
        this.expired = false;
        this.usersData = [];
        this.searchKey = "email_id";
        this.searchText = "";
        this.userCount = 0;
        this.pager = {};
        this.productName = '';
    }
    UsermysizeComponent.prototype.ngOnInit = function () {
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Users";
        var param = this.rout.snapshot.paramMap.get("id");
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.productName = 'MYSIZE';
        if (d['product'].MYSIZE.planType == 'FREETRIAL') {
            console.log(d['product'].MYSIZE.remainingDays);
            if (d['product'].MYSIZE.remainingDays < 0) {
                this.expired = true;
            }
            else {
                this.expired = false;
            }
        }
        if (this.expired == false) {
            this.getUserListMysize(1);
        }
    };
    UsermysizeComponent.prototype.getUserListMysize = function (page) {
        var _this = this;
        this.showLoader = true;
        var obj = { "emailId": this.email, "token": this.token, "productName": this.productName, "pageNo": page.toString() };
        this.service.getuserlogs(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                if (response.data) {
                    _this.userCount = response.record_count;
                    _this.usersData = response.data;
                    _this.pager = _this.pagerService.getPager(_this.userCount, page);
                }
            }
            else {
                _this.toast.warning(response.message, 'Message', { timeOut: 2000, });
            }
        });
    };
    UsermysizeComponent.prototype.searchUser = function () {
        var _this = this;
        this.showLoader = true;
        this.usersData = [];
        var obj = { "token": this.token, "emailId": this.email, "productName": 'MYSIZE', "key": this.searchKey, "value": this.searchText };
        this.service.searchuserlogs(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.usersData = response.data;
                _this.userCount = response.record_count;
            }
            else {
                Swal("Error", response.message, 'erro');
            }
        });
    };
    UsermysizeComponent.prototype.clear = function () {
        this.getUserListMysize(1);
    };
    UsermysizeComponent = __decorate([
        Component({
            selector: 'app-usermysize',
            templateUrl: './usermysize.component.html',
            styleUrls: ['./usermysize.component.css']
        }),
        __metadata("design:paramtypes", [MsServices, ToastrService, ActivatedRoute, PagerService])
    ], UsermysizeComponent);
    return UsermysizeComponent;
}());
export { UsermysizeComponent };
//# sourceMappingURL=usermysize.component.js.map