var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { MsServices } from '../services/ms-services';
var Swal = require('sweetalert2');
var SupportComponent = /** @class */ (function () {
    function SupportComponent(service) {
        this.service = service;
        this.subject = '';
        this.content = '';
        this.email = '';
        this.token = '';
        this.ticketsData = [];
        this.status = 'PENDING';
        this.showLoader = false;
    }
    SupportComponent.prototype.ngOnInit = function () {
        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.getTickets();
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Support";
    };
    SupportComponent.prototype.createTicket = function () {
        var _this = this;
        this.showLoader = true;
        var obj = { emailId: this.email, token: this.token, content: this.content, subject: this.subject };
        this.service.raiseTicket(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                Swal('success', response.message, 'success');
                _this.getTickets();
                $('#createTicket').modal('hide');
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    SupportComponent.prototype.getTickets = function () {
        var _this = this;
        this.showLoader = true;
        var obj = { emailId: this.email, token: this.token, pageNo: 1 };
        this.service.getTickets(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                _this.ticketsData = response.data;
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    SupportComponent.prototype.getStatus = function (e, tid) {
        this.status = e.target.value;
        this.tid = tid;
        this.updateTickets();
    };
    SupportComponent.prototype.updateTickets = function () {
        var _this = this;
        this.showLoader = true;
        var obj = { ticketId: this.tid, token: this.token, emailId: this.email, status: this.status };
        this.service.updateTicketStatus(obj).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == "1") {
                Swal('success', response.message, 'success');
                _this.getTickets();
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    SupportComponent.prototype.addClass = function () {
        document.getElementById('headerShow').style.zIndex = '1';
        document.getElementById('outerMain').style.zIndex = "9999";
        document.getElementById('createTicket').style.background = 'rgba(0, 0, 0, 0.61)';
    };
    SupportComponent.prototype.closeModal = function () {
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
    };
    SupportComponent = __decorate([
        Component({
            selector: 'app-support',
            templateUrl: './support.component.html',
            styleUrls: ['./support.component.css']
        }),
        __metadata("design:paramtypes", [MsServices])
    ], SupportComponent);
    return SupportComponent;
}());
export { SupportComponent };
//# sourceMappingURL=support.component.js.map