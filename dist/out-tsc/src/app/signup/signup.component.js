var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { MsServices } from './../services/ms-services';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
var Swal = require('sweetalert2');
var SignupComponent = /** @class */ (function () {
    function SignupComponent(router, service, toast, spinner) {
        this.router = router;
        this.service = service;
        this.toast = toast;
        this.spinner = spinner;
        this.signUp = true;
        this.companyProfile = false;
        this.industries = [];
        this.countriesList = [];
        this.states = [];
        this.contactPerson = false;
        this.companyAgreement = false;
        this.forward1 = true;
        this.forward2 = false;
        this.forward3 = false;
        this.forward4 = false;
        this.forward5 = false;
        this.freeTrial = false;
        this.data = {};
        this.image = "";
        this.oneYear = true;
        this.twoYear = false;
        this.threeYear = false;
        this.image1 = "";
        this.registerData = {};
        this.checkEmail = true;
        this.title = 'Mr';
        this.countryCode = [];
        this.otp = "";
        this.verifyOTP = false;
        this.isVerified = 'false';
        this.planType = '';
        this.showLoader = false;
        this.plan = '';
        this.paidAmount = '';
        this.paid = false;
        this.ftrial = false;
        this.planChecked = false;
        this.MYSIZE = false;
        this.QUICKSIZE = false;
        this.getPayment = false;
        this.showPaymentIframe = false;
        this.signUpForm = new FormGroup({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', Validators.required),
            confirmPassword: new FormControl('', Validators.required),
            terms: new FormControl('', Validators.required)
        });
        this.companyProfileForm = new FormGroup({
            name: new FormControl('', Validators.required),
            industryType: new FormControl('Please Select'),
            country: new FormControl('Please Select', Validators.required),
            state: new FormControl('Please Select', Validators.required),
            city: new FormControl('', Validators.required),
            pinCode: new FormControl('', Validators.required),
            companyAddress: new FormControl('', Validators.required),
            companyLogo: new FormControl(this.image),
            landlineNo: new FormControl()
        });
        this.contactPersonForm = new FormGroup({
            cpName: new FormControl('', Validators.required),
            cpDesignation: new FormControl('', Validators.required),
            cpEmailId: new FormControl('', Validators.required),
            cpMobileNo: new FormControl('', [Validators.required]),
            cpProfilePic: new FormControl(this.image1),
        });
        this.industries =
            [
                { 'Description': 'Accounting' },
                { 'Description': 'Airlines/Aviation' },
                { 'Description': 'Alternative Dispute Resolution' },
                { 'Code': 125, 'Groups': 'hlth', 'Description': 'Alternative Medicine' },
                { 'Code': 127, 'Groups': 'art, med', 'Description': 'Animation' },
                { 'Code': 19, 'Groups': 'good', 'Description': 'Apparel & Fashion' },
                { 'Code': 50, 'Groups': 'cons', 'Description': 'Architecture & Planning' },
                { 'Code': 111, 'Groups': 'art, med, rec', 'Description': 'Arts and Crafts' },
                { 'Code': 53, 'Groups': 'man', 'Description': 'Automotive' },
                { 'Code': 52, 'Groups': 'gov, man', 'Description': 'Aviation & Aerospace' },
                { 'Code': 41, 'Groups': 'fin', 'Description': 'Banking' },
                { 'Code': 12, 'Groups': 'gov, hlth, tech', 'Description': 'Biotechnology' },
                { 'Code': 36, 'Groups': 'med, rec', 'Description': 'Broadcast Media' },
                { 'Code': 49, 'Groups': 'cons', 'Description': 'Building Materials' },
                { 'Code': 138, 'Groups': 'corp, man', 'Description': 'Business Supplies and Equipment' },
                { 'Code': 129, 'Groups': 'fin', 'Description': 'Capital Markets' },
                { 'Code': 54, 'Groups': 'man', 'Description': 'Chemicals' },
                { 'Code': 90, 'Groups': 'org, serv', 'Description': 'Civic & Social Organization' },
                { 'Code': 51, 'Groups': 'cons, gov', 'Description': 'Civil Engineering' },
                { 'Code': 128, 'Groups': 'cons, corp, fin', 'Description': 'Commercial Real Estate' },
                { 'Code': 118, 'Groups': 'tech', 'Description': 'Computer & Network Security' },
                { 'Code': 109, 'Groups': 'med, rec', 'Description': 'Computer Games' },
                { 'Code': 3, 'Groups': 'tech', 'Description': 'Computer Hardware' },
                { 'Code': 5, 'Groups': 'tech', 'Description': 'Computer Networking' },
                { 'Code': 4, 'Groups': 'tech', 'Description': 'Computer Software' },
                { 'Code': 48, 'Groups': 'cons', 'Description': 'Construction' },
                { 'Code': 24, 'Groups': 'good, man', 'Description': 'Consumer Electronics' },
                { 'Code': 25, 'Groups': 'good, man', 'Description': 'Consumer Goods' },
                { 'Code': 91, 'Groups': 'org, serv', 'Description': 'Consumer Services' },
                { 'Code': 18, 'Groups': 'good', 'Description': 'Cosmetics' },
                { 'Code': 65, 'Groups': 'agr', 'Description': 'Dairy' },
                { 'Code': 1, 'Groups': 'gov, tech', 'Description': 'Defense & Space' },
                { 'Code': 99, 'Groups': 'art, med', 'Description': 'Design' },
                { 'Code': '', 'Groups': 'ecom', 'Description': 'Ecommerce' },
                { 'Code': 69, 'Groups': 'edu', 'Description': 'Education Management' },
                { 'Code': 132, 'Groups': 'edu, org', 'Description': 'E-Learning' },
                { 'Code': 112, 'Groups': 'good, man', 'Description': 'Electrical/Electronic Manufacturing' },
                { 'Code': 28, 'Groups': 'med, rec', 'Description': 'Entertainment' },
                { 'Code': 86, 'Groups': 'org, serv', 'Description': 'Environmental Services' },
                { 'Code': 110, 'Groups': 'corp, rec, serv', 'Description': 'Events Services' },
                { 'Code': 76, 'Groups': 'gov', 'Description': 'Executive Office' },
                { 'Code': 122, 'Groups': 'corp, serv', 'Description': 'Facilities Services' },
                { 'Code': 63, 'Groups': 'agr', 'Description': 'Farming' },
                { 'Code': 43, 'Groups': 'fin', 'Description': 'Financial Services' },
                { 'Code': 38, 'Groups': 'art, med, rec', 'Description': 'Fine Art' },
                { 'Code': 66, 'Groups': 'agr', 'Description': 'Fishery' },
                { 'Code': 34, 'Groups': 'rec, serv', 'Description': 'Food & Beverages' },
                { 'Code': 23, 'Groups': 'good, man, serv', 'Description': 'Food Production' },
                { 'Code': 101, 'Groups': 'org', 'Description': 'Fund-Raising' },
                { 'Code': 26, 'Groups': 'good, man', 'Description': 'Furniture' },
                { 'Code': 29, 'Groups': 'rec', 'Description': 'Gambling & Casinos' },
                { 'Code': 145, 'Groups': 'cons, man', 'Description': 'Glass, Ceramics & Concrete' },
                { 'Code': 75, 'Groups': 'gov', 'Description': 'Government Administration' },
                { 'Code': 148, 'Groups': 'gov', 'Description': 'Government Relations' },
                { 'Code': 140, 'Groups': 'art, med', 'Description': 'Graphic Design' },
                { 'Code': 124, 'Groups': 'hlth, rec', 'Description': 'Health, Wellness and Fitness' },
                { 'Code': 68, 'Groups': 'edu', 'Description': 'Higher Education' },
                { 'Code': 14, 'Groups': 'hlth', 'Description': 'Hospital & Health Care' },
                { 'Code': 31, 'Groups': 'rec, serv, tran', 'Description': 'Hospitality' },
                { 'Code': 137, 'Groups': 'corp', 'Description': 'Human Resources' },
                { 'Code': 134, 'Groups': 'corp, good, tran', 'Description': 'Import and Export' },
                { 'Code': 88, 'Groups': 'org, serv', 'Description': 'Individual & Family Services' },
                { 'Code': 147, 'Groups': 'cons, man', 'Description': 'Industrial Automation' },
                { 'Code': 84, 'Groups': 'med, serv', 'Description': 'Information Services' },
                { 'Code': 96, 'Groups': 'tech', 'Description': 'Information Technology and Services' },
                { 'Code': 42, 'Groups': 'fin', 'Description': 'Insurance' },
                { 'Code': 74, 'Groups': 'gov', 'Description': 'International Affairs' },
                { 'Code': 141, 'Groups': 'gov, org, tran', 'Description': 'International Trade and Development' },
                { 'Code': 6, 'Groups': 'tech', 'Description': 'Internet' },
                { 'Code': 45, 'Groups': 'fin', 'Description': 'Investment Banking' },
                { 'Code': 46, 'Groups': 'fin', 'Description': 'Investment Management' },
                { 'Code': 73, 'Groups': 'gov, leg', 'Description': 'Judiciary' },
                { 'Code': 77, 'Groups': 'gov, leg', 'Description': 'Law Enforcement' },
                { 'Code': 9, 'Groups': 'leg', 'Description': 'Law Practice' },
                { 'Code': 10, 'Groups': 'leg', 'Description': 'Legal Services' },
                { 'Code': 72, 'Groups': 'gov, leg', 'Description': 'Legislative Office' },
                { 'Code': 30, 'Groups': 'rec, serv, tran', 'Description': 'Leisure, Travel & Tourism' },
                { 'Code': 85, 'Groups': 'med, rec, serv', 'Description': 'Libraries' },
                { 'Code': 116, 'Groups': 'corp, tran', 'Description': 'Logistics and Supply Chain' },
                { 'Code': 143, 'Groups': 'good', 'Description': 'Luxury Goods & Jewelry' },
                { 'Code': 55, 'Groups': 'man', 'Description': 'Machinery' },
                { 'Code': 11, 'Groups': 'corp', 'Description': 'Management Consulting' },
                { 'Code': 95, 'Groups': 'tran', 'Description': 'Maritime' },
                { 'Code': 97, 'Groups': 'corp', 'Description': 'Market Research' },
                { 'Code': 80, 'Groups': 'corp, med', 'Description': 'Marketing and Advertising' },
                { 'Code': 135, 'Groups': 'cons, gov, man', 'Description': 'Mechanical or Industrial Engineering' },
                { 'Code': 126, 'Groups': 'med, rec', 'Description': 'Media Production' },
                { 'Code': 17, 'Groups': 'hlth', 'Description': 'Medical Devices' },
                { 'Code': 13, 'Groups': 'hlth', 'Description': 'Medical Practice' },
                { 'Code': 139, 'Groups': 'hlth', 'Description': 'Mental Health Care' },
                { 'Code': 71, 'Groups': 'gov', 'Description': 'Military' },
                { 'Code': 56, 'Groups': 'man', 'Description': 'Mining & Metals' },
                { 'Code': 35, 'Groups': 'art, med, rec', 'Description': 'Motion Pictures and Film' },
                { 'Code': 37, 'Groups': 'art, med, rec', 'Description': 'Museums and Institutions' },
                { 'Code': 115, 'Groups': 'art, rec', 'Description': 'Music' },
                { 'Code': 114, 'Groups': 'gov, man, tech', 'Description': 'Nanotechnology' },
                { 'Code': 81, 'Groups': 'med, rec', 'Description': 'Newspapers' },
                { 'Code': 100, 'Groups': 'org', 'Description': 'Non-Profit Organization Management' },
                { 'Code': 57, 'Groups': 'man', 'Description': 'Oil & Energy' },
                { 'Code': 113, 'Groups': 'med', 'Description': 'Online Media' },
                { 'Code': 123, 'Groups': 'corp', 'Description': 'Outsourcing/Offshoring' },
                { 'Code': 87, 'Groups': 'serv, tran', 'Description': 'Package/Freight Delivery' },
                { 'Code': 146, 'Groups': 'good, man', 'Description': 'Packaging and Containers' },
                { 'Code': 61, 'Groups': 'man', 'Description': 'Paper & Forest Products' },
                { 'Code': 39, 'Groups': 'art, med, rec', 'Description': 'Performing Arts' },
                { 'Code': 15, 'Groups': 'hlth, tech', 'Description': 'Pharmaceuticals' },
                { 'Code': 131, 'Groups': 'org', 'Description': 'Philanthropy' },
                { 'Code': 136, 'Groups': 'art, med, rec', 'Description': 'Photography' },
                { 'Code': 117, 'Groups': 'man', 'Description': 'Plastics' },
                { 'Code': 107, 'Groups': 'gov, org', 'Description': 'Political Organization' },
                { 'Code': 67, 'Groups': 'edu', 'Description': 'Primary/Secondary Education' },
                { 'Code': 83, 'Groups': 'med, rec', 'Description': 'Printing' },
                { 'Code': 105, 'Groups': 'corp', 'Description': 'Professional Training & Coaching' },
                { 'Code': 102, 'Groups': 'corp, org', 'Description': 'Program Development' },
                { 'Code': 79, 'Groups': 'gov', 'Description': 'Public Policy' },
                { 'Code': 98, 'Groups': 'corp', 'Description': 'Public Relations and Communications' },
                { 'Code': 78, 'Groups': 'gov', 'Description': 'Public Safety' },
                { 'Code': 82, 'Groups': 'med, rec', 'Description': 'Publishing' },
                { 'Code': 62, 'Groups': 'man', 'Description': 'Railroad Manufacture' },
                { 'Code': 64, 'Groups': 'agr', 'Description': 'Ranching' },
                { 'Code': 44, 'Groups': 'cons, fin, good', 'Description': 'Real Estate' },
                { 'Code': 40, 'Groups': 'rec, serv', 'Description': 'Recreational Facilities and Services' },
                { 'Code': 89, 'Groups': 'org, serv', 'Description': 'Religious Institutions' },
                { 'Code': 144, 'Groups': 'gov, man, org', 'Description': 'Renewables & Environment' },
                { 'Code': 70, 'Groups': 'edu, gov', 'Description': 'Research' },
                { 'Code': 32, 'Groups': 'rec, serv', 'Description': 'Restaurants' },
                { 'Code': 27, 'Groups': 'good, man', 'Description': 'Retail' },
                { 'Code': 121, 'Groups': 'corp, org, serv', 'Description': 'Security and Investigations' },
                { 'Code': 7, 'Groups': 'tech', 'Description': 'Semiconductors' },
                { 'Code': 58, 'Groups': 'man', 'Description': 'Shipbuilding' },
                { 'Code': 20, 'Groups': 'good, rec', 'Description': 'Sporting Goods' },
                { 'Code': 33, 'Groups': 'rec', 'Description': 'Sports' },
                { 'Code': 104, 'Groups': 'corp', 'Description': 'Staffing and Recruiting' },
                { 'Code': 22, 'Groups': 'good', 'Description': 'Supermarkets' },
                { 'Code': 8, 'Groups': 'gov, tech', 'Description': 'Telecommunications' },
                { 'Code': 60, 'Groups': 'man', 'Description': 'Textiles' },
                { 'Code': 130, 'Groups': 'gov, org', 'Description': 'Think Tanks' },
                { 'Code': 21, 'Groups': 'good', 'Description': 'Tobacco' },
                { 'Code': 108, 'Groups': 'corp, gov, serv', 'Description': 'Translation and Localization' },
                { 'Code': 92, 'Groups': 'tran', 'Description': 'Transportation/Trucking/Railroad' },
                { 'Code': 59, 'Groups': 'man', 'Description': 'Utilities' },
                { 'Code': 106, 'Groups': 'fin, tech', 'Description': 'Venture Capital & Private Equity' },
                { 'Code': 16, 'Groups': 'hlth', 'Description': 'Veterinary' },
                { 'Code': 93, 'Groups': 'tran', 'Description': 'Warehousing' },
                { 'Code': 133, 'Groups': 'good', 'Description': 'Wholesale' },
                { 'Code': 142, 'Groups': 'good, man, rec', 'Description': 'Wine and Spirits' },
                { 'Code': 119, 'Groups': 'tech', 'Description': 'Wireless' },
                { 'Code': 103, 'Groups': 'art, med, rec', 'Description': 'Writing and Editing' }
            ];
    }
    SignupComponent.prototype.getUrl = function () {
        return 'url(\'assets/img/background5.jpg\')';
    };
    SignupComponent.prototype.ngOnInit = function () {
        if (this.service.isLoggedIn()) {
            this.router.navigate(['/graph']);
        }
        if (localStorage.getItem('regStatus') == 'complete') {
            this.router.navigate(['/login']);
        }
        this.countryCode = this.service.phoneCode;
        this.planType = 'PAID';
        this.productName = sessionStorage.getItem('productName');
        if (this.productName === 'GET_MEASURED') {
            this.GET_MEASURED = true;
        }
        else if (this.productName === 'SIZE2FIT') {
            this.SIZE2FIT = true;
        }
        else if (this.productName === 'MYSIZE') {
            this.MYSIZE = true;
            this.planChecked = true;
        }
        else if (this.productName === 'QUICKSIZE') {
            this.QUICKSIZE = true;
            this.planChecked = true;
        }
        else {
            this.router.navigate(['/home']);
        }
        if (this.planType === 'PAID') {
            this.paid = true;
        }
        else {
            this.ammount = 1;
            this.plan = 'FREETRIAL';
            this.ftrial = true;
        }
        if (!this.MYSIZE) {
            this.ammount = 1;
            this.year = 1;
            this.monthly = false;
            this.yearly = true;
        }
        this.getcommondata();
        this.companyProfileForm.patchValue({ industryType: this.industries[0].Description });
        if (localStorage.getItem('verified') == 'true') {
            this.signUp = false;
            this.forward2 = true;
            this.verifyOTP = false;
            this.companyProfile = true;
        }
        else if (localStorage.getItem('verified') == 'false') {
            this.signUp = false;
            this.verifyOTP = true;
            this.companyProfile = false;
        }
        else {
            this.verifyOTP = false;
            this.signUp = true;
        }
        this.companyAgreementForm = new FormGroup({
            freetrial: new FormControl(''),
            billedM: new FormControl(''),
            billedY: new FormControl('1'),
            one: new FormControl('1'),
            two: new FormControl(''),
            three: new FormControl(''),
            termsCondition: new FormControl('')
        });
        console.log("one year", this.oneYear);
        console.log("paid", this.paid);
        console.log("QUICKSIZE", this.paid);
    };
    SignupComponent.prototype.getcommondata = function () {
        var _this = this;
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        this.regDate = dd + '/' + mm + '/' + yyyy;
        this.service.getAllCountries().subscribe(function (response) {
            if (response) {
                _this.countriesList = response;
            }
        });
    };
    SignupComponent.prototype.verifyOtp = function () {
        var _this = this;
        if (localStorage.getItem('userId')) {
            var obj = { "emailId": localStorage.getItem('userId'), "otp": this.otp };
        }
        else {
            var obj = { "emailId": localStorage.getItem('userID'), "otp": this.otp };
        }
        this.service.verifyEmail(obj).subscribe(function (response) {
            if (response.code == "1") {
                _this.isVerified = 'true';
                _this.companyProfile = true;
                _this.signUp = false;
                _this.verifyOTP = false;
                _this.forward1 = true;
                _this.forward2 = true;
                localStorage.setItem('verified', _this.isVerified);
                localStorage.removeItem('userID');
            }
            else {
                _this.isVerified = 'false';
                Swal('Oops!', response.message, 'error');
            }
        });
    };
    SignupComponent.prototype.signupUser = function () {
        var _this = this;
        console.log(this.productName);
        if (this.signUpForm.value.password != this.signUpForm.value.confirmPassword) {
            Swal('Oops!', 'Password and Confirm Password did not matched', 'info');
            console.log(this.productName);
        }
        else {
            this.showLoader = true;
            console.log(this.productName);
            if (this.productName == 'GET_MEASURED' || this.productName == 'SIZE2FIT' || this.productName == 'MYSIZE' || this.productName == 'QUICKSIZE') {
                var obj = { "emailId": this.signUpForm.value.email, "password": this.signUpForm.value.password, "productName": this.productName };
                this.service.signupUser(obj).subscribe(function (response) {
                    _this.showLoader = false;
                    if (response.code == "1") {
                        _this.signUp = false;
                        _this.verifyOTP = true;
                        localStorage.setItem('userId', _this.signUpForm.value.email);
                        localStorage.setItem('accountId', response.data.accountId);
                    }
                    else {
                        _this.spinner.hide();
                        Swal('Oops!', response.message, 'error');
                    }
                });
            }
            else {
                this.toast.info('Please select the product.', 'Get Started!', {
                    timeOut: 12000,
                });
                this.router.navigate(['/home']);
            }
        }
    };
    SignupComponent.prototype.next = function (val) {
        if (val == "3") {
            this.contactPerson = true;
            this.forward1 = true;
            this.forward2 = true;
            this.forward3 = true;
            this.signUp = false;
            this.companyProfile = false;
            this.verifyOTP = false;
        }
        else if (val == "4") {
            this.forward1 = true;
            this.forward2 = true;
            this.forward3 = true;
            this.forward4 = true;
            this.companyAgreement = true;
            this.oneYear = true;
            this.paid = true;
            this.ftrial = false;
            this.yearly = true;
            this.companyProfile = false;
            this.contactPerson = false;
            this.signUp = false;
            this.verifyOTP = false;
        }
        else if (val === "5") {
            this.forward5 = true;
            this.companyAgreement = false;
        }
    };
    SignupComponent.prototype.back = function (val) {
        if (val == '4') {
            this.companyProfile = false;
            this.signUp = false;
            this.forward4 = false;
            this.companyAgreement = false;
            this.contactPerson = true;
        }
        else if (val == '3') {
            this.signUp = false;
            this.contactPerson = false;
            this.companyAgreement = false;
            this.companyProfile = true;
            this.forward3 = false;
        }
    };
    SignupComponent.prototype.getStates = function (val) {
        this.states = [];
        this.states = this.service.AllStates.filter(function (item) { return item.short == val.target.value; })[0].states;
    };
    SignupComponent.prototype.changeBillingPeriod = function (val, e) {
        if (e.target.checked) {
            if (this.QUICKSIZE) {
                this.monthly = true;
                this.yearly = false;
                if (this.oneYear) {
                    if (this.plan == 'small') {
                        this.ammount = 1000;
                    }
                    else if (this.plan == 'medium') {
                        this.ammount = 2000;
                    }
                }
                else if (this.twoYear) {
                    if (this.plan == 'small') {
                        this.ammount = 800;
                    }
                    else if (this.plan == 'medium') {
                        this.ammount = 1625;
                    }
                }
                else if (this.threeYear) {
                    if (this.plan == 'small') {
                        this.ammount = 600;
                    }
                    else if (this.plan == 'medium') {
                        this.ammount = 1250;
                    }
                }
            }
            else {
                if (val == 'm') {
                    this.companyAgreementForm.patchValue({ billedY: false });
                    this.monthly = true;
                    this.yearly = false;
                    if (this.oneYear) {
                        if (this.plan == 'small') {
                            this.ammount = 119;
                        }
                        else if (this.plan == 'medium') {
                            this.ammount = 536;
                        }
                    }
                    else if (this.twoYear) {
                        if (this.plan == 'small') {
                            this.ammount = 208;
                        }
                        else if (this.plan == 'medium') {
                            this.ammount = 975;
                        }
                    }
                    else if (this.threeYear) {
                        if (this.plan == 'small') {
                            this.ammount = 785;
                        }
                        else if (this.plan == 'medium') {
                            this.ammount = 2800;
                        }
                    }
                }
                else if (val == 'y') {
                    this.companyAgreementForm.patchValue({ billedM: false });
                    this.monthly = false;
                    this.yearly = true;
                    if (this.oneYear) {
                        if (this.plan == 'small') {
                            this.ammount = 1200;
                        }
                        else if (this.plan == 'medium') {
                            this.ammount = 5400;
                        }
                    }
                    else if (this.twoYear) {
                        if (this.plan == 'small') {
                            this.ammount = 1920;
                        }
                        else if (this.plan == 'medium') {
                            this.ammount = 9000;
                        }
                    }
                    else if (this.threeYear) {
                        if (this.plan == 'small') {
                            this.ammount = 6500;
                        }
                        else if (this.plan == 'medium') {
                            this.ammount = 24000;
                        }
                    }
                }
            }
        }
        else {
            console.log("unchecked");
        }
    };
    SignupComponent.prototype.freetrial = function (e) {
        if (e.target.checked) {
            this.planChecked = false;
            this.planType = 'FREETRIAL';
            this.year = 1;
            this.ftrial = true;
            this.paid = false;
            this.oneYear = false;
            this.twoYear = false;
            this.threeYear = false;
            this.companyAgreementForm.patchValue({ one: false });
            this.companyAgreementForm.patchValue({ two: false });
            this.companyAgreementForm.patchValue({ three: false });
        }
        else {
            console.log("unchecked");
        }
    };
    SignupComponent.prototype.buyForOneYear = function (e) {
        if (e.target.checked) {
            if (!this.monthly) {
                this.yearly = true;
            }
            this.companyAgreementForm.value.billedY = "1";
            this.planChecked = false;
            this.planType = 'PAID';
            this.paid = true;
            this.year = 1;
            this.oneYear = true;
            this.twoYear = false;
            this.threeYear = false;
            this.ftrial = false;
            this.companyAgreementForm.patchValue({ two: false });
            this.companyAgreementForm.patchValue({ three: false });
            this.companyAgreementForm.patchValue({ freetrial: false });
        }
        else {
            console.log("checked");
        }
    };
    SignupComponent.prototype.buyForTwoYear = function (e) {
        if (e.target.checked) {
            if (!this.monthly) {
                this.yearly = true;
            }
            this.companyAgreementForm.value.billedY = "1";
            this.planChecked = false;
            this.year = 2;
            this.planType = 'PAID';
            this.paid = true;
            this.twoYear = true;
            this.oneYear = false;
            this.threeYear = false;
            this.ftrial = false;
            // this.companyAgreementForm.patchValue({ ftrial: false })
            this.companyAgreementForm.patchValue({ one: false });
            this.companyAgreementForm.patchValue({ three: false });
            this.companyAgreementForm.patchValue({ freetrial: false });
        }
        else {
            console.log('unchecked');
        }
    };
    SignupComponent.prototype.buyForThreeYear = function (e) {
        if (e.target.checked) {
            if (!this.monthly) {
                this.yearly = true;
            }
            this.companyAgreementForm.value.billedY = "1";
            this.planChecked = false;
            this.year = 3;
            this.planType = 'PAID';
            this.paid = true;
            this.oneYear = false;
            this.twoYear = false;
            this.threeYear = true;
            this.ftrial = false;
            // this.companyAgreementForm.patchValue({ ftrial: false })
            this.companyAgreementForm.patchValue({ one: false });
            this.companyAgreementForm.patchValue({ two: false });
            this.companyAgreementForm.patchValue({ freetrial: false });
        }
        else {
            console.log("unchecked");
        }
    };
    SignupComponent.prototype.buyProduct = function (val, am) {
        this.planChecked = true;
        if (this.GET_MEASURED || this.SIZE2FIT) {
            this.plan = val;
            if (this.monthly) {
                if (this.oneYear) {
                    if (val == 'small') {
                        this.ammount = 119;
                    }
                    else if (val == 'medium') {
                        this.ammount = 536;
                    }
                }
                else if (this.twoYear) {
                    if (val == 'small') {
                        this.ammount = 208;
                    }
                    else if (val == 'medium') {
                        this.ammount = 975;
                    }
                }
                else if (this.threeYear) {
                    if (val == 'small') {
                        this.ammount = 785;
                    }
                    else if (val == 'medium') {
                        this.ammount = 2800;
                    }
                }
            }
            else if (this.yearly) {
                if (this.oneYear) {
                    if (this.plan == 'small') {
                        this.ammount = 1200;
                    }
                    else if (this.plan == 'medium') {
                        this.ammount = 5400;
                    }
                }
                else if (this.twoYear) {
                    if (this.plan == 'small') {
                        this.ammount = 1920;
                    }
                    else if (this.plan == 'medium') {
                        this.ammount = 9000;
                    }
                }
                else if (this.threeYear) {
                    if (this.plan == 'small') {
                        this.ammount = 6500;
                    }
                    else if (this.plan == 'medium') {
                        this.ammount = 24000;
                    }
                }
            }
        }
        else if (this.QUICKSIZE) {
            this.plan = val;
            if (this.oneYear) {
                if (this.plan == 'small') {
                    this.ammount = 1000;
                }
                else if (this.plan == 'medium') {
                    this.ammount = 2000;
                }
            }
            else if (this.twoYear) {
                if (this.plan == 'small') {
                    this.ammount = 800;
                }
                else if (this.plan == 'medium') {
                    this.ammount = 1625;
                }
            }
            else if (this.threeYear) {
                if (this.plan == 'small') {
                    this.ammount = 600;
                }
                else if (this.plan == 'medium') {
                    this.ammount = 1250;
                }
            }
        }
    };
    SignupComponent.prototype.changeListener = function ($event) {
        this.readThis($event.target);
    };
    SignupComponent.prototype.readThis = function (inputValue) {
        var _this = this;
        var file = inputValue.files[0];
        var myReader = new FileReader();
        myReader.onloadend = function (e) {
            _this.image = myReader.result;
        };
        myReader.readAsDataURL(file);
    };
    SignupComponent.prototype.changeListener1 = function ($event) {
        this.readThis1($event.target);
    };
    SignupComponent.prototype.readThis1 = function (inputValue) {
        var _this = this;
        var file = inputValue.files[0];
        var myReader = new FileReader();
        myReader.onloadend = function (e) {
            _this.image1 = myReader.result;
        };
        myReader.readAsDataURL(file);
    };
    SignupComponent.prototype.makePayment = function () {
        var _this = this;
        this.showLoader = true;
        if (this.monthly) {
            this.billPeriod = 'monthly';
        }
        else if (this.yearly) {
            this.billPeriod = 'annual';
        }
        this.data =
            {
                planType: this.planType,
                productName: this.productName,
                planPeriod: this.year.toString(),
                plan: this.plan,
                paidAmount: this.ammount,
                billPeriod: this.billPeriod
            };
        this.registerData = { companyProfile: this.companyProfileForm.value, contactPerson: this.contactPersonForm.value, planDetail: this.data, emailId: localStorage.getItem('userId'), accountId: localStorage.getItem('accountId'), logo: this.image, profilePic: this.image1, registrationType: "new" };
        this.service.merchantRegistration(this.registerData).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == 1) {
                localStorage.setItem('regStatus', 'complete');
                var data = {
                    "getHostedPaymentPageRequest": {
                        "merchantAuthentication": {
                            "name": "9863nDF2VGfG",
                            "transactionKey": "4Vv4T8h8Z5vc9Wjn"
                        },
                        "transactionRequest": {
                            "transactionType": "authCaptureTransaction",
                            "amount": _this.ammount,
                            "profile": {
                                "customerProfileId": "111"
                            },
                            "customer": {
                                "email": localStorage.getItem('userId')
                            },
                            "billTo": {
                                "firstName": _this.title + " " + _this.contactPersonForm.value.cpName,
                                "lastName": "",
                                "company": _this.companyProfileForm.value.name,
                                "address": _this.companyProfileForm.value.companyAddress,
                                "city": _this.companyProfileForm.value.city,
                                "state": _this.companyProfileForm.value.state,
                                "zip": _this.companyProfileForm.value.pinCode,
                                "country": _this.companyProfileForm.value.country,
                            }
                        },
                        "hostedPaymentSettings": {
                            "setting": [
                                {
                                    "settingName": "hostedPaymentReturnOptions",
                                    "settingValue": "{\"showReceipt\": true}"
                                    // , \"url\": \"https://services.mirrorsize.com/#/paymentconfirmation\", \"urlText\": \"Continue\", \"cancelUrl\": \"https://services.mirrorsize.com/#/signup\", \"cancelUrlText\": \"Cancel
                                },
                                {
                                    "settingName": "hostedPaymentButtonOptions",
                                    "settingValue": "{\"text\": \"Pay\"}"
                                },
                                {
                                    "settingName": "hostedPaymentStyleOptions",
                                    "settingValue": "{\"bgColor\": \"blue\"}"
                                },
                                {
                                    "settingName": "hostedPaymentPaymentOptions",
                                    "settingValue": "{\"cardCodeRequired\": true, \"showCreditCard\": true, \"showBankAccount\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentSecurityOptions",
                                    "settingValue": "{\"captcha\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentShippingAddressOptions",
                                    "settingValue": "{\"show\": false, \"required\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentBillingAddressOptions",
                                    "settingValue": "{\"show\": true, \"required\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentCustomerOptions",
                                    "settingValue": "{\"showEmail\": false, \"requiredEmail\": false, \"addPaymentProfile\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentOrderOptions",
                                    "settingValue": "{\"show\": true, \"merchantName\": \"Mirrorsize US Inc\"}"
                                },
                                {
                                    "settingName": "hostedPaymentIFrameCommunicatorUrl",
                                    "settingValue": "{\"url\": \"http://services.mirrorsize.com\"}"
                                }
                            ]
                        }
                    }
                };
                _this.service.makePayment(data).subscribe(function (response) {
                    _this.showLoader = false;
                    if (response.messages.resultCode == 'Ok') {
                        _this.authorizetoken = response.token;
                        _this.getPayment = true;
                        document.getElementById('headerShow').style.zIndex = '1';
                        document.getElementById('outerMain').style.zIndex = "9999";
                        _this.showPaymentIframe = true;
                        _this.next('5');
                        _this.loadScript();
                        localStorage.clear();
                    }
                    else {
                        Swal('Oops!', response.messages.message.text, 'error');
                    }
                });
            }
            else {
                Swal("Oops!", response.message, 'error');
            }
        });
    };
    SignupComponent.prototype.makePaymentMysize = function () {
        var _this = this;
        this.showLoader = true;
        this.billPeriod = 'monthly';
        this.ammount = 1;
        this.plan = 'small';
        this.planType = 'PAID';
        this.data =
            {
                planType: this.planType,
                productName: this.productName,
                planPeriod: "1",
                plan: this.plan,
                paidAmount: this.ammount,
                billPeriod: this.billPeriod
            };
        this.registerData = { companyProfile: this.companyProfileForm.value, contactPerson: this.contactPersonForm.value, planDetail: this.data, emailId: localStorage.getItem('userId'), accountId: localStorage.getItem('accountId'), logo: this.image, profilePic: this.image1, registrationType: "new" };
        this.service.merchantRegistration(this.registerData).subscribe(function (response) {
            _this.showLoader = false;
            if (response.code == 1) {
                localStorage.setItem('regStatus', 'complete');
                var data = {
                    "getHostedPaymentPageRequest": {
                        "merchantAuthentication": {
                            "name": "9863nDF2VGfG",
                            "transactionKey": "4Vv4T8h8Z5vc9Wjn"
                        },
                        "transactionRequest": {
                            "transactionType": "authCaptureTransaction",
                            "amount": _this.ammount,
                            "profile": {
                                "customerProfileId": "111"
                            },
                            "customer": {
                                "email": localStorage.getItem('userId')
                            },
                            "billTo": {
                                "firstName": _this.title + " " + _this.contactPersonForm.value.cpName,
                                "lastName": "",
                                "company": _this.companyProfileForm.value.name,
                                "address": _this.companyProfileForm.value.companyAddress,
                                "city": _this.companyProfileForm.value.city,
                                "state": _this.companyProfileForm.value.state,
                                "zip": _this.companyProfileForm.value.pinCode,
                                "country": _this.companyProfileForm.value.country,
                            }
                        },
                        "hostedPaymentSettings": {
                            "setting": [
                                {
                                    "settingName": "hostedPaymentReturnOptions",
                                    "settingValue": "{\"showReceipt\": true, \"url\": \"https://services.mirrorsize.com/#/paymentconfirmation\", \"urlText\": \"Continue\", \"cancelUrl\": \"https://services.mirrorsize.com/#/signup\", \"cancelUrlText\": \"Cancel\"}"
                                },
                                {
                                    "settingName": "hostedPaymentButtonOptions",
                                    "settingValue": "{\"text\": \"Pay\"}"
                                },
                                {
                                    "settingName": "hostedPaymentStyleOptions",
                                    "settingValue": "{\"bgColor\": \"blue\"}"
                                },
                                {
                                    "settingName": "hostedPaymentPaymentOptions",
                                    "settingValue": "{\"cardCodeRequired\": true, \"showCreditCard\": true, \"showBankAccount\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentSecurityOptions",
                                    "settingValue": "{\"captcha\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentShippingAddressOptions",
                                    "settingValue": "{\"show\": false, \"required\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentBillingAddressOptions",
                                    "settingValue": "{\"show\": true, \"required\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentCustomerOptions",
                                    "settingValue": "{\"showEmail\": false, \"requiredEmail\": false, \"addPaymentProfile\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentOrderOptions",
                                    "settingValue": "{\"show\": true, \"merchantName\": \"Mirrorsize US Inc\"}"
                                },
                                {
                                    "settingName": "hostedPaymentIFrameCommunicatorUrl",
                                    // "settingValue": "{\"url\": \"http://services.mirrorsize.com\"}"
                                    "settingValue": "{\"url\": \"http://" + location.host + "/#/iFrameCommunicator\"}"
                                }
                            ]
                        }
                    }
                };
                _this.service.makePayment(data).subscribe(function (response) {
                    _this.showLoader = false;
                    if (response.messages.resultCode == 'Ok') {
                        _this.authorizetoken = response.token;
                        document.getElementById('headerShow').style.zIndex = '1';
                        document.getElementById('outerMain').style.zIndex = "9999";
                        document.getElementById('paymentmodal').style.background = 'rgba(0, 0, 0, 0.35)';
                        $('#paymentmodal').modal('show');
                        _this.loadScript();
                        localStorage.clear();
                    }
                    else {
                        Swal('Oops!', response.messages.message.text, 'error');
                    }
                });
            }
            else {
                Swal("Oops!", response.message, 'error');
            }
        });
    };
    SignupComponent.prototype.numberOnly = function (event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    };
    SignupComponent.prototype.ValidateEmail = function (e) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(e.target.value)) {
            this.checkEmail = true;
        }
        else {
            this.checkEmail = false;
            return (false);
        }
    };
    SignupComponent.prototype.select = function (val) {
        if (val == 'one') {
            document.getElementById('one').style.backgroundColor = 'transparent';
            document.getElementById('one').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('one').style.color = '#fff';
            document.getElementById('one').style.zIndex = '999';
            document.getElementById('one').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed1').style.color = '#fff';
            document.getElementById('two').style.backgroundColor = '#fff';
            document.getElementById('two').style.backgroundImage = 'none';
            document.getElementById('two').style.color = '#333';
            document.getElementById('two').style.zIndex = '0';
            document.getElementById('two').style.boxShadow = 'none';
            document.getElementById('hed2').style.color = '#333';
        }
        else if (val == 'two') {
            document.getElementById('one').style.backgroundColor = '#fff';
            document.getElementById('one').style.backgroundImage = 'none';
            document.getElementById('one').style.color = '#333';
            document.getElementById('one').style.zIndex = '0';
            document.getElementById('one').style.boxShadow = 'none';
            document.getElementById('hed1').style.color = '#333';
            document.getElementById('two').style.backgroundColor = 'transparent';
            document.getElementById('two').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('two').style.color = '#fff';
            document.getElementById('two').style.zIndex = '999';
            document.getElementById('two').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed2').style.color = '#fff';
        }
        else if (val == 'three') {
            document.getElementById('three').style.backgroundColor = 'transparent';
            document.getElementById('three').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('three').style.color = '#fff';
            document.getElementById('three').style.zIndex = '999';
            document.getElementById('three').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed3').style.color = '#fff';
            document.getElementById('four').style.backgroundColor = '#fff';
            document.getElementById('four').style.backgroundImage = 'none';
            document.getElementById('four').style.color = '#333';
            document.getElementById('four').style.zIndex = '0';
            document.getElementById('four').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed4').style.color = '#333';
        }
        else if (val == 'four') {
            document.getElementById('four').style.backgroundColor = 'transparent';
            document.getElementById('four').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('four').style.color = '#fff';
            document.getElementById('four').style.zIndex = '999';
            document.getElementById('four').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed4').style.color = '#fff';
            document.getElementById('three').style.backgroundColor = '#fff';
            document.getElementById('three').style.backgroundImage = 'none';
            document.getElementById('three').style.color = '#333';
            document.getElementById('three').style.zIndex = '0';
            document.getElementById('three').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed3').style.color = '#333';
        }
        else if (val == 'five') {
            document.getElementById('five').style.backgroundColor = 'transparent';
            document.getElementById('five').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('five').style.color = '#fff';
            document.getElementById('five').style.zIndex = '999';
            document.getElementById('five').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed5').style.color = '#fff';
            document.getElementById('six').style.backgroundColor = '#fff';
            document.getElementById('six').style.backgroundImage = 'none';
            document.getElementById('six').style.color = '#333';
            document.getElementById('six').style.zIndex = '0';
            document.getElementById('six').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed6').style.color = '#333';
        }
        else if (val == 'six') {
            document.getElementById('six').style.backgroundColor = 'transparent';
            document.getElementById('six').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('six').style.color = '#fff';
            document.getElementById('six').style.zIndex = '999';
            document.getElementById('six').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed6').style.color = '#fff';
            document.getElementById('five').style.backgroundColor = '#fff';
            document.getElementById('five').style.backgroundImage = 'none';
            document.getElementById('five').style.color = '#333';
            document.getElementById('five').style.zIndex = '0';
            document.getElementById('five').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed5').style.color = '#333';
        }
    };
    SignupComponent.prototype.addClass = function () {
        document.getElementById('headerShow').style.zIndex = '1';
        document.getElementById('outerMain').style.zIndex = "9999";
        // document.getElementById('updatemeaurement').style.background = 'rgba(0, 0, 0, 0.35)';
        document.getElementById('agreementModal').style.background = 'rgba(0, 0, 0, 0.35)';
        document.getElementById('paymentmodal').style.background = 'rgba(0, 0, 0, 0.35)';
    };
    SignupComponent.prototype.closeModal = function () {
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
    };
    SignupComponent.prototype.loadScript = function () {
        var isFound = false;
        var scripts = document.getElementsByTagName("script");
        for (var i = 0; i < scripts.length; ++i) {
            if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("AcceptUI")) {
                isFound = true;
            }
        }
        if (!isFound) {
            var dynamicScripts = ["https://js.authorize.net/v3/AcceptUI.js"];
            for (var i = 0; i < dynamicScripts.length; i++) {
                var node = document.createElement('script');
                node.src = dynamicScripts[i];
                node.type = 'text/javascript';
                node.async = false;
                node.charset = 'utf-8';
                document.getElementsByTagName('head')[0].appendChild(node);
            }
        }
    };
    SignupComponent.prototype.resendOtp = function () {
        var obj = { "emailId": localStorage.getItem('userId') };
        this.service.resendOtp(obj).subscribe(function (response) {
            if (response.code == "1") {
                Swal('success', response.message, 'success');
            }
            else {
                Swal('error', response.message, 'error');
            }
        });
    };
    SignupComponent.prototype.acceptTerms = function () {
        $('#agreementModal').modal('hide');
        $("#add_payment").show();
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
        this.makePayment();
    };
    SignupComponent.prototype.acceptTermsFt = function () {
        $('#agreementModalFT').modal('hide');
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
        this.makePayment();
    };
    SignupComponent.prototype.acceptTermsMysize = function () {
        $('#agreementModalMysize').modal('hide');
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
        this.makePaymentMysize();
    };
    SignupComponent.prototype.acceptTermsQuicksize = function () {
        $('#agreementModalQuicksize').modal('hide');
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
        this.makePayment();
    };
    SignupComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        window['CommunicationHandler'] = {};
        window['CommunicationHandler'].onReceiveCommunication = function (argument) {
            var message;
            // console.log("iFrame argument: " + JSON.stringify(argument));
            //Get the parameters from the argument.
            var params = _this.parseQueryString(argument.qstr);
            console.log(params);
            switch (params['action']) {
                case "resizeWindow":
                    console.log('resize window');
                    //{"qstr":"action=resizeWindow&width=551&height=825","parent":"https://test.authorize.net/customer/addPayment"}
                    //We can use this to resize the window, if needed. Just log it for now.
                    break;
                case "successfulSave":
                    //{"qstr":"action=successfulSave","parent":"https://test.authorize.net/customer/addPayment"}
                    break;
                case "cancel":
                    //{"qstr":"action=cancel","parent":"https://test.authorize.net/customer/addPayment"}
                    break;
                case "transactResponse":
                    var ifrm = document.getElementById("add_payment");
                    ifrm.style.display = 'none';
                    //sessionStorage.removeItem("HPTokenTime");
                    break;
                default:
                    console.log('Unknown action from iframe. Action: ' + params['action']);
                    break;
            }
        };
    };
    SignupComponent.prototype.parseQueryString = function (str) {
        var vars = [];
        var arr = str.split('&');
        var pair;
        for (var i = 0; i < arr.length; i++) {
            pair = arr[i].split('=');
            //vars[pair[0]] = unescape(pair[1]);
            vars[pair[0]] = pair[1];
        }
        return vars;
    };
    SignupComponent = __decorate([
        Component({
            selector: 'app-signup',
            templateUrl: './signup.component.html',
            styleUrls: ['./signup.component.css'],
        }),
        __metadata("design:paramtypes", [Router, MsServices, ToastrService, NgxSpinnerService])
    ], SignupComponent);
    return SignupComponent;
}());
export { SignupComponent };
//# sourceMappingURL=signup.component.js.map