import { Router, ActivatedRoute } from '@angular/router';
import { MerchantDetails } from '../model/merchantdetails';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { CookieService } from 'ngx-cookie-service';
// import {DomSanitizer} from '@angular/platform-browser';
import { DomSanitizer } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { PagerService } from '../services/pagerService';
import * as jspdf from 'jspdf';
const Swal = require('sweetalert2');
import { saveAs } from 'file-saver';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;

@Component({
    selector: 'app-apparel',
    templateUrl: './apparel.component.html',
    styleUrls: ['./apparel.component.css']
})
export class ApparelComponent implements OnInit {
    selectedItems = [];
    imageUrl = '';
    key = '';
    pager: any = {};
    pagedItems: any[];
    showLoader = false;
    storeCount = 0;
    keyValue = "apparelName"
    isSearch = false;
    email: any;
    token: any;
    mid: any;
    gender;
    editData = [];
    finalPoints = {}
    result = [];
    newArry = []
    selectedAll: any
    expired = false;

    constructor(private rout: ActivatedRoute, public pagerService: PagerService, private spinner: NgxSpinnerService, 
        config: NgbModalConfig, private modalService: NgbModal, private toastr: ToastrService, private route: Router, 
        private cookieService: CookieService, private service: MsServices, public _sanitizer: DomSanitizer) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    now = new Date();
    public searchText: string;
    minNUmber: string;
    uid: string;
    name: string;
    public data: any;
    public selectedMeasuredUpdate: any;
    public title: 'My Home Page';
    missing: any[];
    merchantDetails: MerchantDetails;
    hello = [];
    country: string;
    routeParam: any;
    products: any[] = [];
    selectedProducts = [];
    dummyData = [];
    measurementData: any;
    measurement: any;

    @Output() checked: EventEmitter<boolean> = new EventEmitter<boolean>();

    brandNameForModal: any;

    fordeleteConfirm: boolean;
    // ------for  downloading pdf created function 28/jan/2019------//
    responseForDate: any;

    setClickedRow(i) {

        if (i.measurementPoints === this.products) {

        } else {

        }
    }

    mybtn(measured) {
        // measured points comming

        this.hello = measured.measurementPoints;

        this.country = measured.country;
        this.selectedMeasuredUpdate = { merchantKey: { ...measured.merchantKey }, measurementPoints: [...measured.measurementPoints] };
        this.missing = this.products.filter(item => measured.measurementPoints.indexOf(item.products) < 0);

    }

    ngOnInit() {
        var param = this.rout.snapshot.paramMap.get("id");
        if (param == 'GetMeasured' || param == 'GET_MEASURED') {
            this.routeParam = "GET_MEASURED"
        }
        else {
            this.routeParam = "SIZE2FIT"
        }

        var d = JSON.parse(localStorage.getItem('data'));
        this.email = d['emailId'];
        this.token = d['token'];
        this.name = d['companyName']
        this.mid = d['mid'];

        if (d['product'].GET_MEASURED.paymentStatus == 'INACTIVE') {
          
            if(d['product'].GET_MEASURED.remainingDays < 0)
            {
              this.expired = false;
            }
            else
            {
              this.expired = true;
            }
        }
        if (this.expired == false) {
            this.getData(1);
        }
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Apparels";
    }


    getData(page) 
    {
        this.data=[];
        this.showLoader = true;
        var data = { "emailId": this.email, "token": this.token, "productName": this.routeParam, "pageNo": page };
        this.service.getApparel(data).subscribe(res => {

            this.showLoader = false;
            if (res.code == 1 && res.data) {
                this.storeCount = res.count;
                this.data = res.data;
                this.pager = this.pagerService.getPager(this.storeCount, page);
                this.data.map((item) => {
                    item.measurementPoint = JSON.parse(item.measurementPoint)
                })
            }
            else {
                Swal.fire("Oops!", res.message, 'info');
            }
        });
    }



    setPage(page: number) {
        this.pager = this.pagerService.getPager(this.storeCount, page);
        this.pagedItems = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
        this.getData(page);
    }

    onChecked(app, checked) {
        if (checked) {
            this.result.push({ key: app.key, value: app.displayName });
        }
        else {
            var myArray = this.result.filter(function (obj) {
                return obj.value !== app.displayName;
            });
            this.result = [];
            this.result = myArray;
        }

        this.finalPoints = {};
        for (var i = 0; i < this.result.length; i++) {
            this.finalPoints[this.result[i].key] = this.result[i].value;
        }

    }

    mapValue(data2) {
        return data2;
    }


    updateMeasurement() {
        var obj =
        {
            "emailId": this.email,
            "token": this.token,
            "action": 'update',
            "brandName": this.measurementData.brandName,
            "gender": this.measurementData.gender,
            "countrySize": this.measurementData.coutry,
            "apparelId": this.measurementData.apparelId,
            "productName": this.routeParam,
            "measurementPointData": this.finalPoints
        }
        console.log(this.finalPoints);

        this.service.addApparel(obj).subscribe(response => {
            if (response.code == "1") {
                Swal('Success', response.message, 'success');
                this.getData(1);
                this.newArry = [];
                $('#updatePoints').modal('hide')

            }
            else {
                Swal('Oops!', response.message, 'info');
            }
        })

    }

    editMeasurement(measured) {

        this.measurementData = measured
        var measurement = measured.measurementPoint;

        this.result = Object.keys(measurement).map(function (key) {
            return ({ "key": (key), "value": measurement[key] });
        });

        this.gender = measured.gender;
        var obj = { "gender": this.gender, "token": this.token, "productName": this.routeParam, "emailId": this.email }
        this.service.getMeasurementPoints(obj).subscribe(response => {
            if (response.code == "1") {
                this.editData = response.data
                $('#updatePoints').modal('show');
            }
            else {
                Swal('error', response.message, 'error');
            }
        })
    }

    searchApparel() {
        this.data = [];
        if (this.searchText == "") {
            Swal('info', 'please enter brand name or apparel name', 'info')
        }
        else {
            var obj = { token: this.token, emailId: this.email, productName: this.routeParam, "searchKey": this.keyValue, "searchValue": this.searchText.trim(), "pageNo": "1" };
            this.service.searchApparel(obj).subscribe(response => {
                if (response.code == "1") {
                    this.data = response.data;
                    // console.log(this.data[0].measurementPoints)
                    this.data.map((item) => {
                        item.measurementPoint = JSON.parse(item.measurementPoint)
                    })
                }
                else {
                    Swal('Error', response.message, 'error')
                }
            })
        }
    }

    isSelected(data, k) {
        for (var i = 0; i < this.result.length; i++) {
            if (this.result[i].key == data.key) {
                return true
            }
            else {
                $('#check' + k).prop('checked', false)
            }
        }
    }


    onChange(value: string, checked: boolean) {

        if (checked) {
            this.hello.push(value);
        } else {
            const index = this.hello.indexOf(value);
            this.hello.splice(index, 1);
        }

    }

    delete(measured) {
        Swal({
            title: 'Are you sure?',
            text: 'You will not be able to recover this data!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            this.fordeleteConfirm = result.value;
            if (result.value == true) {
                var data = { "emailId": measured.merchantId, "brandName": measured.brandName, "apparelId": measured.apparelId, "gender": measured.gender, "productName": measured.productName, "token": this.token }
                this.service.deleteMerchantInfo(data).subscribe(res => {
                    if (res.code == 1) {
                        this.getData(1);
                        this.toastr.success(res.message, 'Success', {timeOut: 1100,});

                    } else {
                        this.toastr.warning(res.message, 'Error', {timeOut: 1100,});
                    }
                });
            } else {
                this.toastr.warning('Not deleted', 'Failed', {timeOut: 1100,});
            }
        });
    }

    dynamicMeasurementPoints(measured) {
        this.showLoader = true;
        this.products = [];
        const onAddApparel = {
            'merchantID': measured.merchantKey.merchatID,
            'apparelName': measured.merchantKey.apparelName,
            'gender': measured.merchantKey.gender,
            'brandName': measured.merchantKey.brandName,
            'country': measured.country,
            'productname': 'SIZE2FIT',
        };

        this.service.measurementPoints(onAddApparel).subscribe(res => {
            this.showLoader = false;
            if (res.code == 1) {
                this.products = res.data;
            }
            else {
                Swal.fire('Oops!', res.message, 'error')
            }

        });
    }
    downloadTable() {
        const jsonForPdf = {
            'merchantID': this.email,
            'productname': 'size2fit',
            'token': this.cookieService.get('token'),
        };

        this.service.forSize2FitPdfSizes(jsonForPdf).subscribe(res => {

            this.responseForDate = res;
            if (this.responseForDate.status === 'FAILURE') {
                this.toastr.error('Date not exist', 'Date is invalid', {
                    timeOut: 1200,
                });
            } else {
                this.service.getDownload(this.responseForDate.message).subscribe(respose => {

                    this.downloadCompletePDF(respose);

                });
            }
        });
    }

    downloadCompletePDF(res) {
        const doc = new jspdf();
        const reader = new FileReader();
        reader.addEventListener('loadend', function () {
        });
        saveAs(res, this.email + '.pdf');
        this.spinner.hide();
        reader.readAsArrayBuffer(res);
        const file = new Blob([res], { type: 'application/pdf' });
        const fileURL = URL.createObjectURL(res);
        const down11 = window.open(fileURL, '_blank');
        down11.print();
    }

    clear() {
        this.getData(1);
    }

    getImage(val, gen, keyval) {
        this.key = keyval;
        this.imageUrl = "https://s3.ap-south-1.amazonaws.com/commonms/" + gen.toLowerCase() + "_large_icon/" + val.toLowerCase() + ".png";
        $('#viewImage').modal('show');
    }

    // showModal(val) 
    // {
    //     if (val == 'hover') 
    //     {
    //         $(".block__pic").imagezoomsl({
    //             zoomrange: [5, 5]
    //         });
    //     }
    //     else {
    //         document.getElementById('blockPik').classList.remove('magnifier');
    //     }

    // }


    addClass()
    {
        document.getElementById('headerShow').style.zIndex='1';
        document.getElementById('outerMain').style.zIndex="9999"; 
        document.getElementById('viewDescription').style.background='rgba(0, 0, 0, 0.61)';
        
    }

    closeModal()
    {
        document.getElementById('headerShow').style.zIndex='99999';
        document.getElementById('outerMain').style.zIndex="0"; 
    }

}