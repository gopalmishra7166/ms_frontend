import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizechartquicksizeComponent } from './sizechartquicksize.component';

describe('SizechartquicksizeComponent', () => {
  let component: SizechartquicksizeComponent;
  let fixture: ComponentFixture<SizechartquicksizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizechartquicksizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizechartquicksizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
