import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {ToastrModule} from 'ngx-toastr';
import {MsServices} from './services/ms-services';
import {CookieService} from 'ngx-cookie-service';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HomePageComponent} from './home-page/home-page.component';
import {CompanyProfileComponent} from './company-profile/company-profile.component';
import {NavbarComponent} from './navbar/navbar.component';
import {PrivacyPolicyComponent} from './privacy-policy/privacy-policy.component';
import {TermsConditionComponent} from './terms-condition/terms-condition.component';
import {FooterComponent} from './footer/footer.component';
import {AuthGuard} from './auth.guard';
import {NgxSpinnerModule} from 'ngx-spinner';
import {ModalModule} from 'angular-custom-modal';
import {MerchantAccesscodeComponent} from './merchant-accesscode/merchant-accesscode.component';
import {ApparelDetailComponent} from './apparel-detail/apparel-detail.component';
import {AccessCodeS2fComponent } from './access-code-s2f/access-code-s2f.component';
import {MyDatePickerModule } from 'mydatepicker';
import {PagerService} from './services/pagerService'
import {AppRoutingModule } from './app-routing.module';
import {PaymentComponent } from './payment/payment.component';
import {GraphComponent } from './graph/graph.component';
import {LoginComponent} from './login/login.component'
import {SignupComponent} from './signup/signup.component';
import { ReportsComponent } from './reports/reports.component';
import { SubscriptionComponent } from './subscription/subscription.component';
import { ResourcesComponent } from './resources/resources.component';
import { UsersComponent } from './users/users.component';
import { ApparelComponent } from './apparel/apparel.component';
import { AddapparelComponent } from './addapparel/addapparel.component';
import { ApparelS2fComponent } from './apparel-s2f/apparel-s2f.component';
import { UserComponent } from './user/user.component';
import { MeasurementguideComponent } from './measurementguide/measurementguide.component';
import { SupportComponent } from './support/support.component';
import { PaymentconfirmationComponent } from './paymentconfirmation/paymentconfirmation.component';
import { UpgradeplangmComponent } from './upgradeplangm/upgradeplangm.component';
import { UpgradeplansfComponent } from './upgradeplansf/upgradeplansf.component';
import { GuidedtourComponent } from './guidedtour/guidedtour.component';
import { JoyrideModule} from 'ngx-joyride';
import { SizechartComponent } from './sizechart/sizechart.component';
import { Guidedtours2fComponent } from './guidedtours2f/guidedtours2f.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { UsermysizeComponent } from './usermysize/usermysize.component';
import { SizechartmysizeComponent } from './sizechartmysize/sizechartmysize.component';
import { MysizelistingComponent } from './mysizelisting/mysizelisting.component';
import { UserquicksizeComponent } from './userquicksize/userquicksize.component';
import { GuidedtourqsComponent } from './guidedtourqs/guidedtourqs.component';
import { SizechartquicksizeComponent } from './sizechartquicksize/sizechartquicksize.component';
import { GuidedtourmsComponent } from './guidedtourms/guidedtourms.component';
import { CoupanhistoryComponent } from './coupanhistory/coupanhistory.component';
import { TestingComponent } from './testing/testing.component';
import { IFrameCommunicatorComponent } from './i-frame-communicator/i-frame-communicator.component';


@NgModule({
    declarations: [
        SignupComponent,
        ReportsComponent,
        LoginComponent,
        AccessCodeS2fComponent,
        AppComponent,
        HomePageComponent,
        CompanyProfileComponent,
        NavbarComponent,
        PaymentComponent,
        PrivacyPolicyComponent,
        TermsConditionComponent,
        FooterComponent,
        MerchantAccesscodeComponent,
        ApparelDetailComponent,
        AccessCodeS2fComponent,
        GraphComponent,
        SubscriptionComponent,
        ResourcesComponent,
        UsersComponent,
        ApparelComponent,
        AddapparelComponent,
        ApparelS2fComponent,
        UserComponent,
        MeasurementguideComponent,
        SupportComponent,
        PaymentconfirmationComponent,
        UpgradeplangmComponent,
        UpgradeplansfComponent,
        GuidedtourComponent,
        SizechartComponent,
        Guidedtours2fComponent,
        UsermysizeComponent,
        SizechartmysizeComponent,
        MysizelistingComponent,
        UserquicksizeComponent,
        GuidedtourqsComponent,
        SizechartquicksizeComponent,
        GuidedtourmsComponent,
        CoupanhistoryComponent,
        TestingComponent,
        IFrameCommunicatorComponent,
    ],

    imports: [
        BrowserModule,
        NgbModule,
        CommonModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        HttpModule,
        HttpClientModule,
        BrowserModule,
        FormsModule,
        ModalModule,
        MyDatePickerModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        NgxSpinnerModule,
        ReactiveFormsModule,
        AppRoutingModule,
        RouterModule,
        JoyrideModule.forRoot(),
        NgxFileDropModule
    ],

    providers: [MsServices, CookieService, AuthGuard,PagerService],
    bootstrap: [AppComponent]
})

export class AppModule {

}
