import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MsServices } from '../services/ms-services';
import { $ } from 'protractor';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {

  mForm: FormGroup;
  email: any;
  merchantId: any;
  bodyShape = "";
  showLoader = false;
  isChecked = false;
  isWeight = false;
  sizes=[];

  constructor(private service: MsServices) {
    this.mForm = new FormGroup({
      heightInInch: new FormControl(''),
      heightInCM: new FormControl(''),
      heightInFeet: new FormControl(''),
      age: new FormControl(''),
      gender: new FormControl(''),
      weightInKg: new FormControl(''),
      weightInPound: new FormControl(''),

    })
  }

  ngOnInit() {
    var d = JSON.parse(localStorage.getItem('data'));
    this.email = d['emailId'];
    this.merchantId = d['accountId']
  }


  selectSize(id) {
    if (id == 's1') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s2').style.background = '#fff';
      document.getElementById('s2').style.color = '#333';
      document.getElementById('s3').style.background = '#fff';
      document.getElementById('s3').style.color = '#333';
      document.getElementById('s4').style.background = '#fff';
      document.getElementById('s4').style.color = '#333';
      document.getElementById('s5').style.background = '#fff';
      document.getElementById('s5').style.color = '#333';
    }
    else if (id == 's2') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s1').style.background = '#fff';
      document.getElementById('s1').style.color = '#333';
      document.getElementById('s3').style.background = '#fff';
      document.getElementById('s3').style.color = '#333';
      document.getElementById('s4').style.background = '#fff';
      document.getElementById('s4').style.color = '#333';
      document.getElementById('s5').style.background = '#fff';
      document.getElementById('s5').style.color = '#333';
    }

    else if (id == 's3') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s2').style.background = '#fff';
      document.getElementById('s2').style.color = '#333';
      document.getElementById('s1').style.background = '#fff';
      document.getElementById('s1').style.color = '#333';
      document.getElementById('s4').style.background = '#fff';
      document.getElementById('s4').style.color = '#333';
      document.getElementById('s5').style.background = '#fff';
      document.getElementById('s5').style.color = '#333';
    }
    else if (id == 's4') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s2').style.background = '#fff';
      document.getElementById('s2').style.color = '#333';
      document.getElementById('s3').style.background = '#fff';
      document.getElementById('s3').style.color = '#333';
      document.getElementById('s1').style.background = '#fff';
      document.getElementById('s1').style.color = '#333';
      document.getElementById('s5').style.background = '#fff';
      document.getElementById('s5').style.color = '#333';
    }
    else if (id == 's5') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s1').style.background = '#fff';
      document.getElementById('s1').style.color = '#333';
      document.getElementById('s3').style.background = '#fff';
      document.getElementById('s3').style.color = '#333';
      document.getElementById('s4').style.background = '#fff';
      document.getElementById('s4').style.color = '#333';
      document.getElementById('s2').style.background = '#fff';
      document.getElementById('s2').style.color = '#333';
    }
  }

  getMeasurementQuickSize() {
 
      var height = 0;
      var weight = 0;

      if (this.isChecked) {
        height = (this.mForm.value.heightInCM) * 10
      }
      else {
        if (!this.mForm.value.heightInFeet) {
          var inc = (parseInt(this.mForm.value.heightInInch) * 2.54) * 10
          height = inc;
        }
        else if (!this.mForm.value.heightInInch) {
          var f = parseInt(this.mForm.value.heightInFeet) * 12;
          var ff = (f * 2.54) * 10;
          height = ff;
        }
        else {
          var inc = (parseInt(this.mForm.value.heightInInch) * 2.54) * 10
          var f = parseInt(this.mForm.value.heightInFeet) * 12
          var ff = (f * 2.54) * 10;
          height = ff + inc;
        }

        if (this.isWeight) {
          if ((Number(this.mForm.value.weightInKg))>=30 && (Number(this.mForm.value.weightInKg))<=150) {
            weight = this.mForm.value.weightInKg
          }
          else
          {
            Swal('info!', 'weight must be in 30 to 150 kg', 'info')
            weight=0;
          }
        }
        else {
          if((Number(this.mForm.value.weightInPound))>=66 && (Number(this.mForm.value.weightInPound))<=330) 
          {
            weight = (parseInt(this.mForm.value.weightInPound)) * 2.10
          }
          else
          {
            Swal('info!', 'weight must be in 66 to 330lbs kg', 'info');
            weight=0;
          }
        }
      }

      if(this.mForm.value.age<"18" || this.mForm.value.age>"80")
      {
        Swal('info!', 'age must be between 18 to 80 year', 'info');
      }
      

      var obj = {
        // "userId"     : "passionategopal001@gmail.com",
        "height": Math.round(height),
        "weight": Math.round(weight),
        "age": this.mForm.value.age,
        "gender": this.mForm.value.gender,
        "userType": "merchant",
        "merchantId": this.email,
        "productName": "QUICKSIZE",
        "stomachShape": this.bodyShape,
        "apparelName": "shirts",
        "fitType": "regular"
      };

     
      this.service.getMeasurementQuickSize(obj).subscribe(response => {
       
        if (response.code == "1") 
        {
          this.sizes=response.data.sizes
          // Swal('success', response.message, 'success');
          document.getElementById('myModal2').style.display = 'none';
          this.mForm.reset();
          document.getElementById('average').style.backgroundColor = 'rgb(2, 19, 81)';
          document.getElementById('flat').style.backgroundColor = 'rgb(2, 19, 81)';
          document.getElementById('belly').style.backgroundColor = 'rgb(2, 19, 81)';
          document.getElementById('size').style.display='block';
          document.getElementById('size').style.opacity='1';
        
        }
        else {
          Swal('Oops!', response.message, 'info');
          document.getElementById('myModal2').style.display = 'none';
        }
      })
  
  }

  getBodyShape(id) {
    if (id == 'flat') {
      this.bodyShape = id;
      document.getElementById(id).style.backgroundColor = '#ecd00a';
      document.getElementById('average').style.backgroundColor = 'rgb(2, 19, 81)';
      document.getElementById('belly').style.backgroundColor = 'rgb(2, 19, 81)';
    }
    else if (id == 'average') {
      this.bodyShape = id;
      document.getElementById(id).style.backgroundColor = '#ecd00a';
      document.getElementById('flat').style.backgroundColor = 'rgb(2, 19, 81)';
      document.getElementById('belly').style.backgroundColor = 'rgb(2, 19, 81)';
    }
    else if (id == 'belly') {
      this.bodyShape = id;
      document.getElementById(id).style.backgroundColor = '#ecd00a';
      document.getElementById('average').style.backgroundColor = 'rgb(2, 19, 81)';
      document.getElementById('flat').style.backgroundColor = 'rgb(2, 19, 81)';
    }
    else {
      this.bodyShape = "";
    }

  }

  switchValue(e) {
    if (e.target.checked) {
      this.isChecked = true;
    }
    else {
      this.isChecked = false;
    }
  }

  switchWeight(e) {
    if (e.target.checked) {
      this.isWeight = true;
    }
    else {
      this.isWeight = false;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  addClass()
  {
      document.getElementById('headerShow').style.zIndex='1';
      document.getElementById('outerMain').style.zIndex="9999"; 
      document.getElementById('size').style.background='rgba(0, 0, 0, 0.61)';
  }

  closeModal()
  {
      document.getElementById('headerShow').style.zIndex='99999';
      document.getElementById('outerMain').style.zIndex="0"; 
      document.getElementById('size').style.display='none';

  }

}
