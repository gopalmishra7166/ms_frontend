export class MerchantAdress {
    'addressType': string;
    'state': string;
    'country': string;
    'zip': string;
    'house_no': string;
    'building_name': string;
    'street_name': string;
    'merchant_landmark': string;
    'city': string;
}
