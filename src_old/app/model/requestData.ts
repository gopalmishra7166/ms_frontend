export class requestData {
    userID: string;
    merchantID: string;
    gender: string;
    brandName: string;
    apiKey: string;
    apparelName: string;
    accessCode: string;
    measurementData;
    productname: string;
    productType:string;
  }
