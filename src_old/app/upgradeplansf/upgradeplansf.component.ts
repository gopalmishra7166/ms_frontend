import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MsServices } from './../services/ms-services';
import { Router } from '@angular/router';
const Swal = require('sweetalert2');
declare var $: any;


@Component({
    selector: 'app-upgradeplansf',
    templateUrl: './upgradeplansf.component.html',
    styleUrls: ['./upgradeplansf.component.css']
})
export class UpgradeplansfComponent implements OnInit {
    companyAgreementForm: FormGroup;
    companyAgreement = true;
    oneYear = false;
    twoYear = false;
    threeYear = false;
    year: any;
    authorizetoken;
    showLoader = false;
    pdfSrc = '';
    plan;
    planChecked = false;
    ammount;
    account = false;
    email: any;
    token: any;
    name: any;
    mid: any;
    d: any;
    subproductName: any;
    planType: any;
    newProductName: any;
    cpName: any;
    city: any;
    state: any;
    pinCode: any;
    country: any;
    productName: string;
    regDate: string;
    cpMobileNo: any;
    cpEmailId: any;
    cpDesignation: any;
    companyAddress: any;
    ftrial = false;
    SIZE2FIT = false;
    GET_MEASURED = false;
    paid = false;
    monthly: boolean;
    yearly: boolean;
    billPeriod: any;

    constructor(public service: MsServices, public router: Router) {
        this.companyAgreementForm = new FormGroup({
            ftrial: new FormControl(''),
            oneYear: new FormControl(''),
            twoYear: new FormControl(''),
            threeYear: new FormControl('')
        })
    }

    ngOnInit() {
        var today = new Date();
        var dd: any = today.getDate();
        var mm: any = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        this.regDate = dd + '/' + mm + '/' + yyyy;

        var d = JSON.parse(localStorage.getItem('data'))
        this.cpName = d['contactPerson'].cpName;
        this.cpMobileNo = d['contactPerson'].cpMobileNo;
        this.cpEmailId = d['contactPerson'].cpEmailId;
        this.cpDesignation = d['contactPerson'].cpDesignation;
        this.name = d['companyProfile'].name;
        this.companyAddress = d['companyProfile'].companyAddress;
        this.city = d['companyProfile'].city;
        this.state = d['companyProfile'].state;
        this.pinCode = d['companyProfile'].pinCode;
        this.country = d['companyProfile'].country;


        this.productName = 'SIZE2FIT';
        this.SIZE2FIT = true;

        this.buyForOneYear();
        this.monthly = false;
        this.yearly = true;
    }

    changeBillingPeriod(val)
    {
        if(val=='m'){
            this.monthly=true;
            this.yearly=false;
            if(this.oneYear)
            {
                if(this.plan =='small'){
                    this.ammount = 119;
                }
                else if(this.plan =='medium'){
                    this.ammount = 536;
                }
            }
            else if(this.twoYear)
            {
                if(this.plan =='small'){
                    this.ammount = 208;
                }
                else if(this.plan =='medium'){
                    this.ammount = 975;
                }
            }
            else if(this.threeYear)
            {
                if(this.plan =='small'){
                    this.ammount = 785;
                }
                else if(this.plan =='medium'){
                    this.ammount = 2800;
                }
            }
        }
        else if(val=='y'){
            this.monthly=false;
            this.yearly=true;
            if(this.oneYear)
            {
                if(this.plan =='small'){
                    this.ammount = 1200;
                }
                else if(this.plan =='medium'){
                    this.ammount = 5400;
                }
            }
            else if(this.twoYear)
            {
                if(this.plan =='small'){
                    this.ammount = 1920;
                }
                else if(this.plan =='medium'){
                    this.ammount = 9000;
                }
            }
            else if(this.threeYear)
            {
                if(this.plan =='small'){
                    this.ammount = 6500;
                }
                else if(this.plan =='medium'){
                    this.ammount = 24000;
                }
            }
        }
    }

    freetrial() {
        this.planChecked = false;
        this.planType = 'FREETRIAL';
        this.year = 1
        this.ftrial = true;
        this.paid = false;
        this.oneYear = false;
        this.twoYear = false;
        this.threeYear = false;
        this.companyAgreementForm.patchValue({ oneYear: false })
        this.companyAgreementForm.patchValue({ twoYear: false })
        this.companyAgreementForm.patchValue({ threeYear: false })
        console.log(this.paid, this.GET_MEASURED, this.oneYear)
    }

    buyForOneYear() {
        this.planChecked = false;
        this.planType = 'PAID';
        this.paid = true;
        this.year = 1
        this.oneYear = true;
        this.twoYear = false;
        this.threeYear = false;
        this.ftrial = false;
        this.companyAgreementForm.patchValue({ ftrial: false })
        this.companyAgreementForm.patchValue({ twoYear: false })
        this.companyAgreementForm.patchValue({ threeYear: false })
        console.log(this.paid, this.GET_MEASURED, this.oneYear)
    }

    buyForTwoYear() {
        this.planChecked = false;
        this.year = 2
        this.planType = 'PAID';
        this.paid = true;
        this.twoYear = true;
        this.oneYear = false;
        this.threeYear = false;
        this.ftrial = false;
        this.companyAgreementForm.patchValue({ ftrial: false })
        this.companyAgreementForm.patchValue({ oneYear: false })
        this.companyAgreementForm.patchValue({ threeYear: false })
        console.log(this.paid, this.GET_MEASURED, this.oneYear)
    }

    buyForThreeYear() {
        this.planChecked = false;
        this.year = 3
        this.planType = 'PAID';
        this.paid = true;
        this.oneYear = false;
        this.twoYear = false;
        this.threeYear = true;
        this.ftrial = false;
        this.companyAgreementForm.patchValue({ ftrial: false })
        this.companyAgreementForm.patchValue({ oneYear: false })
        this.companyAgreementForm.patchValue({ twoYear: false })
        console.log(this.paid, this.GET_MEASURED, this.oneYear)
    }

    buyProduct(val, am) {
        this.planChecked=true;
        if(this.monthly)
        {
            if(this.oneYear)
            {
                if(val == 'small')
                {
                    this.ammount = 119
                    this.plan = val;
                }
                else if(val == 'medium')
                {
                    this.ammount = 536
                    this.plan = val;
                }
            }
            else if(this.twoYear)
            {
                if(val == 'small')
                {
                    this.ammount = 208
                    this.plan = val;
                }
                else if(val == 'medium')
                {
                    this.ammount = 975
                    this.plan = val;
                }
            }
            else if(this.threeYear)
            {
                if(val == 'small')
                {
                    this.ammount = 785
                    this.plan = val;
                }
                else if(val == 'medium')
                {
                    this.ammount = 2800
                    this.plan = val;
                }
            }
        }
        if (val == 'small') {
            this.ammount = am
            this.plan = val;
        }
        else if (val == 'medium') {
            this.plan = val;
            this.ammount = am
        }
    }

    select(val) {
        if (val == 'one') {
            document.getElementById('one').style.backgroundColor = 'transparent';
            document.getElementById('one').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('one').style.color = '#fff';
            document.getElementById('one').style.zIndex = '9999';
            document.getElementById('one').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed1').style.color = '#fff';

            document.getElementById('two').style.backgroundColor = '#fff';
            document.getElementById('two').style.backgroundImage = 'none';
            document.getElementById('two').style.color = '#333';
            document.getElementById('two').style.zIndex = '0';
            document.getElementById('two').style.boxShadow = 'none';
            document.getElementById('hed2').style.color = '#333';

        }
        else if (val == 'two') {
            document.getElementById('one').style.backgroundColor = '#fff';
            document.getElementById('one').style.backgroundImage = 'none';
            document.getElementById('one').style.color = '#333';
            document.getElementById('one').style.zIndex = '0';
            document.getElementById('one').style.boxShadow = 'none';
            document.getElementById('hed1').style.color = '#333';

            document.getElementById('two').style.backgroundColor = 'transparent';
            document.getElementById('two').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('two').style.color = '#fff';
            document.getElementById('two').style.zIndex = '9999';
            document.getElementById('two').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed2').style.color = '#fff';

        }
        else if (val == 'three') {
            document.getElementById('three').style.backgroundColor = 'transparent';
            document.getElementById('three').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('three').style.color = '#fff';
            document.getElementById('three').style.zIndex = '9999';
            document.getElementById('three').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed3').style.color = '#fff';


            document.getElementById('four').style.backgroundColor = '#fff';
            document.getElementById('four').style.backgroundImage = 'none';
            document.getElementById('four').style.color = '#333';
            document.getElementById('four').style.zIndex = '0';
            document.getElementById('four').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed4').style.color = '#333';

        }
        else if (val == 'four') {
            document.getElementById('four').style.backgroundColor = 'transparent';
            document.getElementById('four').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('four').style.color = '#fff';
            document.getElementById('four').style.zIndex = '9999';
            document.getElementById('four').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed4').style.color = '#fff';

            document.getElementById('three').style.backgroundColor = '#fff';
            document.getElementById('three').style.backgroundImage = 'none';
            document.getElementById('three').style.color = '#333';
            document.getElementById('three').style.zIndex = '0';
            document.getElementById('three').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed3').style.color = '#333';
        }
        else if (val == 'five') {
            document.getElementById('five').style.backgroundColor = 'transparent';
            document.getElementById('five').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('five').style.color = '#fff';
            document.getElementById('five').style.zIndex = '9999';
            document.getElementById('five').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed5').style.color = '#fff';

            document.getElementById('six').style.backgroundColor = '#fff';
            document.getElementById('six').style.backgroundImage = 'none';
            document.getElementById('six').style.color = '#333';
            document.getElementById('six').style.zIndex = '0';
            document.getElementById('six').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed6').style.color = '#333';

        }
        else if (val == 'six') {
            document.getElementById('six').style.backgroundColor = 'transparent';
            document.getElementById('six').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('six').style.color = '#fff';
            document.getElementById('six').style.zIndex = '9999';
            document.getElementById('six').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed6').style.color = '#fff';


            document.getElementById('five').style.backgroundColor = '#fff';
            document.getElementById('five').style.backgroundImage = 'none';
            document.getElementById('five').style.color = '#333';
            document.getElementById('five').style.zIndex = '0';
            document.getElementById('five').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed5').style.color = '#333';

        }
    }

    addClass() {
        document.getElementById('headerShow').style.zIndex = '1';
        document.getElementById('outerMain').style.zIndex = "9999";
        document.getElementById('agreementModal').style.background = 'rgba(0, 0, 0, 0.35)';
        document.getElementById('agreementModalFT').style.background = 'rgba(0, 0, 0, 0.35)';
        document.getElementById('paymentmodal').style.background = 'rgba(0, 0, 0, 0.35)';
    }

    closeModal() {
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
    }

    public loadScript() {
        var isFound = false;
        var scripts = document.getElementsByTagName("script")
        for (var i = 0; i < scripts.length; ++i) {
            if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("AcceptUI")) {
                isFound = true;
            }
        }

        if (!isFound) {
            var dynamicScripts = ["https://js.authorize.net/v3/AcceptUI.js"];

            for (var i = 0; i < dynamicScripts.length; i++) {
                let node = document.createElement('script');
                node.src = dynamicScripts[i];
                node.type = 'text/javascript';
                node.async = false;
                node.charset = 'utf-8';
                document.getElementsByTagName('head')[0].appendChild(node);
            }
        }
    }

    upgrade() 
    {
        var d = JSON.parse(localStorage.getItem('data'))
        if(this.monthly)
        {
          this.billPeriod='monthly';
        }
        else{
          this.billPeriod='annual';
        }
        var obj =
        {
            accountId: d['accountId'],
            token: d['token'],
            planType: this.planType,
            productName: this.productName,
            planPeriod: this.year.toString(),
            plan: this.plan,
            paidAmount: this.ammount,
            billPeriod:this.billPeriod
        }
  
        this.service.upgradePlan(obj).subscribe(response => {
            if (response.code == 1) {
                d['product'].GET_MEASURED=response.product.GET_MEASURED;
                d['product'].SIZE2FIT=response.product.SIZE2FIT;
                localStorage.setItem('data',JSON.stringify(d));
                var data =
                {
                    "getHostedPaymentPageRequest": {
                        "merchantAuthentication": {
                            "name": "9863nDF2VGfG",
                            "transactionKey": "4Vv4T8h8Z5vc9Wjn"
                        },
                        "transactionRequest": {
                            "transactionType": "authCaptureTransaction",
                            "amount": this.ammount,
                            "customer": {
                                "email": d['emailId']
                            },
                            "billTo": {
                                "firstName": d['contactPerson'].cpName,
                                "lastName": '',
                                "company": d['companyProfile'].name,
                                "address": d['companyProfile'].companyAddress,
                                "city": d['companyProfile'].city,
                                "state": d['companyProfile'].state,
                                "zip": d['companyProfile'].pinCode,
                                "country": d['companyProfile'].country,
                            }
                        },
                        "hostedPaymentSettings": {
                            "setting": [
                                {
                                    "settingName": "hostedPaymentReturnOptions",
                                    "settingValue": "{\"showReceipt\": true, \"url\": \"https://services.mirrorsize.com/#/paymentconfirmation\", \"urlText\": \"Continue\", \"cancelUrl\": \"https://services.mirrorsize.com/#/graph\", \"cancelUrlText\": \"Cancel\"}"
                                },
                                {
                                    "settingName": "hostedPaymentButtonOptions",
                                    "settingValue": "{\"text\": \"Pay\"}"
                                },
                                {
                                    "settingName": "hostedPaymentStyleOptions",
                                    "settingValue": "{\"bgColor\": \"blue\"}"
                                },
                                {
                                    "settingName": "hostedPaymentPaymentOptions",
                                    "settingValue": "{\"cardCodeRequired\": true, \"showCreditCard\": true, \"showBankAccount\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentSecurityOptions",
                                    "settingValue": "{\"captcha\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentShippingAddressOptions",
                                    "settingValue": "{\"show\": false, \"required\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentBillingAddressOptions",
                                    "settingValue": "{\"show\": true, \"required\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentCustomerOptions",
                                    "settingValue": "{\"showEmail\": false, \"requiredEmail\": false, \"addPaymentProfile\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentOrderOptions",
                                    "settingValue": "{\"show\": true, \"merchantName\": \"Mirrorsize US Inc\"}"
                                },
                                {
                                    "settingName": "hostedPaymentIFrameCommunicatorUrl",
                                    "settingValue": "{\"url\": \"http://services.mirrorsize.com\"}"
                                }
                            ]
                        }
                    }
                }
                this.showLoader = true;
                this.service.makePayment(data).subscribe(response => {
                    this.showLoader = false;
                    if (response.messages.resultCode == 'Ok') {
                        this.authorizetoken = response.token;
                        document.getElementById('headerShow').style.zIndex = '1';
                        document.getElementById('outerMain').style.zIndex = "9999";
                        document.getElementById('paymentmodal').style.background = 'rgba(0, 0, 0, 0.35)';
                        $('#paymentmodal').modal('show');
                        this.loadScript();
                    }
                    else {
                        $('#agreementModalFT').modal('hide');
                        $('#agreementModal').modal('hide');
                        Swal('Oops!', response.messages.message.text, 'error');
                    }
                })
            }
            else {
                Swal("Oops!", response.message, 'error');
                document.getElementById('headerShow').style.zIndex = '99999';
                document.getElementById('outerMain').style.zIndex = "0";
            }
        })
    }
  
    acceptTerms() {
        $('#agreementModal').modal('hide');
        this.upgrade();
    }
  
    acceptTermsFt() {
        $('#agreementModalFT').modal('hide');
        this.upgrade();
    }

}
