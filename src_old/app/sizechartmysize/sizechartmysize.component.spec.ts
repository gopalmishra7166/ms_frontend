import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SizechartmysizeComponent } from './sizechartmysize.component';

describe('SizechartmysizeComponent', () => {
  let component: SizechartmysizeComponent;
  let fixture: ComponentFixture<SizechartmysizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SizechartmysizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SizechartmysizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
