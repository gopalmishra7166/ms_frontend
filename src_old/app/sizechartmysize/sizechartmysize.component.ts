import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import * as S3 from 'aws-sdk/clients/s3';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { Router } from '@angular/router';
declare var $: any;
const Swal = require('sweetalert2');

@Component({
  selector: 'app-sizechartmysize',
  templateUrl: './sizechartmysize.component.html',
  styleUrls: ['./sizechartmysize.component.css']
})

export class SizechartmysizeComponent implements OnInit {
  muid = '';
  mid = '';
  data: any;
  email: any;
  token: any;
  accountId: any;
  timestamp: number;
  upflag = false;
  filedata: any;
  progressbar: boolean;
  pv = 0
  showLoader = false;
  public files: NgxFileDropEntry[] = [];
  product_type: any;
  paid: boolean;
  fileName='';


  constructor(private service: MsServices,private router:Router) { }

  ngOnInit() {
    var d = JSON.parse(localStorage.getItem('data'));
    this.email = d['emailId'];
    this.token = d['token'];
    this.accountId = d['accountId'];
    if (d['product'].MYSIZE.paymentStatus == 'ACTIVE') {
      this.paid = true;
      this.getfiles();
    }
    else
    {
      this.paid=false;
    }

    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Upload size chart";
  }

  uploadToS3() {
    this.showLoader = true;
     var self = this;
    const contentType = this.data.type;
    const bucket = new S3(
      {
        accessKeyId: 'AKIAIFXOEPN7XBYCUXBA',
        secretAccessKey: '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',
        region: 'us-east-1'
      }
    );

    this.fileName=self.data.name;

    this.timestamp = Date.now();
    var self = this;
    const params = {
      Bucket: 'miiror-vijay',
      Key: 'merchantuploads/' + this.accountId + '/sizecharts/' + this.timestamp+self.data.name,
      Body: this.data,
      ContentType: contentType,
    };
   
    bucket.putObject(params).on('httpUploadProgress', function (evt) {
      this.pv = (evt.loaded * 100) / evt.total
      // console.log("Uploaded :: " + (evt.loaded * 100) / evt.total+'%');
    }).send(function (err, data) {
      if (!err) {
        self.showLoader = false;
        self.s3fileupload();
        self.getfiles()

      } else {
        console.log(err);
      }
    });
  }

  getfiles() {
    var obj = { "emailId": this.email, "token": this.token, "productName": "MYSIZE", "fileType": "sizechart" }
    this.service.getfiles(obj).subscribe(response => {
      if (response.code == "1") {
        this.filedata = response.data;
      }
      else {
        Swal('error', response.message, 'error');
      }
    })
  }

  downloadfile(response) {
    window.open(response.filePath, '_blank');
  }

  deletefile(d) {
    var obj = { "emailId": this.email, "token": this.token, "id": d.id }
    Swal({

      title: 'Are you sure, want to delete this file?',
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '',
      cancelButtonColor: '',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,

    }).then((result) => {

      if (result.value) {
        this.service.removefile(obj).subscribe(response => {
          if (response.code == "1") {
            Swal('Success', response.message, 'success');
            this.getfiles()
          }
          else {
            Swal('error', response.message, 'error');
          }
        })
      }
    });

  }

  s3fileupload() {
    this.showLoader = true;
    var path = 'https://miiror-vijay.s3.amazonaws.com/merchantuploads/' + this.accountId + '/sizecharts/' + this.timestamp+this.fileName
    var obj = { "emailId": this.email, "token": this.token, "productName": "MYSIZE", "filePath": path, "fileType": "sizechart" }
    this.service.fileUpload(obj).subscribe(response => {
      if (response.code == "1") {
        this.showLoader = false;
        Swal('Success', response.message, 'success');
        this.getfiles();
        this.createTicket(path);
        this.data = '';
      }
      else {
        Swal('error', response.message, 'error');
      }
    })
    console.log("Success");
  }

  createTicket(path) {
    this.showLoader = true;
    var obj =
    {
      emailId: this.email,
      token: this.token,
      content: "Review and deploy size chart on the mirrorsize platform, sizechart path " + path,
      subject: "New Size chart Upload request"
    }
    this.service.raiseTicket(obj).subscribe(response => {
      this.showLoader = false;
      if (response.code == "1") {
        // Swal('success', response.message, 'success');
      }
      else {
        // Swal('error', response.message, 'error');
      }
    })
  }

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.data = file;
        });
      } else {
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public fileOver(event) {
    console.log(event);
  }

  public fileLeave(event) {
    console.log(event);
  }

  gotoSubscription()
  {
    localStorage.setItem('size','mysize')
    this.router.navigate(['/subscribe'])
  }

}