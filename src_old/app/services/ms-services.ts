import {ContactUs} from './../model/contactUs';
import {ProductMapping} from './../model/ProductMapping';
import {Http} from '@angular/http';
import {tap, map} from 'rxjs/operators';
import {URL_api} from './url_api';
import {Observable, Subject} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {Injectable, OnChanges, OnInit} from '@angular/core';
import {LoginData} from '../model/loginData';
import {MerchantInfo} from '../model/merchant-info';
import {CookieService} from 'ngx-cookie-service';
import {ResetPassword} from '../model/resetpassword';
import {requestData} from '../model/requestData';
import {postDataOfMerchantInfo} from '../model/postDataOfMerchantInfo';
import {MerchantPostInfo} from '../model/merchantPostInfo';
import {ProductData} from '../model/productData';
import {MissingInfo} from '../model/MissingInfo';
import {MerchantSpecificDataContainerArray} from '../SizeToFit/MerchantSpecificDataContainerArray';
import 'rxjs/add/operator/map';

var countriesData=[];
var phoneCode=[];

@Injectable()
export class MsServices implements OnInit {

    
    navbarhide = false;
    statechange: Subject<boolean> = new Subject<boolean>();
    merchantSpecificData: MerchantSpecificDataContainerArray[] = [];
    res: any;
    // for images of sizes with 36 points
    public download_url = 'https://s3.amazonaws.com/miiror-vijay/images/public/document/';
    // for testing so that we can get and set the data fromm local storage
    public getUserInfoFromLocalStorage = () => {
        return JSON.parse(localStorage.getItem('userInfo'));
    }
    public setUserInfoOnLocalStorage = (data) => {
        localStorage.setItem('userInfo', JSON.stringify(data));
    }

    constructor(public httpClient: HttpClient, private http: Http, private cookieService: CookieService) {
        this.statechange.next(this.navbarhide);
        this.navbarhide = false;
        /*        this.bhatuk();*/
    }

    ngOnInit() 
    {
       
    }


    loginUser(data)
    {
        return this.httpClient.post(URL_api.loginUser, data).map((res:any) =>{return res});
    }



    // size to fit mapped apparel
    sizeToFit()
    {
      return this.httpClient.get(URL_api.divisionSize + this.cookieService.get('email') + '/SIZE2FIT').map((res:any) => {return res});
    }

    // size 2 fit sizes param with other things
    getSize2Fit(): Observable<string> {
  
        return this.httpClient.get<String>(URL_api.getSizeToFitDetail + sessionStorage.getItem('uid') + '/' + 'SIZE2FIT' + '/' + sessionStorage.getItem('token') + '/').pipe(
            tap((res: string) => console.log(res))
        );
    }


    // mail verify
    getMissingInfo(): Observable<MissingInfo> {
      
        return this.httpClient.get<MissingInfo>(URL_api.getMissingInfo + '?email=' + this.cookieService.get('uid')).pipe(
            tap((res) => console.log(res))
        );
    }

// get measured update
    getMeasuredUpdate(measured): Observable<string> {
      
        return this.httpClient.post<String>(URL_api.getMeasuredUpdate, measured).pipe(
            tap((res: string) => console.log(res)
            )
        );
    }

    // --------//
    forupdateTolerencePost(updatedInfo){
      
        return this.httpClient.post(URL_api.postUpdateAllTolerence, updatedInfo).map((response:any)=>{return response});
    }


    // -------- get Tolerence and other points for edit--------//
    getFilledTolerences(measured)
    {
        return this.httpClient.post(URL_api.editAllTolerence, measured).map((response:any)=>{return response});
    }


    // size to fit
    getSizeToFitUpdate(measured): Observable<string> {
      
        return this.httpClient.post<String>(URL_api.getMeasuredUpdate, measured).pipe(
            tap((res: string) => console.log(res)
            )
        );
    }
    // ------ Accourding to date-------//
    pdfAccourdingToDate(startEndDate): Observable<string> {
     
        return this.httpClient.post<String>(URL_api.pdfAccourdingToDate, startEndDate).pipe(
            tap((res: string) => console.log(res)
            )
        );
    }


    pdfGmStoreDownload(JsonForSingleBrand): Observable<string> {
     
        return this.httpClient.post<String>(URL_api.pdfStoreGMDownload, JsonForSingleBrand).pipe(
            tap((res: string) => console.log(res)
            )
        );
    }

// graph
    getGraphData(mail): Observable<any> {
   
        return this.httpClient.get(URL_api.graphData, mail).pipe((res: any) => res
        );
    }

// posting update sizes

    size2FitUpdateSizeTable(measured): Observable<string> {
   
        return this.httpClient.post<String>(URL_api.updateSizesSize2Fit, measured).pipe(
            tap((res: string) => console.log(res)
            )
        );
    }

   
    getApparelDetail(apparelDetail): Observable<any> {
       
        return this.httpClient.post<any>(URL_api.apparelDetail, apparelDetail).pipe(
            tap((res: any) => console.log(res)
            ));
    }

// Size chart
    sizeChartApparel(apparelDetail): Observable<any> {
        return this.httpClient.post<any>(URL_api.sizeChart, apparelDetail).pipe(
            tap((res: any) => console.log(res)
            ));
    }

    // size update in size to fit
    sizeUpdate(updatedPoints): Observable<any> {
      
        return this.httpClient.post(URL_api.sizeChart, updatedPoints).map((response:any)=>{return response})
    }

    /*  res:any;*/
    deleteMerchantInfo(merchantInfo): Observable<any> {
        return this.httpClient.post<any>(URL_api.deleteMerchant, merchantInfo).pipe(
            tap((res: any) => console.log(res))
        );
    }

    signupUser(obj)
    {
          return this.httpClient.post(URL_api.signupUser,obj).map((response:any)=>{return response})
    }

    getApiDetail<T>(email): Observable<T> {
        return this.httpClient.get<T>(URL_api.apiDetails + email + '/key/');
    }

    updateMerchantInfo(data: MerchantInfo): Observable<String> {
     
        return this.httpClient.put<String>(URL_api.updatMerchant, data).pipe(
            tap((res: String) => console.log(res))
        );
    }

   

    // for apparelDetailSave
    apparelDetailsSave(parentData): Observable<any> {
        return this.httpClient.post<any>(URL_api.apparelDetailSave, parentData).pipe(
            tap((res: string) => console.log(res))
        );
    }

    // for apparel Table generation
    tableGeneration(apparelDetail): Observable<any> {
     
        const params = new HttpParams()
            .set('brandName', apparelDetail.brandName)
            .set('gender', apparelDetail.gender)
            .set('merchatID', apparelDetail.merchatID)
            .set('apparelName', apparelDetail.apparelName);

   
        return this.httpClient.post<any>(URL_api.apparelTable, apparelDetail).pipe(
            tap(res => console.log(res))
        );
    }

    public getListOfMeasuremnt<T>(): Observable<T> {
        return this.httpClient.get<T>(URL_api.host + '/api/merchant/get-measured/measurement/deatils/' + this.cookieService.get('uid') + '/');

    }

    public getdownloadFile<T>(input): Observable<T> {
        const params = new HttpParams()
            .set('measurementId', input.measurement_id)
            .set('merchantId', this.cookieService.get('email'))
            .set('custName', input.userName);

        return this.httpClient.get<T>(URL_api.downloadFile + params);
    }

    public getDownload<T>(down): Observable<T> {
        // const param = new HttpParams().set("",this.cookieService.get('email'));
        let headers = new HttpHeaders();


        headers = headers.set('Accept', 'application/pdf');
        return this.httpClient.get<T>(this.download_url + down, {headers: headers, responseType: 'blob' as 'json'});
    }

    resetPasswordmerchant1(reset: ResetPassword): Observable<String> {
      
        return this.httpClient.put<String>(URL_api.resetPassword, reset).pipe(tap((res: String) => console.log('Merchant Password Updated....' + res)));
    }

 

// -------------------------------

    public getCsp<T>(email): Observable<T> {
        return this.httpClient.get<T>(URL_api.getCspDetails);
    }

// from fixing issue by clone
    updateMerchantInfov2(data: MerchantPostInfo): Observable<String> {
     
        return this.httpClient.put<String>(URL_api.host + '/api/merchant/update/', data).pipe(
            tap((res: String) => console.log(res))
        );
    }

    gettingUserMeasurement(data: requestData): Observable<requestData> {
        return this.httpClient.post<requestData>(URL_api.host + '/api/merchant/get-measured/measurement/deatils/', data).pipe(
            tap((res) => console.log(res))
        );
    }

    public getCurrentMeasurement<T>(email): Observable<T> {
        return this.httpClient.get<T>(URL_api.host + '/api/service/getCurrentMeasurement/' + email + '/');
    }

    public getMerchantDetailsOfContactPerson<T>(email): Observable<T> {
        return this.httpClient.get<T>(URL_api.host + '/merchantInfo/' + email + '/');
    }

 

// updated sizes points
    public updateApparelSizes(updateSize): Observable<any> {
        return this.httpClient.post(URL_api.sizeUpdate, updateSize).pipe(
            tap((res: any) => console.log(res))
        );
    }

// for  logout User
    public logoutUser(logoutUserPoint): Observable<any> {
        return this.httpClient.post(URL_api.logout, logoutUserPoint).pipe(
            tap((res: any) => console.log(res))
        );
    }


// ------for all measurement points accourding to gender--------//
    public measurementPoints(measurementPoints): Observable<any> {
        return this.httpClient.post(URL_api.allMeasurementPoints, measurementPoints).map((response:any)=>{return response})
    }

// ----------17/01/2019---------Get measured filtered data with brand and apparel----------//
    public getMeasuredFilteredMeasurementPoints(points): Observable<any> {
        return this.httpClient.post(URL_api.gmMeasurementPointsWithBrand, points).pipe(
            tap((res: any) => console.log(res))
        );
    }
// ----------20/02/2019---------Admin disabling merchant----------//
    public adminDisablingMerchant(merchantId): Observable<any> {
        return this.httpClient.post(URL_api.merchantDisable, merchantId).pipe(
            tap((res: any) => console.log(res))
        );
    }


    // ----------Download complete pdf for size to fir for sizes and measurement points-------//
    public forSize2FitPdfSizes(data): Observable<any> {
        return this.httpClient.post(URL_api.pdfS2fCompletlyDownload, data).pipe(
            tap((res: any) => console.log(res))
        );
    }

    // ----------Download complete pdf for size to fir for sizes and measurement points-------//
    public forBlankApiInPriority(data): Observable<any> {
        return this.httpClient.post(URL_api.blankApiForData, data).pipe(
            tap((res: any) => console.log(res))
        );
    }
    // ----------For total clicks and failed clicks-------//
    public totalFailClicksGraph(data): Observable<any> {
        return this.httpClient.post(URL_api.totalClicksForGraph, data).pipe(
            tap((res: any) => console.log(res))
        );
    }
    // ----------For All Merchant clicks-------//
    public gettingAllMerchantInformations(data): Observable<any> {
        return this.httpClient.post(URL_api.gettingAllMerchant, data).pipe(
            tap((res: any) => console.log(res))
        );
    }
    // ----------Download complete pdf for Get measure for mapped apparel and measurement points-------//
    public getMeasuredMappedApparel(data): Observable<any> {
        return this.httpClient.post(URL_api.pdfGmCompletlyDownload, data).pipe(
            tap((res: any) => console.log(res))
        );
    }


    public getApparelSizes(): Observable<any> {
        return this.httpClient.get(URL_api.getSize + 'brandSize').pipe(
            tap((res: any) => console.log(res))
        );
    }

    // for navigation points
    public navigationPoints(ID): Observable<any> {
        return this.httpClient.get(URL_api.navigation + ID).pipe(
            tap((res: any) => console.log(res))
        );
    }
// for nonline api key
    public apiKeyGenerationForOnlineUser(Id): Observable<any> {
        return this.httpClient.post(URL_api.onlineApiKeyGeneration,Id).pipe(
            tap((res: any) => console.log(res))
        );
    }

// ------------------------------------------------------------
    updateMerchantInfoV2(data: postDataOfMerchantInfo): Observable<String> {
    
        return this.httpClient.put<String>(URL_api.updatMerchant, data).pipe(
            tap((res: String) => console.log(res))
        );
    }

    public getMerchantInfov2<T>(email): Observable<T> {
        return this.httpClient.get<T>(URL_api.merchantInfo + email + '/');
    }

  
    public subscribedProductStatus<T>(email): Observable<T> {
        return this.httpClient.get<T>(URL_api.host + '/api/sub/get/product/' + email + '/names/');
    }

    public getStoreMerchantData(email,page){
        return this.httpClient.get(URL_api.storeUserWithOneZero + email + '/0/'+page).map((response:any)=>{return response});
    }


    public getonlineMerchantData<T>(email,page): Observable<T> {
        return this.httpClient.get<T>(URL_api.storeUserWithOneZero + email + '/1/'+page);
    }

    updateProductMapping(data: ProductData): Observable<string> {
        return this.httpClient.post<string>(URL_api.host + '/api/sub/add/product/', data).pipe(
           
        );
    }

   
   
    public apparelRecommendation(data): Observable<String> {
       
        return this.httpClient.post<String>(URL_api.recommendation, data).pipe(
            tap((res) =>
                console.log(res))
        );
    }
    // ---------for clicks graph-------//
    public graphDataForClicksSpecifically(data): Observable<String> {
      
        return this.httpClient.post<String>(URL_api.graphDataForClicks, data).pipe(
            tap((res) =>
                console.log(res))
        );
    }
// -------For free trial and buy now product-------selection
    public notificationsFreeTrialOrBuyNowAccourdingToProductType(data): Observable<String> {
      
        return this.httpClient.post<String>(URL_api.notificationForFreeTrialAndBuyNow, data).pipe(
            tap((res) =>
                console.log(res))
        );
    }
    // --------For billing pdf generation as on 13/fe/2019-------//
   

    public purchaseNowYearday(data): Observable<String> {
       
        return this.httpClient.post<String>(URL_api.purchaseYear, data).pipe(
            tap((res) =>
                console.log(res))
        );
    }
// -----for removing size 2 fit selected sizes-----//
    public size2FitSelectedSizeRemove(data): Observable<String> {
    
        return this.httpClient.post<String>(URL_api.removeSizeSize2Fit, data).pipe(
            tap((res) =>
                console.log(res))
        );
    }


  
    public productMappedOrNot(data){
        return this.httpClient.post(URL_api.checkForProductMapSize2Fit, data).map((response:any)=>{return response})
    }


    // store size to fit
    public getstoreSizeToFit(email): Observable<any> {
     
        return this.httpClient.get<any>(URL_api.storeSizeToFit + email + '/').pipe(
            tap((res) =>
                console.log(res))
        );
    }

// ---for get measured store-----//

    public getSize2FitStoreData(email): Observable<any> {
     
        return this.httpClient.get<any>(URL_api.allKindOfData + email + '&product_type=store&product_name=SIZE2FIT&status=complete').pipe(
            tap((res) =>
                console.log(res, 'after getting data.....'))
        );
    }


    public getSize2FitOnlineData(email): Observable<any> {
    
        return this.httpClient.get<any>(URL_api.allKindOfData + email + '&product_type=online&product_name=SIZE2FIT&status=complete').pipe(
            tap((res) =>
                console.log(res, 'after getting data.....'))
        );
    }


    // free trial
    public dateTime(email): Observable<any> {
   
        return this.httpClient.get<any>(URL_api.freeTrial + email).pipe(
            tap((res) =>console.log(res)));
    }// ----{observe: 'response'}-----


    public getAccessCode<T>(email): Observable<T> {
        return this.httpClient.get<T>(URL_api.host + '/api/merchant/update_merchant_assecc_code?merchantId=' + email);
    }



    deleteProductMapping<T>(email): Observable<T> {
        return this.httpClient.get<T>(URL_api.host + '/api/deleteProducts?merchantID=' + email);
    }

    public getAcceessCodeDetail<T>(email): Observable<T> {
        return this.httpClient.get<T>(URL_api.host + '/api/merchantCtrl/getHistoryData?merchantId=' + email);
    }
  

    graphData(obj)
    {
        return this.httpClient.post(URL_api.host + '/api/graph/getgraph',obj).map((response:any)=>{return response});
    }

    graphMaleFemale(obj)
    {
        return this.httpClient.post(URL_api.host + '/api/GraphController/graphdataMaleFemale',obj).map((response:any)=>{return response});
    }

    getAllCountries()
    {
        return this.httpClient.get("https://restcountries.eu/rest/v2/all").map((response:any)=>{return response})
    }
    merchantRegistration(obj)
    {
        return this.httpClient.post(URL_api.host + '/api/merchant/registration',obj).map((response:any)=>{return response});
    }

    getAgreement(obj)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/getagreement',obj).map((response:any)=>{return response});
     
    }

    getProfileData(obj)
    {
      return this.httpClient.post(URL_api.host+'/api/merchantCtrl/getMerchantInfo/',obj).map((response:any)=>{return response});
    }

    updateCompanyProfile(obj)
    {
      return this.httpClient.post(URL_api.host+'/api/merchantCtrl/updateCompanyInfo/',obj).map((response:any)=>{return response});
    }

    changePassword(obj)
    {
      return this.httpClient.post(URL_api.host+'/api/merchant/updatepassword',obj).map((response:any)=>{return response});
    }


    archiveData(obj)
    {
      return this.httpClient.post(URL_api.host+'/api/widgetController/archiveData',obj).map((response:any)=>{return response});
    }

    getApiKey(obj)
    {
      return this.httpClient.post(URL_api.host+'/api/merchantCtrl/getApikey',obj).map((response:any)=>{return response});
    }
    public billingPdfGeneration(data)
    {
        return this.httpClient.post(URL_api.host+'/api/pdf/getuserreport', data).map((response:any) =>{return response});
    }

    contactUs(obj)
    {
      return this.httpClient.post(URL_api.host + '/api/utils/contactform/',obj).map((response:any)=>{return response})
    }
    
    createSubUser(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/createsubuser/',data).map((response:any)=>{return response})
    }

    getSubUsers(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/getaccountlist/',data).map((response:any)=>{return response})
    }

    updateAccountStatus(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/updateaccountstatus/',data).map((response:any)=>{return response})
    }

    getActivityLogs(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/getactivitylog/',data).map((response:any)=>{return response})

    }

    verifyEmail(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/verifyEmail/',data).map((response:any)=>{return response})
    }
    
    sendOtp(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchantCtrl/resentOtp/',data).map((response:any)=>{return response})
    }

    getApparel(data)
    {
      return this.httpClient.post(URL_api.host + '/api/data/getapparels/',data).map((response:any)=>{return response})
    }

    getMeasurementPoints(data)
    {
      return this.httpClient.post(URL_api.host + '/api/data/merchantmp/',data).map((response:any)=>{return response})
    }  

    addApparel(data)
    {
      return this.httpClient.post(URL_api.host + '/api/data/addapparel/',data).map((response:any)=>{return response})
    }  

    getUserList(data)
    {
      return this.httpClient.post(URL_api.host + '/api/data/getuserlist/',data).map((response:any)=>{return response})
    }

    processIdData(data)
    {
      return this.httpClient.post(URL_api.host + '/api/data/processiddata/',data).map((response:any)=>{return response})
    }

    getUserPdf(data)
    {
      return this.httpClient.post(URL_api.host + '/api/pdf/getuserpdf/',data).map((response:any)=>{return response})
    }

    searchApparel(data)
    { 
      return this.httpClient.post(URL_api.host + '/api/data/searchapparel',data).map((response:any)=>{return response})
    }
   
    searchUserList(data)
    { 
      return this.httpClient.post(URL_api.host + '/api/data/searchuserlist',data).map((response:any)=>{return response})
    }

    updateAccessCode(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/updateaccesscode',data).map((response:any)=>{return response})
    }

    getAccessCodeHistory(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/getaccesscodehistory',data).map((response:any)=>{return response})
    }

    updateProfile(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/editmerchantdetail',data).map((response:any)=>{return response})
    }

    getMesurementGuide(data)
    {
      return this.httpClient.post(URL_api.host + '/api/utils/mpguide',data).map((response:any)=>{return response})

    }

    raiseTicket(data)
    {
      return this.httpClient.post(URL_api.host + '/api/ticket/raiseticket',data).map((response:any)=>{return response})
    }

    getTickets(data)
    {
      return this.httpClient.post(URL_api.host + '/api/ticket/getticketlist',data).map((response:any)=>{return response})
    }

    updateTicketStatus(data)
    {
      return this.httpClient.post(URL_api.host + '/api/ticket/updateticketstatus',data).map((response:any)=>{return response})
    }

    changeAproval(data)
    {
      return this.httpClient.post(URL_api.host + '/api/merchant/changeaproval',data).map((response:any)=>{return response})
    }

    makePayment(data)
    {
      return this.httpClient.post('https://api.authorize.net/xml/v1/request.api',data).map((response:any)=>{return response})
    }

    resendOtp(obj)
    {
      return this.httpClient.post(URL_api.host +'/api/merchant/resendotp',obj).map((response:any)=>{return response})
    }

    recoverPassword(obj)
    {
        return this.httpClient.post(URL_api.host +'/api/merchant/forgetpassword/',obj).map((response:any)=>{return response})
    }

    upgradePlan(obj)
    {
        return this.httpClient.post(URL_api.host +'/api/merchant/upgradeplan/',obj).map((response:any)=>{return response})
    }

    fileUpload(obj)
    {
        return this.httpClient.post(URL_api.host +'/api/data/uploadfile/',obj).map((response:any)=>{return response})
    }
    
    getfiles(obj)
    {
        return this.httpClient.post(URL_api.host +'/api/data/getfiles/',obj).map((response:any)=>{return response})
    }

    removefile(obj)
    {
        return this.httpClient.post(URL_api.host +'/api/data/removefile/',obj).map((response:any)=>{return response})
    }

    getuserlogs(obj)
    {
      return this.httpClient.post(URL_api.host +'/api/msmysize/getuserlogs/',obj).map((response:any)=>{return response})
    }

    searchuserlogs(obj){
      return this.httpClient.post(URL_api.host +'/api/msmysize/searchuserlogs/',obj).map((response:any)=>{return response})
    }

    getMeasurementQuickSize(obj)
    {
      return this.httpClient.post('http://192.168.0.128:8080'+'/api/quicksize/getrecommendation',obj).map((response:any)=>{return response})     
    }

    imageViewApproval(obj)
    {
      return this.httpClient.post(URL_api.host +'/api/merchant/checkimageagreement',obj).map((response:any)=>{return response})    
    }

    changeStatus(obj)
    {
      return this.httpClient.post(URL_api.host +'/api/msmysize/rewardstatus',obj).map((response:any)=>{return response})    
    }

    // public getJSON():Observable<any>  
    // {
    //   return this.http.get("http://localhost:4200/assets/states.json");
    // }

    isLoggedIn()
    {
        var t=localStorage.getItem("data");
        if(t){
            return true;
        }
        else
        {
            return false;
        }
    }

    getProductType()
    {
      if(this.isLoggedIn())
      {
        var item=localStorage.getItem('productType');
        return item;
      }
    }
    getpaymenthistorydata(obj)
    {
      return this.httpClient.post(URL_api.host +'/api/merchant/getplanhistory/',obj).map((response:any)=>{return response})
    }

    setreward(obj){
      return this.httpClient.post(URL_api.host +'/api/msmysize/setreward/',obj).map((response:any)=>{return response})
    }

    getrewarddata(obj){
      return this.httpClient.post(URL_api.host +'/api/msmysize/getrewarddata/',obj).map((response:any)=>{return response})
    }

    searchuserlistqs(obj){
      return this.httpClient.post(URL_api.host +'/api/msquicksize/searchuserlist/',obj).map((response:any)=>{return response})
    }

    countriesData=[
        {
          "countryName": "United Arab Emirates",
          "countryCode": "AE",
          "stateName": "Abu Dhabi",
          "stateCode": ""
        },
        {
          "countryName": "United Arab Emirates",
          "countryCode": "AE",
          "stateName": "Ajman",
          "stateCode": ""
        },
        {
          "countryName": "United Arab Emirates",
          "countryCode": "AE",
          "stateName": "Al Ain",
          "stateCode": ""
        },
        {
          "countryName": "United Arab Emirates",
          "countryCode": "AE",
          "stateName": "Dubai",
          "stateCode": ""
        },
        {
          "countryName": "United Arab Emirates",
          "countryCode": "AE",
          "stateName": "Fujairah",
          "stateCode": ""
        },
        {
          "countryName": "United Arab Emirates",
          "countryCode": "AE",
          "stateName": "Ras-al-Khaimah",
          "stateCode": ""
        },
        {
          "countryName": "United Arab Emirates",
          "countryCode": "AE",
          "stateName": "Sharjah",
          "stateCode": ""
        },
        {
          "countryName": "United Arab Emirates",
          "countryCode": "AE",
          "stateName": "Umm-al-Quawain",
          "stateCode": ""
        },
        {
          "countryName": "Antigua and Barbuda",
          "countryCode": "AG",
          "stateName": "San Juan",
          "stateCode": ""
        },
        {
          "countryName": "Antigua and Barbuda",
          "countryCode": "AG",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Antigua and Barbuda",
          "countryCode": "AG",
          "stateName": "St James",
          "stateCode": ""
        },
        {
          "countryName": "Antigua and Barbuda",
          "countryCode": "AG",
          "stateName": "St John",
          "stateCode": ""
        },
        {
          "countryName": "Antigua and Barbuda",
          "countryCode": "AG",
          "stateName": "St. George",
          "stateCode": ""
        },
        {
          "countryName": "Antigua and Barbuda",
          "countryCode": "AG",
          "stateName": "St. John",
          "stateCode": ""
        },
        {
          "countryName": "Antigua and Barbuda",
          "countryCode": "AG",
          "stateName": "WI",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Abaco Is",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Alajuela",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Antioquia",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Antofagasta",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Anzoategui",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Aragua",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Caldas",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Carabobo",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Cauls Pond",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Cundinamarca",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Curacao",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "D. C.",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "D. N.",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "D.F.",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Gr Bahama",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Gr Cayman",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Grand Turk",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Guatemala",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Guayas",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Heredia",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Kingston",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Los Santos",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Managua",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Miranda",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "New Prov",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "North",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "North Hill",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Nte Sant",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Panama",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Pembroke",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Pichincha",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Quest",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Risaralda",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "San Jose",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "San Pedro",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "San Salv",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Santiago",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "St. John",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "The Valley",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Tortola",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Valle",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "West End",
          "stateCode": ""
        },
        {
          "countryName": "Anguilla",
          "countryCode": "AI",
          "stateName": "Zulia",
          "stateCode": ""
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "BUENOS AIRES",
          "stateCode": "BA"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "Buenos Aires City",
          "stateCode": ""
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "CATAMARCA",
          "stateCode": "CT"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "CHACO",
          "stateCode": "CC"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "CHUBUT",
          "stateCode": "CH"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "CORDOBA",
          "stateCode": "CB"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "CORRIENTES",
          "stateCode": "CN"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "ENTRE RIOS",
          "stateCode": "ER"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "FEDERAL DISTRICT",
          "stateCode": ""
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "FORMOSA",
          "stateCode": "FM"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "JUJUY",
          "stateCode": "JY"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "LA PAMPA",
          "stateCode": "LP"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "LA RIOJA",
          "stateCode": "LR"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "MENDOZA",
          "stateCode": "MZ"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "MISIONES",
          "stateCode": "MN"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "NEUQUEN",
          "stateCode": "NQ"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "RIO NEGRO",
          "stateCode": "RN"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "SALTA",
          "stateCode": "SA"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "SAN JUAN",
          "stateCode": "SJ"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "SAN LUIS",
          "stateCode": "SL"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "SANTA CRUZ",
          "stateCode": "SC"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "SANTA FE",
          "stateCode": "SF"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "Santiago del",
          "stateCode": ""
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "SANTIAGO DEL ESTERO",
          "stateCode": "SE"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "Tierra del Fu",
          "stateCode": ""
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "TIERRA DEL FUEGO",
          "stateCode": "TF"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "TUCUMAN",
          "stateCode": "TM"
        },
        {
          "countryName": "Argentina",
          "countryCode": "AR",
          "stateName": "Tucum n",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "American Samoa",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "APIA",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "CO",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "IN",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "MA",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "MD",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "PAGO PAGO",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "RI",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "SAVALALO",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "TA'U-MANU'A",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "Tafuna",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "TENNESSE",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "TX",
          "stateCode": ""
        },
        {
          "countryName": "American Samoa",
          "countryCode": "AS",
          "stateName": "VA",
          "stateCode": ""
        },
        {
          "countryName": "Austria",
          "countryCode": "AT",
          "stateName": "Burgenland",
          "stateCode": "BU"
        },
        {
          "countryName": "Austria",
          "countryCode": "AT",
          "stateName": "Salzburg",
          "stateCode": "SZ"
        },
        {
          "countryName": "Austria",
          "countryCode": "AT",
          "stateName": "Steiermark",
          "stateCode": ""
        },
        {
          "countryName": "Austria",
          "countryCode": "AT",
          "stateName": "Tirol",
          "stateCode": ""
        },
        {
          "countryName": "Austria",
          "countryCode": "AT",
          "stateName": "Vienna",
          "stateCode": "WI"
        },
        {
          "countryName": "Austria",
          "countryCode": "AT",
          "stateName": "Vorarlberg",
          "stateCode": "VO"
        },
        {
          "countryName": "Austria",
          "countryCode": "AT",
          "stateName": "Wien",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "ARG",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "AUS",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "AUSTRALIAN CAPITAL TERRITORY",
          "stateCode": "ACT"
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "CAN",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "CHI",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "DEN",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "FIJ",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "FPO",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "GHA",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "HK",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "IND",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "JAP",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "MAL",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "NEW SOUTH WALES",
          "stateCode": "NSW"
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "NORTHERN TERRITORY",
          "stateCode": "NT"
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "NS",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "NZ",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "PER",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "PHI",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "PNG",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "QL",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "QUEENSLAND",
          "stateCode": "QLD"
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "SAF",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "SIN",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "SOUTH AUSTRALIA",
          "stateCode": "SA"
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "SUS",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "TAI",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "TASMANIA",
          "stateCode": "TAS"
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "THA",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "TUR",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "UAR",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "UK",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "USA",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "VI",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "VICTORIA",
          "stateCode": "VIC"
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "VIE",
          "stateCode": ""
        },
        {
          "countryName": "Australia",
          "countryCode": "AU",
          "stateName": "WESTERN AUSTRALIA",
          "stateCode": "WA"
        },
        {
          "countryName": "Aruba",
          "countryCode": "AW",
          "stateName": "A. Verapaz",
          "stateCode": ""
        },
        {
          "countryName": "Aruba",
          "countryCode": "AW",
          "stateName": "Aruba",
          "stateCode": ""
        },
        {
          "countryName": "Aruba",
          "countryCode": "AW",
          "stateName": "Curacao",
          "stateCode": ""
        },
        {
          "countryName": "Aruba",
          "countryCode": "AW",
          "stateName": "Mandiki",
          "stateCode": ""
        },
        {
          "countryName": "Aruba",
          "countryCode": "AW",
          "stateName": "Noord",
          "stateCode": ""
        },
        {
          "countryName": "Aruba",
          "countryCode": "AW",
          "stateName": "Orange Wk",
          "stateCode": ""
        },
        {
          "countryName": "Aruba",
          "countryCode": "AW",
          "stateName": "Oranjestad",
          "stateCode": ""
        },
        {
          "countryName": "Aruba",
          "countryCode": "AW",
          "stateName": "Santa Cruz",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "Bridgetown",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "Christ Church",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "Gr Cayman",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "Granada",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "Pembroke",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "San Miguel",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St Andrew",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St James",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St John",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St Joseph",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St Lucy",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St Maarten",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St Michael",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St Peter",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St Philip",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St Thomas",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St. George",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St. James",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "St. Thomas",
          "stateCode": ""
        },
        {
          "countryName": "Barbados",
          "countryCode": "BB",
          "stateName": "W.I.",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Bangkok",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Barisal",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Bogra",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Chittagong",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Colombo",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Comilla",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Dhaka",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Dinajpur",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Dong Thap",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Karachi",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Karnataka",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Kulna",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Kushtia",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Mymensigh",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Noakhalr",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Pabna",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Rajshahi",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Rangpur",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Sylhet",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Tangail",
          "stateCode": ""
        },
        {
          "countryName": "Bangladesh",
          "countryCode": "BD",
          "stateName": "Thuan An",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "ANTWERP",
          "stateCode": "AN"
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "ANTWERPEN",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "ANVERS",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "BRABANT",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "BRABANT FLAMAND",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "BRABANT WALLON",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "BRUSSEL-HOOFDSTAD",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "BRUSSELS-CAPITAL",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "BRUXELLES-CAPITALE",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "EAST-FLANDERS",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "FLANDRE - OCCIDENTALE",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "FLANDRE - ORIENTALE",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "FLEMISH BRABANT",
          "stateCode": "VB"
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "HAINAUT",
          "stateCode": "HT"
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "HENEGOUWEN",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "LIEGE",
          "stateCode": "LG"
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "LIMBOURG",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "LIMBURG",
          "stateCode": "LI"
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "LUIK",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "LUTTICH",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "LUXEMBOURG",
          "stateCode": "LX"
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "LUXEMBURG",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "NAMEN",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "NAMUR",
          "stateCode": "NA"
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "OOST-VLAANDEREN",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "VLAAMS BRABANT",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "WAALS BRABANT",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "WALLOON BRABANT",
          "stateCode": "BW"
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "WEST-FLANDERS",
          "stateCode": ""
        },
        {
          "countryName": "Belgium",
          "countryCode": "BE",
          "stateName": "WEST-VLAANDEREN",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Artibonite",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Curacao",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Devonshire",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Gr Cayman",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Hamilton",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Kowloon",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "New Prov",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Pedernales",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Pembroke",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Sandy's",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Smith's",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Southampton",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "St David",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Tortola",
          "stateCode": ""
        },
        {
          "countryName": "Bermuda",
          "countryCode": "BM",
          "stateName": "Warwick",
          "stateCode": ""
        },
        {
          "countryName": "Brunei Darussalam",
          "countryCode": "BN",
          "stateName": "Belait",
          "stateCode": ""
        },
        {
          "countryName": "Brunei Darussalam",
          "countryCode": "BN",
          "stateName": "Brunei & Muara",
          "stateCode": ""
        },
        {
          "countryName": "Brunei Darussalam",
          "countryCode": "BN",
          "stateName": "Chittagong",
          "stateCode": ""
        },
        {
          "countryName": "Brunei Darussalam",
          "countryCode": "BN",
          "stateName": "Ragama",
          "stateCode": ""
        },
        {
          "countryName": "Brunei Darussalam",
          "countryCode": "BN",
          "stateName": "Tutong",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Beni",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Bolivar",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Cercado",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Chuquisaca",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Cochabamba",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Hamilton",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "La Paz",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Los Andes",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Murillo",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Oruro",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Pando",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Potosi",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "San Miguel",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Santa Ana",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Santa Cruz",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Santa Rosa",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Sucre",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Tarija",
          "stateCode": ""
        },
        {
          "countryName": "Bolivia",
          "countryCode": "BO",
          "stateName": "Trinity",
          "stateCode": ""
        },
        {
          "countryName": "Bonaire, Sint Eustatius and Saba",
          "countryCode": "BQ",
          "stateName": "Bonaire",
          "stateCode": ""
        },
        {
          "countryName": "Bonaire, Sint Eustatius and Saba",
          "countryCode": "BQ",
          "stateName": "Saba",
          "stateCode": ""
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "ACRE",
          "stateCode": "AC"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "ALAGOAS",
          "stateCode": "AL"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "AMAPA",
          "stateCode": "AP"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "AMAZONAS",
          "stateCode": "AM"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "BAHIA",
          "stateCode": "BA"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "Be",
          "stateCode": ""
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "Br",
          "stateCode": ""
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "Ca",
          "stateCode": ""
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "CEARA",
          "stateCode": "CE"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "DISTRITO FEDERAL",
          "stateCode": "DF"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "ESPIRITO SANTO",
          "stateCode": "ES"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "GOIAS",
          "stateCode": "GO"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "MARANHAO",
          "stateCode": "MA"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "MATO GROSSO",
          "stateCode": "MT"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "MATO GROSSO DO SUL",
          "stateCode": "MS"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "MINAS GERAIS",
          "stateCode": "MG"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "NORTH RIO GRANDE",
          "stateCode": ""
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "PARA",
          "stateCode": "PA"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "PARAIBA",
          "stateCode": "PB"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "PARANA",
          "stateCode": "PR"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "PERNAMBUCO",
          "stateCode": "PE"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "PIAUI",
          "stateCode": "PI"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "Ri",
          "stateCode": ""
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "RIO DE JANEIRO",
          "stateCode": "RJ"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "RIO GRANDE DO NORTE",
          "stateCode": "RN"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "RIO GRANDE DO SUL",
          "stateCode": "RS"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "RONDONIA",
          "stateCode": "RO"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "RORAIMA",
          "stateCode": "RR"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "Sa",
          "stateCode": ""
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "SANTA CATARINA",
          "stateCode": "SC"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "SAO PAULO",
          "stateCode": "SP"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "SERGIPE",
          "stateCode": "SE"
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "SOUTH MATO GROSSO",
          "stateCode": ""
        },
        {
          "countryName": "Brazil",
          "countryCode": "BR",
          "stateName": "TOCANTINS",
          "stateCode": "TO"
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Abaco Is",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Abaco Isla",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Andros",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Bimini Is",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Curacao",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Eleuthera",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Exuma",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Gr Bahama",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Gr Cayman",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Gr Inagua",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Grand Cay",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Hamilton",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Harbour Is",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Inagua",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Lucaya",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Nassau",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "New Prov",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Pembroke",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Providence",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Southampton",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Bahamas",
          "countryCode": "BS",
          "stateName": "Tortola",
          "stateCode": ""
        },
        {
          "countryName": "Belize",
          "countryCode": "BZ",
          "stateName": "Ambergris",
          "stateCode": ""
        },
        {
          "countryName": "Belize",
          "countryCode": "BZ",
          "stateName": "Barinas",
          "stateCode": ""
        },
        {
          "countryName": "Belize",
          "countryCode": "BZ",
          "stateName": "Belize",
          "stateCode": ""
        },
        {
          "countryName": "Belize",
          "countryCode": "BZ",
          "stateName": "Cayo",
          "stateCode": ""
        },
        {
          "countryName": "Belize",
          "countryCode": "BZ",
          "stateName": "Corozal",
          "stateCode": ""
        },
        {
          "countryName": "Belize",
          "countryCode": "BZ",
          "stateName": "Orange Wk",
          "stateCode": ""
        },
        {
          "countryName": "Belize",
          "countryCode": "BZ",
          "stateName": "San Pedro",
          "stateCode": ""
        },
        {
          "countryName": "Belize",
          "countryCode": "BZ",
          "stateName": "Stann Ck",
          "stateCode": ""
        },
        {
          "countryName": "Belize",
          "countryCode": "BZ",
          "stateName": "Toledo",
          "stateCode": ""
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "ALBERTA",
          "stateCode": "AB"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "BRITISH COLUMBIA",
          "stateCode": "BC"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "MANITOBA",
          "stateCode": "MB"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "NEW BRUNSWICK",
          "stateCode": "NB"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "NEWFOUNDLAND",
          "stateCode": ""
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "NORTHWEST TERRITORIES",
          "stateCode": "NT"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "NOVA SCOTIA",
          "stateCode": "NS"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "NUNAVUT",
          "stateCode": "NU"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "ONTARIO",
          "stateCode": "ON"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "PRINCE EDWARD ISLAND",
          "stateCode": "PE"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "QUEBEC",
          "stateCode": "QC"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "SASKATCHEWAN",
          "stateCode": "SK"
        },
        {
          "countryName": "Canada",
          "countryCode": "CA",
          "stateName": "YUKON TERRITORY",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "AARGAU",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "APPENZELL AUSSERRHODEN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "APPENZELL INNERRHODEN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "APPENZELL RHODES-EXTÉRIEURES",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "APPENZELL RHODES-INTÉRIEURES",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "APPENZELL-OUTER RHODES",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "APPENZELLO ESTERNO",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "APPENZELLO INTERNO",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "ARGOVIA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "ARGOVIE",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "BASEL-LAND",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "BASEL-STADT",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "BASILEA-CAMPAGNA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "BASILEA-CITTÀ",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "BERN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "BERNA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "BERNE",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "BS",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "FL",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "FREIBURG",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "FRIBOURG",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "FRIBURGO",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GENEVA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GENÈVE",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GENF",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GINEVRA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GIURA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GLARIS",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GLARONA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GLARUS",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GRAUBÜNDEN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GRIGIONI",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "GRISONS",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "I",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "IT",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "JURA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "LUCERNA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "LUCERNE",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "LUZERN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "NEUCHÂTEL",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "NEUENBURG",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "NIDWALD",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "NIDWALDEN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "NIDWALDO",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "OBWALD",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "OBWALDEN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "OBWALDO",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SAINT GALLEN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SAN GALLO",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SANKT GALLEN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SCHAFFHAUSEN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SCHAFFHOUSE",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SCHWYZ",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SCIAFFUSA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SOLETTA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SOLEURE",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SOLOTHURN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "ST-GALL",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "SVITTO",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "TESSIN",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "THURGAU",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "THURGOVIE",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "TICINO",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "TURGOVIA",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "URI",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "VALAIS",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "VALLESE",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "VAUD",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "WAADT",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "WALLIS",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "ZOUG",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "ZUG",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "ZUGO",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "ZÜRICH",
          "stateCode": ""
        },
        {
          "countryName": "Switzerland",
          "countryCode": "CH",
          "stateName": "ZURIGO",
          "stateCode": ""
        },
        {
          "countryName": "Cook Islands",
          "countryCode": "CK",
          "stateName": "AITUTAKI",
          "stateCode": ""
        },
        {
          "countryName": "Cook Islands",
          "countryCode": "CK",
          "stateName": "Cook Islands",
          "stateCode": ""
        },
        {
          "countryName": "Cook Islands",
          "countryCode": "CK",
          "stateName": "England",
          "stateCode": ""
        },
        {
          "countryName": "Cook Islands",
          "countryCode": "CK",
          "stateName": "MANIHIKI",
          "stateCode": ""
        },
        {
          "countryName": "Cook Islands",
          "countryCode": "CK",
          "stateName": "NIUE",
          "stateCode": ""
        },
        {
          "countryName": "Cook Islands",
          "countryCode": "CK",
          "stateName": "PR",
          "stateCode": ""
        },
        {
          "countryName": "Cook Islands",
          "countryCode": "CK",
          "stateName": "RARATONGA",
          "stateCode": ""
        },
        {
          "countryName": "Cook Islands",
          "countryCode": "CK",
          "stateName": "RAROTONGA",
          "stateCode": ""
        },
        {
          "countryName": "Cook Islands",
          "countryCode": "CK",
          "stateName": "WA",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "A. Verapaz",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Aisen",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Antartica Chilena",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Antioquia",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Antofagasta",
          "stateCode": "AN"
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Arauca",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Araucania",
          "stateCode": "AR"
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Arauco",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Arica",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Bio-Bio",
          "stateCode": "BI"
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Cachapoal",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Capitan Prat",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Cardenal Caro",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Cauquenes",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Cautin",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Chacabuco",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Chanaral",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Chiloe",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Choapa",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Coihaique",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Colchagua",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Concepcion",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Copiapo",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Coquimbo",
          "stateCode": "CO"
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Cordillera",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Curico",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "El Loa",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Elqui",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "General Carrera",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Huasco",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Iquique",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Isla de Pascua",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "La Union",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Lagos",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Las Condes",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Limari",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Linares",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Llanquihue",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Los Andes",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Los Lagos",
          "stateCode": "LL"
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Los Rios",
          "stateCode": "LR"
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Magallanes",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Maipo",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Malleco",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Marga Marga",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Maule",
          "stateCode": "ML"
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Melipilla",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Metropolitan Region",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Nuble",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "O Higgins",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Osorno",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Ovalle",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Palena",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Parinacota",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Petorca",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Providence",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Puntarenas",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Quillota",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Ranco",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Reg VIII",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Region X",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "San Antonio",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "San Felipe",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "San Miguel",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Santa Ana",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Santander",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Santiago",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Talagante",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Talca",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Tamarugal",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Tarapaca",
          "stateCode": "TA"
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Temuco",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Tierra Del Fuego",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Tocopilla",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Ultima Esperanza",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "V Region",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Valdivia",
          "stateCode": ""
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Valparaiso",
          "stateCode": "VS"
        },
        {
          "countryName": "Chile",
          "countryCode": "CL",
          "stateName": "Victoria",
          "stateCode": ""
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Anhui",
          "stateCode": "AH"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Beijing",
          "stateCode": "BJ"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Chongqing",
          "stateCode": "CQ"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Fujian",
          "stateCode": "FJ"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Gansu",
          "stateCode": "GS"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Guangdong",
          "stateCode": "GD"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Guangxi",
          "stateCode": ""
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Guizhou",
          "stateCode": "GZ"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Hainan",
          "stateCode": "HA"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Hebei",
          "stateCode": "HB"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Heilongjiang",
          "stateCode": "HL"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Henan",
          "stateCode": "HE"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Hubei",
          "stateCode": "HU"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Hunan",
          "stateCode": "HN"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Inner Mongolia",
          "stateCode": ""
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Jiangsu",
          "stateCode": "JS"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Jiangxi",
          "stateCode": "JX"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Jilin",
          "stateCode": "JL"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Liaoning",
          "stateCode": "LN"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Ningxia",
          "stateCode": ""
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Qinghai",
          "stateCode": "QH"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Shaanxi",
          "stateCode": "SA"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Shandong",
          "stateCode": "SD"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Shanghai",
          "stateCode": "SH"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Shanxi",
          "stateCode": "SX"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Sichuan",
          "stateCode": "SC"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Tianjin",
          "stateCode": "TJ"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Xinjiang",
          "stateCode": ""
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Xizang",
          "stateCode": "XZ"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Yunnan",
          "stateCode": "YN"
        },
        {
          "countryName": "China",
          "countryCode": "CN",
          "stateName": "Zhejiang",
          "stateCode": "ZJ"
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Altagracia",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Amazonas",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Antioquia",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Arauca",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Atlantico",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Barranquil",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Bolivar",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Boyaca",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Cabanas",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Caldas",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Caqueta",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Casanare",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Cauca",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Cesar",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Choco",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Cordoba",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Cundinamar",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Cundinamarca",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "D. C.",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "D.C.",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "D.F.",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Guajira",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Guaviare",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Huila",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Islas S A",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Magdalena",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Meta",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Narino",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Nte Sant",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Panama",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Providence",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Putumayo",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Quindio",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Risaralda",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "San Andres",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Santander",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Sucre",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Tolima",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Tortola",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Valle",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Vaupes",
          "stateCode": ""
        },
        {
          "countryName": "Colombia",
          "countryCode": "CO",
          "stateName": "Vichada",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Alajuela",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Altagracia",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Bolivar",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Cartago",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Coronie",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Cortes",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "D. C.",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Escazu",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Guanacaste",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Guayas",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Heredia",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Herrera",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Limon",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Magdalena",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Managua",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Morazan",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Panama",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Puntarenas",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "San Antonio",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "San Cristo",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "San Felipe",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "San Jose",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "San Juan",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "San Miguel",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "San Pedro",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Santa Ana",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Talamanca",
          "stateCode": ""
        },
        {
          "countryName": "Costa Rica",
          "countryCode": "CR",
          "stateName": "Vicente",
          "stateCode": ""
        },
        {
          "countryName": "CuraÃ§ao",
          "countryCode": "CW",
          "stateName": "Curacao",
          "stateCode": ""
        },
        {
          "countryName": "Christmas Island",
          "countryCode": "CX",
          "stateName": "BEIJING",
          "stateCode": ""
        },
        {
          "countryName": "Christmas Island",
          "countryCode": "CX",
          "stateName": "GEORGE TOWN",
          "stateCode": ""
        },
        {
          "countryName": "Christmas Island",
          "countryCode": "CX",
          "stateName": "GUANG XI",
          "stateCode": ""
        },
        {
          "countryName": "Christmas Island",
          "countryCode": "CX",
          "stateName": "GUANGDONG",
          "stateCode": ""
        },
        {
          "countryName": "Christmas Island",
          "countryCode": "CX",
          "stateName": "LIAONING",
          "stateCode": ""
        },
        {
          "countryName": "Czech Republic",
          "countryCode": "CZ",
          "stateName": "",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "BADEN-WUERTTEMBERG",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Bayern",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Berlin",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Brandenburg",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Bremen",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Hamburg",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Hessen",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Mecklenburg-Vorpommern",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Niedersachsen",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Nordrhein-Westfalen",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Rheinland-Pfalz",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Saarland",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Sachsen",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Sachsen-Anhalt",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "Schleswig-Holstein",
          "stateCode": ""
        },
        {
          "countryName": "Germany",
          "countryCode": "DE",
          "stateName": "THUERINGEN",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "BORNHOLMS",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "Frederiksberg kommune",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "FREDERIKSBORG",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "FYN",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "Hovedstaden",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "Midtjylland",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "Nordjylland",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "RIBE",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "RINGKOBING",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "ROSKILDE",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "SONDERJYLLAND",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "STORSTR?M",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "STORSTROM",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "STORSTRØM",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "Syddanmark",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "VEJLE",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "VESTSJAELLAND",
          "stateCode": ""
        },
        {
          "countryName": "Denmark",
          "countryCode": "DK",
          "stateName": "VIBORG",
          "stateCode": ""
        },
        {
          "countryName": "Dominica",
          "countryCode": "DM",
          "stateName": "D. N.",
          "stateCode": ""
        },
        {
          "countryName": "Dominica",
          "countryCode": "DM",
          "stateName": "New Prov",
          "stateCode": ""
        },
        {
          "countryName": "Dominica",
          "countryCode": "DM",
          "stateName": "St Andrew",
          "stateCode": ""
        },
        {
          "countryName": "Dominica",
          "countryCode": "DM",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Dominica",
          "countryCode": "DM",
          "stateName": "St John",
          "stateCode": ""
        },
        {
          "countryName": "Dominica",
          "countryCode": "DM",
          "stateName": "St Paul",
          "stateCode": ""
        },
        {
          "countryName": "Dominica",
          "countryCode": "DM",
          "stateName": "St. George",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Altagracia",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Azua",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Bahoruco",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Barahona",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Bavaro",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Carchi",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "D. C.",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "D. N.",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "D.Amacuro",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "D.F.",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Dajabon",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Duarte",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "El Seibo",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Espaillat",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Hato Mayor",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Heredia",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Herrera",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Higuey",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Independencia",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "La Romana",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "La Vega",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Maria Trinidad Sanchez",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Mons Nouel",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Monte Plata",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Montecristi",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "MontePlata",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Pedernales",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Peravia",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Providence",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Puerto Plata",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Punta Cana",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Puntarenas",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Salcedo",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Samana",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "San Benito",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "San Cristo",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "San Cristobal",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "San Jose de Ocoa",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "San Juan",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "San Miguel",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "San Pedro",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Sanchez Ramirez",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Santiago",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Santo Domingo",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Slias Pina",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Stg Rdguez",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Sto Dom",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Tenares",
          "stateCode": ""
        },
        {
          "countryName": "Dominican Republic",
          "countryCode": "DO",
          "stateName": "Valverde",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d Adrar",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d Ain Defla",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d Ain Temouchent",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d Alger",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d Annaba",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d El Bayadh",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d El Oued",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d El Tarf",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d Illizi",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d Oran",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d Ouargla",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya d Oum El Bouaghi",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Batna",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Bechar",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Bejaia",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Biskra",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Blida",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Bordj Bou Arreridj",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Bouira",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Boumerdes",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Chlef",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Constantine",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Djelfa",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Ghardaia",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Guelma",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Jijel",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Khenchela",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Laghouat",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de M Sila",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Mascara",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Medea",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Mila",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Mostaganem",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Naama",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Relizane",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Saida",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Setif",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Sidi Bel Abbes",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Skikda",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Souk Ahras",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Tamanrasset",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Tebessa",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Tiaret",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Tindouf",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Tipaza",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Tissemsilt",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Tizi Ouzou",
          "stateCode": ""
        },
        {
          "countryName": "Algeria",
          "countryCode": "DZ",
          "stateName": "Wilaya de Tlemcen",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Atuntaqui",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Azua",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Azuay",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Belize",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Bolivar",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Canar",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Carchi",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Chimaltenango",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Chimborazo",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Chingoraza",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Cotopaxi",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "D. N.",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "El Oro",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Esmeraldas",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Galapagos",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Guayas",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Ibarra",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Imbabura",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Latacunga",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Libertad",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Loja",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Los Rios",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "M Santiago",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Manabi",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Miranda",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Morona",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Morona Stg",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Napo",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Orellana",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Pastaza",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Pichincha",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Portoviejo",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Quijos",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Quito",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "S Do Tsach",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "S Dom Tsac",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "S Domingo",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Sachilas",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "San Cristobal",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Santa Cruz",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Santa Elena",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Santiago",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Santo Domi",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Santo Domingo",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Sta Elena",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Sto Dgo Tsachilas",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Sucumbios",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Tarapoa",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Tena",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Tungurahua",
          "stateCode": ""
        },
        {
          "countryName": "Ecuador",
          "countryCode": "EC",
          "stateName": "Zamora",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "ALAVA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "ALBACETE",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "ALICANTE",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "ALMERIA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "ASTURIAS (OVIEDO)",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "AVILA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "BADAJOZ",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "BALEARES",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "BARCELONA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "BURGOS",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "CACERES",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "CADIZ",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "CANTABRIA (SANTANDER)",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "CASTELLON",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "CEUTA",
          "stateCode": "CE"
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "CIUDAD REAL",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "CORDOBA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "CUENCA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "GIRONA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "GRANADA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "GUADALAJARA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "GUIPUZCOA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "HUELVA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "HUESCA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "JAEN",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "LA CORUÑA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "LA RIOJA (LOGRONO)",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "LEON",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "LERIDA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "LUGO",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "MADRID",
          "stateCode": "MD"
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "MALAGA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "MELILLA",
          "stateCode": "ML"
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "MURCIA",
          "stateCode": "MC"
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "NAVARRA",
          "stateCode": "NC"
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "ORENSE",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "PALENCIA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "PALMAS (LAS)",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "PONTEVEDRA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "SALAMANCA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "SEGOVIA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "SEVILLA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "SORIA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "TARRAGONA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "TENERIFE",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "TERUEL",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "TOLEDO",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "VALENCIA",
          "stateCode": "VC"
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "VALLADOLID",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "VIZCAYA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "ZAMORA",
          "stateCode": ""
        },
        {
          "countryName": "Spain",
          "countryCode": "ES",
          "stateName": "ZARAGOZA",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "AHVENANMAA",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "HAME",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "KAINUU",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "Kanta-Häme",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "KESKI-POHJANMAA",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "Keski-Suomi",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "KUOPIO",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "Kymenlaakso",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "KYMI",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "LAPPI",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "MIKKELI",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "OULU",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "PIRKANMAA",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "POHJANMAA",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "POHJOIS-KARJALA",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "POHJOIS-POHJANMAA",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "POHJOIS-SAVO",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "Satakunta",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "TURKU JA PORI",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "UUSIMAA",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "VAASA",
          "stateCode": ""
        },
        {
          "countryName": "Finland",
          "countryCode": "FI",
          "stateName": "Varsinais-Suomi",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "BA PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "CENTRAL",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "DENARAU",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "DEU",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "FIJI",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "FIJI ISLAND",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "FJ",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "FJI",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "LAMI",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "LAUTOKA",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "NADI",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "NANDI",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "NASINU",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "REWA",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "SAVU SAVU",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "SUV",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "SUVA",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "VANUA LEVU",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "VITI LEVU",
          "stateCode": ""
        },
        {
          "countryName": "Fiji",
          "countryCode": "FJ",
          "stateName": "YALALEVU",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "BOZEN",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "CAROLINE ISLANDS",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "CHUUK",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "D.F",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "Federated State of Micronesia",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "FM",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "FSM",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "KOSRAE",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "Ngardmau",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "PALAU",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "POH",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "POHNPEI",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "POHNPEI FEDERATED STATES",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "TAHITI",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "YAP",
          "stateCode": ""
        },
        {
          "countryName": "Micronesia, Federated States Of",
          "countryCode": "FM",
          "stateName": "YAP STATE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "AIN",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "AISNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ALLIER",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ALPES DE HAUTE PROVENCE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ALPES MARITIMES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ALSACE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ALSACE-CHAMPAGNE-ARDENNE-LORRAINE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "AQUITAINE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "AQUITAINE-LIMOUSIN-POITOU-CHARENTES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ARDECHE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ARDENNES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ARIEGE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "AUBE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "AUDE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "AUVERGNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "AVEYRON",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "BAS RHIN",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "BASSE-NORMANDIE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "BOUCHES DU RHONE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "BOURGOGNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "BOURGOGNE-FRANCHE-COMTE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "BRETAGNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CALVADOS",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CANTAL",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CENTRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CENTRE-VAL DE LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CHAMPAGNE-ARDENNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CHARENTE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CHARENTE MARITIME",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CHER",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CORREZE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CORSE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "COTE D OR",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "COTES D ARMOR",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "COTES-DU-NORD",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "CREUZE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "DEUX SEVRES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "DORDOGNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "DOUBS",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "DROME",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ESSONNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "EURE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "EURE ET LOIR",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "FINISTERE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "GARD",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "GERS",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "GIRONDE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "GUADELOUPE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "GUYANE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUT RHIN",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTE GARONNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTE LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTE MARNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTE SAONE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTE SAVOIE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTE VIENNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTE-NORMANDIE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTES ALPES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTES PYRENEES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HAUTS DE SEINE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "HERAULT",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ILE-DE-FRANCE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ILLE ET VILAINE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "INDRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "INDRE ET LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ISERE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "JURA",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LANDES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LANGUEDOC-ROUSSILLON",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LANGUEDOC-ROUSSILLON-MIDI-PYRENEES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LIMOUSIN",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LOIR ET CHER",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LOIRE ATLANTIQUE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LOIRET",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LORRAINE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LOT",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LOT ET GARONNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "LOZERE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "MAINE ET LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "MANCHE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "MARNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "MARTINIQUE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "MAYENNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "MEURTHE ET MOSELLE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "MEUSE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "MORBIHAN",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "MOSELLE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "NIEVRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "NORD",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "NORD-PAS-DE-CALAIS",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "NORD-PAS-DE-CALAIS-PICARDIE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "NORMANDIE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "OISE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "ORNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "PARIS",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "PAS DE CALAIS",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "Pays Etrangers",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "PAYS-DE-LA-LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "PICARDIE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "POITOU-CHARENTE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "PUY DE DOME",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "PYRENEES ATLANTIQUES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "PYRENEES ORIENTALES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "REUNION",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "RHONE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "RHÔNE-ALPES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "SAONE ET LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "SARTHE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "SAVOIE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "SEINE ET MARNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "SEINE MARITIME",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "SEINE SAINT DENIS",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "SOMME",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "TARN",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "TARN ET GARONNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "TERRITOIRE DE BELFORT",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "VAL D OISE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "VAL DE MARNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "VAR",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "VAUCLUSE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "VENDEE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "VIENNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "VOSGES",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "YONNE",
          "stateCode": ""
        },
        {
          "countryName": "France",
          "countryCode": "FR",
          "stateName": "YVELINES",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "Carriacou",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "Granada",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "Grand'Anse",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "St Andrew",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "St David",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "St John",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "St Patrick",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "St. George",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "St. Peter",
          "stateCode": ""
        },
        {
          "countryName": "Grenada",
          "countryCode": "GD",
          "stateName": "St.Georges",
          "stateCode": ""
        },
        {
          "countryName": "French Guiana",
          "countryCode": "GF",
          "stateName": "CENTRE-VAL DE LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "French Guiana",
          "countryCode": "GF",
          "stateName": "GUADELOUPE",
          "stateCode": ""
        },
        {
          "countryName": "French Guiana",
          "countryCode": "GF",
          "stateName": "GUYANE",
          "stateCode": ""
        },
        {
          "countryName": "French Guiana",
          "countryCode": "GF",
          "stateName": "ILE-DE-FRANCE",
          "stateCode": ""
        },
        {
          "countryName": "French Guiana",
          "countryCode": "GF",
          "stateName": "MARTINIQUE",
          "stateCode": ""
        },
        {
          "countryName": "French Guiana",
          "countryCode": "GF",
          "stateName": "PAYS-DE-LA-LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "French Guiana",
          "countryCode": "GF",
          "stateName": "PROVENCE-ALPES-COTE D'AZUR",
          "stateCode": ""
        },
        {
          "countryName": "Greenland",
          "countryCode": "GL",
          "stateName": "Gr?nland",
          "stateCode": ""
        },
        {
          "countryName": "Greenland",
          "countryCode": "GL",
          "stateName": "Grønland",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "BRETAGNE",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "CENTRE-VAL DE LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "CORSE",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "GUADELOUPE",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "ILE-DE-FRANCE",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "LANGUEDOC-ROUSSILLON-MIDI-PYRENEES",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "MARTINIQUE",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "NORMANDIE",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "PROVENCE-ALPES-COTE D'AZUR",
          "stateCode": ""
        },
        {
          "countryName": "Guadeloupe",
          "countryCode": "GP",
          "stateName": "REUNION",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "A. Verapaz",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Alta Verap",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Amatitlan",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "B. Verapaz",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Chimaltenango",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Chiquimula",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Coban",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "D. C.",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "D.F.",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Elqui",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Escuintla",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Guatemala",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Guayas",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Huehuetenango",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Izabal",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Jalapa",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Jutiapa",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "La Liberta",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "m",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Managua",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Mixco",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Peten",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Progreso",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Quezaltenango",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Quiche",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Retalhuleu",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "S.Catarina",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Sacatepeq",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "San Marcos",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "San Miguel",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Santa Rosa",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Solola",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Suchitepeq",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Totonicapan",
          "stateCode": ""
        },
        {
          "countryName": "Guatemala",
          "countryCode": "GT",
          "stateName": "Zacapa",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "AGANA",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "BARRIGADA",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "CHALAN SANANTONIO",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "Dededo",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "GMF",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "GU",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "GUA",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "GUAM",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "HAGATNA",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "HATAGNA",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "MP",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "NT",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "Piti",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "SAN SALVADOR",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "TAMUNING",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "TG",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "TUMON",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "US",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "USA",
          "stateCode": ""
        },
        {
          "countryName": "Guam",
          "countryCode": "GU",
          "stateName": "VIC",
          "stateCode": ""
        },
        {
          "countryName": "Guyana",
          "countryCode": "GY",
          "stateName": "Berbice",
          "stateCode": ""
        },
        {
          "countryName": "Guyana",
          "countryCode": "GY",
          "stateName": "Demerara",
          "stateCode": ""
        },
        {
          "countryName": "Guyana",
          "countryCode": "GY",
          "stateName": "Essiquibo",
          "stateCode": ""
        },
        {
          "countryName": "Guyana",
          "countryCode": "GY",
          "stateName": "Georgetown",
          "stateCode": ""
        },
        {
          "countryName": "Guyana",
          "countryCode": "GY",
          "stateName": "Kingston",
          "stateCode": ""
        },
        {
          "countryName": "Guyana",
          "countryCode": "GY",
          "stateName": "Progreso",
          "stateCode": ""
        },
        {
          "countryName": "Guyana",
          "countryCode": "GY",
          "stateName": "Zacapa",
          "stateCode": ""
        },
        {
          "countryName": "Hong Kong",
          "countryCode": "HK",
          "stateName": "HONG KONG",
          "stateCode": ""
        },
        {
          "countryName": "Hong Kong",
          "countryCode": "HK",
          "stateName": "KOWLOON",
          "stateCode": ""
        },
        {
          "countryName": "Hong Kong",
          "countryCode": "HK",
          "stateName": "NEW TERRITORIES",
          "stateCode": ""
        },
        {
          "countryName": "Hong Kong",
          "countryCode": "HK",
          "stateName": "OUTLYING ISLANDS",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "A. Verapaz",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Atlantico",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Atlantida",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Bay Island",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Choluteca",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Colon",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Comayagua",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Copan",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Cordoba",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Cortes",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "D. C.",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "D.C.",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "D.F.",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "El Paraiso",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "F. Morazan",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Guatemala",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Honduras",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Intibuca",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Isla Bahia",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Jutiapa",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "La Paz",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Lempira",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "MDC",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Morazan",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Ocotepeque",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Olancho",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "S. Barbara",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "San Marcos",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "San Pedro",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Santa Rosa",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "St Barbara",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Tegucigalp",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Valle",
          "stateCode": ""
        },
        {
          "countryName": "Honduras",
          "countryCode": "HN",
          "stateName": "Yoro",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Arcahaie",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Artibonite",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "C F",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "CARAIB",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "caraibe",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Centre",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Ganthier",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Grand'Anse",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Haiti",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Jacmel",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Kenscoff",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Montrouis",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Nippes",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Nord",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Nord Est",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Nord Quest",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Ouest",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Ouest dep",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Port au Pr",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Quest",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "South",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "South East",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Sud",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "Sud Est",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "West",
          "stateCode": ""
        },
        {
          "countryName": "Haiti",
          "countryCode": "HT",
          "stateName": "West End",
          "stateCode": ""
        },
        {
          "countryName": "Hungary",
          "countryCode": "HU",
          "stateName": "Budapest",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Aceh",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Bali",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Balikpapan",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Bandung",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Bangkok",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Banjarmasin",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Basilan Province",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Bataan",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Batam",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Bekasi",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Bengkulu",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Bogor",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Can Tho",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Central Java",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Central Kalimantan",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Central Sulawesi",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Denpasar",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "East Java",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "East Kalimantan",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "East Nusa Tenggara",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "East Timor",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Gresik",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Ho Chi Minh City",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Irian Jaya",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Jaffna",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Jakarta",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Jambi",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Jogya",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Kediri",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Lampang",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Lampung",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Maluku",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Medan",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "North Sulawesi",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "North Sumatra",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Palembang",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Pontianak",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Purwakarta",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Riau",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Samut Prakan",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Semarang",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Solo",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "South Kalimantan",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "South Sulawesi",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "South Sumatra",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "South-East Sulawesi",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Surabaya",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Tangerang",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Tanjung Pinang",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Thuan An",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "West Bengal",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "West Java",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "West Kalimantan",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "West Nusa Tenggara",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "West Sumatra",
          "stateCode": ""
        },
        {
          "countryName": "Indonesia",
          "countryCode": "ID",
          "stateName": "Yogyakarta",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO CARLOW",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO CAVAN",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO CLARE",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO CORK",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO DONEGAL",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO DUBLIN",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO GALWAY",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO KERRY",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO KILDARE",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO KILKENNY",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO LAOIS",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO LEITRIM",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO LIMERICK",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO LONGFORD",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO LOUTH",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO MAYO",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO MEATH",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO MONAGHAN",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO OFFALY",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO ROSCOMMON",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO SLIGO",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO TIPPERARY",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO WATERFORD",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO WESTMEATH",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO WEXFORD",
          "stateCode": ""
        },
        {
          "countryName": "Ireland",
          "countryCode": "IE",
          "stateName": "CO WICKLOW",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Andaman & Nicobar",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Andaman & Nicobar Islands",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Andhra Pradesh",
          "stateCode": "AP"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Arunachal Pradesh",
          "stateCode": "AR"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "AS",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Assam",
          "stateCode": "AS"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Bihar",
          "stateCode": "BR"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Chandigarh",
          "stateCode": "CH"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Chhattisgarh",
          "stateCode": "CT"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Dadra & Nagar Haveli",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Daman & Diu",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Delhi",
          "stateCode": "DL"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Goa",
          "stateCode": "GA"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Gujarat",
          "stateCode": "GJ"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Haryana",
          "stateCode": "HR"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Himachal Pradesh",
          "stateCode": "HP"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Ho Chi Minh City",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Hyderabad",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Jammu & Kashmir",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Jharkhand",
          "stateCode": "JH"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Kanchanaburi",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Karnataka",
          "stateCode": "KA"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Kerala",
          "stateCode": "KL"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Lakshadweep",
          "stateCode": "LD"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Madhya Pradesh",
          "stateCode": "MP"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Maharashtra",
          "stateCode": "MH"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Manipur",
          "stateCode": "MN"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Meghalaya",
          "stateCode": "ML"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Mindoro Oriental",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Mizoram",
          "stateCode": "MZ"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Nagaland",
          "stateCode": "NL"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "New Delhi",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Odisha",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Puducherry",
          "stateCode": "PY"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Punjab",
          "stateCode": "PB"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Rajasthan",
          "stateCode": "RJ"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Rajshahi",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Sikkim",
          "stateCode": "SK"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Tamil Nadu",
          "stateCode": "TN"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Tamilnadu",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Telangana",
          "stateCode": ""
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Tripura",
          "stateCode": "TR"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Uttar Pradesh",
          "stateCode": "UP"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "Uttarakhand",
          "stateCode": "UT"
        },
        {
          "countryName": "India",
          "countryCode": "IN",
          "stateName": "West Bengal",
          "stateCode": "WB"
        },
        {
          "countryName": "Iran (Islamic Republic Of)",
          "countryCode": "IR",
          "stateName": "Islamabad",
          "stateCode": ""
        },
        {
          "countryName": "Iran (Islamic Republic Of)",
          "countryCode": "IR",
          "stateName": "Jakarta",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "AGRIGENTO",
          "stateCode": "AG"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "ALESSANDRIA",
          "stateCode": "AL"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "ANCONA",
          "stateCode": "AN"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "AOSTA",
          "stateCode": "AO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "AREZZO",
          "stateCode": "AR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "ASCOLI PICENO",
          "stateCode": "AP"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "ASTI",
          "stateCode": "AT"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "AVELLINO",
          "stateCode": "AV"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "BARI",
          "stateCode": "BA"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "BELLUNO",
          "stateCode": "BL"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "BENEVENTO",
          "stateCode": "BN"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "BERGAMO",
          "stateCode": "BG"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "BIELLA",
          "stateCode": "BI"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "BOLOGNA",
          "stateCode": "BO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "BOLZANO",
          "stateCode": "BZ"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "BRESCIA",
          "stateCode": "BS"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "BRINDISI",
          "stateCode": "BR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CAGLIARI",
          "stateCode": "CA"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CALTANISSETTA",
          "stateCode": "CL"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CAMPOBASSO",
          "stateCode": "CB"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CARBONIA-IGLESIAS",
          "stateCode": "CI"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CASERTA",
          "stateCode": "CE"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CATANIA",
          "stateCode": "CT"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CATANZARO",
          "stateCode": "CZ"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CHIETI",
          "stateCode": "CH"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "COMO",
          "stateCode": "CO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "COSENZA",
          "stateCode": "CS"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CREMONA",
          "stateCode": "CR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CROTONE",
          "stateCode": "KR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "CUNEO",
          "stateCode": "CN"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "ENNA",
          "stateCode": "EN"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "FERMO",
          "stateCode": "FM"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "FERRARA",
          "stateCode": "FE"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "FIRENZE",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "FOGGIA",
          "stateCode": "FG"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "FORLI'-CESENA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "FROSINONE",
          "stateCode": "FR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "GENOVA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "GORIZIA",
          "stateCode": "GO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "GROSSETO",
          "stateCode": "GR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "IMPERIA",
          "stateCode": "IM"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "ISERNIA",
          "stateCode": "IS"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "L' AQUILA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "LA SPEZIA",
          "stateCode": "SP"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "LATINA",
          "stateCode": "LT"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "LECCE",
          "stateCode": "LE"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "LECCO",
          "stateCode": "LC"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "LIVORNO",
          "stateCode": "LI"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "LODI",
          "stateCode": "LO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "LUCCA",
          "stateCode": "LU"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "MACERATA",
          "stateCode": "MC"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "MANTOVA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "MASSA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "MATERA",
          "stateCode": "MT"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "MEDIO CAMPIDANO",
          "stateCode": "VS"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "MESSINA",
          "stateCode": "ME"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "MILANO",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "MODENA",
          "stateCode": "MO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "MONZA E BRIANZA",
          "stateCode": "MB"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "NAPOLI",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "NOVARA",
          "stateCode": "NO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "NUORO",
          "stateCode": "NU"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "OGLIASTRA",
          "stateCode": "OG"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "OLBIA-TEMPIO",
          "stateCode": "OT"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "ORISTANO",
          "stateCode": "OR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PADOVA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PALERMO",
          "stateCode": "PA"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PARMA",
          "stateCode": "PR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PAVIA",
          "stateCode": "PV"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PERUGIA",
          "stateCode": "PG"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PESARO E URBINO",
          "stateCode": "PU"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PESCARA",
          "stateCode": "PE"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PIACENZA",
          "stateCode": "PC"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PISA",
          "stateCode": "PI"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PISTOIA",
          "stateCode": "PT"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PORDENONE",
          "stateCode": "PN"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "POTENZA",
          "stateCode": "PZ"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "PRATO",
          "stateCode": "PO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "RAGUSA",
          "stateCode": "RG"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "RAVENNA",
          "stateCode": "RA"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "REGGIO CALABRIA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "REGGIO EMILIA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "RIETI",
          "stateCode": "RI"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "RIMINI",
          "stateCode": "RN"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "ROMA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "ROVIGO",
          "stateCode": "RO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "SALERNO",
          "stateCode": "SA"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "SASSARI",
          "stateCode": "SS"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "SAVONA",
          "stateCode": "SV"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "SIENA",
          "stateCode": "SI"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "SIRACUSA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "SONDRIO",
          "stateCode": "SO"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "TARANTO",
          "stateCode": "TA"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "TERAMO",
          "stateCode": "TE"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "TERNI",
          "stateCode": "TR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "TORINO",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "TRAPANI",
          "stateCode": "TP"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "TRENTO",
          "stateCode": "TN"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "TREVISO",
          "stateCode": "TV"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "TRIESTE",
          "stateCode": "TS"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "UDINE",
          "stateCode": "UD"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "VARESE",
          "stateCode": "VA"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "VENEZIA",
          "stateCode": ""
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "VERBANO-CUSIO-OSSOLA",
          "stateCode": "VB"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "VERCELLI",
          "stateCode": "VC"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "VERONA",
          "stateCode": "VR"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "VIBO VALENTIA",
          "stateCode": "VV"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "VICENZA",
          "stateCode": "VI"
        },
        {
          "countryName": "Italy",
          "countryCode": "IT",
          "stateName": "VITERBO",
          "stateCode": "VT"
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "BlackRiver",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "Clarendon",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "Gr Bahama",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "Gr Cayman",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "Hanover",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "Kingston",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "Manchester",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "Portland",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "St Andrew",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "St Ann",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "St Cather",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "St Elizabeth",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "St James",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "St Mary",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "St Thomas",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "St. Anne",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "Trelawny",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "W.I.",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "Westmoreld",
          "stateCode": ""
        },
        {
          "countryName": "Jamaica",
          "countryCode": "JM",
          "stateName": "WI",
          "stateCode": ""
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "AICHI",
          "stateCode": "AI"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "AKITA",
          "stateCode": "AK"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "AOMORI",
          "stateCode": "AO"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "CHIBA",
          "stateCode": "CH"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "EHIME",
          "stateCode": "EH"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "FUKUI",
          "stateCode": "FI"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "FUKUOKA",
          "stateCode": "FO"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "FUKUSHIMA",
          "stateCode": "FS"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "GIFU",
          "stateCode": "GF"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "GUNMA",
          "stateCode": ""
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "HIROSHIMA",
          "stateCode": "HS"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "HOKKAIDO",
          "stateCode": "HK"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "HYOGO",
          "stateCode": "HG"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "IBARAKI",
          "stateCode": "IB"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "ISHIKAWA",
          "stateCode": "IS"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "IWATE",
          "stateCode": "IW"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "KAGAWA",
          "stateCode": "KG"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "KAGOSHIMA",
          "stateCode": "KS"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "KANAGAWA",
          "stateCode": "KN"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "KOCHI",
          "stateCode": "KC"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "KUMAMOTO",
          "stateCode": "KM"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "KYOTO",
          "stateCode": "KY"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "MIE",
          "stateCode": "ME"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "MIYAGI",
          "stateCode": "MG"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "MIYAZAKI",
          "stateCode": "MZ"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "NAGANO",
          "stateCode": "NN"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "NAGASAKI",
          "stateCode": "NS"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "NARA",
          "stateCode": "NR"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "NIIGATA",
          "stateCode": "NI"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "OITA",
          "stateCode": "OT"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "OKAYAMA",
          "stateCode": "OY"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "OKINAWA",
          "stateCode": "ON"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "OSAKA",
          "stateCode": "OS"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "SAGA",
          "stateCode": "SG"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "SAITAMA",
          "stateCode": "ST"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "SHIGA",
          "stateCode": "SH"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "SHIMANE",
          "stateCode": "SM"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "SHIZUOKA",
          "stateCode": "SZ"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "TOCHIGI",
          "stateCode": "TC"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "TOKUSHIMA",
          "stateCode": "TS"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "TOKYO",
          "stateCode": "TK"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "TOTTORI",
          "stateCode": "TT"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "TOYAMA",
          "stateCode": "TY"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "WAKAYAMA",
          "stateCode": "WK"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "YAMAGATA",
          "stateCode": "YT"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "YAMAGUCHI",
          "stateCode": "YC"
        },
        {
          "countryName": "Japan",
          "countryCode": "JP",
          "stateName": "YAMANASHI",
          "stateCode": "YN"
        },
        {
          "countryName": "Kiribati",
          "countryCode": "KI",
          "stateName": "TARAWA",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "(in Nevis)",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "Basseterre",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "Christ Church",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "Nevis",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "Port Louis",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "St John",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "St Paul",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "St Peter",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "St. James",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "St. Mary",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "St. Paul",
          "stateCode": ""
        },
        {
          "countryName": "Saint Kitts and Nevis",
          "countryCode": "KN",
          "stateName": "St. Thomas",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "BUSAN",
          "stateCode": "PU"
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "CHEJU-DO",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "CHOLLABUK-DO",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "CHOLLANAM-DO",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "CHUNGBUK",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "CHUNGCHONGBUK-DO",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "CHUNGNAM",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "DAEGU",
          "stateCode": "TG"
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "DAEJEON",
          "stateCode": "TJ"
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "GANGWON",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "GWANGJU",
          "stateCode": "KJ"
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "GYEONGBUK",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "GYEONGGI",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "GYEONGNAM",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "Hong Kong Island",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "INCHEON",
          "stateCode": "IN"
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "INCHON",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "JEJU",
          "stateCode": "CJ"
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "JEONBUK",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "JEONNAM",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "KANGWON-DO",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "KWANGJU",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "KYONGGI-DO",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "KYONGSANGBUK-DO",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "KYONGSANGNAM-DO",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "New Territories",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "PUSAN",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "SEOUL",
          "stateCode": "SO"
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "TAEGU",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "TAEJON",
          "stateCode": ""
        },
        {
          "countryName": "Republic of Korea",
          "countryCode": "KR",
          "stateName": "ULSAN",
          "stateCode": "UL"
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "Abaco Is",
          "stateCode": ""
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "Cayman Br",
          "stateCode": ""
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "CAYMAN BRAC",
          "stateCode": ""
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "Gr Bahama",
          "stateCode": ""
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "Grand Cay",
          "stateCode": ""
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "GRAND CAYMAN",
          "stateCode": ""
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "New Prov",
          "stateCode": ""
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "St Michael",
          "stateCode": ""
        },
        {
          "countryName": "Cayman Islands",
          "countryCode": "KY",
          "stateName": "Tortola",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Akmolinskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Aktyubinskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Almatinskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Atyrauskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Karagandinskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Kostanaiskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Kyzylordinskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Mangistauskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Pavlodarskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Severo-Kazakhstanskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Vostochno-Kazakhstanskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Yuzhno-Kazakhstanskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Zapadno-Kazakhstanskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Kazakhstan",
          "countryCode": "KZ",
          "stateName": "Zhambylskaya obl.",
          "stateCode": ""
        },
        {
          "countryName": "Laos",
          "countryCode": "LA",
          "stateName": "Chanthaburi",
          "stateCode": ""
        },
        {
          "countryName": "Saint Lucia",
          "countryCode": "LC",
          "stateName": "Castries",
          "stateCode": ""
        },
        {
          "countryName": "Saint Lucia",
          "countryCode": "LC",
          "stateName": "Gros Islet",
          "stateCode": ""
        },
        {
          "countryName": "Saint Lucia",
          "countryCode": "LC",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Saint Lucia",
          "countryCode": "LC",
          "stateName": "St Lucia",
          "stateCode": ""
        },
        {
          "countryName": "Saint Lucia",
          "countryCode": "LC",
          "stateName": "St Lucy",
          "stateCode": ""
        },
        {
          "countryName": "Saint Lucia",
          "countryCode": "LC",
          "stateName": "Vieux Fort",
          "stateCode": ""
        },
        {
          "countryName": "Saint Lucia",
          "countryCode": "LC",
          "stateName": "Zacapa",
          "stateCode": ""
        },
        {
          "countryName": "Liechtenstein",
          "countryCode": "LI",
          "stateName": "FL",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Abra",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Amparal",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "An Giang",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Aurora Province",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Badulla",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Batticaloa",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Colombo",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Dhaka",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Galle",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Hambantota",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Jaffna",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Kalutara",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Kandy",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Kurunegala",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Mannar",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Matale",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Matara",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Polonnaruwa",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Puttalam",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Ragama",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Ratnapura",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Trincomalee",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Vavuniya",
          "stateCode": ""
        },
        {
          "countryName": "Sri Lanka",
          "countryCode": "LK",
          "stateName": "Western Samar",
          "stateCode": ""
        },
        {
          "countryName": "Monaco",
          "countryCode": "MC",
          "stateName": "TERRITOIRES D'OUTRE-MER",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "ACT",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "AJELTAKE ISLAND",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "CAROLINE ISLANDS",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "CHU",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "Majleo",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "MAJURO",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "MH",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "MI",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "MP",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "NSW",
          "stateCode": ""
        },
        {
          "countryName": "Marshall Islands",
          "countryCode": "MH",
          "stateName": "REPUBLIC OF MARSHALL ISLANDS",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Chin",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Irrawaddy",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Kachi",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Kayah",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Mandalay",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Mon",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Rakhine",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Sagaing",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Shan",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Tawi-Tawi",
          "stateCode": ""
        },
        {
          "countryName": "Myanmar",
          "countryCode": "MM",
          "stateName": "Yangon",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Aguascalientes",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Chiapas",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Chihuahua",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Coahuila",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "COLOANE",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Distrito Federal",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Guanajuato",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Hidalgo",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Jalisco",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "MACAO",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Michoacán",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "TAIPA",
          "stateCode": ""
        },
        {
          "countryName": "Macao",
          "countryCode": "MO",
          "stateName": "Yucatán",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "CNMI",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "FSN",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "MP",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "NORTHERN MARIANA ISLANDS MP",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "NORTHERN MARIANAS ISLAND",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "PALAU",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "ROTA",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "SAIPAN",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "SAIPAN MP",
          "stateCode": ""
        },
        {
          "countryName": "Northern Mariana Islands",
          "countryCode": "MP",
          "stateName": "SAIPAN, MP",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "AQUITAINE-LIMOUSIN-POITOU-CHARENTES",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "BRETAGNE",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "CENTRE-VAL DE LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "GUADELOUPE",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "GUYANE",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "ILE-DE-FRANCE",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "LANGUEDOC-ROUSSILLON-MIDI-PYRENEES",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "MARTINIQUE",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "NORMANDIE",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "PAYS-DE-LA-LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "PROVENCE-ALPES-COTE D'AZUR",
          "stateCode": ""
        },
        {
          "countryName": "Martinique",
          "countryCode": "MQ",
          "stateName": "REUNION",
          "stateCode": ""
        },
        {
          "countryName": "Mauritania",
          "countryCode": "MR",
          "stateName": "Michoacán",
          "stateCode": ""
        },
        {
          "countryName": "Montserrat",
          "countryCode": "MS",
          "stateName": "St Anthony",
          "stateCode": ""
        },
        {
          "countryName": "Montserrat",
          "countryCode": "MS",
          "stateName": "St Peter",
          "stateCode": ""
        },
        {
          "countryName": "Maldives",
          "countryCode": "MV",
          "stateName": "Male",
          "stateCode": ""
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "AGUASCALIENTES",
          "stateCode": "AG"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "BAJA CALIFORNIA",
          "stateCode": "BN"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "BAJA CALIFORNIA NORTE",
          "stateCode": ""
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "BAJA CALIFORNIA SUR",
          "stateCode": "BS"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "CAMPECHE",
          "stateCode": "CM"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "CHIAPAS",
          "stateCode": "CP"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "CHIHUAHUA",
          "stateCode": "CH"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "CIUDAD DE MEXICO",
          "stateCode": ""
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "COAHUILA",
          "stateCode": "CA"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "COLIMA",
          "stateCode": "CL"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "DISTRITO FEDERAL",
          "stateCode": "DF"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "DURANGO",
          "stateCode": "DU"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "ESTADO DE MEXICO",
          "stateCode": ""
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "GUANAJUATO",
          "stateCode": "GJ"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "GUERRERO",
          "stateCode": "GR"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "HIDALGO",
          "stateCode": "HI"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "JALISCO",
          "stateCode": "JA"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "MICHOACAN",
          "stateCode": "MC"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "Michoac n",
          "stateCode": ""
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "MORELOS",
          "stateCode": "MR"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "NAYARIT",
          "stateCode": "NA"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "NUEVO LEON",
          "stateCode": "NL"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "Nuevo Le¢n",
          "stateCode": ""
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "OAXACA",
          "stateCode": "OA"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "PUEBLA",
          "stateCode": "PU"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "QUERETARO",
          "stateCode": "QE"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "QUINTANA ROO",
          "stateCode": "QR"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "SAN LUIS POTOSI",
          "stateCode": "SL"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "SINALOA",
          "stateCode": "SI"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "SONORA",
          "stateCode": "SO"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "TABASCO",
          "stateCode": "TB"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "TAMAULIPAS",
          "stateCode": "TM"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "TLAXCALA",
          "stateCode": "TL"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "VERACRUZ",
          "stateCode": "VE"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "YUCATAN",
          "stateCode": "YU"
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "Yucat n",
          "stateCode": ""
        },
        {
          "countryName": "Mexico",
          "countryCode": "MX",
          "stateName": "ZACATECAS",
          "stateCode": "ZA"
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "JOHOR",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "KEDAH",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "KELANTAN",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "KUALA LUMPUR",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "LABUAN",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "MELAKA",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "NEGERI SEMBILAN",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "PAHANG",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "PERAK",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "PERLIS",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "PULAU PINANG",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "Punjab",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "SABAH",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "SARAWAK",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "Satun",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "Sel",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "SELANGOR",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "Semarang",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "TERENGGANU",
          "stateCode": ""
        },
        {
          "countryName": "Malaysia",
          "countryCode": "MY",
          "stateName": "Trang",
          "stateCode": ""
        },
        {
          "countryName": "New Caledonia",
          "countryCode": "NC",
          "stateName": "Puebla",
          "stateCode": ""
        },
        {
          "countryName": "Norfolk Island",
          "countryCode": "NF",
          "stateName": "NSW",
          "stateCode": ""
        },
        {
          "countryName": "Nigeria",
          "countryCode": "NG",
          "stateName": "Abra",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Atlantico",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Boaco",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Carazo",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Chinandega",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Chontales",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Esteli",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Granada",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Guatemala",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Jinotega",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Leon",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Limon",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Madriz",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Manabi",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Managua",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Masaya",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Matagalpa",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Mateare",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "N Segovia",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "RAAN",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "RACCN",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "RACCS",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Racs",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Rivas",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "San Juan",
          "stateCode": ""
        },
        {
          "countryName": "Nicaragua",
          "countryCode": "NI",
          "stateName": "Zelaya",
          "stateCode": ""
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "DRENTHE",
          "stateCode": "DR"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "FLEVOLAND",
          "stateCode": "FL"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "FRIESLAND",
          "stateCode": "FR"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "GELDERLAND",
          "stateCode": "GE"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "GRONINGEN",
          "stateCode": "GR"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "LIMBURG",
          "stateCode": "LI"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "NOORD-BRABANT",
          "stateCode": "NB"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "NOORD-HOLLAND",
          "stateCode": "NH"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "OVERIJSSEL",
          "stateCode": "OV"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "UTRECHT",
          "stateCode": "UT"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "ZEELAND",
          "stateCode": "ZE"
        },
        {
          "countryName": "Netherlands",
          "countryCode": "NL",
          "stateName": "ZUID-HOLLAND",
          "stateCode": "ZH"
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "AKERSHUS",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "AUST-AGDER",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "BUSKERUD",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "FINNMARK",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "HEDMARK",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "HORDALAND",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "JAN MAYEN",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "MORE OG ROMSDAL",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "NORD-TRONDELAG",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "NORD-TRØNDELAG",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "NORDLAND",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "OPPLAND",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "OSLO",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "OSTFOLD",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "ROGALAND",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "SOGN OG FJORDANE",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "SOR-TRONDELAG",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "SVALBARD",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "TELEMARK",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "TROMS",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "VEST-AGDER",
          "stateCode": ""
        },
        {
          "countryName": "Norway",
          "countryCode": "NO",
          "stateName": "VESTFOLD",
          "stateCode": ""
        },
        {
          "countryName": "Nepal",
          "countryCode": "NP",
          "stateName": "Jakarta",
          "stateCode": ""
        },
        {
          "countryName": "Nepal",
          "countryCode": "NP",
          "stateName": "Karnataka",
          "stateCode": ""
        },
        {
          "countryName": "Nepal",
          "countryCode": "NP",
          "stateName": "Kathmandu",
          "stateCode": ""
        },
        {
          "countryName": "Nepal",
          "countryCode": "NP",
          "stateName": "Leyte Province",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "AKID",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "AU",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "AUCKLAND",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "BAY OF PLENTY",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "CANTERBURY",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "CHRISTCHURCH",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "COROMANDEL",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "DUNEDIN",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "FEILDING",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "GISBORNE",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "Hamilton",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "Hastings",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "HAWERA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "HAWKE'S BAY",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "Hawkes Bay",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "HENDERSON",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "INVERCARGILL",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "KAIKOURA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "KAWERAU",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "KERIKERI",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "LEVIN",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "MANAWATU-WANGANUI",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "MANUKAU",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "MAPUA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "MARLBOROUGH",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "MASTERTON",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "MATAMATA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "MORRINSVILLE",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "MT MAUNGANUI",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "NELSON",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "NEW PLYMOUTH",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "New Zealand",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "NORTH CANTERBURY",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "NORTH SHORE CITY",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "NORTHLAND",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "NORTHLANDS",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "NSW",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "NT",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "NZ",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "OPOTIKI",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "OTAGO",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "Palmerston North",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "PARAPARAUMU",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "PUTARURU",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "QLD",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "QUEENSTOWN",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "Rodney",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "ROTORUA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "SA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "South Island",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "SOUTHLAND",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "STRANDON",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "STRATFORD",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "TARANAKI",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "TASMAN",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "TAUPO",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "Tauranga",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "TAURANGA - FRASER COV",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "TE KUITI",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "TE RAPA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "THAMES",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "TIMARU",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "TOKOROA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "UPPER HUT",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "UPPER HUTT",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "WA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "WAIKATO",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "WAIRARAPA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "WAITAKERE",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "WAITARA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "WANGANUI",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "WELLINGTON",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "WEST COAST",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "Whakatane",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "WHANGAPARAOA",
          "stateCode": ""
        },
        {
          "countryName": "New Zealand",
          "countryCode": "NZ",
          "stateName": "Whangarei",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "A. Verapaz",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Bocas Toro",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Chiriqui",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Cocle",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Colon",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "D. C.",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "D. N.",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "D.F.",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Herrera",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Los Santos",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Panama",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Panama Oeste",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Pando",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Providence",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "San Jose",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "San Salv",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Santiago",
          "stateCode": ""
        },
        {
          "countryName": "Panama",
          "countryCode": "PA",
          "stateName": "Veraguas",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "AC",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "AM",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "AMAZONAS",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Ancash",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "ANDAHUAYLA",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "APURIMAC",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "AR",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "AREQUIPA",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "AY",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Ayacucho",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "CAJAMARCA",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Callao",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "CC",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "CL",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Cusco",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "CUZCO",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Guayas",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "HN",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "HUANCAVELICA",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "HUANUCO",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "IC",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "ICA",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "JN",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "JUNIN",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "LA LIBERTAD",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "LAMBAYEQUE",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Libertad",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "LIMA",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "LL",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "LM",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "LORETO",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "LR",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Madre de Dios",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "MD",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "MOQUEGUA",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Pasco",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "PC",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "PE",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "PER",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Peravia",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "PIURA",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "PN",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "PR",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Puno",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "SAN MARTIN",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "SM",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "TACNA",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "TB",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "TC",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Trujillo",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "TUMBES",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "UC",
          "stateCode": ""
        },
        {
          "countryName": "Peru",
          "countryCode": "PE",
          "stateName": "Ucayali",
          "stateCode": ""
        },
        {
          "countryName": "French Polynesia",
          "countryCode": "PF",
          "stateName": "APIA",
          "stateCode": ""
        },
        {
          "countryName": "French Polynesia",
          "countryCode": "PF",
          "stateName": "BORA BORA",
          "stateCode": ""
        },
        {
          "countryName": "French Polynesia",
          "countryCode": "PF",
          "stateName": "PAPEETE",
          "stateCode": ""
        },
        {
          "countryName": "French Polynesia",
          "countryCode": "PF",
          "stateName": "PIRAE",
          "stateCode": ""
        },
        {
          "countryName": "French Polynesia",
          "countryCode": "PF",
          "stateName": "PUNAAUIA",
          "stateCode": ""
        },
        {
          "countryName": "French Polynesia",
          "countryCode": "PF",
          "stateName": "TAHIT",
          "stateCode": ""
        },
        {
          "countryName": "French Polynesia",
          "countryCode": "PF",
          "stateName": "Tahiti",
          "stateCode": ""
        },
        {
          "countryName": "French Polynesia",
          "countryCode": "PF",
          "stateName": "TI",
          "stateCode": ""
        },
        {
          "countryName": "French Polynesia",
          "countryCode": "PF",
          "stateName": "WALLIS AND FORTUNA ISLANDS",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "AB",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "ANTONOMOUS REGION",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "BOROKO",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "BOROKO NCD",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "BOUGAINVILLE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "CENTRAL",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "CONAKRY",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "EAST NEW BRITAIN",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "EAST SEPIK",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "EASTERN HIGHLAND PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "EHP",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "Enga Province",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "HOHOLA",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "LAE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "LAE MOROBE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "LAE MOROBE PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "LAO",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "MADANG",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "MCD",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "MILNE BAY PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "MOMASE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "MOMBE PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "MOROBE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "MOROBE PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "N.C.D",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "National Capital District",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "NCD",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "NCO",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "NSP",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "PG",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "PNG",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "PORT MORESBY",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "PORT MORESBY NCD",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "WA",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "WEST NEW BRITAIN",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "WESTERN HIGHLAND PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "WESTERN HIGHLANDS PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Papua New Guinea",
          "countryCode": "PG",
          "stateName": "WNB",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "ABRA",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "AGUSAN DEL NORTE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "AGUSAN DEL SUR",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "ANTIQUE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Aurora Province",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Bandarban",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Basilan Province",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Bataan",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Batanes",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "BATANGAS",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Benguet",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Biliran Sub-Province",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "BOHOL",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Bukidnon",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "BULACAN",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "CAMARINES NORTE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Camarines Sur",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Camiguin",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "CAPIZ",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "CATANDUANES",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "CAVITE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Cebu",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Central Java",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "DAVAO DEL NORTE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Davao Del Sur",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Davao Oriental",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "EASTERN SAMAR",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Guimaras Sub-Province",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "IFUGAO",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "ILOCOS NORTE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "ILOCOS SUR",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Iloilo",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Irian Jaya",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "ISABELA",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Jambi",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "KALINGA APAYAO",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Kediri",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "LA UNION",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "LAGUNA",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Lahore",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Lanao Del Norte",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "LANAO DEL SUR",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Lao Cai",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Leyte Province",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Long Dong",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "MAGUINDANAO",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Mandalay",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Manila",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Manipur",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "MARINDUQUE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "MASBATE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Matale",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Mindoro Occ.",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "MINDORO ORIENTAL",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "MISAMIS OCCIDENTAL",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Misamis Oriental",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Mountain Province",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Negros Occidental",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "NEGROS ORIENTAL",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "NORTH COTABATO",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "NORTHERN SAMAR",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "NUEVA ECIJA",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "NUEVA VISCAYA",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "PALAWAN",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Pampanga",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "PANGASINAN",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Pathum Thani",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Phangnga",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Quetta",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Quezon",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "RIZAL",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "ROMBLON",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Siquijor Province",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "SORSOGON",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "SOUTH COTABATO",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "SOUTHERN LEYTE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "SULTAN KUDARAT",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Sulu Province",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "SURIGAO DEL NORTE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "SURIGAO DEL SUR",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "TARLAC",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "TAWI-TAWI",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Thai Binh",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Thuan An",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Udon Thani",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Uthai Thani",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "WESTERN SAMAR",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "ZAMBALES",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "ZAMBOANGA DEL NORTE",
          "stateCode": ""
        },
        {
          "countryName": "Philippines",
          "countryCode": "PH",
          "stateName": "Zamboanga Del Sur",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Amnat Charoen",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Bahawalpur",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Cebu",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Colombo",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Dhaka",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Gilgit",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Gujarat",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Gujranwala",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Hyderabad",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Isabela",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Islamabad",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Jaffna",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Jakarta",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Kachi",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Karachi",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Karnataka",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Kien Giang",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Lahore",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Lai Chau",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Multan",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Muzaffarabad",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Pattani",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Peshawar",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Punjab",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "Quetta",
          "stateCode": ""
        },
        {
          "countryName": "Pakistan",
          "countryCode": "PK",
          "stateName": "West Java",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "DOLNOSLASKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "KUJAWSKO-POMORSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "LÓDZKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "LUBELSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "LUBUSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "MALOPOLSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "MAZOWIECKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "OPOLSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "PODKARPACKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "PODLASKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "POMORSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "POZNAN",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "SLASKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "SWIETOKRZYSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "WARMINSKO-MAZURSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "WIELKOPOLSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Poland",
          "countryCode": "PL",
          "stateName": "ZACHODNIOPOMORSKIE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ABRANTES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ÁGUEDA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "AGUIAR DA BEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALANDROAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALBERGARIA-A-VELHA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALBUFEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALCACER DO SAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALCANENA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALCOCHETE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALCOUTIM",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALENQUER",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALFANDEGA DA FE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALIJO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALJEZUR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALJUSTREL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALMADA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALMEIDA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALMEIRIM",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALTER DO CHÃO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALVAIÁZERE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ALVITO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "AMADORA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "AMARANTE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "AMARES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ANADIA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ANGRA DO HEROISMO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ANSIÃO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ARCOS DE VALDEVEZ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ARGANIL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ARMAMAR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "AROUCA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ARRAIOLOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ARRONCHES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ARRUDA DOS VINHOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "AVEIRO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "AVIS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "AZAMBUJA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BARCELOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BARRANCOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BARREIRO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BATALHA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BEJA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BELMONTE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BENAVENTE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BOMBARRAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BORBA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BOTICAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "BRAGA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CABECEIRAS DE BASTO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CADAVAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CALDAS DA RAINHA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CALHETA (MADEIRA)",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CALHETA (SÃO JORGE)",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CÂMARA DE LOBOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CAMINHA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CAMPO MAIOR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CANTANHEDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CARRAZEDA DE ANSIÃES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CARREGAL DO SAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CARTAXO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CASCAIS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CASTANHEIRA DE PÊRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CASTELO BRANCO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CASTELO DE PAIVA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CASTELO DE VIDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CASTRO DAIRE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CASTRO MARIM",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CASTRO VERDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CELORICO DA BEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CELORICO DE BASTO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CHAMUSCA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CHAVES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CINFÃES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "COIMBRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CONDEIXA-A-NOVA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CONSTANCIA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CORUCHE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CORVO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "COVILHÃ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CRATO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "CUBA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ELVAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ENTRONCAMENTO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ESPINHO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ESPOSENDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ESTARREJA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ESTREMOZ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "EVORA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FAFE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FARO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FELGUEIRAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FERREIRA DO ALENTEJO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FERREIRA DO ZÊZERE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FIGUEIRA DA FOZ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FIGUEIRA DE CASTELO RODRIGO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FIGUEIRO DOS VINHOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FORNOS DE ALGODRES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FREIXO DE ESPADA À CINTA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FRONTEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "FUNCHAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "GONDOMAR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "GOUVEIA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "GR? NDOLA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "GUARDA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "HORTA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "IDANHA-A-NOVA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ÍLHAVO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "INDEFINIDO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LAGOA (ALGARVE)",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LAGOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LAJES DAS FLORES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LAJES DO PICO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LAMEGO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LEIRIA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LISBOA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LOULE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LOURES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LOURINHÃ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LOURINHÒ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LOUSÃ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "LOUSADA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MACEDO DE CAVALEIROS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MACHICO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MADALENA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MAFRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MAIA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MANGUALDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MANTEIGAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MARCO DE CANAVESES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MARINHA GRANDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MARVÃO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MATOSINHOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MEALHADA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MEDA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MIRANDA DO CORVO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MIRANDA DO DOURO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MIRANDELA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MOGADOURO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MOIMENTA DA BEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MOITA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MONCHIQUE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MONDIM DE BASTO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MONFORTE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MONTALEGRE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MONTEMOR-O-NOVO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MONTEMOR-O-VELHO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MONTIJO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MORA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MORTÁGUA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MOURA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "MURTOSA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "NELAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "NISA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "NORDESTE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OBIDOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ODEMIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "ODIVELAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OEIRAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OLEIROS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OLIVEIRA DE AZEMEIS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OLIVEIRA DE FRADES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OLIVEIRA DO BAIRRO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OLIVEIRA DO HOSPITAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OURÉM",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OURIQUE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "OVAR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PAÇOS DE FERREIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PALMELA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PAMPILHOSA DA SERRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PAREDES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PAREDES DE COURA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PENACOVA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PENAFIEL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PENALVA DO CASTELO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PENAMACOR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PENEDONO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PENELA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PENICHE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PINHEL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "POMBAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PONTA DELGADA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PONTA DO SOL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PONTE DA BARCA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PONTE DE LIMA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PONTE DE SOR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PORTALEGRE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PORTEL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PORTO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PORTO MONIZ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "PORTO SANTO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "POVOA DE LANHOSO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "POVOA DE VARZIM",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "REDONDO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "REGUENGOS DE MONSARAZ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "RESENDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "RIBEIRA BRAVA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "RIBEIRA DE PENA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "RIBEIRA GRANDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "RIO MAIOR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "S.MONTE AGRAÇO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SABROSA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SABUGAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SALVATERRA DE MAGOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTA COMBA DÃO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTA CRUZ",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTA CRUZ DA GRACIOSA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTA CRUZ DAS FLORES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTA MARIA DA FEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTA MARTA DE PENAGUIÃO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTANA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTARÉM",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTIAGO DO CACEM",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SANTO TIRSO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SÃO BRÁS DE ALPORTEL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SÃO JOÃO DA MADEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SÃO JOÃO DA PESQUEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SÃO PEDRO DO SUL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SÃO ROQUE DO PICO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SÃO VICENTE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SARDOAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SEIA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SEIXAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SERNANCELHE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SERPA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SESIMBRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SETUBAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SEVER DO VOUGA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SILVES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SINES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SINTRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SOBRAL DE MONTE AGRAÇO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SÒO PEDRO DO SUL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SOURE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "SOUSEL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TABUA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TABUAÇO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TAROUCA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TAVIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TERRAS DE BOURO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TOMAR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TONDELA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TORRE DE MONCORVO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TORRES NOVAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TORRES VEDRAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TRANCOSO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "TROFA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VAGOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VALE DE CAMBRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VALEN?!A",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VALENÇA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VALONGO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VALPAÇOS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VELAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VENDAS NOVAS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VIANA DO ALENTEJO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VIANA DO CASTELO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VIDIGUEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VIEIRA DO MINHO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA DE REI",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA DO BISPO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA DO CONDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA DO PORTO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA FLOR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA FRANCA DE XIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA FRANCA DO CAMPO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA NOVA DA BARQUINHA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA NOVA DE CERVEIRA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA NOVA DE FAMALICÃO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA NOVA DE FAMALICÒO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA NOVA DE FOZ CÔA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA NOVA DE GAIA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA NOVA DE PAIVA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA NOVA DE POIARES",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA POUCA DE AGUIAR",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA REAL",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA REAL DE SANTO ANTÓNIO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA VELHA DE RÓDÃO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA VERDE",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VILA VIÇOSA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VIMIOSO",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VINHAIS",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VISEU",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VIZELA",
          "stateCode": ""
        },
        {
          "countryName": "Portugal",
          "countryCode": "PT",
          "stateName": "VOUZELA",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "ALTO PARANA",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "AMAMBAY",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "BOQUERON",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "CAAGUAZU",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "CAAZAPA",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "CENTRAL",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "CONCEPCION",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "CORDILLERA",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "GUAIRA",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "ITAPUA",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "MISIONES",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "NEEMBUCU",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "PARAGUARI",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "PRESIDENTE HAYES",
          "stateCode": ""
        },
        {
          "countryName": "Paraguay",
          "countryCode": "PY",
          "stateName": "SAN PEDRO",
          "stateCode": ""
        },
        {
          "countryName": "Reunion",
          "countryCode": "RE",
          "stateName": "AQUITAINE-LIMOUSIN-POITOU-CHARENTES",
          "stateCode": ""
        },
        {
          "countryName": "Reunion",
          "countryCode": "RE",
          "stateName": "CENTRE-VAL DE LOIRE",
          "stateCode": ""
        },
        {
          "countryName": "Reunion",
          "countryCode": "RE",
          "stateName": "GUADELOUPE",
          "stateCode": ""
        },
        {
          "countryName": "Reunion",
          "countryCode": "RE",
          "stateName": "ILE-DE-FRANCE",
          "stateCode": ""
        },
        {
          "countryName": "Reunion",
          "countryCode": "RE",
          "stateName": "PROVENCE-ALPES-COTE D'AZUR",
          "stateCode": ""
        },
        {
          "countryName": "Reunion",
          "countryCode": "RE",
          "stateName": "REUNION",
          "stateCode": ""
        },
        {
          "countryName": "Russian Federation",
          "countryCode": "RU",
          "stateName": "Altaiski krai",
          "stateCode": ""
        },
        {
          "countryName": "Russian Federation",
          "countryCode": "RU",
          "stateName": "Astrakhanskaya oblast",
          "stateCode": ""
        },
        {
          "countryName": "Solomon Islands",
          "countryCode": "SB",
          "stateName": "GUADALCANAL",
          "stateCode": ""
        },
        {
          "countryName": "Solomon Islands",
          "countryCode": "SB",
          "stateName": "HONIARA",
          "stateCode": ""
        },
        {
          "countryName": "Solomon Islands",
          "countryCode": "SB",
          "stateName": "Malaita",
          "stateCode": ""
        },
        {
          "countryName": "Solomon Islands",
          "countryCode": "SB",
          "stateName": "WESTERN PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "ALVSBORGS LAN",
          "stateCode": ""
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "BLEKINGE",
          "stateCode": "BL"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "DALARNA",
          "stateCode": "KO"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "GAVLEBORGS LAN",
          "stateCode": "GV"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "GOTEBORGS OCH BOHUS LAN",
          "stateCode": ""
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "GOTLAND",
          "stateCode": "GT"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "GOTLANDS LAN",
          "stateCode": ""
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "HALLAND",
          "stateCode": "HA"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "JAMTLANDS LAN",
          "stateCode": "JA"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "JONKOPINGS LAN",
          "stateCode": "JO"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "KALMAR",
          "stateCode": "KA"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "KOPPARBERGS LAN",
          "stateCode": ""
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "KRISTIANSTADS LAN",
          "stateCode": ""
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "KRONOBERG",
          "stateCode": "KR"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "MALMOHUS LAN",
          "stateCode": ""
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "NORRBOTTEN",
          "stateCode": "NB"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "OREBRO LAN",
          "stateCode": "OR"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "OSTERGOTLANDS LAN",
          "stateCode": "OG"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "SKARABORGS LAN",
          "stateCode": "SN"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "SODERMANLANDS LAN",
          "stateCode": "SD"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "STOCKHOLM",
          "stateCode": "ST"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "STOCKHOLMS LAN",
          "stateCode": ""
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "UPPSALA",
          "stateCode": "UP"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "UPPSALA LAN",
          "stateCode": ""
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "VARMLANDS LAN",
          "stateCode": "VR"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "VASTERBOTTENS LAN",
          "stateCode": "VB"
        },
        {
          "countryName": "Sweden",
          "countryCode": "SE",
          "stateName": "VASTERNORRLANDS LAN",
          "stateCode": "VN"
        },
        {
          "countryName": "Singapore",
          "countryCode": "SG",
          "stateName": "Sing Buri",
          "stateCode": ""
        },
        {
          "countryName": "Slovakia",
          "countryCode": "SK",
          "stateName": "",
          "stateCode": ""
        },
        {
          "countryName": "Suriname",
          "countryCode": "SR",
          "stateName": "Commewijne",
          "stateCode": ""
        },
        {
          "countryName": "Suriname",
          "countryCode": "SR",
          "stateName": "Nickerie",
          "stateCode": ""
        },
        {
          "countryName": "Suriname",
          "countryCode": "SR",
          "stateName": "Para",
          "stateCode": ""
        },
        {
          "countryName": "Suriname",
          "countryCode": "SR",
          "stateName": "Paramaribo",
          "stateCode": ""
        },
        {
          "countryName": "Suriname",
          "countryCode": "SR",
          "stateName": "Saramacca",
          "stateCode": ""
        },
        {
          "countryName": "Suriname",
          "countryCode": "SR",
          "stateName": "Suriname",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Ahuachapan",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Apopa",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Cabanas",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Chalatenango",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Colon",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Cuscatlan",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "La Liberta",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "La Libertad",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "La Paz",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "La Union",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Libertad",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Morazan",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Quest",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Salvador",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "San Jose",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "San Juan",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "San Marcos",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "San Miguel",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "San Salv",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "San Salvad",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "San Vicent",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Santa Ana",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Sonsonate",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Usulutan",
          "stateCode": ""
        },
        {
          "countryName": "El Salvador",
          "countryCode": "SV",
          "stateName": "Vicente",
          "stateCode": ""
        },
        {
          "countryName": "Sint Maarten (Dutch part)",
          "countryCode": "SX",
          "stateName": "Pointe Bla",
          "stateCode": ""
        },
        {
          "countryName": "Sint Maarten (Dutch part)",
          "countryCode": "SX",
          "stateName": "St Maarten",
          "stateCode": ""
        },
        {
          "countryName": "Swaziland",
          "countryCode": "SZ",
          "stateName": "Hhohho",
          "stateCode": ""
        },
        {
          "countryName": "Swaziland",
          "countryCode": "SZ",
          "stateName": "Lubombo",
          "stateCode": ""
        },
        {
          "countryName": "Swaziland",
          "countryCode": "SZ",
          "stateName": "Manzini",
          "stateCode": ""
        },
        {
          "countryName": "Swaziland",
          "countryCode": "SZ",
          "stateName": "Shiselweni",
          "stateCode": ""
        },
        {
          "countryName": "Turks and Caicos Islands",
          "countryCode": "TC",
          "stateName": "Gr Cayman",
          "stateCode": ""
        },
        {
          "countryName": "Turks and Caicos Islands",
          "countryCode": "TC",
          "stateName": "Grand Turk",
          "stateCode": ""
        },
        {
          "countryName": "Turks and Caicos Islands",
          "countryCode": "TC",
          "stateName": "Grand'Anse",
          "stateCode": ""
        },
        {
          "countryName": "Turks and Caicos Islands",
          "countryCode": "TC",
          "stateName": "Mid Caicos",
          "stateCode": ""
        },
        {
          "countryName": "Turks and Caicos Islands",
          "countryCode": "TC",
          "stateName": "Providence",
          "stateCode": ""
        },
        {
          "countryName": "Turks and Caicos Islands",
          "countryCode": "TC",
          "stateName": "Tortola",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Amnat Charoen",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Ang Thong",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Ayutthaya (Ayudhya)",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Bangkok",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Binh Dinh",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Buri Ram",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Chachoengsao",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Chai Nat",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Chaiyaphum",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Chanthaburi",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Chiang Mai",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Chiang Rai",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Chon Buri",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Chumphon",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Chumpon",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Hanoi",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Ho Chi Minh City",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Kahon Ratchasima",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Kalasin",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Kamphaeng Phet",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Kanchanaburi",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Khon Kaen",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Krabi",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Lampang",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Lamphun",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Loei",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Lop Buri",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Mae Hong Son",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Maha Sarakham",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Maharashtra",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Mukdahan",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nakhon Nayok",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nakhon Pathom",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nakhon Phanom",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nakhon Ratchasima",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nakhon Sawan",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nakhon Si Thammarat",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nakhon Srithammarat",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nan",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Narathiwat",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nong Bua Lam Phu",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nong Khai",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Nonthaburi",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Pathum Thani",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Pattani",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phangnga",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phatthalung",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phayao",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phetchabun",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phetchaburi",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phichit",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phitsanulok",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phra Nakhon Si Ayutthaya",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phrae",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Phuket",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Prachin Buri",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Prachuap Khiri Khan",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Ranong",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Ratchaburi",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Rayong",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Roi Et",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Sa Kaeo",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Sakon Nakhon",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Samut Prakan",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Samut Sakhon",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Samut Songkhram",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Saraburi",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Satun",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Si Sa Ket",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Sing Buri",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Songkhla",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "South Kalimantan",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Sukhothai",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Suphan Buri",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Surat Thani",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Surin",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Tak",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Thai Binh",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Trang",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Trat",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Ubon Ratchathani",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Udon Thani",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Uthai Thani",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Uttaradit",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Yala",
          "stateCode": ""
        },
        {
          "countryName": "Thailand",
          "countryCode": "TH",
          "stateName": "Yasothon",
          "stateCode": ""
        },
        {
          "countryName": "Timor-Leste",
          "countryCode": "TL",
          "stateName": "East Timor",
          "stateCode": ""
        },
        {
          "countryName": "Timor-Leste",
          "countryCode": "TL",
          "stateName": "Jambi",
          "stateCode": ""
        },
        {
          "countryName": "Tonga",
          "countryCode": "TO",
          "stateName": "MAUFANGA NUKU'ALOFA",
          "stateCode": ""
        },
        {
          "countryName": "Tonga",
          "countryCode": "TO",
          "stateName": "NUKU'ALOFA",
          "stateCode": ""
        },
        {
          "countryName": "Tonga",
          "countryCode": "TO",
          "stateName": "NUKUALOFA",
          "stateCode": ""
        },
        {
          "countryName": "Tonga",
          "countryCode": "TO",
          "stateName": "NUKU`ALOFA",
          "stateCode": ""
        },
        {
          "countryName": "Tonga",
          "countryCode": "TO",
          "stateName": "TONGATAPU",
          "stateCode": ""
        },
        {
          "countryName": "Tonga",
          "countryCode": "TO",
          "stateName": "VAVA'U",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Arauca",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Arima",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Barataria",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Canaan",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Caroni",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Chaguanas",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Coronie",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Couva",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Debe",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Diego Mar.",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Glencoe",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "La Romana",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Maraval",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Marowijne",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Mayaro",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Paramaribo",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Port of Sp",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Portoviejo",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Quest",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Roxborough",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "S. Fernand",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "San Juan",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Santa Cruz",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Santander",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "St Andrew",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "St James",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "St Joseph",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "St Patrick",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "St. George",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Tabaquite",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Tobago",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Trinidad",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Trinity",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Tunapuna",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Tunapuna P",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "Victoria",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "W.I",
          "stateCode": ""
        },
        {
          "countryName": "Trinidad and Tobago",
          "countryCode": "TT",
          "stateName": "West",
          "stateCode": ""
        },
        {
          "countryName": "Tuvalu",
          "countryCode": "TV",
          "stateName": "FUNAFUTI",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Changhua",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Chiayi",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Chinmen",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Hsinchu",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Hualien",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Ilan",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Kaohsiung",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Miaoli",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Nantou",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Penghu",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Pingtung",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Taichung",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Tainan",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Taipei",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Taitung",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Taoyuan",
          "stateCode": ""
        },
        {
          "countryName": "Taiwan",
          "countryCode": "TW",
          "stateName": "Yunlin",
          "stateCode": ""
        },
        {
          "countryName": "Tanzania, United Republic Of",
          "countryCode": "TZ",
          "stateName": "Quetta",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Abbot",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Aberdeen",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ABERDEENSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Aberffraw Anglesey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Accountant Meeson Ho",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Acre Garth Lane Hamb",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Acres Berrow",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Activity Centre Rowl",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Address Line",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Airfield Saffron Wal",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Airport Chobham Surr",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Airport Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Alfred Crescent Nort",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Alley Heping Road Li",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Anglesey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ANGUS",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Annoyed",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Antrim",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Approach East Horsle",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Approach Moor Park",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Apron Street South K",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Arden West Midlands",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Arena Arena Approach",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ARGYLL",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Armadale",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ashish Chs, Moolji N",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ashwell Hertfordshir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avanta House College",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Baildon Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Barkingside R",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Barrhead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Beeston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Bishopbriggs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Blackhall",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Broughton Gat",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Chorleywood H",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Culverstone K",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue East",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Eastcote Midd",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Essex Essex E",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Gants Hill Es",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Great Harwood",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Hazlemere",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Houghton Regi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Houston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Ickenham Midd",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Kempston Bedf",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Kintore",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Kirkby-in-ash",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Lenzie",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Liverppool",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Marton-in-cle",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Mossley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Rainhil Merse",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Rawtenstall L",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Rottingdean",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Shawhead Coar",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Shenfield",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue South Shield'",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Southbourne D",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Staines-upon-",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Stapleford St",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Westgate",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue Winchmore Hil",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue, Burren Warre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Avenue, North Berste",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "AVON",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Awel Hawarden",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "AYRSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bagot Henley-in-arde",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bambers Green Takele",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Banes",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "BANFFSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bank Davenham Cheshi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bank Downend Horsley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Barlborough Derbyshi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Barn Close Stanwell",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Barn Highwycombe Buc",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Barn Lower Tynewydd",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Barrowford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bassetts Lane Woodha",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bath",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bath N.e. Somerset",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bath North East Some",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bedfirdshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bedfordhsire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "BEDFORDSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bedfordshire (county",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bedfordshire England",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Beds.",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Beeches Deddington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Beeston Nottinghamsh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Belcoo Fermanagh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "BELFAST",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bends Parson Drove C",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "BERKSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Berkshire Berkshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Berries Road Cookham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "BERWICKSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Birmingham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Birstall West Yorksh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bishops Cleeve",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bold Avenue Stoney S",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Borrowaash",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bransgore",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bridge Aberdeenshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bridge Road Stratfor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bridge Suite Ediburg",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bromley, Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Brook Cheshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Brook Royal Wootton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Brooklands Eversley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Brow Huyton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bryn Ashton Makerfie",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bryn Hendy Miskin",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bryn Llysfaen",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Brynmawr Gwent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Buckinghamshir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "BUCKINGHAMSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Buckinghamshite",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bukinghamshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Bungalow Park Road B",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Burnham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Burnham Road Althorn",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Businerobins Road St",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Business Centre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Business Centre Crab",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Business Centre Knig",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Business Park Cores",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "BUTE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Caerphilly",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CAITHNESS",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Calle Javier Pedro A",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cambrideshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CAMBRIDGESHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cambridgeshirs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cambrigeshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cambs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cambs.",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Carmunnock Clarkston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Carnoustie Angus",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Castle Business Park",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Castlebay",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Catrine Ayrshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cedars Whickham Tyne",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Central",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Central Region",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Centre Chalton Stree",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Centre Lynchford Roa",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Centre Sunrise Amesb",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ceredigion",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ceveland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CHANNEL ISLANDS",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Chase Weston Super-m",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CHESHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cheshire West",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Chillies Lane High H",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Chiltern",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Chobham Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Circus Moorgate",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CLACKMANNANSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Claygate Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CLEVELAND",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Ampthill",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Ampthill Beds",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Begelly",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Blaby",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Black Bull Yar",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Burscough Lanc",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Caister Norfol",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Carbis Cornwal",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Carlshalton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Cheam Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Chorleywood He",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Clacton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Clacton Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Clanfield Hamp",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Compton West B",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Corringham Sta",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Custom House N",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close East",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Flitwick",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Flitwick Bedfo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Giles Park Dus",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Gresford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Harwell",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Hernebay",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Hersham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Higham Ferrers",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Hucclecote Glo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Impington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Irlam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Jews Wajews Wa",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Langford Beds",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Loose Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Murrumba Downs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Nancegollan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Narborough Lei",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Narborough Nor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Polgooth, Aust",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Raunds",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Rawmarsh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Royal Wootton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Saint Neots",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Standford Hope",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Suffolk",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Suttoncoldfiel",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Sweetcroft Lan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Tollesbury",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Walton- Thames",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Weeting",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Close Wellinghboroug",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CLWYD",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CO ANTRIM",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CO ARMAGH",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Co Derry Northern Ireland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CO DOWN",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CO FERMANAGH",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CO LONDONDERRY",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CO TYRONE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Co. Antrim.",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Co. Down",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Co. Dublin",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Co.antrim",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Co.down",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Co.durham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Coggeshall Road Kelv",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Coldfeild",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Coldfield",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Colney",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Common",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Common Bucks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Common Burkinghamshi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Conwy",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cooper Road Shenston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Copse Storrington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cornare' Basalghelle",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CORNWALL",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cosham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cottage Goosnargh La",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cottage School Lane",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cottages Childsbridg",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cottages Holmer Gree",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Councillor County",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "County (optional",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "County Antrim",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "County Armagh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "County Borough",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "County Borough Bridg",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "County Down",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "COUNTY DURHAM",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "County Fermanagh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "County Tyrone",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Alan Ramsbotto",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Bathroad Berks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Beaumont Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Berwick Tweed",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Brotton Clevel",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Calthrope Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Chobham Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Church Lane Ki",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Copmanthorpe",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Farm Bentley S",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court High Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Hirwaun Glam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Hutton Drive E",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court John Harrison",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Putney",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Southfields Bu",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Wallis Square",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Whitehills Eas",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court Worsley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Court, Southend Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Courtenay Abingdon-o",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Courtyard Chalvingto",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Courtyard Puddletown",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Courtyard Rufford La",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cover Road Severn Be",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Craven",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Carlilse",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Cowley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Dalgety",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Flitwick Be",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Godshill Is",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Heath Town",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Killayswans",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Middleton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Newmachar A",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent North Yate",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Quarrington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Shoeburynes",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Stanford-le",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Uddingston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Whitburn",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crescent Yarpole",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cresent Oldbrook Old",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Croft Kirk Merringto",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Croft Mill Lane Ight",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Croft Wormley Hertfo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cross Lane North Low",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Crydon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cryers Hill Lane Cry",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "CUMBRIA",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Cymru Wales",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Darlingtons Rustingt",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Deadwaters",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Dean Centre Castle R",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Decon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Delaval",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Delaval Northumberla",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Dennyloanhead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Derbyhsire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "DERBYSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Derry",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Derry City",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "DEVON",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "District Columbia",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Donnington Road Will",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "DORSET",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Dorset Dorset",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Dorset England",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Douglas Drive Cumber",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Down",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drayton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Amble",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Apperley Bridg",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Auchinleck",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Binfield Berks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Bollington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Bradwell Bradw",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Burbage Leices",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Church Gresley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Cumbernauld",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Curridge",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Dalgetty Fife",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Desford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Ditcheat",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Eltham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Fiskerton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Gants Hill Ess",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Hawarden Flint",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Hethersett Nor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Houghton Regis",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Hutton Lancash",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Kirby Muxloe L",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Kirkby Ashfiel",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Knareborough",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Langley Berksh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Lawrence Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Lodonderry",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Monton Eccles",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Newton Mearns",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Newtonabbey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Oundle",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Silverstone",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Thornbury Sout",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Varlton Nortin",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Warsop",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Drive Yarnfield",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Driving Range Rowley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Dumfries",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Dumfries Galloway",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "DUMFRIESSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "DUNBARTONSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Dutch Barn Ford Esse",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "DYFED",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Earlswood Chesptow M",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "East Bach",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "East Barnett Hertfor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "East Kinsgton-upon-t",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "EAST LOTHIAN",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "East Riding",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "East Riding Yorkshir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "East Road Longhorsle",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "EAST SUSSEX",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "East Yorkshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Easy Yorkshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Edderton Ross-shire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Edgworth Lancashire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Edinburgh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Edmunds",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Eltham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Embankment Vale Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "England",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "England And Wales",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "England Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "England Wales",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Eryr Cwmhrydycairw",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Essed",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ESSEX",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Essex Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Estate Stanbridge Ea",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Fairway Oadby",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Farm Chapel Lane Eas",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Farm Colgate West Su",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Farm Crescent Stando",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Farm Drive Saint Alb",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Farm Lane Hartley Wi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Farm Lane Osmington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Farm Road South Wood",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Fauldhouse",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Feild Hatch Middlese",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ferring",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Field Greys Oxfordsh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Fields Scaldwell Roa",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "FIFE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Financial Centsingap",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Finningley Airport S",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Five Green Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Fold Lane Addingham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Folly Laindon Laindo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Food Court Bailrigg",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Forstal Hawkinge Ken",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Fulwood",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Acton Town",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Aldenham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Appleton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Appleton Che",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Binfield Ber",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Chadwell Hea",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Culcheth Che",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Hamble Hamps",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Hutton Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Killingworth",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Newton Mearn",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Oakwood,lond",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Stoneleigh K",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Twyford, Ber",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gardens Whiston Mers",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Garelochhead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gate West Milton Key",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gholhak Area, Sajad",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gill Northiam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Glam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Glamorgan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Glamorgan South Wale",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Glasgow",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Glebes Snape",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Glenavy",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gloucester",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "GLOUCESTERSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gloucestrshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Glous",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gopsport",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gqwent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Grange Ashton-in-mak",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Great Britain",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Greater London",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Greater Manachester",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Greater Manchest",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Greater Manchester",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Greater Manchester-l",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Greator London",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Denchworth Roa",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Devon Devon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Henham Bishop",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Lane Burnham B",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Lane Colney He",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Little Barring",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Potton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Road Finsbury",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Road Southflee",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Road Tewin Her",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Southwick West",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Trading Estate",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Green Winchmore Hill",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Greenwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Greine, Faucheldean,",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Grove Aldwick West S",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Grove Birminghan Wes",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Grove Draycott Derby",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Grove East Dulwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Grove Fairfield Stoc",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Grove Greenwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Grove Handsworth/bir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gtr. Manchester",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gwennyn Rhoose Point",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "GWENT",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Gwent.",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "GWYNEDD",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hall Farm Sandon Her",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hall Lane Cawston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hall Road Colcheste",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "HAMPSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hampshire Hants",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hampshre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hampsjire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hampton Wick Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hamshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hants,",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hants.",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Harbour",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Harbour Kilkeel Down",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Harrogate",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hartfordshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Haslingden Lancashir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hatton Garden Holbor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hawthorns Main Stree",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Heinkel-str Burbach",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Helizabeth Close Sto",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hemel Hemstead Hemel",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hendon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "HEREFORDSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Herfortshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Herifordshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hernebay Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hertfodrshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "HERTFORDSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hertfordshire Englan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hertfordshire,",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hertfortshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Herts",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hertsfordshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hertz",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Heulyn Gwynedd",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "High Street Greenlaw",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Highland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Highworth Wiltshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Barnt Green Wor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Bough Beech Ken",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Bounemouth",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Church Langley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Clifton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Cottages Park D",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Cricklade Wilts",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Middlesex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Needham Market",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hill Road Wonersh Su",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hillingdon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hills",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hills Middlesex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hmerseyside",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Horsforth",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Horwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Hounslow",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Admirals Canar",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Calleva Park A",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Coddington Cre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Farm Close She",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Galbraith Stre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Greenwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Hamson Drive B",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Manor Rainahm",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Mead Oxshott S",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Middlebrough",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Middleton Lanc",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House North Street R",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Park Side Aven",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Rillaton Walk",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Rose Lane Nash",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House St.catherine S",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House Watling Street",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "House, Allarburn Kil",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Huntington Cambridge",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ield Road Hurtmore",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Inchinnan Renfrewshi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Industrial Estate De",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Interchange",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "INVERNESS-SHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Investment Managemen",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Island Vale Glamorga",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLANDS",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Isle Dogs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Isle Lewis",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF ARRAN",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF BARRA",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF CUMBRAE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF HARRIS",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF ISLAY",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF JURA",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF LEWIS",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF MAN",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Isle Of Mann",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF NORTH UIST",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Isle Of Scilly",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF SKYE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF SOUTH UIST",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF TIREE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Isle Of Weight",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLE OF WIGHT",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ISLES OF SCILLY",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Jackson Stanton Unde",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "KENT",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kent,",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kent, Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kenton Middlesex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "KINCARDINESHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kings",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kings Lynn Norfolk",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kingston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kingston Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kingston Upon Thames",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "KINROSS-SHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kirkby Ashfield Nott",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kirkbymoorside",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "KIRKCUDBRIGHTSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Kite High Wycombe, B",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Knoll Somerset",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lacncashire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lake Hilltop' West B",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lake, Spine Road Sou",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lake, Watermark Spin",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lakes Larkfield",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lamncashire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lanacashire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "LANARKSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lanc",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lancashir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "LANCASHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lancashire Greater M",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Alvechurch",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Ashby Magna Lei",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Balsall Common",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Barwell",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Blackwater",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Bradley Green W",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Burnham Bucking",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Cassington Oxon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Cole Green Hert",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Coseley West Mi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Cowden Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Danbury Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Dunsfold",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Eggborough York",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Englefield Gree",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Frant East Suss",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Gantshill",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Goring",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Great Cambourne",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Harlaxton Linco",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Hertfordshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane High Heath Pels",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Hopwood West Mi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Kelsall",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Moulton Northan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Radley Oxfordsh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Radnage",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Ramsey Cambridg",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Rosyth Fife",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Rustington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Seatown Dorset",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Shenley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Sible Hedingham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Sipson West Dra",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane South Elmsall W",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane South Ockendon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Staine Upon Tha",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Stanwick Northa",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Thornbury",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Wanstead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane West Ewell",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane West Yorkshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Whitford Devon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Willingham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Wimblington Cam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Wincham Cheshir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Winterborne Sti",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Wiswell Lancs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane Withnell",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lane, Ottry Mary Dev",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lawrence Somerset",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "LEICESTERSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Leicesteshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Leonards",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lewis Morris Llangun",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Leys Gileston Vale G",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "LINCOLNSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lincs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Linford Leicestershi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Little Hulton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Liverpool",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Llanedwen Gwynedd",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Llansilin",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lodge Farm, Tolchest",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "LONDON",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "London,",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Longfords Minchinham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Longridge",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lothian",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lound",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Low, Romiley Cheshir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lurgan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lutin",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Lynn Norfolk",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "M Glam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Maltings Roydon Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "MANCHESTER",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Margarets",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Market Harborough Le",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Maws Craft Centre Fe",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mayfair",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mead North Weald Ess",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Meadow Burton Pidsea",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Meadows Stables Lave",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Melin Ifan Bont",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mersesyside",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "MERSEYSIDE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Merstham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mews Walthamstow",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mews Woolwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mid  Glamorgan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mid Glamogan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "MID GLAMORGAN",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mid-glamorgan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mid. Glamorgan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Middelsex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Middlese",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "MIDDLESEX",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Middlesex, United Ki",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Middx Middlesex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Middx.",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Midlands",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "MIDLOTHIAN",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Millington Road Midd",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Milton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Milton Keynes",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Minutes Ducie Street",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Monkey Island Lane B",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "MORAYSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Morlais Trimsaran",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Morland Road Birch V",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Moulton Seas",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Mountfitchet Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "N Humberside",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "N.e. Lincolnshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "N.yorkshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "NAIRNSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Naite Thornbury Sout",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Nden Road Headcorn K",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Newcastle",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Newham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Newton Aycliffe",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Newton Ferrers Devon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Non-us/other",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "None",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "NORFOLK",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Norfolk England",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Norfolk,",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North Ayrshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North Braunstone Lei",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North East Lincolnsh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North East Linconshi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "NORTH HUMBERSIDE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North Lincolnshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North Lincs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North West Lancashir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North Wraxall Wiltsh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North Yorks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North Yorkshie",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "NORTH YORKSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "North Yoskhire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "NORTHAMPTONSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Northamptonshirei",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Northamtonshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Northern Ireland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Northern Road Kintor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Northfleet",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Northmberland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "NORTHUMBERLAND",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Northumberside",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Northumnberland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Norwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "NOTTINGHAMSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Nottinshamshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Nr.leominster Herefo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Oakley Beds",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Office Village Bambe",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ohio",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Olaves Close, Staine",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ORKNEY",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ouse Hill Eversley H",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "OXFORDSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Oxon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Paarade Redbridge Es",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Padiham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Papworth Everard",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Parade Bawtry",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Parade Caversfield O",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Chorleywood Her",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Croyde North De",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Moss Side Lytha",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Portlethen Aber",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Rathfriland Cou",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Road Hednesford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Road Kingsheath",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Road Stanstead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Road Upton Mers",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Waterside Derry",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park West Lothian",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Park Yatton North So",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Parks Haydock",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "PEEBLESSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Pen-y-bont Ogwr",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Percy North Yorkshir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "PERTHSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Peterborough",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Peverel",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Bidford Avon W",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Burton Upon Tr",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Cambridgeshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Chicheley Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Dagehnam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Fauldhouse",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Kingston Surre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Oldbrook",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Portishead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place S-o-t",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Wickhams Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Place Woolwich Lindo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Plumstead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Point Bridge Walk Gr",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Point Poplar Place T",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Pontardawe",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Pontbren Road Hafody",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Portishead Brustol S",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "POWYS",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Prade Goodmayes Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Precinct Compstall R",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Preston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Primorsko Goranska",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Quays",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Radcliffe Trent Nott",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Registration Refer P",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "RENFREWSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Research Park Waterb",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ride Finchampstead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ridgeway Codicote He",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ridgeway Friston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Rise Eltham Greenwic",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Rise Moulton Vale Gl",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Rise South Kingston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Abronhill Cumbe",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Aghalee",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Aigburth",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Albrighton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Alvechurch",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Alveston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Amesbury",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Astley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Ballygowan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Ballygowan Down",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Beamish",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Beddington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Bedfont Middles",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Bere Regis Dors",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Bickerd Upon Av",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Bidford Avon Wa",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Birstall",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Boroughbridge",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Brides Major Va",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Bromborough Mer",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Bromham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Brumley Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Bulphan Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Burnside Kilsyt",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Burton Latimer",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Cambridgeshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Catford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Chadwell Heath",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Chemsford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Cheveley Suffol",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Claudy Derry",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Clayhill Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Clun Shropshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Cockfosters",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Codicote Hertfo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Condorrat",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Corfe Mullen Do",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Corfe Mullen Wi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Crossgates Fife",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Croxley Green H",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Datchet",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Datchet Berkshi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Degenham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Denton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Dersingham King",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Disley Cheshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Downham Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Dromara Down",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Duxford Cambrid",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Dyserth Denbigh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Ealing",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road East",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road East Lothian",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road East Suusex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Eldwick West Yo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Ferring West Su",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Flat",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Flitwick Beds",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Flockton West Y",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Fulham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Glaston Rutland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Glouchester",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Goodmayes Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Great Bardfield",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Great Bookham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Great Bookham S",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Greywell Hampsh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Gwaun-leision",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hadleigh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hale Cheshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hale Hale Chesh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hampshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hampstead Camde",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Handsworth",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Harold Hill Ess",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Headington Oxfo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Heald Green",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Heald Green Che",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Heaton Mersey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Henley-upon-tha",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hersham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hersham Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hetton Hole Tyn",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Heysham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Holmes Chapel C",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hoxne Suffolk",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Hoylake",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Irthlingborough",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Kennett Suffolk",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Kesh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Kilburn Park",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Kingston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Kirkby Ashfield",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Knaphill Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Langford Bedfor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Langham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Laugharne Carma",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Leicetser",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Little Bytham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Llanharan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Long Hanborough",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Lordswood Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Lytham Annes",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Maghull Merseys",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Market Deeping",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Merseyside",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Mills High Peak",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Monkton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Moreden",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Ness",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Ness Cheshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Newtownstewart",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road North Carbrain",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road North Warnborou",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Northfields Ind",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Northfleet Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Northumberland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Oakwood",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Oundle",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Paddock Wood Ke",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Parkgate",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Parkstone Dorse",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Penge",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Penkridge Staff",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Pettswood",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Prestwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Preswitch",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Rathfriland Dow",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Ribchester Lanc",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Rolleston Dove",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Rosyth Fife",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Runwell Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Sauthsea",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Sheebeg Lisnask",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Shrewley Warwic",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road South Cave East",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road South Norwood",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road South Woodham F",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Southgate",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Standon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Staplehurst",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Staveley, Cumbr",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Steppingley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Stock-on-trent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Stony Stratford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Stratford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Strathallan Bri",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Strood",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Swinton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Thorne",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Thornley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Tiptree Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Tiptree Essex E",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Tivetshall Marg",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Trimdon Village",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Turnford Hertfo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Upminister",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Upper Rissingto",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Upton Park",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Vale Hampshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Waddington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Waltham Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Walthamstow",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Wawne East York",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Wenhaston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road West Ealing",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road West Horndon Es",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road West Kingsdown",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road West Kirby Mers",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road West Ramsbottom",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Westgate Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Whetstone Leice",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Winson Green",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Wolverhempton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Woodford Green",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Worcs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Wraysbury Stain",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road Yatton Somerset",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road, Islington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Road,flat Kent,maids",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Rookery West Thurroc",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Ropeworks Brayford P",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ROSS-SHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Rousdon, Lyme Regis",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "ROXBURGHSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Rural Hertfordshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "S Glam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "S Yorkshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "S.glamorgan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "S/glam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Saint Edmunds Suffol",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Sceaux Gardens Cambe",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "School Much Birch He",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Scotland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Scotney Hampshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SELKIRKSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Service Station Goud",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Severn Gloucestershi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Sherwood Hope Derbys",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SHETLAND",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Shetland Islands",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Shillingstone",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SHROPSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Sinton Worcestershir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Soar Leicestershire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SOMERSET",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Sorn",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South  Glamorgan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SOUTH GLAMORGAN",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SOUTH HUMBERSIDE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Lanarkshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Oxfordshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Staffordshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Suffolk",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Tyneside",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Uist",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Wales",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South West",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Wirral",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Yorkhire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "South Yorks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SOUTH YORKSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Southampton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Southdene Kirkby",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Southglamorgan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Southwater",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Southwick",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Spinney Finchampstea",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Spur Kingswood Surre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Square Belmont",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Square Darley Abbey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Square Whitechapel",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Stables Chestnut Far",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Stables Dagnall",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "STAFFORDSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Staffordshired",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Staines-upon-thames",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Stanwell Staines-upo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Steep Rise Headingto",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Stffordshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Stirling",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "STIRLINGSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Stoneclough Radcliff",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Stowe Drive Catteric",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Strabe Monchengladba",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Alvaston Derd",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Bacton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Barkingside",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Bawtry South",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Beckley Oxfor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Boldon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Boughton Unde",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Brightlingsea",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Broughton Ast",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Bucks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Caerdydd",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Caergybi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Canvey Islend",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Charmouth Dor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Claverham Nor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Comber",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Derbyshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Derry",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Doddington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Douglas",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Douglas Lanar",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Douglas South",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Earl Shilton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Edinbrugh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Edlesborough",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Farnworth",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Fleckney Leic",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Flitwick Bedf",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Fofar",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Garforth West",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Green Road Gr",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Greenwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Ground Floor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Headley Surre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Heath Reach B",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Henley-in-ard",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Herzlyia",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Hetton Hole",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Higham Ferrer",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Horbury",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Horwich",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Hurstpierpoin",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Inverallochy",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Kegworth",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Ketton Rutlan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Lambourn Berk",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Littlebourgh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Llanberis",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Lower Sunbury",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Lytham Lancas",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Milngavie",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Mursley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Neilston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Newcastle-und",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street North Wood Hi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Orford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Oswaldtwistle",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Plaistow",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Portadown",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Portishead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Portishead No",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Rishton Lanca",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Scopwick Linc",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Shrivenham Ox",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Southshields",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Stokesley Nor",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Stratford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Swannington L",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Teweksbury Gl",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Thringstone",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Thurlaston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Thurnscoe Eas",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Warnham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Weston-s-mare",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Whalley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street White Notley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Woburn Sands",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Woodcross",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Street Woodville Swa",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Strood",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Sudley Warwickshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SUFFOLK",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Sunderland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SURREY",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Surrey England",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Surrey Lodnon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Surrey London",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Surrey United Kingdo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Sussex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "SUTHERLAND",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Sutton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Sycamore Street Carm",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Syston Leicestershir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Taunton",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Technopole Kingston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Terrace Forgeside Bl",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Terrace Foveran Aber",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Terrace Quarry Hill",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Terrace Rosemarkie",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Terrace Viewpark",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Thames",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Thamesmead",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Thapston Ketteirng N",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Thrapston Northampto",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Thrapston Northants",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tree Avenue Newton M",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tree Bebington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tree Close Barwell",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tree Court Avenue Pi",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Trees Newcastle-unde",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tumble",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tune And Wear",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tyne & Wear",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tyne &amp; Wear",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "TYNE AND WEAR",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tyne Wear",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Tyrone",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Uk",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Unit Bell Lane Gibra",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "United Arab Emirates",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "United Kingdom",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "United Kingdom Essex",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Upon Mendip Somerset",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Upon Thames",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Upon Thames Surrey",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Vale Glam",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Vale Glamorgan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Vale Peterborugh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Verne Church Crookha",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "View Ellistown",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "View Hamstreet Kent",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "View Kempsford",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "View Knott",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "View Manse Road Aber",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "View West Road High",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Village Chaddesley C",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Village Hampshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "W.yorks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Waddington",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wales",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Walk Laureneckirk Ab",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Walk Shantallow Derr",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wandsworth",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Warkwickshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Warley West Midlands",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "WARWICKSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Warwiickshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Waye Heston",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Welford Northants",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Berkshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "WEST GLAMORGAN",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Hadley Wood Her",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "WEST LOTHIAN",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Midland",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "WEST MIDLANDS",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Midlands Staffo",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Midlands,",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Mids",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Susse",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "WEST SUSSEX",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Sussexs",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Yorks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Yorkshifre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "WEST YORKSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "West Yorskhire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Westbeams Road Sway",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Westhoughton Lancash",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Westmidlands",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Weston Mare Somerset",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Whaley Bridge",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wharf Dobcross Yorks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Whitburn Westlothian",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Whitwick Leicestersh",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wickham Bishops Esse",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wigan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "WIGTOWNSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wiiltshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wilenhall Westmidlan",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "William Street",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "William Street Tunst",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wilthire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wiltshir",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "WILTSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wiltshre",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wiltsshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wimbledon",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wirral",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Witlshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Witshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wood Glover Hoewes C",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wood Herts",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "WORCESTERSHIRE",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Worcestershite",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Worth",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Wrexham",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Yard Bedgebury Road",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Yarn Close Hampshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Yiewsley",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Yorks",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Yorkshire",
          "stateCode": ""
        },
        {
          "countryName": "United Kingdom",
          "countryCode": "UK",
          "stateName": "Yorkshire England",
          "stateCode": ""
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Alabama",
          "stateCode": "AL"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Alaska",
          "stateCode": "AK"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Arizona",
          "stateCode": "AZ"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Arkansas",
          "stateCode": "AR"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "California",
          "stateCode": "CA"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Colorado",
          "stateCode": "CO"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Connecticut",
          "stateCode": "CT"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Delaware",
          "stateCode": "DE"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "District of Columbia",
          "stateCode": "DC"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Florida",
          "stateCode": "FL"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Georgia",
          "stateCode": "GA"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Hawaii",
          "stateCode": "HI"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Idaho",
          "stateCode": "ID"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Illinois",
          "stateCode": "IL"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Indiana",
          "stateCode": "IN"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Iowa",
          "stateCode": "IA"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Kansas",
          "stateCode": "KS"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Kentucky",
          "stateCode": "KY"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Louisiana",
          "stateCode": "LA"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Maine",
          "stateCode": "ME"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Maryland",
          "stateCode": "MD"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Massachusetts",
          "stateCode": "MA"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Michigan",
          "stateCode": "MI"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Minnesota",
          "stateCode": "MN"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Mississippi",
          "stateCode": "MS"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Missouri",
          "stateCode": "MO"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Montana",
          "stateCode": "MT"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Nebraska",
          "stateCode": "NE"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Nevada",
          "stateCode": "NV"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "New Hampshire",
          "stateCode": "NH"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "New Jersey",
          "stateCode": "NJ"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "New Mexico",
          "stateCode": "NM"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "New York",
          "stateCode": "NY"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "North Carolina",
          "stateCode": "NC"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "North Dakota",
          "stateCode": "ND"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Ohio",
          "stateCode": "OH"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Oklahoma",
          "stateCode": "OK"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Oregon",
          "stateCode": "OR"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Pennsylvania",
          "stateCode": "PA"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Puerto Rico",
          "stateCode": "PR"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Rhode Island",
          "stateCode": "RI"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "South Carolina",
          "stateCode": "SC"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "South Dakota",
          "stateCode": "SD"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Tennessee",
          "stateCode": "TN"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Texas",
          "stateCode": "TX"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Utah",
          "stateCode": "UT"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Vermont",
          "stateCode": "VT"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Virgin Islands",
          "stateCode": "VI"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Virginia",
          "stateCode": "VA"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Washington",
          "stateCode": "WA"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "West Virginia",
          "stateCode": "WV"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Wisconsin",
          "stateCode": "WI"
        },
        {
          "countryName": "United States",
          "countryCode": "US",
          "stateName": "Wyoming",
          "stateCode": "WY"
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "ARTIGAS",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "CANELONES",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "CERRO LARGO",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "COLONIA",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "DURAZNO",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "FLORES",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "FLORIDA",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "LAVALLEJA",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "MALDONADO",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "MONTEVIDEO",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "PAYSANDU",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "RIO NEGRO",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "RIVERA",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "ROCHA",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "SALTO",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "SAN JOSE",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "SORIANO",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "TACUAREMBO",
          "stateCode": ""
        },
        {
          "countryName": "Uruguay",
          "countryCode": "UY",
          "stateName": "TREINTA Y TRES",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "Arnos Vale",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "Beachmont",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "Bequia Isl",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "Granada",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "Kingston",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "Kingstown",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "Mustique",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "St George",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "St Vincent",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "St.Vincent",
          "stateCode": ""
        },
        {
          "countryName": "Saint Vincent and the Grenadines",
          "countryCode": "VC",
          "stateName": "Vicente",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "ANZOATEGUI",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "APURE",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "ARAGUA",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Bahoruco",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "BARINAS",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "BOLIVAR",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "CARABOBO",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "COJEDES",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Curacao",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "D. C.",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "DELTA AMACURO",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "DISTRITO FEDERAL",
          "stateCode": "DF"
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "FALCON",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "FEDERAL AMAZONAS",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Florida",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Guajira",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "GUARICO",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Guatemala",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Guayas",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "LARA",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "MERIDA",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Micoud",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "MIRANDA",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "MONAGAS",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "NUEVA ESPARTA",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Pampartar",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Panama",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Porlamar",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "PORTUGUESA",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "San Cristo",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "SUCRE",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "TACHIRA",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Trelawny",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "TRUJILLO",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "Vargas",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "YARACUY",
          "stateCode": ""
        },
        {
          "countryName": "Venezuela",
          "countryCode": "VE",
          "stateName": "ZULIA",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "A. Verapaz",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "Gr Cayman",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "Grand Turk",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "New Prov",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "Olancho",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "Pembroke",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "Road Town",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "St John",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "St Maarten",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "St Thomas",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "Tobago",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "Tolima",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "TORTOLA",
          "stateCode": ""
        },
        {
          "countryName": "Virgin Islands (British)",
          "countryCode": "VG",
          "stateName": "V. Gorda",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "An Giang",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Ba Ria-Vung Tau",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Ben Tre",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Binh Dinh",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Binh Thuan",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Can Tho",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Cao Bang",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Dac Lac",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Dong Nai",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Dong Thap",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "East Java",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Gia Lai",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Ha Giang",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Ha Tay",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Ha Tinh",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Hai Phong",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Hanoi",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Hau Giang",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Ho Chi Minh City",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Hoa Binh",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Khanh Hoa",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Kien Giang",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Kon Tum",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Lai Chau",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Lam Dong",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Lang Son",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Lao Cai",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Long An",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Nam Ha",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Nghe An",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Ninh Binh",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Ninh Thuan",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Quang Binh",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Quang Nam",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Quang Nam Da Nang",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Quang Ngai",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Quang Ninh",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Quang Tri",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Soc Trang",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Son La",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Tay Ninh",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Thai Binh",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Thanh Hoa",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Thua Thien-Hue",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Thuan An",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Tien Giang",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Tra Vinh",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Tuyen Quang",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Vinh Long",
          "stateCode": ""
        },
        {
          "countryName": "Vietnam",
          "countryCode": "VN",
          "stateName": "Yen Bai",
          "stateCode": ""
        },
        {
          "countryName": "Vanuatu",
          "countryCode": "VU",
          "stateName": "EFATE",
          "stateCode": ""
        },
        {
          "countryName": "Vanuatu",
          "countryCode": "VU",
          "stateName": "ESPIRITU SANTO",
          "stateCode": ""
        },
        {
          "countryName": "Vanuatu",
          "countryCode": "VU",
          "stateName": "NC",
          "stateCode": ""
        },
        {
          "countryName": "Vanuatu",
          "countryCode": "VU",
          "stateName": "Nguna",
          "stateCode": ""
        },
        {
          "countryName": "Vanuatu",
          "countryCode": "VU",
          "stateName": "PORT VILA",
          "stateCode": ""
        },
        {
          "countryName": "Vanuatu",
          "countryCode": "VU",
          "stateName": "SANTO",
          "stateCode": ""
        },
        {
          "countryName": "Vanuatu",
          "countryCode": "VU",
          "stateName": "SHEFA PROVINCE",
          "stateCode": ""
        },
        {
          "countryName": "Vanuatu",
          "countryCode": "VU",
          "stateName": "VA",
          "stateCode": ""
        },
        {
          "countryName": "Vanuatu",
          "countryCode": "VU",
          "stateName": "VAN",
          "stateCode": ""
        },
        {
          "countryName": "Samoa",
          "countryCode": "WS",
          "stateName": "APIA",
          "stateCode": ""
        },
        {
          "countryName": "Samoa",
          "countryCode": "WS",
          "stateName": "SA",
          "stateCode": ""
        },
        {
          "countryName": "Samoa",
          "countryCode": "WS",
          "stateName": "SAMOA",
          "stateCode": ""
        },
        {
          "countryName": "Samoa",
          "countryCode": "WS",
          "stateName": "Taipei",
          "stateCode": ""
        },
        {
          "countryName": "Samoa",
          "countryCode": "WS",
          "stateName": "THE INDEPENDENT STATE OF SAMOA",
          "stateCode": ""
        },
        {
          "countryName": "Samoa",
          "countryCode": "WS",
          "stateName": "UPOLU",
          "stateCode": ""
        },
        {
          "countryName": "Samoa",
          "countryCode": "WS",
          "stateName": "WEST SAMOA",
          "stateCode": ""
        },
        {
          "countryName": "Samoa",
          "countryCode": "WS",
          "stateName": "WESTERN SAMOA",
          "stateCode": ""
        },
        {
          "countryName": "Samoa",
          "countryCode": "WS",
          "stateName": "WS",
          "stateCode": ""
        },
        {
          "countryName": "",
          "countryCode": "XT",
          "stateName": "Turkish Republic Of Northern Cyprus",
          "stateCode": ""
        },
        {
          "countryName": "South Africa",
          "countryCode": "ZA",
          "stateName": "Eastern Cape",
          "stateCode": ""
        },
        {
          "countryName": "South Africa",
          "countryCode": "ZA",
          "stateName": "Free State",
          "stateCode": ""
        },
        {
          "countryName": "South Africa",
          "countryCode": "ZA",
          "stateName": "Gauteng",
          "stateCode": ""
        },
        {
          "countryName": "South Africa",
          "countryCode": "ZA",
          "stateName": "KwaZulu-Natal",
          "stateCode": ""
        },
        {
          "countryName": "South Africa",
          "countryCode": "ZA",
          "stateName": "Limpopo",
          "stateCode": ""
        },
        {
          "countryName": "South Africa",
          "countryCode": "ZA",
          "stateName": "Mpumalanga",
          "stateCode": ""
        },
        {
          "countryName": "South Africa",
          "countryCode": "ZA",
          "stateName": "North West",
          "stateCode": ""
        },
        {
          "countryName": "South Africa",
          "countryCode": "ZA",
          "stateName": "Northern Cape",
          "stateCode": ""
        },
        {
          "countryName": "South Africa",
          "countryCode": "ZA",
          "stateName": "Western Cape",
          "stateCode": ""
        }
      ]

    AllStates=[
      {
        "short": "AF",
        "name": "Afghanistan",
        "states": [
          "Badakhshan",
          "Badgis",
          "Baglan",
          "Balkh",
          "Bamiyan",
          "Farah",
          "Faryab",
          "Gawr",
          "Gazni",
          "Herat",
          "Hilmand",
          "Jawzjan",
          "Kabul",
          "Kapisa",
          "Khawst",
          "Kunar",
          "Lagman",
          "Lawghar",
          "Nangarhar",
          "Nimruz",
          "Nuristan",
          "Paktika",
          "Paktiya",
          "Parwan",
          "Qandahar",
          "Qunduz",
          "Samangan",
          "Sar-e Pul",
          "Takhar",
          "Uruzgan",
          "Wardag",
          "Zabul"
        ]
      },
      {
        "short": "AL",
        "name": "Albania",
        "states": [
          "Berat",
          "Bulqize",
          "Delvine",
          "Devoll",
          "Dibre",
          "Durres",
          "Elbasan",
          "Fier",
          "Gjirokaster",
          "Gramsh",
          "Has",
          "Kavaje",
          "Kolonje",
          "Korce",
          "Kruje",
          "Kucove",
          "Kukes",
          "Kurbin",
          "Lezhe",
          "Librazhd",
          "Lushnje",
          "Mallakaster",
          "Malsi e Madhe",
          "Mat",
          "Mirdite",
          "Peqin",
          "Permet",
          "Pogradec",
          "Puke",
          "Sarande",
          "Shkoder",
          "Skrapar",
          "Tepelene",
          "Tirane",
          "Tropoje",
          "Vlore"
        ]
      },
      {
        "short": "DZ",
        "name": "Algeria",
        "states": [
          "Ayn Daflah",
          "Ayn Tamushanat",
          "Adrar",
          "Algiers",
          "Annabah",
          "Bashshar",
          "Batnah",
          "Bijayah",
          "Biskrah",
          "Blidah",
          "Buirah",
          "Bumardas",
          "Burj Bu Arririj",
          "Ghalizan",
          "Ghardayah",
          "Ilizi",
          "Jijili",
          "Jilfah",
          "Khanshalah",
          "Masilah",
          "Midyah",
          "Milah",
          "Muaskar",
          "Mustaghanam",
          "Naama",
          "Oran",
          "Ouargla",
          "Qalmah",
          "Qustantinah",
          "Sakikdah",
          "Satif",
          "Sayda",
          "Sidi ban-al-''Abbas",
          "Suq Ahras",
          "Tamanghasat",
          "Tibazah",
          "Tibissah",
          "Tilimsan",
          "Tinduf",
          "Tisamsilt",
          "Tiyarat",
          "Tizi Wazu",
          "Umm-al-Bawaghi",
          "Wahran",
          "Warqla",
          "Wilaya d Alger",
          "Wilaya de Bejaia",
          "Wilaya de Constantine",
          "al-Aghwat",
          "al-Bayadh",
          "al-Jaza''ir",
          "al-Wad",
          "ash-Shalif",
          "at-Tarif"
        ]
      },
      {
        "short": "AS",
        "name": "American Samoa",
        "states": [
          "Eastern",
          "Manu''a",
          "Swains Island",
          "Western"
        ]
      },
      {
        "short": "AD",
        "name": "Andorra",
        "states": [
          "Andorra la Vella",
          "Canillo",
          "Encamp",
          "La Massana",
          "Les Escaldes",
          "Ordino",
          "Sant Julia de Loria"
        ]
      },
      {
        "short": "AO",
        "name": "Angola",
        "states": [
          "Bengo",
          "Benguela",
          "Bie",
          "Cabinda",
          "Cunene",
          "Huambo",
          "Huila",
          "Kuando-Kubango",
          "Kwanza Norte",
          "Kwanza Sul",
          "Luanda",
          "Lunda Norte",
          "Lunda Sul",
          "Malanje",
          "Moxico",
          "Namibe",
          "Uige",
          "Zaire"
        ]
      },
      {
        "short": "AI",
        "name": "Anguilla",
        "states": [
          "Other Provinces"
        ]
      },
      {
        "short": "AQ",
        "name": "Antarctica",
        "states": [
          "Sector claimed by Argentina/Ch",
          "Sector claimed by Argentina/UK",
          "Sector claimed by Australia",
          "Sector claimed by France",
          "Sector claimed by New Zealand",
          "Sector claimed by Norway",
          "Unclaimed Sector"
        ]
      },
      {
        "short": "AG",
        "name": "Antigua And Barbuda",
        "states": [
          "Barbuda",
          "Saint George",
          "Saint John",
          "Saint Mary",
          "Saint Paul",
          "Saint Peter",
          "Saint Philip"
        ]
      },
      {
        "short": "AR",
        "name": "Argentina",
        "states": [
          "Buenos Aires",
          "Catamarca",
          "Chaco",
          "Chubut",
          "Cordoba",
          "Corrientes",
          "Distrito Federal",
          "Entre Rios",
          "Formosa",
          "Jujuy",
          "La Pampa",
          "La Rioja",
          "Mendoza",
          "Misiones",
          "Neuquen",
          "Rio Negro",
          "Salta",
          "San Juan",
          "San Luis",
          "Santa Cruz",
          "Santa Fe",
          "Santiago del Estero",
          "Tierra del Fuego",
          "Tucuman"
        ]
      },
      {
        "short": "AM",
        "name": "Armenia",
        "states": [
          "Aragatsotn",
          "Ararat",
          "Armavir",
          "Gegharkunik",
          "Kotaik",
          "Lori",
          "Shirak",
          "Stepanakert",
          "Syunik",
          "Tavush",
          "Vayots Dzor",
          "Yerevan"
        ]
      },
      {
        "short": "AW",
        "name": "Aruba",
        "states": [
          "Aruba"
        ]
      },
      {
        "short": "AU",
        "name": "Australia",
        "states": [
          "Auckland",
          "Australian Capital Territory",
          "Balgowlah",
          "Balmain",
          "Bankstown",
          "Baulkham Hills",
          "Bonnet Bay",
          "Camberwell",
          "Carole Park",
          "Castle Hill",
          "Caulfield",
          "Chatswood",
          "Cheltenham",
          "Cherrybrook",
          "Clayton",
          "Collingwood",
          "Frenchs Forest",
          "Hawthorn",
          "Jannnali",
          "Knoxfield",
          "Melbourne",
          "New South Wales",
          "Northern Territory",
          "Perth",
          "Queensland",
          "South Australia",
          "Tasmania",
          "Templestowe",
          "Victoria",
          "Werribee south",
          "Western Australia",
          "Wheeler"
        ]
      },
      {
        "short": "AT",
        "name": "Austria",
        "states": [
          "Bundesland Salzburg",
          "Bundesland Steiermark",
          "Bundesland Tirol",
          "Burgenland",
          "Carinthia",
          "Karnten",
          "Liezen",
          "Lower Austria",
          "Niederosterreich",
          "Oberosterreich",
          "Salzburg",
          "Schleswig-Holstein",
          "Steiermark",
          "Styria",
          "Tirol",
          "Upper Austria",
          "Vorarlberg",
          "Wien"
        ]
      },
      {
        "short": "AZ",
        "name": "Azerbaijan",
        "states": [
          "Abseron",
          "Baki Sahari",
          "Ganca",
          "Ganja",
          "Kalbacar",
          "Lankaran",
          "Mil-Qarabax",
          "Mugan-Salyan",
          "Nagorni-Qarabax",
          "Naxcivan",
          "Priaraks",
          "Qazax",
          "Saki",
          "Sirvan",
          "Xacmaz"
        ]
      },
      {
        "short": "BS",
        "name": "Bahamas The",
        "states": [
          "Abaco",
          "Acklins Island",
          "Andros",
          "Berry Islands",
          "Biminis",
          "Cat Island",
          "Crooked Island",
          "Eleuthera",
          "Exuma and Cays",
          "Grand Bahama",
          "Inagua Islands",
          "Long Island",
          "Mayaguana",
          "New Providence",
          "Ragged Island",
          "Rum Cay",
          "San Salvador"
        ]
      },
      {
        "short": "BH",
        "name": "Bahrain",
        "states": [
          "Isa",
          "Badiyah",
          "Hidd",
          "Jidd Hafs",
          "Mahama",
          "Manama",
          "Sitrah",
          "al-Manamah",
          "al-Muharraq",
          "ar-Rifa''a"
        ]
      },
      {
        "short": "BD",
        "name": "Bangladesh",
        "states": [
          "Bagar Hat",
          "Bandarban",
          "Barguna",
          "Barisal",
          "Bhola",
          "Bogora",
          "Brahman Bariya",
          "Chandpur",
          "Chattagam",
          "Chittagong Division",
          "Chuadanga",
          "Dhaka",
          "Dinajpur",
          "Faridpur",
          "Feni",
          "Gaybanda",
          "Gazipur",
          "Gopalganj",
          "Habiganj",
          "Jaipur Hat",
          "Jamalpur",
          "Jessor",
          "Jhalakati",
          "Jhanaydah",
          "Khagrachhari",
          "Khulna",
          "Kishorganj",
          "Koks Bazar",
          "Komilla",
          "Kurigram",
          "Kushtiya",
          "Lakshmipur",
          "Lalmanir Hat",
          "Madaripur",
          "Magura",
          "Maimansingh",
          "Manikganj",
          "Maulvi Bazar",
          "Meherpur",
          "Munshiganj",
          "Naral",
          "Narayanganj",
          "Narsingdi",
          "Nator",
          "Naugaon",
          "Nawabganj",
          "Netrakona",
          "Nilphamari",
          "Noakhali",
          "Pabna",
          "Panchagarh",
          "Patuakhali",
          "Pirojpur",
          "Rajbari",
          "Rajshahi",
          "Rangamati",
          "Rangpur",
          "Satkhira",
          "Shariatpur",
          "Sherpur",
          "Silhat",
          "Sirajganj",
          "Sunamganj",
          "Tangayal",
          "Thakurgaon"
        ]
      },
      {
        "short": "BB",
        "name": "Barbados",
        "states": [
          "Christ Church",
          "Saint Andrew",
          "Saint George",
          "Saint James",
          "Saint John",
          "Saint Joseph",
          "Saint Lucy",
          "Saint Michael",
          "Saint Peter",
          "Saint Philip",
          "Saint Thomas"
        ]
      },
      {
        "short": "BY",
        "name": "Belarus",
        "states": [
          "Brest",
          "Homjel",
          "Hrodna",
          "Mahiljow",
          "Mahilyowskaya Voblasts",
          "Minsk",
          "Minskaja Voblasts",
          "Petrik",
          "Vicebsk"
        ]
      },
      {
        "short": "BE",
        "name": "Belgium",
        "states": [
          "Antwerpen",
          "Berchem",
          "Brabant",
          "Brabant Wallon",
          "Brussel",
          "East Flanders",
          "Hainaut",
          "Liege",
          "Limburg",
          "Luxembourg",
          "Namur",
          "Ontario",
          "Oost-Vlaanderen",
          "Provincie Brabant",
          "Vlaams-Brabant",
          "Wallonne",
          "West-Vlaanderen"
        ]
      },
      {
        "short": "BZ",
        "name": "Belize",
        "states": [
          "Belize",
          "Cayo",
          "Corozal",
          "Orange Walk",
          "Stann Creek",
          "Toledo"
        ]
      },
      {
        "short": "BJ",
        "name": "Benin",
        "states": [
          "Alibori",
          "Atacora",
          "Atlantique",
          "Borgou",
          "Collines",
          "Couffo",
          "Donga",
          "Littoral",
          "Mono",
          "Oueme",
          "Plateau",
          "Zou"
        ]
      },
      {
        "short": "BM",
        "name": "Bermuda",
        "states": [
          "Hamilton",
          "Saint George"
        ]
      },
      {
        "short": "BT",
        "name": "Bhutan",
        "states": [
          "Bumthang",
          "Chhukha",
          "Chirang",
          "Daga",
          "Geylegphug",
          "Ha",
          "Lhuntshi",
          "Mongar",
          "Pemagatsel",
          "Punakha",
          "Rinpung",
          "Samchi",
          "Samdrup Jongkhar",
          "Shemgang",
          "Tashigang",
          "Timphu",
          "Tongsa",
          "Wangdiphodrang"
        ]
      },
      {
        "short": "BO",
        "name": "Bolivia",
        "states": [
          "Beni",
          "Chuquisaca",
          "Cochabamba",
          "La Paz",
          "Oruro",
          "Pando",
          "Potosi",
          "Santa Cruz",
          "Tarija"
        ]
      },
      {
        "short": "BA",
        "name": "Bosnia and Herzegovina",
        "states": [
          "Federacija Bosna i Hercegovina",
          "Republika Srpska"
        ]
      },
      {
        "short": "BW",
        "name": "Botswana",
        "states": [
          "Central Bobonong",
          "Central Boteti",
          "Central Mahalapye",
          "Central Serowe-Palapye",
          "Central Tutume",
          "Chobe",
          "Francistown",
          "Gaborone",
          "Ghanzi",
          "Jwaneng",
          "Kgalagadi North",
          "Kgalagadi South",
          "Kgatleng",
          "Kweneng",
          "Lobatse",
          "Ngamiland",
          "Ngwaketse",
          "North East",
          "Okavango",
          "Orapa",
          "Selibe Phikwe",
          "South East",
          "Sowa"
        ]
      },
      {
        "short": "BV",
        "name": "Bouvet Island",
        "states": [
          "Bouvet Island"
        ]
      },
      {
        "short": "BR",
        "name": "Brazil",
        "states": [
          "Acre",
          "Alagoas",
          "Amapa",
          "Amazonas",
          "Bahia",
          "Ceara",
          "Distrito Federal",
          "Espirito Santo",
          "Estado de Sao Paulo",
          "Goias",
          "Maranhao",
          "Mato Grosso",
          "Mato Grosso do Sul",
          "Minas Gerais",
          "Para",
          "Paraiba",
          "Parana",
          "Pernambuco",
          "Piaui",
          "Rio Grande do Norte",
          "Rio Grande do Sul",
          "Rio de Janeiro",
          "Rondonia",
          "Roraima",
          "Santa Catarina",
          "Sao Paulo",
          "Sergipe",
          "Tocantins"
        ]
      },
      {
        "short": "IO",
        "name": "British Indian Ocean Territory",
        "states": [
          "British Indian Ocean Territory"
        ]
      },
      {
        "short": "BN",
        "name": "Brunei",
        "states": [
          "Belait",
          "Brunei-Muara",
          "Temburong",
          "Tutong"
        ]
      },
      {
        "short": "BG",
        "name": "Bulgaria",
        "states": [
          "Blagoevgrad",
          "Burgas",
          "Dobrich",
          "Gabrovo",
          "Haskovo",
          "Jambol",
          "Kardzhali",
          "Kjustendil",
          "Lovech",
          "Montana",
          "Oblast Sofiya-Grad",
          "Pazardzhik",
          "Pernik",
          "Pleven",
          "Plovdiv",
          "Razgrad",
          "Ruse",
          "Shumen",
          "Silistra",
          "Sliven",
          "Smoljan",
          "Sofija grad",
          "Sofijska oblast",
          "Stara Zagora",
          "Targovishte",
          "Varna",
          "Veliko Tarnovo",
          "Vidin",
          "Vraca",
          "Yablaniza"
        ]
      },
      {
        "short": "BF",
        "name": "Burkina Faso",
        "states": [
          "Bale",
          "Bam",
          "Bazega",
          "Bougouriba",
          "Boulgou",
          "Boulkiemde",
          "Comoe",
          "Ganzourgou",
          "Gnagna",
          "Gourma",
          "Houet",
          "Ioba",
          "Kadiogo",
          "Kenedougou",
          "Komandjari",
          "Kompienga",
          "Kossi",
          "Kouritenga",
          "Kourweogo",
          "Leraba",
          "Mouhoun",
          "Nahouri",
          "Namentenga",
          "Noumbiel",
          "Oubritenga",
          "Oudalan",
          "Passore",
          "Poni",
          "Sanguie",
          "Sanmatenga",
          "Seno",
          "Sissili",
          "Soum",
          "Sourou",
          "Tapoa",
          "Tuy",
          "Yatenga",
          "Zondoma",
          "Zoundweogo"
        ]
      },
      {
        "short": "BI",
        "name": "Burundi",
        "states": [
          "Bubanza",
          "Bujumbura",
          "Bururi",
          "Cankuzo",
          "Cibitoke",
          "Gitega",
          "Karuzi",
          "Kayanza",
          "Kirundo",
          "Makamba",
          "Muramvya",
          "Muyinga",
          "Ngozi",
          "Rutana",
          "Ruyigi"
        ]
      },
      {
        "short": "KH",
        "name": "Cambodia",
        "states": [
          "Banteay Mean Chey",
          "Bat Dambang",
          "Kampong Cham",
          "Kampong Chhnang",
          "Kampong Spoeu",
          "Kampong Thum",
          "Kampot",
          "Kandal",
          "Kaoh Kong",
          "Kracheh",
          "Krong Kaeb",
          "Krong Pailin",
          "Krong Preah Sihanouk",
          "Mondol Kiri",
          "Otdar Mean Chey",
          "Phnum Penh",
          "Pousat",
          "Preah Vihear",
          "Prey Veaeng",
          "Rotanak Kiri",
          "Siem Reab",
          "Stueng Traeng",
          "Svay Rieng",
          "Takaev"
        ]
      },
      {
        "short": "CM",
        "name": "Cameroon",
        "states": [
          "Adamaoua",
          "Centre",
          "Est",
          "Littoral",
          "Nord",
          "Nord Extreme",
          "Nordouest",
          "Ouest",
          "Sud",
          "Sudouest"
        ]
      },
      {
        "short": "CA",
        "name": "Canada",
        "states": [
          "Alberta",
          "British Columbia",
          "Manitoba",
          "New Brunswick",
          "Newfoundland and Labrador",
          "Northwest Territories",
          "Nova Scotia",
          "Nunavut",
          "Ontario",
          "Prince Edward Island",
          "Quebec",
          "Saskatchewan",
          "Yukon"
        ]
      },
      {
        "short": "CV",
        "name": "Cape Verde",
        "states": [
          "Boavista",
          "Brava",
          "Fogo",
          "Maio",
          "Sal",
          "Santo Antao",
          "Sao Nicolau",
          "Sao Tiago",
          "Sao Vicente"
        ]
      },
      {
        "short": "KY",
        "name": "Cayman Islands",
        "states": [
          "Grand Cayman"
        ]
      },
      {
        "short": "CF",
        "name": "Central African Republic",
        "states": [
          "Bamingui-Bangoran",
          "Bangui",
          "Basse-Kotto",
          "Haut-Mbomou",
          "Haute-Kotto",
          "Kemo",
          "Lobaye",
          "Mambere-Kadei",
          "Mbomou",
          "Nana-Gribizi",
          "Nana-Mambere",
          "Ombella Mpoko",
          "Ouaka",
          "Ouham",
          "Ouham-Pende",
          "Sangha-Mbaere",
          "Vakaga"
        ]
      },
      {
        "short": "TD",
        "name": "Chad",
        "states": [
          "Batha",
          "Biltine",
          "Bourkou-Ennedi-Tibesti",
          "Chari-Baguirmi",
          "Guera",
          "Kanem",
          "Lac",
          "Logone Occidental",
          "Logone Oriental",
          "Mayo-Kebbi",
          "Moyen-Chari",
          "Ouaddai",
          "Salamat",
          "Tandjile"
        ]
      },
      {
        "short": "CL",
        "name": "Chile",
        "states": [
          "Aisen",
          "Antofagasta",
          "Araucania",
          "Atacama",
          "Bio Bio",
          "Coquimbo",
          "Libertador General Bernardo O",
          "Los Lagos",
          "Magellanes",
          "Maule",
          "Metropolitana",
          "Metropolitana de Santiago",
          "Tarapaca",
          "Valparaiso"
        ]
      },
      {
        "short": "CN",
        "name": "China",
        "states": [
          "Anhui",
          "Anhui Province",
          "Anhui Sheng",
          "Aomen",
          "Beijing",
          "Beijing Shi",
          "Chongqing",
          "Fujian",
          "Fujian Sheng",
          "Gansu",
          "Guangdong",
          "Guangdong Sheng",
          "Guangxi",
          "Guizhou",
          "Hainan",
          "Hebei",
          "Heilongjiang",
          "Henan",
          "Hubei",
          "Hunan",
          "Jiangsu",
          "Jiangsu Sheng",
          "Jiangxi",
          "Jilin",
          "Liaoning",
          "Liaoning Sheng",
          "Nei Monggol",
          "Ningxia Hui",
          "Qinghai",
          "Shaanxi",
          "Shandong",
          "Shandong Sheng",
          "Shanghai",
          "Shanxi",
          "Sichuan",
          "Tianjin",
          "Xianggang",
          "Xinjiang",
          "Xizang",
          "Yunnan",
          "Zhejiang",
          "Zhejiang Sheng"
        ]
      },
      {
        "short": "CX",
        "name": "Christmas Island",
        "states": [
          "Christmas Island"
        ]
      },
      {
        "short": "CC",
        "name": "Cocos (Keeling) Islands",
        "states": [
          "Cocos (Keeling) Islands"
        ]
      },
      {
        "short": "CO",
        "name": "Colombia",
        "states": [
          "Amazonas",
          "Antioquia",
          "Arauca",
          "Atlantico",
          "Bogota",
          "Bolivar",
          "Boyaca",
          "Caldas",
          "Caqueta",
          "Casanare",
          "Cauca",
          "Cesar",
          "Choco",
          "Cordoba",
          "Cundinamarca",
          "Guainia",
          "Guaviare",
          "Huila",
          "La Guajira",
          "Magdalena",
          "Meta",
          "Narino",
          "Norte de Santander",
          "Putumayo",
          "Quindio",
          "Risaralda",
          "San Andres y Providencia",
          "Santander",
          "Sucre",
          "Tolima",
          "Valle del Cauca",
          "Vaupes",
          "Vichada"
        ]
      },
      {
        "short": "KM",
        "name": "Comoros",
        "states": [
          "Mwali",
          "Njazidja",
          "Nzwani"
        ]
      },
      {
        "short": "CG",
        "name": "Republic Of The Congo",
        "states": [
          "Bouenza",
          "Brazzaville",
          "Cuvette",
          "Kouilou",
          "Lekoumou",
          "Likouala",
          "Niari",
          "Plateaux",
          "Pool",
          "Sangha"
        ]
      },
      {
        "short": "CD",
        "name": "Democratic Republic Of The Congo",
        "states": [
          "Bandundu",
          "Bas-Congo",
          "Equateur",
          "Haut-Congo",
          "Kasai-Occidental",
          "Kasai-Oriental",
          "Katanga",
          "Kinshasa",
          "Maniema",
          "Nord-Kivu",
          "Sud-Kivu"
        ]
      },
      {
        "short": "CK",
        "name": "Cook Islands",
        "states": [
          "Aitutaki",
          "Atiu",
          "Mangaia",
          "Manihiki",
          "Mauke",
          "Mitiaro",
          "Nassau",
          "Pukapuka",
          "Rakahanga",
          "Rarotonga",
          "Tongareva"
        ]
      },
      {
        "short": "CR",
        "name": "Costa Rica",
        "states": [
          "Alajuela",
          "Cartago",
          "Guanacaste",
          "Heredia",
          "Limon",
          "Puntarenas",
          "San Jose"
        ]
      },
      {
        "short": "CI",
        "name": "Cote D''Ivoire (Ivory Coast)",
        "states": [
          "Abidjan",
          "Agneby",
          "Bafing",
          "Denguele",
          "Dix-huit Montagnes",
          "Fromager",
          "Haut-Sassandra",
          "Lacs",
          "Lagunes",
          "Marahoue",
          "Moyen-Cavally",
          "Moyen-Comoe",
          "N''zi-Comoe",
          "Sassandra",
          "Savanes",
          "Sud-Bandama",
          "Sud-Comoe",
          "Vallee du Bandama",
          "Worodougou",
          "Zanzan"
        ]
      },
      {
        "short": "HR",
        "name": "Croatia (Hrvatska)",
        "states": [
          "Bjelovar-Bilogora",
          "Dubrovnik-Neretva",
          "Grad Zagreb",
          "Istra",
          "Karlovac",
          "Koprivnica-Krizhevci",
          "Krapina-Zagorje",
          "Lika-Senj",
          "Medhimurje",
          "Medimurska Zupanija",
          "Osijek-Baranja",
          "Osjecko-Baranjska Zupanija",
          "Pozhega-Slavonija",
          "Primorje-Gorski Kotar",
          "Shibenik-Knin",
          "Sisak-Moslavina",
          "Slavonski Brod-Posavina",
          "Split-Dalmacija",
          "Varazhdin",
          "Virovitica-Podravina",
          "Vukovar-Srijem",
          "Zadar",
          "Zagreb"
        ]
      },
      {
        "short": "CU",
        "name": "Cuba",
        "states": [
          "Camaguey",
          "Ciego de Avila",
          "Cienfuegos",
          "Ciudad de la Habana",
          "Granma",
          "Guantanamo",
          "Habana",
          "Holguin",
          "Isla de la Juventud",
          "La Habana",
          "Las Tunas",
          "Matanzas",
          "Pinar del Rio",
          "Sancti Spiritus",
          "Santiago de Cuba",
          "Villa Clara"
        ]
      },
      {
        "short": "CY",
        "name": "Cyprus",
        "states": [
          "Government controlled area",
          "Limassol",
          "Nicosia District",
          "Paphos",
          "Turkish controlled area"
        ]
      },
      {
        "short": "CZ",
        "name": "Czech Republic",
        "states": [
          "Central Bohemian",
          "Frycovice",
          "Jihocesky Kraj",
          "Jihochesky",
          "Jihomoravsky",
          "Karlovarsky",
          "Klecany",
          "Kralovehradecky",
          "Liberecky",
          "Lipov",
          "Moravskoslezsky",
          "Olomoucky",
          "Olomoucky Kraj",
          "Pardubicky",
          "Plzensky",
          "Praha",
          "Rajhrad",
          "Smirice",
          "South Moravian",
          "Straz nad Nisou",
          "Stredochesky",
          "Unicov",
          "Ustecky",
          "Valletta",
          "Velesin",
          "Vysochina",
          "Zlinsky"
        ]
      },
      {
        "short": "DK",
        "name": "Denmark",
        "states": [
          "Arhus",
          "Bornholm",
          "Frederiksborg",
          "Fyn",
          "Hovedstaden",
          "Kobenhavn",
          "Kobenhavns Amt",
          "Kobenhavns Kommune",
          "Nordjylland",
          "Ribe",
          "Ringkobing",
          "Roervig",
          "Roskilde",
          "Roslev",
          "Sjaelland",
          "Soeborg",
          "Sonderjylland",
          "Storstrom",
          "Syddanmark",
          "Toelloese",
          "Vejle",
          "Vestsjalland",
          "Viborg"
        ]
      },
      {
        "short": "DJ",
        "name": "Djibouti",
        "states": [
          "Ali Sabih",
          "Dikhil",
          "Jibuti",
          "Tajurah",
          "Ubuk"
        ]
      },
      {
        "short": "DM",
        "name": "Dominica",
        "states": [
          "Saint Andrew",
          "Saint David",
          "Saint George",
          "Saint John",
          "Saint Joseph",
          "Saint Luke",
          "Saint Mark",
          "Saint Patrick",
          "Saint Paul",
          "Saint Peter"
        ]
      },
      {
        "short": "DO",
        "name": "Dominican Republic",
        "states": [
          "Azua",
          "Bahoruco",
          "Barahona",
          "Dajabon",
          "Distrito Nacional",
          "Duarte",
          "El Seybo",
          "Elias Pina",
          "Espaillat",
          "Hato Mayor",
          "Independencia",
          "La Altagracia",
          "La Romana",
          "La Vega",
          "Maria Trinidad Sanchez",
          "Monsenor Nouel",
          "Monte Cristi",
          "Monte Plata",
          "Pedernales",
          "Peravia",
          "Puerto Plata",
          "Salcedo",
          "Samana",
          "San Cristobal",
          "San Juan",
          "San Pedro de Macoris",
          "Sanchez Ramirez",
          "Santiago",
          "Santiago Rodriguez",
          "Valverde"
        ]
      },
      {
        "short": "TP",
        "name": "East Timor",
        "states": [
          "Aileu",
          "Ainaro",
          "Ambeno",
          "Baucau",
          "Bobonaro",
          "Cova Lima",
          "Dili",
          "Ermera",
          "Lautem",
          "Liquica",
          "Manatuto",
          "Manufahi",
          "Viqueque"
        ]
      },
      {
        "short": "EC",
        "name": "Ecuador",
        "states": [
          "Azuay",
          "Bolivar",
          "Canar",
          "Carchi",
          "Chimborazo",
          "Cotopaxi",
          "El Oro",
          "Esmeraldas",
          "Galapagos",
          "Guayas",
          "Imbabura",
          "Loja",
          "Los Rios",
          "Manabi",
          "Morona Santiago",
          "Napo",
          "Orellana",
          "Pastaza",
          "Pichincha",
          "Sucumbios",
          "Tungurahua",
          "Zamora Chinchipe"
        ]
      },
      {
        "short": "EG",
        "name": "Egypt",
        "states": [
          "Aswan",
          "Asyut",
          "Bani Suwayf",
          "Bur Sa''id",
          "Cairo",
          "Dumyat",
          "Kafr-ash-Shaykh",
          "Matruh",
          "Muhafazat ad Daqahliyah",
          "Muhafazat al Fayyum",
          "Muhafazat al Gharbiyah",
          "Muhafazat al Iskandariyah",
          "Muhafazat al Qahirah",
          "Qina",
          "Sawhaj",
          "Sina al-Janubiyah",
          "Sina ash-Shamaliyah",
          "ad-Daqahliyah",
          "al-Bahr-al-Ahmar",
          "al-Buhayrah",
          "al-Fayyum",
          "al-Gharbiyah",
          "al-Iskandariyah",
          "al-Ismailiyah",
          "al-Jizah",
          "al-Minufiyah",
          "al-Minya",
          "al-Qahira",
          "al-Qalyubiyah",
          "al-Uqsur",
          "al-Wadi al-Jadid",
          "as-Suways",
          "ash-Sharqiyah"
        ]
      },
      {
        "short": "SV",
        "name": "El Salvador",
        "states": [
          "Ahuachapan",
          "Cabanas",
          "Chalatenango",
          "Cuscatlan",
          "La Libertad",
          "La Paz",
          "La Union",
          "Morazan",
          "San Miguel",
          "San Salvador",
          "San Vicente",
          "Santa Ana",
          "Sonsonate",
          "Usulutan"
        ]
      },
      {
        "short": "GQ",
        "name": "Equatorial Guinea",
        "states": [
          "Annobon",
          "Bioko Norte",
          "Bioko Sur",
          "Centro Sur",
          "Kie-Ntem",
          "Litoral",
          "Wele-Nzas"
        ]
      },
      {
        "short": "ER",
        "name": "Eritrea",
        "states": [
          "Anseba",
          "Debub",
          "Debub-Keih-Bahri",
          "Gash-Barka",
          "Maekel",
          "Semien-Keih-Bahri"
        ]
      },
      {
        "short": "EE",
        "name": "Estonia",
        "states": [
          "Harju",
          "Hiiu",
          "Ida-Viru",
          "Jarva",
          "Jogeva",
          "Laane",
          "Laane-Viru",
          "Parnu",
          "Polva",
          "Rapla",
          "Saare",
          "Tartu",
          "Valga",
          "Viljandi",
          "Voru"
        ]
      },
      {
        "short": "ET",
        "name": "Ethiopia",
        "states": [
          "Addis Abeba",
          "Afar",
          "Amhara",
          "Benishangul",
          "Diredawa",
          "Gambella",
          "Harar",
          "Jigjiga",
          "Mekele",
          "Oromia",
          "Somali",
          "Southern",
          "Tigray"
        ]
      },
      {
        "short": "XA",
        "name": "External Territories of Australia",
        "states": [
          "Christmas Island",
          "Cocos Islands",
          "Coral Sea Islands"
        ]
      },
      {
        "short": "FK",
        "name": "Falkland Islands",
        "states": [
          "Falkland Islands",
          "South Georgia"
        ]
      },
      {
        "short": "FO",
        "name": "Faroe Islands",
        "states": [
          "Klaksvik",
          "Nor ara Eysturoy",
          "Nor oy",
          "Sandoy",
          "Streymoy",
          "Su uroy",
          "Sy ra Eysturoy",
          "Torshavn",
          "Vaga"
        ]
      },
      {
        "short": "FJ",
        "name": "Fiji Islands",
        "states": [
          "Central",
          "Eastern",
          "Northern",
          "South Pacific",
          "Western"
        ]
      },
      {
        "short": "FI",
        "name": "Finland",
        "states": [
          "Ahvenanmaa",
          "Etela-Karjala",
          "Etela-Pohjanmaa",
          "Etela-Savo",
          "Etela-Suomen Laani",
          "Ita-Suomen Laani",
          "Ita-Uusimaa",
          "Kainuu",
          "Kanta-Hame",
          "Keski-Pohjanmaa",
          "Keski-Suomi",
          "Kymenlaakso",
          "Lansi-Suomen Laani",
          "Lappi",
          "Northern Savonia",
          "Ostrobothnia",
          "Oulun Laani",
          "Paijat-Hame",
          "Pirkanmaa",
          "Pohjanmaa",
          "Pohjois-Karjala",
          "Pohjois-Pohjanmaa",
          "Pohjois-Savo",
          "Saarijarvi",
          "Satakunta",
          "Southern Savonia",
          "Tavastia Proper",
          "Uleaborgs Lan",
          "Uusimaa",
          "Varsinais-Suomi"
        ]
      },
      {
        "short": "FR",
        "name": "France",
        "states": [
          "Ain",
          "Aisne",
          "Albi Le Sequestre",
          "Allier",
          "Alpes-Cote dAzur",
          "Alpes-Maritimes",
          "Alpes-de-Haute-Provence",
          "Alsace",
          "Aquitaine",
          "Ardeche",
          "Ardennes",
          "Ariege",
          "Aube",
          "Aude",
          "Auvergne",
          "Aveyron",
          "Bas-Rhin",
          "Basse-Normandie",
          "Bouches-du-Rhone",
          "Bourgogne",
          "Bretagne",
          "Brittany",
          "Burgundy",
          "Calvados",
          "Cantal",
          "Cedex",
          "Centre",
          "Charente",
          "Charente-Maritime",
          "Cher",
          "Correze",
          "Corse-du-Sud",
          "Cote-d''Or",
          "Cotes-d''Armor",
          "Creuse",
          "Crolles",
          "Deux-Sevres",
          "Dordogne",
          "Doubs",
          "Drome",
          "Essonne",
          "Eure",
          "Eure-et-Loir",
          "Feucherolles",
          "Finistere",
          "Franche-Comte",
          "Gard",
          "Gers",
          "Gironde",
          "Haut-Rhin",
          "Haute-Corse",
          "Haute-Garonne",
          "Haute-Loire",
          "Haute-Marne",
          "Haute-Saone",
          "Haute-Savoie",
          "Haute-Vienne",
          "Hautes-Alpes",
          "Hautes-Pyrenees",
          "Hauts-de-Seine",
          "Herault",
          "Ile-de-France",
          "Ille-et-Vilaine",
          "Indre",
          "Indre-et-Loire",
          "Isere",
          "Jura",
          "Klagenfurt",
          "Landes",
          "Languedoc-Roussillon",
          "Larcay",
          "Le Castellet",
          "Le Creusot",
          "Limousin",
          "Loir-et-Cher",
          "Loire",
          "Loire-Atlantique",
          "Loiret",
          "Lorraine",
          "Lot",
          "Lot-et-Garonne",
          "Lower Normandy",
          "Lozere",
          "Maine-et-Loire",
          "Manche",
          "Marne",
          "Mayenne",
          "Meurthe-et-Moselle",
          "Meuse",
          "Midi-Pyrenees",
          "Morbihan",
          "Moselle",
          "Nievre",
          "Nord",
          "Nord-Pas-de-Calais",
          "Oise",
          "Orne",
          "Paris",
          "Pas-de-Calais",
          "Pays de la Loire",
          "Pays-de-la-Loire",
          "Picardy",
          "Puy-de-Dome",
          "Pyrenees-Atlantiques",
          "Pyrenees-Orientales",
          "Quelmes",
          "Rhone",
          "Rhone-Alpes",
          "Saint Ouen",
          "Saint Viatre",
          "Saone-et-Loire",
          "Sarthe",
          "Savoie",
          "Seine-Maritime",
          "Seine-Saint-Denis",
          "Seine-et-Marne",
          "Somme",
          "Sophia Antipolis",
          "Souvans",
          "Tarn",
          "Tarn-et-Garonne",
          "Territoire de Belfort",
          "Treignac",
          "Upper Normandy",
          "Val-d''Oise",
          "Val-de-Marne",
          "Var",
          "Vaucluse",
          "Vellise",
          "Vendee",
          "Vienne",
          "Vosges",
          "Yonne",
          "Yvelines"
        ]
      },
      {
        "short": "GF",
        "name": "French Guiana",
        "states": [
          "Cayenne",
          "Saint-Laurent-du-Maroni"
        ]
      },
      {
        "short": "PF",
        "name": "French Polynesia",
        "states": [
          "Iles du Vent",
          "Iles sous le Vent",
          "Marquesas",
          "Tuamotu",
          "Tubuai"
        ]
      },
      {
        "short": "TF",
        "name": "French Southern Territories",
        "states": [
          "Amsterdam",
          "Crozet Islands",
          "Kerguelen"
        ]
      },
      {
        "short": "GA",
        "name": "Gabon",
        "states": [
          "Estuaire",
          "Haut-Ogooue",
          "Moyen-Ogooue",
          "Ngounie",
          "Nyanga",
          "Ogooue-Ivindo",
          "Ogooue-Lolo",
          "Ogooue-Maritime",
          "Woleu-Ntem"
        ]
      },
      {
        "short": "GM",
        "name": "Gambia The",
        "states": [
          "Banjul",
          "Basse",
          "Brikama",
          "Janjanbureh",
          "Kanifing",
          "Kerewan",
          "Kuntaur",
          "Mansakonko"
        ]
      },
      {
        "short": "GE",
        "name": "Georgia",
        "states": [
          "Abhasia",
          "Ajaria",
          "Guria",
          "Imereti",
          "Kaheti",
          "Kvemo Kartli",
          "Mcheta-Mtianeti",
          "Racha",
          "Samagrelo-Zemo Svaneti",
          "Samche-Zhavaheti",
          "Shida Kartli",
          "Tbilisi"
        ]
      },
      {
        "short": "DE",
        "name": "Germany",
        "states": [
          "Auvergne",
          "Baden-Wurttemberg",
          "Bavaria",
          "Bayern",
          "Beilstein Wurtt",
          "Berlin",
          "Brandenburg",
          "Bremen",
          "Dreisbach",
          "Freistaat Bayern",
          "Hamburg",
          "Hannover",
          "Heroldstatt",
          "Hessen",
          "Kortenberg",
          "Laasdorf",
          "Land Baden-Wurttemberg",
          "Land Bayern",
          "Land Brandenburg",
          "Land Hessen",
          "Land Mecklenburg-Vorpommern",
          "Land Nordrhein-Westfalen",
          "Land Rheinland-Pfalz",
          "Land Sachsen",
          "Land Sachsen-Anhalt",
          "Land Thuringen",
          "Lower Saxony",
          "Mecklenburg-Vorpommern",
          "Mulfingen",
          "Munich",
          "Neubeuern",
          "Niedersachsen",
          "Noord-Holland",
          "Nordrhein-Westfalen",
          "North Rhine-Westphalia",
          "Osterode",
          "Rheinland-Pfalz",
          "Rhineland-Palatinate",
          "Saarland",
          "Sachsen",
          "Sachsen-Anhalt",
          "Saxony",
          "Schleswig-Holstein",
          "Thuringia",
          "Webling",
          "Weinstrabe",
          "schlobborn"
        ]
      },
      {
        "short": "GH",
        "name": "Ghana",
        "states": [
          "Ashanti",
          "Brong-Ahafo",
          "Central",
          "Eastern",
          "Greater Accra",
          "Northern",
          "Upper East",
          "Upper West",
          "Volta",
          "Western"
        ]
      },
      {
        "short": "GI",
        "name": "Gibraltar",
        "states": [
          "Gibraltar"
        ]
      },
      {
        "short": "GR",
        "name": "Greece",
        "states": [
          "Acharnes",
          "Ahaia",
          "Aitolia kai Akarnania",
          "Argolis",
          "Arkadia",
          "Arta",
          "Attica",
          "Attiki",
          "Ayion Oros",
          "Crete",
          "Dodekanisos",
          "Drama",
          "Evia",
          "Evritania",
          "Evros",
          "Evvoia",
          "Florina",
          "Fokis",
          "Fthiotis",
          "Grevena",
          "Halandri",
          "Halkidiki",
          "Hania",
          "Heraklion",
          "Hios",
          "Ilia",
          "Imathia",
          "Ioannina",
          "Iraklion",
          "Karditsa",
          "Kastoria",
          "Kavala",
          "Kefallinia",
          "Kerkira",
          "Kiklades",
          "Kilkis",
          "Korinthia",
          "Kozani",
          "Lakonia",
          "Larisa",
          "Lasithi",
          "Lesvos",
          "Levkas",
          "Magnisia",
          "Messinia",
          "Nomos Attikis",
          "Nomos Zakynthou",
          "Pella",
          "Pieria",
          "Piraios",
          "Preveza",
          "Rethimni",
          "Rodopi",
          "Samos",
          "Serrai",
          "Thesprotia",
          "Thessaloniki",
          "Trikala",
          "Voiotia",
          "West Greece",
          "Xanthi",
          "Zakinthos"
        ]
      },
      {
        "short": "GL",
        "name": "Greenland",
        "states": [
          "Aasiaat",
          "Ammassalik",
          "Illoqqortoormiut",
          "Ilulissat",
          "Ivittuut",
          "Kangaatsiaq",
          "Maniitsoq",
          "Nanortalik",
          "Narsaq",
          "Nuuk",
          "Paamiut",
          "Qaanaaq",
          "Qaqortoq",
          "Qasigiannguit",
          "Qeqertarsuaq",
          "Sisimiut",
          "Udenfor kommunal inddeling",
          "Upernavik",
          "Uummannaq"
        ]
      },
      {
        "short": "GD",
        "name": "Grenada",
        "states": [
          "Carriacou-Petite Martinique",
          "Saint Andrew",
          "Saint Davids",
          "Saint George''s",
          "Saint John",
          "Saint Mark",
          "Saint Patrick"
        ]
      },
      {
        "short": "GP",
        "name": "Guadeloupe",
        "states": [
          "Basse-Terre",
          "Grande-Terre",
          "Iles des Saintes",
          "La Desirade",
          "Marie-Galante",
          "Saint Barthelemy",
          "Saint Martin"
        ]
      },
      {
        "short": "GU",
        "name": "Guam",
        "states": [
          "Agana Heights",
          "Agat",
          "Barrigada",
          "Chalan-Pago-Ordot",
          "Dededo",
          "Hagatna",
          "Inarajan",
          "Mangilao",
          "Merizo",
          "Mongmong-Toto-Maite",
          "Santa Rita",
          "Sinajana",
          "Talofofo",
          "Tamuning",
          "Yigo",
          "Yona"
        ]
      },
      {
        "short": "GT",
        "name": "Guatemala",
        "states": [
          "Alta Verapaz",
          "Baja Verapaz",
          "Chimaltenango",
          "Chiquimula",
          "El Progreso",
          "Escuintla",
          "Guatemala",
          "Huehuetenango",
          "Izabal",
          "Jalapa",
          "Jutiapa",
          "Peten",
          "Quezaltenango",
          "Quiche",
          "Retalhuleu",
          "Sacatepequez",
          "San Marcos",
          "Santa Rosa",
          "Solola",
          "Suchitepequez",
          "Totonicapan",
          "Zacapa"
        ]
      },
      {
        "short": "XU",
        "name": "Guernsey and Alderney",
        "states": [
          "Alderney",
          "Castel",
          "Forest",
          "Saint Andrew",
          "Saint Martin",
          "Saint Peter Port",
          "Saint Pierre du Bois",
          "Saint Sampson",
          "Saint Saviour",
          "Sark",
          "Torteval",
          "Vale"
        ]
      },
      {
        "short": "GN",
        "name": "Guinea",
        "states": [
          "Beyla",
          "Boffa",
          "Boke",
          "Conakry",
          "Coyah",
          "Dabola",
          "Dalaba",
          "Dinguiraye",
          "Faranah",
          "Forecariah",
          "Fria",
          "Gaoual",
          "Gueckedou",
          "Kankan",
          "Kerouane",
          "Kindia",
          "Kissidougou",
          "Koubia",
          "Koundara",
          "Kouroussa",
          "Labe",
          "Lola",
          "Macenta",
          "Mali",
          "Mamou",
          "Mandiana",
          "Nzerekore",
          "Pita",
          "Siguiri",
          "Telimele",
          "Tougue",
          "Yomou"
        ]
      },
      {
        "short": "GW",
        "name": "Guinea-Bissau",
        "states": [
          "Bafata",
          "Bissau",
          "Bolama",
          "Cacheu",
          "Gabu",
          "Oio",
          "Quinara",
          "Tombali"
        ]
      },
      {
        "short": "GY",
        "name": "Guyana",
        "states": [
          "Barima-Waini",
          "Cuyuni-Mazaruni",
          "Demerara-Mahaica",
          "East Berbice-Corentyne",
          "Essequibo Islands-West Demerar",
          "Mahaica-Berbice",
          "Pomeroon-Supenaam",
          "Potaro-Siparuni",
          "Upper Demerara-Berbice",
          "Upper Takutu-Upper Essequibo"
        ]
      },
      {
        "short": "HT",
        "name": "Haiti",
        "states": [
          "Artibonite",
          "Centre",
          "Grand''Anse",
          "Nord",
          "Nord-Est",
          "Nord-Ouest",
          "Ouest",
          "Sud",
          "Sud-Est"
        ]
      },
      {
        "short": "HM",
        "name": "Heard and McDonald Islands",
        "states": [
          "Heard and McDonald Islands"
        ]
      },
      {
        "short": "HN",
        "name": "Honduras",
        "states": [
          "Atlantida",
          "Choluteca",
          "Colon",
          "Comayagua",
          "Copan",
          "Cortes",
          "Distrito Central",
          "El Paraiso",
          "Francisco Morazan",
          "Gracias a Dios",
          "Intibuca",
          "Islas de la Bahia",
          "La Paz",
          "Lempira",
          "Ocotepeque",
          "Olancho",
          "Santa Barbara",
          "Valle",
          "Yoro"
        ]
      },
      {
        "short": "HK",
        "name": "Hong Kong S.A.R.",
        "states": [
          "Hong Kong"
        ]
      },
      {
        "short": "HU",
        "name": "Hungary",
        "states": [
          "Bacs-Kiskun",
          "Baranya",
          "Bekes",
          "Borsod-Abauj-Zemplen",
          "Budapest",
          "Csongrad",
          "Fejer",
          "Gyor-Moson-Sopron",
          "Hajdu-Bihar",
          "Heves",
          "Jasz-Nagykun-Szolnok",
          "Komarom-Esztergom",
          "Nograd",
          "Pest",
          "Somogy",
          "Szabolcs-Szatmar-Bereg",
          "Tolna",
          "Vas",
          "Veszprem",
          "Zala"
        ]
      },
      {
        "short": "IS",
        "name": "Iceland",
        "states": [
          "Austurland",
          "Gullbringusysla",
          "Hofu borgarsva i",
          "Nor urland eystra",
          "Nor urland vestra",
          "Su urland",
          "Su urnes",
          "Vestfir ir",
          "Vesturland"
        ]
      },
      {
        "short": "IN",
        "name": "India",
        "states": [
          "Andaman and Nicobar Islands",
          "Andhra Pradesh",
          "Arunachal Pradesh",
          "Assam",
          "Bihar",
          "Chandigarh",
          "Chhattisgarh",
          "Dadra and Nagar Haveli",
          "Daman and Diu",
          "Delhi",
          "Goa",
          "Gujarat",
          "Haryana",
          "Himachal Pradesh",
          "Jammu and Kashmir",
          "Jharkhand",
          "Karnataka",
          "Kenmore",
          "Kerala",
          "Lakshadweep",
          "Madhya Pradesh",
          "Maharashtra",
          "Manipur",
          "Meghalaya",
          "Mizoram",
          "Nagaland",
          "Narora",
          "Natwar",
          "Odisha",
          "Paschim Medinipur",
          "Pondicherry",
          "Punjab",
          "Rajasthan",
          "Sikkim",
          "Tamil Nadu",
          "Telangana",
          "Tripura",
          "Uttar Pradesh",
          "Uttarakhand",
          "Vaishali",
          "West Bengal"
        ]
      },
      {
        "short": "ID",
        "name": "Indonesia",
        "states": [
          "Aceh",
          "Bali",
          "Bangka-Belitung",
          "Banten",
          "Bengkulu",
          "Gandaria",
          "Gorontalo",
          "Jakarta",
          "Jambi",
          "Jawa Barat",
          "Jawa Tengah",
          "Jawa Timur",
          "Kalimantan Barat",
          "Kalimantan Selatan",
          "Kalimantan Tengah",
          "Kalimantan Timur",
          "Kendal",
          "Lampung",
          "Maluku",
          "Maluku Utara",
          "Nusa Tenggara Barat",
          "Nusa Tenggara Timur",
          "Papua",
          "Riau",
          "Riau Kepulauan",
          "Solo",
          "Sulawesi Selatan",
          "Sulawesi Tengah",
          "Sulawesi Tenggara",
          "Sulawesi Utara",
          "Sumatera Barat",
          "Sumatera Selatan",
          "Sumatera Utara",
          "Yogyakarta"
        ]
      },
      {
        "short": "IR",
        "name": "Iran",
        "states": [
          "Ardabil",
          "Azarbayjan-e Bakhtari",
          "Azarbayjan-e Khavari",
          "Bushehr",
          "Chahar Mahal-e Bakhtiari",
          "Esfahan",
          "Fars",
          "Gilan",
          "Golestan",
          "Hamadan",
          "Hormozgan",
          "Ilam",
          "Kerman",
          "Kermanshah",
          "Khorasan",
          "Khuzestan",
          "Kohgiluyeh-e Boyerahmad",
          "Kordestan",
          "Lorestan",
          "Markazi",
          "Mazandaran",
          "Ostan-e Esfahan",
          "Qazvin",
          "Qom",
          "Semnan",
          "Sistan-e Baluchestan",
          "Tehran",
          "Yazd",
          "Zanjan"
        ]
      },
      {
        "short": "IQ",
        "name": "Iraq",
        "states": [
          "Babil",
          "Baghdad",
          "Dahuk",
          "Dhi Qar",
          "Diyala",
          "Erbil",
          "Irbil",
          "Karbala",
          "Kurdistan",
          "Maysan",
          "Ninawa",
          "Salah-ad-Din",
          "Wasit",
          "al-Anbar",
          "al-Basrah",
          "al-Muthanna",
          "al-Qadisiyah",
          "an-Najaf",
          "as-Sulaymaniyah",
          "at-Ta''mim"
        ]
      },
      {
        "short": "IE",
        "name": "Ireland",
        "states": [
          "Armagh",
          "Carlow",
          "Cavan",
          "Clare",
          "Cork",
          "Donegal",
          "Dublin",
          "Galway",
          "Kerry",
          "Kildare",
          "Kilkenny",
          "Laois",
          "Leinster",
          "Leitrim",
          "Limerick",
          "Loch Garman",
          "Longford",
          "Louth",
          "Mayo",
          "Meath",
          "Monaghan",
          "Offaly",
          "Roscommon",
          "Sligo",
          "Tipperary North Riding",
          "Tipperary South Riding",
          "Ulster",
          "Waterford",
          "Westmeath",
          "Wexford",
          "Wicklow"
        ]
      },
      {
        "short": "IL",
        "name": "Israel",
        "states": [
          "Beit Hanania",
          "Ben Gurion Airport",
          "Bethlehem",
          "Caesarea",
          "Centre",
          "Gaza",
          "Hadaron",
          "Haifa District",
          "Hamerkaz",
          "Hazafon",
          "Hebron",
          "Jaffa",
          "Jerusalem",
          "Khefa",
          "Kiryat Yam",
          "Lower Galilee",
          "Qalqilya",
          "Talme Elazar",
          "Tel Aviv",
          "Tsafon",
          "Umm El Fahem",
          "Yerushalayim"
        ]
      },
      {
        "short": "IT",
        "name": "Italy",
        "states": [
          "Abruzzi",
          "Abruzzo",
          "Agrigento",
          "Alessandria",
          "Ancona",
          "Arezzo",
          "Ascoli Piceno",
          "Asti",
          "Avellino",
          "Bari",
          "Basilicata",
          "Belluno",
          "Benevento",
          "Bergamo",
          "Biella",
          "Bologna",
          "Bolzano",
          "Brescia",
          "Brindisi",
          "Calabria",
          "Campania",
          "Cartoceto",
          "Caserta",
          "Catania",
          "Chieti",
          "Como",
          "Cosenza",
          "Cremona",
          "Cuneo",
          "Emilia-Romagna",
          "Ferrara",
          "Firenze",
          "Florence",
          "Forli-Cesena",
          "Friuli-Venezia Giulia",
          "Frosinone",
          "Genoa",
          "Gorizia",
          "L''Aquila",
          "Lazio",
          "Lecce",
          "Lecco",
          "Lecco Province",
          "Liguria",
          "Lodi",
          "Lombardia",
          "Lombardy",
          "Macerata",
          "Mantova",
          "Marche",
          "Messina",
          "Milan",
          "Modena",
          "Molise",
          "Molteno",
          "Montenegro",
          "Monza and Brianza",
          "Naples",
          "Novara",
          "Padova",
          "Parma",
          "Pavia",
          "Perugia",
          "Pesaro-Urbino",
          "Piacenza",
          "Piedmont",
          "Piemonte",
          "Pisa",
          "Pordenone",
          "Potenza",
          "Puglia",
          "Reggio Emilia",
          "Rimini",
          "Roma",
          "Salerno",
          "Sardegna",
          "Sassari",
          "Savona",
          "Sicilia",
          "Siena",
          "Sondrio",
          "South Tyrol",
          "Taranto",
          "Teramo",
          "Torino",
          "Toscana",
          "Trapani",
          "Trentino-Alto Adige",
          "Trento",
          "Treviso",
          "Udine",
          "Umbria",
          "Valle d''Aosta",
          "Varese",
          "Veneto",
          "Venezia",
          "Verbano-Cusio-Ossola",
          "Vercelli",
          "Verona",
          "Vicenza",
          "Viterbo"
        ]
      },
      {
        "short": "JM",
        "name": "Jamaica",
        "states": [
          "Buxoro Viloyati",
          "Clarendon",
          "Hanover",
          "Kingston",
          "Manchester",
          "Portland",
          "Saint Andrews",
          "Saint Ann",
          "Saint Catherine",
          "Saint Elizabeth",
          "Saint James",
          "Saint Mary",
          "Saint Thomas",
          "Trelawney",
          "Westmoreland"
        ]
      },
      {
        "short": "JP",
        "name": "Japan",
        "states": [
          "Aichi",
          "Akita",
          "Aomori",
          "Chiba",
          "Ehime",
          "Fukui",
          "Fukuoka",
          "Fukushima",
          "Gifu",
          "Gumma",
          "Hiroshima",
          "Hokkaido",
          "Hyogo",
          "Ibaraki",
          "Ishikawa",
          "Iwate",
          "Kagawa",
          "Kagoshima",
          "Kanagawa",
          "Kanto",
          "Kochi",
          "Kumamoto",
          "Kyoto",
          "Mie",
          "Miyagi",
          "Miyazaki",
          "Nagano",
          "Nagasaki",
          "Nara",
          "Niigata",
          "Oita",
          "Okayama",
          "Okinawa",
          "Osaka",
          "Saga",
          "Saitama",
          "Shiga",
          "Shimane",
          "Shizuoka",
          "Tochigi",
          "Tokushima",
          "Tokyo",
          "Tottori",
          "Toyama",
          "Wakayama",
          "Yamagata",
          "Yamaguchi",
          "Yamanashi"
        ]
      },
      {
        "short": "XJ",
        "name": "Jersey",
        "states": [
          "Grouville",
          "Saint Brelade",
          "Saint Clement",
          "Saint Helier",
          "Saint John",
          "Saint Lawrence",
          "Saint Martin",
          "Saint Mary",
          "Saint Peter",
          "Saint Saviour",
          "Trinity"
        ]
      },
      {
        "short": "JO",
        "name": "Jordan",
        "states": [
          "Ajlun",
          "Amman",
          "Irbid",
          "Jarash",
          "Ma''an",
          "Madaba",
          "al-''Aqabah",
          "al-Balqa",
          "al-Karak",
          "al-Mafraq",
          "at-Tafilah",
          "az-Zarqa"
        ]
      },
      {
        "short": "KZ",
        "name": "Kazakhstan",
        "states": [
          "Akmecet",
          "Akmola",
          "Aktobe",
          "Almati",
          "Atirau",
          "Batis Kazakstan",
          "Burlinsky Region",
          "Karagandi",
          "Kostanay",
          "Mankistau",
          "Ontustik Kazakstan",
          "Pavlodar",
          "Sigis Kazakstan",
          "Soltustik Kazakstan",
          "Taraz"
        ]
      },
      {
        "short": "KE",
        "name": "Kenya",
        "states": [
          "Central",
          "Coast",
          "Eastern",
          "Nairobi",
          "North Eastern",
          "Nyanza",
          "Rift Valley",
          "Western"
        ]
      },
      {
        "short": "KI",
        "name": "Kiribati",
        "states": [
          "Abaiang",
          "Abemana",
          "Aranuka",
          "Arorae",
          "Banaba",
          "Beru",
          "Butaritari",
          "Kiritimati",
          "Kuria",
          "Maiana",
          "Makin",
          "Marakei",
          "Nikunau",
          "Nonouti",
          "Onotoa",
          "Phoenix Islands",
          "Tabiteuea North",
          "Tabiteuea South",
          "Tabuaeran",
          "Tamana",
          "Tarawa North",
          "Tarawa South",
          "Teraina"
        ]
      },
      {
        "short": "KP",
        "name": "Korea North",
        "states": [
          "Chagangdo",
          "Hamgyeongbukto",
          "Hamgyeongnamdo",
          "Hwanghaebukto",
          "Hwanghaenamdo",
          "Kaeseong",
          "Kangweon",
          "Nampo",
          "Pyeonganbukto",
          "Pyeongannamdo",
          "Pyeongyang",
          "Yanggang"
        ]
      },
      {
        "short": "KR",
        "name": "Korea South",
        "states": [
          "Busan",
          "Cheju",
          "Chollabuk",
          "Chollanam",
          "Chungbuk",
          "Chungcheongbuk",
          "Chungcheongnam",
          "Chungnam",
          "Daegu",
          "Gangwon-do",
          "Goyang-si",
          "Gyeonggi-do",
          "Gyeongsang",
          "Gyeongsangnam-do",
          "Incheon",
          "Jeju-Si",
          "Jeonbuk",
          "Kangweon",
          "Kwangju",
          "Kyeonggi",
          "Kyeongsangbuk",
          "Kyeongsangnam",
          "Kyonggi-do",
          "Kyungbuk-Do",
          "Kyunggi-Do",
          "Kyunggi-do",
          "Pusan",
          "Seoul",
          "Sudogwon",
          "Taegu",
          "Taejeon",
          "Taejon-gwangyoksi",
          "Ulsan",
          "Wonju",
          "gwangyoksi"
        ]
      },
      {
        "short": "KW",
        "name": "Kuwait",
        "states": [
          "Al Asimah",
          "Hawalli",
          "Mishref",
          "Qadesiya",
          "Safat",
          "Salmiya",
          "al-Ahmadi",
          "al-Farwaniyah",
          "al-Jahra",
          "al-Kuwayt"
        ]
      },
      {
        "short": "KG",
        "name": "Kyrgyzstan",
        "states": [
          "Batken",
          "Bishkek",
          "Chui",
          "Issyk-Kul",
          "Jalal-Abad",
          "Naryn",
          "Osh",
          "Talas"
        ]
      },
      {
        "short": "LA",
        "name": "Laos",
        "states": [
          "Attopu",
          "Bokeo",
          "Bolikhamsay",
          "Champasak",
          "Houaphanh",
          "Khammouane",
          "Luang Nam Tha",
          "Luang Prabang",
          "Oudomxay",
          "Phongsaly",
          "Saravan",
          "Savannakhet",
          "Sekong",
          "Viangchan Prefecture",
          "Viangchan Province",
          "Xaignabury",
          "Xiang Khuang"
        ]
      },
      {
        "short": "LV",
        "name": "Latvia",
        "states": [
          "Aizkraukles",
          "Aluksnes",
          "Balvu",
          "Bauskas",
          "Cesu",
          "Daugavpils",
          "Daugavpils City",
          "Dobeles",
          "Gulbenes",
          "Jekabspils",
          "Jelgava",
          "Jelgavas",
          "Jurmala City",
          "Kraslavas",
          "Kuldigas",
          "Liepaja",
          "Liepajas",
          "Limbazhu",
          "Ludzas",
          "Madonas",
          "Ogres",
          "Preilu",
          "Rezekne",
          "Rezeknes",
          "Riga",
          "Rigas",
          "Saldus",
          "Talsu",
          "Tukuma",
          "Valkas",
          "Valmieras",
          "Ventspils",
          "Ventspils City"
        ]
      },
      {
        "short": "LB",
        "name": "Lebanon",
        "states": [
          "Beirut",
          "Jabal Lubnan",
          "Mohafazat Liban-Nord",
          "Mohafazat Mont-Liban",
          "Sidon",
          "al-Biqa",
          "al-Janub",
          "an-Nabatiyah",
          "ash-Shamal"
        ]
      },
      {
        "short": "LS",
        "name": "Lesotho",
        "states": [
          "Berea",
          "Butha-Buthe",
          "Leribe",
          "Mafeteng",
          "Maseru",
          "Mohale''s Hoek",
          "Mokhotlong",
          "Qacha''s Nek",
          "Quthing",
          "Thaba-Tseka"
        ]
      },
      {
        "short": "LR",
        "name": "Liberia",
        "states": [
          "Bomi",
          "Bong",
          "Grand Bassa",
          "Grand Cape Mount",
          "Grand Gedeh",
          "Loffa",
          "Margibi",
          "Maryland and Grand Kru",
          "Montserrado",
          "Nimba",
          "Rivercess",
          "Sinoe"
        ]
      },
      {
        "short": "LY",
        "name": "Libya",
        "states": [
          "Fezzan",
          "Banghazi",
          "Darnah",
          "Ghadamis",
          "Gharyan",
          "Misratah",
          "Murzuq",
          "Sabha",
          "Sawfajjin",
          "Surt",
          "Tarabulus",
          "Tarhunah",
          "Tripolitania",
          "Tubruq",
          "Yafran",
          "Zlitan",
          "al-''Aziziyah",
          "al-Fatih",
          "al-Jabal al Akhdar",
          "al-Jufrah",
          "al-Khums",
          "al-Kufrah",
          "an-Nuqat al-Khams",
          "ash-Shati",
          "az-Zawiyah"
        ]
      },
      {
        "short": "LI",
        "name": "Liechtenstein",
        "states": [
          "Balzers",
          "Eschen",
          "Gamprin",
          "Mauren",
          "Planken",
          "Ruggell",
          "Schaan",
          "Schellenberg",
          "Triesen",
          "Triesenberg",
          "Vaduz"
        ]
      },
      {
        "short": "LT",
        "name": "Lithuania",
        "states": [
          "Alytaus",
          "Anyksciai",
          "Kauno",
          "Klaipedos",
          "Marijampoles",
          "Panevezhio",
          "Panevezys",
          "Shiauliu",
          "Taurages",
          "Telshiu",
          "Telsiai",
          "Utenos",
          "Vilniaus"
        ]
      },
      {
        "short": "LU",
        "name": "Luxembourg",
        "states": [
          "Capellen",
          "Clervaux",
          "Diekirch",
          "Echternach",
          "Esch-sur-Alzette",
          "Grevenmacher",
          "Luxembourg",
          "Mersch",
          "Redange",
          "Remich",
          "Vianden",
          "Wiltz"
        ]
      },
      {
        "short": "MO",
        "name": "Macau S.A.R.",
        "states": [
          "Macau"
        ]
      },
      {
        "short": "MK",
        "name": "Macedonia",
        "states": [
          "Berovo",
          "Bitola",
          "Brod",
          "Debar",
          "Delchevo",
          "Demir Hisar",
          "Gevgelija",
          "Gostivar",
          "Kavadarci",
          "Kichevo",
          "Kochani",
          "Kratovo",
          "Kriva Palanka",
          "Krushevo",
          "Kumanovo",
          "Negotino",
          "Ohrid",
          "Prilep",
          "Probishtip",
          "Radovish",
          "Resen",
          "Shtip",
          "Skopje",
          "Struga",
          "Strumica",
          "Sveti Nikole",
          "Tetovo",
          "Valandovo",
          "Veles",
          "Vinica"
        ]
      },
      {
        "short": "MG",
        "name": "Madagascar",
        "states": [
          "Antananarivo",
          "Antsiranana",
          "Fianarantsoa",
          "Mahajanga",
          "Toamasina",
          "Toliary"
        ]
      },
      {
        "short": "MW",
        "name": "Malawi",
        "states": [
          "Balaka",
          "Blantyre City",
          "Chikwawa",
          "Chiradzulu",
          "Chitipa",
          "Dedza",
          "Dowa",
          "Karonga",
          "Kasungu",
          "Lilongwe City",
          "Machinga",
          "Mangochi",
          "Mchinji",
          "Mulanje",
          "Mwanza",
          "Mzimba",
          "Mzuzu City",
          "Nkhata Bay",
          "Nkhotakota",
          "Nsanje",
          "Ntcheu",
          "Ntchisi",
          "Phalombe",
          "Rumphi",
          "Salima",
          "Thyolo",
          "Zomba Municipality"
        ]
      },
      {
        "short": "MY",
        "name": "Malaysia",
        "states": [
          "Johor",
          "Kedah",
          "Kelantan",
          "Kuala Lumpur",
          "Labuan",
          "Melaka",
          "Negeri Johor",
          "Negeri Sembilan",
          "Pahang",
          "Penang",
          "Perak",
          "Perlis",
          "Pulau Pinang",
          "Sabah",
          "Sarawak",
          "Selangor",
          "Sembilan",
          "Terengganu"
        ]
      },
      {
        "short": "MV",
        "name": "Maldives",
        "states": [
          "Alif Alif",
          "Alif Dhaal",
          "Baa",
          "Dhaal",
          "Faaf",
          "Gaaf Alif",
          "Gaaf Dhaal",
          "Ghaviyani",
          "Haa Alif",
          "Haa Dhaal",
          "Kaaf",
          "Laam",
          "Lhaviyani",
          "Male",
          "Miim",
          "Nuun",
          "Raa",
          "Shaviyani",
          "Siin",
          "Thaa",
          "Vaav"
        ]
      },
      {
        "short": "ML",
        "name": "Mali",
        "states": [
          "Bamako",
          "Gao",
          "Kayes",
          "Kidal",
          "Koulikoro",
          "Mopti",
          "Segou",
          "Sikasso",
          "Tombouctou"
        ]
      },
      {
        "short": "MT",
        "name": "Malta",
        "states": [
          "Gozo and Comino",
          "Inner Harbour",
          "Northern",
          "Outer Harbour",
          "South Eastern",
          "Valletta",
          "Western"
        ]
      },
      {
        "short": "XM",
        "name": "Man (Isle of)",
        "states": [
          "Castletown",
          "Douglas",
          "Laxey",
          "Onchan",
          "Peel",
          "Port Erin",
          "Port Saint Mary",
          "Ramsey"
        ]
      },
      {
        "short": "MH",
        "name": "Marshall Islands",
        "states": [
          "Ailinlaplap",
          "Ailuk",
          "Arno",
          "Aur",
          "Bikini",
          "Ebon",
          "Enewetak",
          "Jabat",
          "Jaluit",
          "Kili",
          "Kwajalein",
          "Lae",
          "Lib",
          "Likiep",
          "Majuro",
          "Maloelap",
          "Mejit",
          "Mili",
          "Namorik",
          "Namu",
          "Rongelap",
          "Ujae",
          "Utrik",
          "Wotho",
          "Wotje"
        ]
      },
      {
        "short": "MQ",
        "name": "Martinique",
        "states": [
          "Fort-de-France",
          "La Trinite",
          "Le Marin",
          "Saint-Pierre"
        ]
      },
      {
        "short": "MR",
        "name": "Mauritania",
        "states": [
          "Adrar",
          "Assaba",
          "Brakna",
          "Dhakhlat Nawadibu",
          "Hudh-al-Gharbi",
          "Hudh-ash-Sharqi",
          "Inshiri",
          "Nawakshut",
          "Qidimagha",
          "Qurqul",
          "Taqant",
          "Tiris Zammur",
          "Trarza"
        ]
      },
      {
        "short": "MU",
        "name": "Mauritius",
        "states": [
          "Black River",
          "Eau Coulee",
          "Flacq",
          "Floreal",
          "Grand Port",
          "Moka",
          "Pamplempousses",
          "Plaines Wilhelm",
          "Port Louis",
          "Riviere du Rempart",
          "Rodrigues",
          "Rose Hill",
          "Savanne"
        ]
      },
      {
        "short": "YT",
        "name": "Mayotte",
        "states": [
          "Mayotte",
          "Pamanzi"
        ]
      },
      {
        "short": "MX",
        "name": "Mexico",
        "states": [
          "Aguascalientes",
          "Baja California",
          "Baja California Sur",
          "Campeche",
          "Chiapas",
          "Chihuahua",
          "Coahuila",
          "Colima",
          "Distrito Federal",
          "Durango",
          "Estado de Mexico",
          "Guanajuato",
          "Guerrero",
          "Hidalgo",
          "Jalisco",
          "Mexico",
          "Michoacan",
          "Morelos",
          "Nayarit",
          "Nuevo Leon",
          "Oaxaca",
          "Puebla",
          "Queretaro",
          "Quintana Roo",
          "San Luis Potosi",
          "Sinaloa",
          "Sonora",
          "Tabasco",
          "Tamaulipas",
          "Tlaxcala",
          "Veracruz",
          "Yucatan",
          "Zacatecas"
        ]
      },
      {
        "short": "FM",
        "name": "Micronesia",
        "states": [
          "Chuuk",
          "Kusaie",
          "Pohnpei",
          "Yap"
        ]
      },
      {
        "short": "MD",
        "name": "Moldova",
        "states": [
          "Balti",
          "Cahul",
          "Chisinau",
          "Chisinau Oras",
          "Edinet",
          "Gagauzia",
          "Lapusna",
          "Orhei",
          "Soroca",
          "Taraclia",
          "Tighina",
          "Transnistria",
          "Ungheni"
        ]
      },
      {
        "short": "MC",
        "name": "Monaco",
        "states": [
          "Fontvieille",
          "La Condamine",
          "Monaco-Ville",
          "Monte Carlo"
        ]
      },
      {
        "short": "MN",
        "name": "Mongolia",
        "states": [
          "Arhangaj",
          "Bajan-Olgij",
          "Bajanhongor",
          "Bulgan",
          "Darhan-Uul",
          "Dornod",
          "Dornogovi",
          "Dundgovi",
          "Govi-Altaj",
          "Govisumber",
          "Hentij",
          "Hovd",
          "Hovsgol",
          "Omnogovi",
          "Orhon",
          "Ovorhangaj",
          "Selenge",
          "Suhbaatar",
          "Tov",
          "Ulaanbaatar",
          "Uvs",
          "Zavhan"
        ]
      },
      {
        "short": "MS",
        "name": "Montserrat",
        "states": [
          "Montserrat"
        ]
      },
      {
        "short": "MA",
        "name": "Morocco",
        "states": [
          "Agadir",
          "Casablanca",
          "Chaouia-Ouardigha",
          "Doukkala-Abda",
          "Fes-Boulemane",
          "Gharb-Chrarda-Beni Hssen",
          "Guelmim",
          "Kenitra",
          "Marrakech-Tensift-Al Haouz",
          "Meknes-Tafilalet",
          "Oriental",
          "Oujda",
          "Province de Tanger",
          "Rabat-Sale-Zammour-Zaer",
          "Sala Al Jadida",
          "Settat",
          "Souss Massa-Draa",
          "Tadla-Azilal",
          "Tangier-Tetouan",
          "Taza-Al Hoceima-Taounate",
          "Wilaya de Casablanca",
          "Wilaya de Rabat-Sale"
        ]
      },
      {
        "short": "MZ",
        "name": "Mozambique",
        "states": [
          "Cabo Delgado",
          "Gaza",
          "Inhambane",
          "Manica",
          "Maputo",
          "Maputo Provincia",
          "Nampula",
          "Niassa",
          "Sofala",
          "Tete",
          "Zambezia"
        ]
      },
      {
        "short": "MM",
        "name": "Myanmar",
        "states": [
          "Ayeyarwady",
          "Bago",
          "Chin",
          "Kachin",
          "Kayah",
          "Kayin",
          "Magway",
          "Mandalay",
          "Mon",
          "Nay Pyi Taw",
          "Rakhine",
          "Sagaing",
          "Shan",
          "Tanintharyi",
          "Yangon"
        ]
      },
      {
        "short": "NA",
        "name": "Namibia",
        "states": [
          "Caprivi",
          "Erongo",
          "Hardap",
          "Karas",
          "Kavango",
          "Khomas",
          "Kunene",
          "Ohangwena",
          "Omaheke",
          "Omusati",
          "Oshana",
          "Oshikoto",
          "Otjozondjupa"
        ]
      },
      {
        "short": "NR",
        "name": "Nauru",
        "states": [
          "Yaren"
        ]
      },
      {
        "short": "NP",
        "name": "Nepal",
        "states": [
          "Bagmati",
          "Bheri",
          "Dhawalagiri",
          "Gandaki",
          "Janakpur",
          "Karnali",
          "Koshi",
          "Lumbini",
          "Mahakali",
          "Mechi",
          "Narayani",
          "Rapti",
          "Sagarmatha",
          "Seti"
        ]
      },
      {
        "short": "AN",
        "name": "Netherlands Antilles",
        "states": [
          "Bonaire",
          "Curacao",
          "Saba",
          "Sint Eustatius",
          "Sint Maarten"
        ]
      },
      {
        "short": "NL",
        "name": "Netherlands The",
        "states": [
          "Amsterdam",
          "Benelux",
          "Drenthe",
          "Flevoland",
          "Friesland",
          "Gelderland",
          "Groningen",
          "Limburg",
          "Noord-Brabant",
          "Noord-Holland",
          "Overijssel",
          "South Holland",
          "Utrecht",
          "Zeeland",
          "Zuid-Holland"
        ]
      },
      {
        "short": "NC",
        "name": "New Caledonia",
        "states": [
          "Iles",
          "Nord",
          "Sud"
        ]
      },
      {
        "short": "NZ",
        "name": "New Zealand",
        "states": [
          "Area Outside Region",
          "Auckland",
          "Bay of Plenty",
          "Canterbury",
          "Christchurch",
          "Gisborne",
          "Hawke''s Bay",
          "Manawatu-Wanganui",
          "Marlborough",
          "Nelson",
          "Northland",
          "Otago",
          "Rodney",
          "Southland",
          "Taranaki",
          "Tasman",
          "Waikato",
          "Wellington",
          "West Coast"
        ]
      },
      {
        "short": "NI",
        "name": "Nicaragua",
        "states": [
          "Atlantico Norte",
          "Atlantico Sur",
          "Boaco",
          "Carazo",
          "Chinandega",
          "Chontales",
          "Esteli",
          "Granada",
          "Jinotega",
          "Leon",
          "Madriz",
          "Managua",
          "Masaya",
          "Matagalpa",
          "Nueva Segovia",
          "Rio San Juan",
          "Rivas"
        ]
      },
      {
        "short": "NE",
        "name": "Niger",
        "states": [
          "Agadez",
          "Diffa",
          "Dosso",
          "Maradi",
          "Niamey",
          "Tahoua",
          "Tillabery",
          "Zinder"
        ]
      },
      {
        "short": "NG",
        "name": "Nigeria",
        "states": [
          "Abia",
          "Abuja Federal Capital Territor",
          "Adamawa",
          "Akwa Ibom",
          "Anambra",
          "Bauchi",
          "Bayelsa",
          "Benue",
          "Borno",
          "Cross River",
          "Delta",
          "Ebonyi",
          "Edo",
          "Ekiti",
          "Enugu",
          "Gombe",
          "Imo",
          "Jigawa",
          "Kaduna",
          "Kano",
          "Katsina",
          "Kebbi",
          "Kogi",
          "Kwara",
          "Lagos",
          "Nassarawa",
          "Niger",
          "Ogun",
          "Ondo",
          "Osun",
          "Oyo",
          "Plateau",
          "Rivers",
          "Sokoto",
          "Taraba",
          "Yobe",
          "Zamfara"
        ]
      },
      {
        "short": "NU",
        "name": "Niue",
        "states": [
          "Niue"
        ]
      },
      {
        "short": "NF",
        "name": "Norfolk Island",
        "states": [
          "Norfolk Island"
        ]
      },
      {
        "short": "MP",
        "name": "Northern Mariana Islands",
        "states": [
          "Northern Islands",
          "Rota",
          "Saipan",
          "Tinian"
        ]
      },
      {
        "short": "NO",
        "name": "Norway",
        "states": [
          "Akershus",
          "Aust Agder",
          "Bergen",
          "Buskerud",
          "Finnmark",
          "Hedmark",
          "Hordaland",
          "Moere og Romsdal",
          "Nord Trondelag",
          "Nordland",
          "Oestfold",
          "Oppland",
          "Oslo",
          "Rogaland",
          "Soer Troendelag",
          "Sogn og Fjordane",
          "Stavern",
          "Sykkylven",
          "Telemark",
          "Troms",
          "Vest Agder",
          "Vestfold",
          "ÃƒÂ˜stfold"
        ]
      },
      {
        "short": "OM",
        "name": "Oman",
        "states": [
          "Al Buraimi",
          "Dhufar",
          "Masqat",
          "Musandam",
          "Rusayl",
          "Wadi Kabir",
          "ad-Dakhiliyah",
          "adh-Dhahirah",
          "al-Batinah",
          "ash-Sharqiyah"
        ]
      },
      {
        "short": "PK",
        "name": "Pakistan",
        "states": [
          "Baluchistan",
          "Federal Capital Area",
          "Federally administered Tribal",
          "North-West Frontier",
          "Northern Areas",
          "Punjab",
          "Sind"
        ]
      },
      {
        "short": "PW",
        "name": "Palau",
        "states": [
          "Aimeliik",
          "Airai",
          "Angaur",
          "Hatobohei",
          "Kayangel",
          "Koror",
          "Melekeok",
          "Ngaraard",
          "Ngardmau",
          "Ngaremlengui",
          "Ngatpang",
          "Ngchesar",
          "Ngerchelong",
          "Ngiwal",
          "Peleliu",
          "Sonsorol"
        ]
      },
      {
        "short": "PS",
        "name": "Palestinian Territory Occupied",
        "states": [
          "Ariha",
          "Bayt Lahm",
          "Bethlehem",
          "Dayr-al-Balah",
          "Ghazzah",
          "Ghazzah ash-Shamaliyah",
          "Janin",
          "Khan Yunis",
          "Nabulus",
          "Qalqilyah",
          "Rafah",
          "Ram Allah wal-Birah",
          "Salfit",
          "Tubas",
          "Tulkarm",
          "al-Khalil",
          "al-Quds"
        ]
      },
      {
        "short": "PA",
        "name": "Panama",
        "states": [
          "Bocas del Toro",
          "Chiriqui",
          "Cocle",
          "Colon",
          "Darien",
          "Embera",
          "Herrera",
          "Kuna Yala",
          "Los Santos",
          "Ngobe Bugle",
          "Panama",
          "Veraguas"
        ]
      },
      {
        "short": "PG",
        "name": "Papua new Guinea",
        "states": [
          "East New Britain",
          "East Sepik",
          "Eastern Highlands",
          "Enga",
          "Fly River",
          "Gulf",
          "Madang",
          "Manus",
          "Milne Bay",
          "Morobe",
          "National Capital District",
          "New Ireland",
          "North Solomons",
          "Oro",
          "Sandaun",
          "Simbu",
          "Southern Highlands",
          "West New Britain",
          "Western Highlands"
        ]
      },
      {
        "short": "PY",
        "name": "Paraguay",
        "states": [
          "Alto Paraguay",
          "Alto Parana",
          "Amambay",
          "Asuncion",
          "Boqueron",
          "Caaguazu",
          "Caazapa",
          "Canendiyu",
          "Central",
          "Concepcion",
          "Cordillera",
          "Guaira",
          "Itapua",
          "Misiones",
          "Neembucu",
          "Paraguari",
          "Presidente Hayes",
          "San Pedro"
        ]
      },
      {
        "short": "PE",
        "name": "Peru",
        "states": [
          "Amazonas",
          "Ancash",
          "Apurimac",
          "Arequipa",
          "Ayacucho",
          "Cajamarca",
          "Cusco",
          "Huancavelica",
          "Huanuco",
          "Ica",
          "Junin",
          "La Libertad",
          "Lambayeque",
          "Lima y Callao",
          "Loreto",
          "Madre de Dios",
          "Moquegua",
          "Pasco",
          "Piura",
          "Puno",
          "San Martin",
          "Tacna",
          "Tumbes",
          "Ucayali"
        ]
      },
      {
        "short": "PH",
        "name": "Philippines",
        "states": [
          "Batangas",
          "Bicol",
          "Bulacan",
          "Cagayan",
          "Caraga",
          "Central Luzon",
          "Central Mindanao",
          "Central Visayas",
          "Cordillera",
          "Davao",
          "Eastern Visayas",
          "Greater Metropolitan Area",
          "Ilocos",
          "Laguna",
          "Luzon",
          "Mactan",
          "Metropolitan Manila Area",
          "Muslim Mindanao",
          "Northern Mindanao",
          "Southern Mindanao",
          "Southern Tagalog",
          "Western Mindanao",
          "Western Visayas"
        ]
      },
      {
        "short": "PN",
        "name": "Pitcairn Island",
        "states": [
          "Pitcairn Island"
        ]
      },
      {
        "short": "PL",
        "name": "Poland",
        "states": [
          "Biale Blota",
          "Dobroszyce",
          "Dolnoslaskie",
          "Dziekanow Lesny",
          "Hopowo",
          "Kartuzy",
          "Koscian",
          "Krakow",
          "Kujawsko-Pomorskie",
          "Lodzkie",
          "Lubelskie",
          "Lubuskie",
          "Malomice",
          "Malopolskie",
          "Mazowieckie",
          "Mirkow",
          "Opolskie",
          "Ostrowiec",
          "Podkarpackie",
          "Podlaskie",
          "Polska",
          "Pomorskie",
          "Poznan",
          "Pruszkow",
          "Rymanowska",
          "Rzeszow",
          "Slaskie",
          "Stare Pole",
          "Swietokrzyskie",
          "Warminsko-Mazurskie",
          "Warsaw",
          "Wejherowo",
          "Wielkopolskie",
          "Wroclaw",
          "Zachodnio-Pomorskie",
          "Zukowo"
        ]
      },
      {
        "short": "PT",
        "name": "Portugal",
        "states": [
          "Abrantes",
          "Acores",
          "Alentejo",
          "Algarve",
          "Braga",
          "Centro",
          "Distrito de Leiria",
          "Distrito de Viana do Castelo",
          "Distrito de Vila Real",
          "Distrito do Porto",
          "Lisboa e Vale do Tejo",
          "Madeira",
          "Norte",
          "Paivas"
        ]
      },
      {
        "short": "PR",
        "name": "Puerto Rico",
        "states": [
          "Arecibo",
          "Bayamon",
          "Carolina",
          "Florida",
          "Guayama",
          "Humacao",
          "Mayaguez-Aguadilla",
          "Ponce",
          "Salinas",
          "San Juan"
        ]
      },
      {
        "short": "QA",
        "name": "Qatar",
        "states": [
          "Doha",
          "Jarian-al-Batnah",
          "Umm Salal",
          "ad-Dawhah",
          "al-Ghuwayriyah",
          "al-Jumayliyah",
          "al-Khawr",
          "al-Wakrah",
          "ar-Rayyan",
          "ash-Shamal"
        ]
      },
      {
        "short": "RE",
        "name": "Reunion",
        "states": [
          "Saint-Benoit",
          "Saint-Denis",
          "Saint-Paul",
          "Saint-Pierre"
        ]
      },
      {
        "short": "RO",
        "name": "Romania",
        "states": [
          "Alba",
          "Arad",
          "Arges",
          "Bacau",
          "Bihor",
          "Bistrita-Nasaud",
          "Botosani",
          "Braila",
          "Brasov",
          "Bucuresti",
          "Buzau",
          "Calarasi",
          "Caras-Severin",
          "Cluj",
          "Constanta",
          "Covasna",
          "Dambovita",
          "Dolj",
          "Galati",
          "Giurgiu",
          "Gorj",
          "Harghita",
          "Hunedoara",
          "Ialomita",
          "Iasi",
          "Ilfov",
          "Maramures",
          "Mehedinti",
          "Mures",
          "Neamt",
          "Olt",
          "Prahova",
          "Salaj",
          "Satu Mare",
          "Sibiu",
          "Sondelor",
          "Suceava",
          "Teleorman",
          "Timis",
          "Tulcea",
          "Valcea",
          "Vaslui",
          "Vrancea"
        ]
      },
      {
        "short": "RU",
        "name": "Russia",
        "states": [
          "Adygeja",
          "Aga",
          "Alanija",
          "Altaj",
          "Amur",
          "Arhangelsk",
          "Astrahan",
          "Bashkortostan",
          "Belgorod",
          "Brjansk",
          "Burjatija",
          "Chechenija",
          "Cheljabinsk",
          "Chita",
          "Chukotka",
          "Chuvashija",
          "Dagestan",
          "Evenkija",
          "Gorno-Altaj",
          "Habarovsk",
          "Hakasija",
          "Hanty-Mansija",
          "Ingusetija",
          "Irkutsk",
          "Ivanovo",
          "Jamalo-Nenets",
          "Jaroslavl",
          "Jevrej",
          "Kabardino-Balkarija",
          "Kaliningrad",
          "Kalmykija",
          "Kaluga",
          "Kamchatka",
          "Karachaj-Cherkessija",
          "Karelija",
          "Kemerovo",
          "Khabarovskiy Kray",
          "Kirov",
          "Komi",
          "Komi-Permjakija",
          "Korjakija",
          "Kostroma",
          "Krasnodar",
          "Krasnojarsk",
          "Krasnoyarskiy Kray",
          "Kurgan",
          "Kursk",
          "Leningrad",
          "Lipeck",
          "Magadan",
          "Marij El",
          "Mordovija",
          "Moscow",
          "Moskovskaja Oblast",
          "Moskovskaya Oblast",
          "Moskva",
          "Murmansk",
          "Nenets",
          "Nizhnij Novgorod",
          "Novgorod",
          "Novokusnezk",
          "Novosibirsk",
          "Omsk",
          "Orenburg",
          "Orjol",
          "Penza",
          "Perm",
          "Primorje",
          "Pskov",
          "Pskovskaya Oblast",
          "Rjazan",
          "Rostov",
          "Saha",
          "Sahalin",
          "Samara",
          "Samarskaya",
          "Sankt-Peterburg",
          "Saratov",
          "Smolensk",
          "Stavropol",
          "Sverdlovsk",
          "Tajmyrija",
          "Tambov",
          "Tatarstan",
          "Tjumen",
          "Tomsk",
          "Tula",
          "Tver",
          "Tyva",
          "Udmurtija",
          "Uljanovsk",
          "Ulyanovskaya Oblast",
          "Ust-Orda",
          "Vladimir",
          "Volgograd",
          "Vologda",
          "Voronezh"
        ]
      },
      {
        "short": "RW",
        "name": "Rwanda",
        "states": [
          "Butare",
          "Byumba",
          "Cyangugu",
          "Gikongoro",
          "Gisenyi",
          "Gitarama",
          "Kibungo",
          "Kibuye",
          "Kigali-ngali",
          "Ruhengeri"
        ]
      },
      {
        "short": "SH",
        "name": "Saint Helena",
        "states": [
          "Ascension",
          "Gough Island",
          "Saint Helena",
          "Tristan da Cunha"
        ]
      },
      {
        "short": "KN",
        "name": "Saint Kitts And Nevis",
        "states": [
          "Christ Church Nichola Town",
          "Saint Anne Sandy Point",
          "Saint George Basseterre",
          "Saint George Gingerland",
          "Saint James Windward",
          "Saint John Capesterre",
          "Saint John Figtree",
          "Saint Mary Cayon",
          "Saint Paul Capesterre",
          "Saint Paul Charlestown",
          "Saint Peter Basseterre",
          "Saint Thomas Lowland",
          "Saint Thomas Middle Island",
          "Trinity Palmetto Point"
        ]
      },
      {
        "short": "LC",
        "name": "Saint Lucia",
        "states": [
          "Anse-la-Raye",
          "Canaries",
          "Castries",
          "Choiseul",
          "Dennery",
          "Gros Inlet",
          "Laborie",
          "Micoud",
          "Soufriere",
          "Vieux Fort"
        ]
      },
      {
        "short": "PM",
        "name": "Saint Pierre and Miquelon",
        "states": [
          "Miquelon-Langlade",
          "Saint-Pierre"
        ]
      },
      {
        "short": "VC",
        "name": "Saint Vincent And The Grenadines",
        "states": [
          "Charlotte",
          "Grenadines",
          "Saint Andrew",
          "Saint David",
          "Saint George",
          "Saint Patrick"
        ]
      },
      {
        "short": "WS",
        "name": "Samoa",
        "states": [
          "A''ana",
          "Aiga-i-le-Tai",
          "Atua",
          "Fa''asaleleaga",
          "Gaga''emauga",
          "Gagaifomauga",
          "Palauli",
          "Satupa''itea",
          "Tuamasaga",
          "Va''a-o-Fonoti",
          "Vaisigano"
        ]
      },
      {
        "short": "SM",
        "name": "San Marino",
        "states": [
          "Acquaviva",
          "Borgo Maggiore",
          "Chiesanuova",
          "Domagnano",
          "Faetano",
          "Fiorentino",
          "Montegiardino",
          "San Marino",
          "Serravalle"
        ]
      },
      {
        "short": "ST",
        "name": "Sao Tome and Principe",
        "states": [
          "Agua Grande",
          "Cantagalo",
          "Lemba",
          "Lobata",
          "Me-Zochi",
          "Pague"
        ]
      },
      {
        "short": "SA",
        "name": "Saudi Arabia",
        "states": [
          "Al Khobar",
          "Aseer",
          "Ash Sharqiyah",
          "Asir",
          "Central Province",
          "Eastern Province",
          "Ha''il",
          "Jawf",
          "Jizan",
          "Makkah",
          "Najran",
          "Qasim",
          "Tabuk",
          "Western Province",
          "al-Bahah",
          "al-Hudud-ash-Shamaliyah",
          "al-Madinah",
          "ar-Riyad"
        ]
      },
      {
        "short": "SN",
        "name": "Senegal",
        "states": [
          "Dakar",
          "Diourbel",
          "Fatick",
          "Kaolack",
          "Kolda",
          "Louga",
          "Saint-Louis",
          "Tambacounda",
          "Thies",
          "Ziguinchor"
        ]
      },
      {
        "short": "RS",
        "name": "Serbia",
        "states": [
          "Central Serbia",
          "Kosovo and Metohija",
          "Vojvodina"
        ]
      },
      {
        "short": "SC",
        "name": "Seychelles",
        "states": [
          "Anse Boileau",
          "Anse Royale",
          "Cascade",
          "Takamaka",
          "Victoria"
        ]
      },
      {
        "short": "SL",
        "name": "Sierra Leone",
        "states": [
          "Eastern",
          "Northern",
          "Southern",
          "Western"
        ]
      },
      {
        "short": "SG",
        "name": "Singapore",
        "states": [
          "Singapore"
        ]
      },
      {
        "short": "SK",
        "name": "Slovakia",
        "states": [
          "Banskobystricky",
          "Bratislavsky",
          "Kosicky",
          "Nitriansky",
          "Presovsky",
          "Trenciansky",
          "Trnavsky",
          "Zilinsky"
        ]
      },
      {
        "short": "SI",
        "name": "Slovenia",
        "states": [
          "Benedikt",
          "Gorenjska",
          "Gorishka",
          "Jugovzhodna Slovenija",
          "Koroshka",
          "Notranjsko-krashka",
          "Obalno-krashka",
          "Obcina Domzale",
          "Obcina Vitanje",
          "Osrednjeslovenska",
          "Podravska",
          "Pomurska",
          "Savinjska",
          "Slovenian Littoral",
          "Spodnjeposavska",
          "Zasavska"
        ]
      },
      {
        "short": "XG",
        "name": "Smaller Territories of the UK",
        "states": [
          "Pitcairn"
        ]
      },
      {
        "short": "SB",
        "name": "Solomon Islands",
        "states": [
          "Central",
          "Choiseul",
          "Guadalcanal",
          "Isabel",
          "Makira and Ulawa",
          "Malaita",
          "Rennell and Bellona",
          "Temotu",
          "Western"
        ]
      },
      {
        "short": "SO",
        "name": "Somalia",
        "states": [
          "Awdal",
          "Bakol",
          "Banadir",
          "Bari",
          "Bay",
          "Galgudug",
          "Gedo",
          "Hiran",
          "Jubbada Hose",
          "Jubbadha Dexe",
          "Mudug",
          "Nugal",
          "Sanag",
          "Shabellaha Dhexe",
          "Shabellaha Hose",
          "Togdher",
          "Woqoyi Galbed"
        ]
      },
      {
        "short": "ZA",
        "name": "South Africa",
        "states": [
          "Eastern Cape",
          "Free State",
          "Gauteng",
          "Kempton Park",
          "Kramerville",
          "KwaZulu Natal",
          "Limpopo",
          "Mpumalanga",
          "North West",
          "Northern Cape",
          "Parow",
          "Table View",
          "Umtentweni",
          "Western Cape"
        ]
      },
      {
        "short": "GS",
        "name": "South Georgia",
        "states": [
          "South Georgia"
        ]
      },
      {
        "short": "SS",
        "name": "South Sudan",
        "states": [
          "Central Equatoria"
        ]
      },
      {
        "short": "ES",
        "name": "Spain",
        "states": [
          "A Coruna",
          "Alacant",
          "Alava",
          "Albacete",
          "Almeria",
          "Andalucia",
          "Asturias",
          "Avila",
          "Badajoz",
          "Balears",
          "Barcelona",
          "Bertamirans",
          "Biscay",
          "Burgos",
          "Caceres",
          "Cadiz",
          "Cantabria",
          "Castello",
          "Catalunya",
          "Ceuta",
          "Ciudad Real",
          "Comunidad Autonoma de Canarias",
          "Comunidad Autonoma de Cataluna",
          "Comunidad Autonoma de Galicia",
          "Comunidad Autonoma de las Isla",
          "Comunidad Autonoma del Princip",
          "Comunidad Valenciana",
          "Cordoba",
          "Cuenca",
          "Gipuzkoa",
          "Girona",
          "Granada",
          "Guadalajara",
          "Guipuzcoa",
          "Huelva",
          "Huesca",
          "Jaen",
          "La Rioja",
          "Las Palmas",
          "Leon",
          "Lerida",
          "Lleida",
          "Lugo",
          "Madrid",
          "Malaga",
          "Melilla",
          "Murcia",
          "Navarra",
          "Ourense",
          "Pais Vasco",
          "Palencia",
          "Pontevedra",
          "Salamanca",
          "Santa Cruz de Tenerife",
          "Segovia",
          "Sevilla",
          "Soria",
          "Tarragona",
          "Tenerife",
          "Teruel",
          "Toledo",
          "Valencia",
          "Valladolid",
          "Vizcaya",
          "Zamora",
          "Zaragoza"
        ]
      },
      {
        "short": "LK",
        "name": "Sri Lanka",
        "states": [
          "Amparai",
          "Anuradhapuraya",
          "Badulla",
          "Boralesgamuwa",
          "Colombo",
          "Galla",
          "Gampaha",
          "Hambantota",
          "Kalatura",
          "Kegalla",
          "Kilinochchi",
          "Kurunegala",
          "Madakalpuwa",
          "Maha Nuwara",
          "Malwana",
          "Mannarama",
          "Matale",
          "Matara",
          "Monaragala",
          "Mullaitivu",
          "North Eastern Province",
          "North Western Province",
          "Nuwara Eliya",
          "Polonnaruwa",
          "Puttalama",
          "Ratnapuraya",
          "Southern Province",
          "Tirikunamalaya",
          "Tuscany",
          "Vavuniyawa",
          "Western Province",
          "Yapanaya",
          "kadawatha"
        ]
      },
      {
        "short": "SD",
        "name": "Sudan",
        "states": [
          "A''ali-an-Nil",
          "Bahr-al-Jabal",
          "Central Equatoria",
          "Gharb Bahr-al-Ghazal",
          "Gharb Darfur",
          "Gharb Kurdufan",
          "Gharb-al-Istiwa''iyah",
          "Janub Darfur",
          "Janub Kurdufan",
          "Junqali",
          "Kassala",
          "Nahr-an-Nil",
          "Shamal Bahr-al-Ghazal",
          "Shamal Darfur",
          "Shamal Kurdufan",
          "Sharq-al-Istiwa''iyah",
          "Sinnar",
          "Warab",
          "Wilayat al Khartum",
          "al-Bahr-al-Ahmar",
          "al-Buhayrat",
          "al-Jazirah",
          "al-Khartum",
          "al-Qadarif",
          "al-Wahdah",
          "an-Nil-al-Abyad",
          "an-Nil-al-Azraq",
          "ash-Shamaliyah"
        ]
      },
      {
        "short": "SR",
        "name": "Suriname",
        "states": [
          "Brokopondo",
          "Commewijne",
          "Coronie",
          "Marowijne",
          "Nickerie",
          "Para",
          "Paramaribo",
          "Saramacca",
          "Wanica"
        ]
      },
      {
        "short": "SJ",
        "name": "Svalbard And Jan Mayen Islands",
        "states": [
          "Svalbard"
        ]
      },
      {
        "short": "SZ",
        "name": "Swaziland",
        "states": [
          "Hhohho",
          "Lubombo",
          "Manzini",
          "Shiselweni"
        ]
      },
      {
        "short": "SE",
        "name": "Sweden",
        "states": [
          "Alvsborgs Lan",
          "Angermanland",
          "Blekinge",
          "Bohuslan",
          "Dalarna",
          "Gavleborg",
          "Gaza",
          "Gotland",
          "Halland",
          "Jamtland",
          "Jonkoping",
          "Kalmar",
          "Kristianstads",
          "Kronoberg",
          "Norrbotten",
          "Orebro",
          "Ostergotland",
          "Saltsjo-Boo",
          "Skane",
          "Smaland",
          "Sodermanland",
          "Stockholm",
          "Uppsala",
          "Varmland",
          "Vasterbotten",
          "Vastergotland",
          "Vasternorrland",
          "Vastmanland",
          "Vastra Gotaland"
        ]
      },
      {
        "short": "CH",
        "name": "Switzerland",
        "states": [
          "Aargau",
          "Appenzell Inner-Rhoden",
          "Appenzell-Ausser Rhoden",
          "Basel-Landschaft",
          "Basel-Stadt",
          "Bern",
          "Canton Ticino",
          "Fribourg",
          "Geneve",
          "Glarus",
          "Graubunden",
          "Heerbrugg",
          "Jura",
          "Kanton Aargau",
          "Luzern",
          "Morbio Inferiore",
          "Muhen",
          "Neuchatel",
          "Nidwalden",
          "Obwalden",
          "Sankt Gallen",
          "Schaffhausen",
          "Schwyz",
          "Solothurn",
          "Thurgau",
          "Ticino",
          "Uri",
          "Valais",
          "Vaud",
          "Vauffelin",
          "Zug",
          "Zurich"
        ]
      },
      {
        "short": "SY",
        "name": "Syria",
        "states": [
          "Aleppo",
          "Dar''a",
          "Dayr-az-Zawr",
          "Dimashq",
          "Halab",
          "Hamah",
          "Hims",
          "Idlib",
          "Madinat Dimashq",
          "Tartus",
          "al-Hasakah",
          "al-Ladhiqiyah",
          "al-Qunaytirah",
          "ar-Raqqah",
          "as-Suwayda"
        ]
      },
      {
        "short": "TW",
        "name": "Taiwan",
        "states": [
          "Changhua County",
          "Chiayi County",
          "Chiayi City",
          "Taipei City",
          "Hsinchu County",
          "Hsinchu City",
          "Hualien County",
          "Kaohsiung City",
          "Keelung City",
          "Kinmen County",
          "Miaoli County",
          "Nantou County",
          "Penghu County",
          "Pingtung County",
          "Taichung City",
          "Tainan City",
          "New Taipei City",
          "Taitung County",
          "Taoyuan City",
          "Yilan County",
          "YunLin County",
          "Lienchiang County"
        ]
      },
      {
        "short": "TJ",
        "name": "Tajikistan",
        "states": [
          "Dushanbe",
          "Gorno-Badakhshan",
          "Karotegin",
          "Khatlon",
          "Sughd"
        ]
      },
      {
        "short": "TZ",
        "name": "Tanzania",
        "states": [
          "Arusha",
          "Dar es Salaam",
          "Dodoma",
          "Iringa",
          "Kagera",
          "Kigoma",
          "Kilimanjaro",
          "Lindi",
          "Mara",
          "Mbeya",
          "Morogoro",
          "Mtwara",
          "Mwanza",
          "Pwani",
          "Rukwa",
          "Ruvuma",
          "Shinyanga",
          "Singida",
          "Tabora",
          "Tanga",
          "Zanzibar and Pemba"
        ]
      },
      {
        "short": "TH",
        "name": "Thailand",
        "states": [
          "Amnat Charoen",
          "Ang Thong",
          "Bangkok",
          "Buri Ram",
          "Chachoengsao",
          "Chai Nat",
          "Chaiyaphum",
          "Changwat Chaiyaphum",
          "Chanthaburi",
          "Chiang Mai",
          "Chiang Rai",
          "Chon Buri",
          "Chumphon",
          "Kalasin",
          "Kamphaeng Phet",
          "Kanchanaburi",
          "Khon Kaen",
          "Krabi",
          "Krung Thep",
          "Lampang",
          "Lamphun",
          "Loei",
          "Lop Buri",
          "Mae Hong Son",
          "Maha Sarakham",
          "Mukdahan",
          "Nakhon Nayok",
          "Nakhon Pathom",
          "Nakhon Phanom",
          "Nakhon Ratchasima",
          "Nakhon Sawan",
          "Nakhon Si Thammarat",
          "Nan",
          "Narathiwat",
          "Nong Bua Lam Phu",
          "Nong Khai",
          "Nonthaburi",
          "Pathum Thani",
          "Pattani",
          "Phangnga",
          "Phatthalung",
          "Phayao",
          "Phetchabun",
          "Phetchaburi",
          "Phichit",
          "Phitsanulok",
          "Phra Nakhon Si Ayutthaya",
          "Phrae",
          "Phuket",
          "Prachin Buri",
          "Prachuap Khiri Khan",
          "Ranong",
          "Ratchaburi",
          "Rayong",
          "Roi Et",
          "Sa Kaeo",
          "Sakon Nakhon",
          "Samut Prakan",
          "Samut Sakhon",
          "Samut Songkhran",
          "Saraburi",
          "Satun",
          "Si Sa Ket",
          "Sing Buri",
          "Songkhla",
          "Sukhothai",
          "Suphan Buri",
          "Surat Thani",
          "Surin",
          "Tak",
          "Trang",
          "Trat",
          "Ubon Ratchathani",
          "Udon Thani",
          "Uthai Thani",
          "Uttaradit",
          "Yala",
          "Yasothon"
        ]
      },
      {
        "short": "TG",
        "name": "Togo",
        "states": [
          "Centre",
          "Kara",
          "Maritime",
          "Plateaux",
          "Savanes"
        ]
      },
      {
        "short": "TK",
        "name": "Tokelau",
        "states": [
          "Atafu",
          "Fakaofo",
          "Nukunonu"
        ]
      },
      {
        "short": "TO",
        "name": "Tonga",
        "states": [
          "Eua",
          "Ha''apai",
          "Niuas",
          "Tongatapu",
          "Vava''u"
        ]
      },
      {
        "short": "TT",
        "name": "Trinidad And Tobago",
        "states": [
          "Arima-Tunapuna-Piarco",
          "Caroni",
          "Chaguanas",
          "Couva-Tabaquite-Talparo",
          "Diego Martin",
          "Glencoe",
          "Penal Debe",
          "Point Fortin",
          "Port of Spain",
          "Princes Town",
          "Saint George",
          "San Fernando",
          "San Juan",
          "Sangre Grande",
          "Siparia",
          "Tobago"
        ]
      },
      {
        "short": "TN",
        "name": "Tunisia",
        "states": [
          "Aryanah",
          "Bajah",
          "Bin ''Arus",
          "Binzart",
          "Gouvernorat de Ariana",
          "Gouvernorat de Nabeul",
          "Gouvernorat de Sousse",
          "Hammamet Yasmine",
          "Jundubah",
          "Madaniyin",
          "Manubah",
          "Monastir",
          "Nabul",
          "Qabis",
          "Qafsah",
          "Qibili",
          "Safaqis",
          "Sfax",
          "Sidi Bu Zayd",
          "Silyanah",
          "Susah",
          "Tatawin",
          "Tawzar",
          "Tunis",
          "Zaghwan",
          "al-Kaf",
          "al-Mahdiyah",
          "al-Munastir",
          "al-Qasrayn",
          "al-Qayrawan"
        ]
      },
      {
        "short": "TR",
        "name": "Turkey",
        "states": [
          "Adana",
          "Adiyaman",
          "Afyon",
          "Agri",
          "Aksaray",
          "Amasya",
          "Ankara",
          "Antalya",
          "Ardahan",
          "Artvin",
          "Aydin",
          "Balikesir",
          "Bartin",
          "Batman",
          "Bayburt",
          "Bilecik",
          "Bingol",
          "Bitlis",
          "Bolu",
          "Burdur",
          "Bursa",
          "Canakkale",
          "Cankiri",
          "Corum",
          "Denizli",
          "Diyarbakir",
          "Duzce",
          "Edirne",
          "Elazig",
          "Erzincan",
          "Erzurum",
          "Eskisehir",
          "Gaziantep",
          "Giresun",
          "Gumushane",
          "Hakkari",
          "Hatay",
          "Icel",
          "Igdir",
          "Isparta",
          "Istanbul",
          "Izmir",
          "Kahramanmaras",
          "Karabuk",
          "Karaman",
          "Kars",
          "Karsiyaka",
          "Kastamonu",
          "Kayseri",
          "Kilis",
          "Kirikkale",
          "Kirklareli",
          "Kirsehir",
          "Kocaeli",
          "Konya",
          "Kutahya",
          "Lefkosa",
          "Malatya",
          "Manisa",
          "Mardin",
          "Mugla",
          "Mus",
          "Nevsehir",
          "Nigde",
          "Ordu",
          "Osmaniye",
          "Rize",
          "Sakarya",
          "Samsun",
          "Sanliurfa",
          "Siirt",
          "Sinop",
          "Sirnak",
          "Sivas",
          "Tekirdag",
          "Tokat",
          "Trabzon",
          "Tunceli",
          "Usak",
          "Van",
          "Yalova",
          "Yozgat",
          "Zonguldak"
        ]
      },
      {
        "short": "TM",
        "name": "Turkmenistan",
        "states": [
          "Ahal",
          "Asgabat",
          "Balkan",
          "Dasoguz",
          "Lebap",
          "Mari"
        ]
      },
      {
        "short": "TC",
        "name": "Turks And Caicos Islands",
        "states": [
          "Grand Turk",
          "South Caicos and East Caicos"
        ]
      },
      {
        "short": "TV",
        "name": "Tuvalu",
        "states": [
          "Funafuti",
          "Nanumanga",
          "Nanumea",
          "Niutao",
          "Nui",
          "Nukufetau",
          "Nukulaelae",
          "Vaitupu"
        ]
      },
      {
        "short": "UG",
        "name": "Uganda",
        "states": [
          "Central",
          "Eastern",
          "Northern",
          "Western"
        ]
      },
      {
        "short": "UA",
        "name": "Ukraine",
        "states": [
          "Cherkas''ka",
          "Chernihivs''ka",
          "Chernivets''ka",
          "Crimea",
          "Dnipropetrovska",
          "Donets''ka",
          "Ivano-Frankivs''ka",
          "Kharkiv",
          "Kharkov",
          "Khersonska",
          "Khmel''nyts''ka",
          "Kirovohrad",
          "Krym",
          "Kyyiv",
          "Kyyivs''ka",
          "L''vivs''ka",
          "Luhans''ka",
          "Mykolayivs''ka",
          "Odes''ka",
          "Odessa",
          "Poltavs''ka",
          "Rivnens''ka",
          "Sevastopol",
          "Sums''ka",
          "Ternopil''s''ka",
          "Volyns''ka",
          "Vynnyts''ka",
          "Zakarpats''ka",
          "Zaporizhia",
          "Zhytomyrs''ka"
        ]
      },
      {
        "short": "AE",
        "name": "United Arab Emirates",
        "states": [
          "Abu Zabi",
          "Ajman",
          "Dubai",
          "Ras al-Khaymah",
          "Sharjah",
          "Sharjha",
          "Umm al Qaywayn",
          "al-Fujayrah",
          "ash-Shariqah"
        ]
      },
      {
        "short": "GB",
        "name": "United Kingdom",
        "states": [
          "Aberdeen",
          "Aberdeenshire",
          "Argyll",
          "Armagh",
          "Bedfordshire",
          "Belfast",
          "Berkshire",
          "Birmingham",
          "Brechin",
          "Bridgnorth",
          "Bristol",
          "Buckinghamshire",
          "Cambridge",
          "Cambridgeshire",
          "Channel Islands",
          "Cheshire",
          "Cleveland",
          "Co Fermanagh",
          "Conwy",
          "Cornwall",
          "Coventry",
          "Craven Arms",
          "Cumbria",
          "Denbighshire",
          "Derby",
          "Derbyshire",
          "Devon",
          "Dial Code Dungannon",
          "Didcot",
          "Dorset",
          "Dunbartonshire",
          "Durham",
          "East Dunbartonshire",
          "East Lothian",
          "East Midlands",
          "East Sussex",
          "East Yorkshire",
          "England",
          "Essex",
          "Fermanagh",
          "Fife",
          "Flintshire",
          "Fulham",
          "Gainsborough",
          "Glocestershire",
          "Gwent",
          "Hampshire",
          "Hants",
          "Herefordshire",
          "Hertfordshire",
          "Ireland",
          "Isle Of Man",
          "Isle of Wight",
          "Kenford",
          "Kent",
          "Kilmarnock",
          "Lanarkshire",
          "Lancashire",
          "Leicestershire",
          "Lincolnshire",
          "Llanymynech",
          "London",
          "Ludlow",
          "Manchester",
          "Mayfair",
          "Merseyside",
          "Mid Glamorgan",
          "Middlesex",
          "Mildenhall",
          "Monmouthshire",
          "Newton Stewart",
          "Norfolk",
          "North Humberside",
          "North Yorkshire",
          "Northamptonshire",
          "Northants",
          "Northern Ireland",
          "Northumberland",
          "Nottinghamshire",
          "Oxford",
          "Powys",
          "Roos-shire",
          "SUSSEX",
          "Sark",
          "Scotland",
          "Scottish Borders",
          "Shropshire",
          "Somerset",
          "South Glamorgan",
          "South Wales",
          "South Yorkshire",
          "Southwell",
          "Staffordshire",
          "Strabane",
          "Suffolk",
          "Surrey",
          "Sussex",
          "Twickenham",
          "Tyne and Wear",
          "Tyrone",
          "Utah",
          "Wales",
          "Warwickshire",
          "West Lothian",
          "West Midlands",
          "West Sussex",
          "West Yorkshire",
          "Whissendine",
          "Wiltshire",
          "Wokingham",
          "Worcestershire",
          "Wrexham",
          "Wurttemberg",
          "Yorkshire"
        ]
      },
      {
        "short": "US",
        "name": "United States",
        "states": [
          "Alabama",
          "Alaska",
          "Arizona",
          "Arkansas",
          "Byram",
          "California",
          "Cokato",
          "Colorado",
          "Connecticut",
          "Delaware",
          "District of Columbia",
          "Florida",
          "Georgia",
          "Hawaii",
          "Idaho",
          "Illinois",
          "Indiana",
          "Iowa",
          "Kansas",
          "Kentucky",
          "Louisiana",
          "Lowa",
          "Maine",
          "Maryland",
          "Massachusetts",
          "Medfield",
          "Michigan",
          "Minnesota",
          "Mississippi",
          "Missouri",
          "Montana",
          "Nebraska",
          "Nevada",
          "New Hampshire",
          "New Jersey",
          "New Jersy",
          "New Mexico",
          "New York",
          "North Carolina",
          "North Dakota",
          "Ohio",
          "Oklahoma",
          "Ontario",
          "Oregon",
          "Pennsylvania",
          "Ramey",
          "Rhode Island",
          "South Carolina",
          "South Dakota",
          "Sublimity",
          "Tennessee",
          "Texas",
          "Trimble",
          "Utah",
          "Vermont",
          "Virginia",
          "Washington",
          "West Virginia",
          "Wisconsin",
          "Wyoming"
        ]
      },
      {
        "short": "UM",
        "name": "United States Minor Outlying Islands",
        "states": [
          "United States Minor Outlying I"
        ]
      },
      {
        "short": "UY",
        "name": "Uruguay",
        "states": [
          "Artigas",
          "Canelones",
          "Cerro Largo",
          "Colonia",
          "Durazno",
          "FLorida",
          "Flores",
          "Lavalleja",
          "Maldonado",
          "Montevideo",
          "Paysandu",
          "Rio Negro",
          "Rivera",
          "Rocha",
          "Salto",
          "San Jose",
          "Soriano",
          "Tacuarembo",
          "Treinta y Tres"
        ]
      },
      {
        "short": "UZ",
        "name": "Uzbekistan",
        "states": [
          "Andijon",
          "Buhoro",
          "Buxoro Viloyati",
          "Cizah",
          "Fargona",
          "Horazm",
          "Kaskadar",
          "Korakalpogiston",
          "Namangan",
          "Navoi",
          "Samarkand",
          "Sirdare",
          "Surhondar",
          "Toskent"
        ]
      },
      {
        "short": "VU",
        "name": "Vanuatu",
        "states": [
          "Malampa",
          "Penama",
          "Sanma",
          "Shefa",
          "Tafea",
          "Torba"
        ]
      },
      {
        "short": "VA",
        "name": "Vatican City State (Holy See)",
        "states": [
          "Vatican City State (Holy See)"
        ]
      },
      {
        "short": "VE",
        "name": "Venezuela",
        "states": [
          "Amazonas",
          "Anzoategui",
          "Apure",
          "Aragua",
          "Barinas",
          "Bolivar",
          "Carabobo",
          "Cojedes",
          "Delta Amacuro",
          "Distrito Federal",
          "Falcon",
          "Guarico",
          "Lara",
          "Merida",
          "Miranda",
          "Monagas",
          "Nueva Esparta",
          "Portuguesa",
          "Sucre",
          "Tachira",
          "Trujillo",
          "Vargas",
          "Yaracuy",
          "Zulia"
        ]
      },
      {
        "short": "VN",
        "name": "Vietnam",
        "states": [
          "Bac Giang",
          "Binh Dinh",
          "Binh Duong",
          "Da Nang",
          "Dong Bang Song Cuu Long",
          "Dong Bang Song Hong",
          "Dong Nai",
          "Dong Nam Bo",
          "Duyen Hai Mien Trung",
          "Hanoi",
          "Hung Yen",
          "Khu Bon Cu",
          "Long An",
          "Mien Nui Va Trung Du",
          "Thai Nguyen",
          "Thanh Pho Ho Chi Minh",
          "Thu Do Ha Noi",
          "Tinh Can Tho",
          "Tinh Da Nang",
          "Tinh Gia Lai"
        ]
      },
      {
        "short": "VG",
        "name": "Virgin Islands (British)",
        "states": [
          "Anegada",
          "Jost van Dyke",
          "Tortola"
        ]
      },
      {
        "short": "VI",
        "name": "Virgin Islands (US)",
        "states": [
          "Saint Croix",
          "Saint John",
          "Saint Thomas"
        ]
      },
      {
        "short": "WF",
        "name": "Wallis And Futuna Islands",
        "states": [
          "Alo",
          "Singave",
          "Wallis"
        ]
      },
      {
        "short": "EH",
        "name": "Western Sahara",
        "states": [
          "Bu Jaydur",
          "Wad-adh-Dhahab",
          "al-''Ayun",
          "as-Samarah"
        ]
      },
      {
        "short": "YE",
        "name": "Yemen",
        "states": [
          "Adan",
          "Abyan",
          "Dhamar",
          "Hadramaut",
          "Hajjah",
          "Hudaydah",
          "Ibb",
          "Lahij",
          "Ma''rib",
          "Madinat San''a",
          "Sa''dah",
          "Sana",
          "Shabwah",
          "Ta''izz",
          "al-Bayda",
          "al-Hudaydah",
          "al-Jawf",
          "al-Mahrah",
          "al-Mahwit"
        ]
      },
      {
        "short": "YU",
        "name": "Yugoslavia",
        "states": [
          "Central Serbia",
          "Kosovo and Metohija",
          "Montenegro",
          "Republic of Serbia",
          "Serbia",
          "Vojvodina"
        ]
      },
      {
        "short": "ZM",
        "name": "Zambia",
        "states": [
          "Central",
          "Copperbelt",
          "Eastern",
          "Luapala",
          "Lusaka",
          "North-Western",
          "Northern",
          "Southern",
          "Western"
        ]
      },
      {
        "short": "ZW",
        "name": "Zimbabwe",
        "states": [
          "Bulawayo",
          "Harare",
          "Manicaland",
          "Mashonaland Central",
          "Mashonaland East",
          "Mashonaland West",
          "Masvingo",
          "Matabeleland North",
          "Matabeleland South",
          "Midlands"
        ]
      }
    ]  
    
    phoneCode=[{"name":"Aruba","code":"AW","callingCode":"297"},{"name":"Afghanistan","code":"AF","callingCode":"93"},{"name":"Angola","code":"AO","callingCode":"244"},{"name":"Anguilla","code":"AI","callingCode":"1264"},{"name":"Ãland Islands","code":"AX","callingCode":"358"},{"name":"Albania","code":"AL","callingCode":"355"},{"name":"Andorra","code":"AD","callingCode":"376"},{"name":"United Arab Emirates","code":"AE","callingCode":"971"},{"name":"Argentina","code":"AR","callingCode":"54"},{"name":"Armenia","code":"AM","callingCode":"374"},{"name":"American Samoa","code":"AS","callingCode":"1684"},{"name":"Antigua and Barbuda","code":"AG","callingCode":"1268"},{"name":"Australia","code":"AU","callingCode":"61"},{"name":"Austria","code":"AT","callingCode":"43"},{"name":"Azerbaijan","code":"AZ","callingCode":"994"},{"name":"Burundi","code":"BI","callingCode":"257"},{"name":"Belgium","code":"BE","callingCode":"32"},{"name":"Benin","code":"BJ","callingCode":"229"},{"name":"Burkina Faso","code":"BF","callingCode":"226"},{"name":"Bangladesh","code":"BD","callingCode":"880"},{"name":"Bulgaria","code":"BG","callingCode":"359"},{"name":"Bahrain","code":"BH","callingCode":"973"},{"name":"Bahamas","code":"BS","callingCode":"1242"},{"name":"Bosnia and Herzegovina","code":"BA","callingCode":"387"},{"name":"Saint BarthÃ©lemy","code":"BL","callingCode":"590"},{"name":"Belarus","code":"BY","callingCode":"375"},{"name":"Belize","code":"BZ","callingCode":"501"},{"name":"Bermuda","code":"BM","callingCode":"1441"},{"name":"Bolivia","code":"BO","callingCode":"591"},{"name":"Brazil","code":"BR","callingCode":"55"},{"name":"Barbados","code":"BB","callingCode":"1246"},{"name":"Brunei","code":"BN","callingCode":"673"},{"name":"Bhutan","code":"BT","callingCode":"975"},{"name":"Botswana","code":"BW","callingCode":"267"},{"name":"Central African Republic","code":"CF","callingCode":"236"},{"name":"Canada","code":"CA","callingCode":"1"},{"name":"Cocos (Keeling) Islands","code":"CC","callingCode":"61"},{"name":"Switzerland","code":"CH","callingCode":"41"},{"name":"Chile","code":"CL","callingCode":"56"},{"name":"China","code":"CN","callingCode":"86"},{"name":"Ivory Coast","code":"CI","callingCode":"225"},{"name":"Cameroon","code":"CM","callingCode":"237"},{"name":"DR Congo","code":"CD","callingCode":"243"},{"name":"Republic of the Congo","code":"CG","callingCode":"242"},{"name":"Cook Islands","code":"CK","callingCode":"682"},{"name":"Colombia","code":"CO","callingCode":"57"},{"name":"Comoros","code":"KM","callingCode":"269"},{"name":"Cape Verde","code":"CV","callingCode":"238"},{"name":"Costa Rica","code":"CR","callingCode":"506"},{"name":"Cuba","code":"CU","callingCode":"53"},{"name":"CuraÃ§ao","code":"CW","callingCode":"5999"},{"name":"Christmas Island","code":"CX","callingCode":"61"},{"name":"Cayman Islands","code":"KY","callingCode":"1345"},{"name":"Cyprus","code":"CY","callingCode":"357"},{"name":"Czechia","code":"CZ","callingCode":"420"},{"name":"Germany","code":"DE","callingCode":"49"},{"name":"Djibouti","code":"DJ","callingCode":"253"},{"name":"Dominica","code":"DM","callingCode":"1767"},{"name":"Denmark","code":"DK","callingCode":"45"},{"name":"Dominican Republic","code":"DO","callingCode":"1809"},{"name":"Dominican Republic","code":"DO","callingCode":"1829"},{"name":"Dominican Republic","code":"DO","callingCode":"1849"},{"name":"Algeria","code":"DZ","callingCode":"213"},{"name":"Ecuador","code":"EC","callingCode":"593"},{"name":"Egypt","code":"EG","callingCode":"20"},{"name":"Eritrea","code":"ER","callingCode":"291"},{"name":"Western Sahara","code":"EH","callingCode":"212"},{"name":"Spain","code":"ES","callingCode":"34"},{"name":"Estonia","code":"EE","callingCode":"372"},{"name":"Ethiopia","code":"ET","callingCode":"251"},{"name":"Finland","code":"FI","callingCode":"358"},{"name":"Fiji","code":"FJ","callingCode":"679"},{"name":"Falkland Islands","code":"FK","callingCode":"500"},{"name":"France","code":"FR","callingCode":"33"},{"name":"Faroe Islands","code":"FO","callingCode":"298"},{"name":"Micronesia","code":"FM","callingCode":"691"},{"name":"Gabon","code":"GA","callingCode":"241"},{"name":"United Kingdom","code":"GB","callingCode":"44"},{"name":"Georgia","code":"GE","callingCode":"995"},{"name":"Guernsey","code":"GG","callingCode":"44"},{"name":"Ghana","code":"GH","callingCode":"233"},{"name":"Gibraltar","code":"GI","callingCode":"350"},{"name":"Guinea","code":"GN","callingCode":"224"},{"name":"Guadeloupe","code":"GP","callingCode":"590"},{"name":"Gambia","code":"GM","callingCode":"220"},{"name":"Guinea-Bissau","code":"GW","callingCode":"245"},{"name":"Equatorial Guinea","code":"GQ","callingCode":"240"},{"name":"Greece","code":"GR","callingCode":"30"},{"name":"Grenada","code":"GD","callingCode":"1473"},{"name":"Greenland","code":"GL","callingCode":"299"},{"name":"Guatemala","code":"GT","callingCode":"502"},{"name":"French Guiana","code":"GF","callingCode":"594"},{"name":"Guam","code":"GU","callingCode":"1671"},{"name":"Guyana","code":"GY","callingCode":"592"},{"name":"Hong Kong","code":"HK","callingCode":"852"},{"name":"Honduras","code":"HN","callingCode":"504"},{"name":"Croatia","code":"HR","callingCode":"385"},{"name":"Haiti","code":"HT","callingCode":"509"},{"name":"Hungary","code":"HU","callingCode":"36"},{"name":"Indonesia","code":"ID","callingCode":"62"},{"name":"Isle of Man","code":"IM","callingCode":"44"},{"name":"India","code":"IN","callingCode":"91"},{"name":"British Indian Ocean Territory","code":"IO","callingCode":"246"},{"name":"Ireland","code":"IE","callingCode":"353"},{"name":"Iran","code":"IR","callingCode":"98"},{"name":"Iraq","code":"IQ","callingCode":"964"},{"name":"Iceland","code":"IS","callingCode":"354"},{"name":"Israel","code":"IL","callingCode":"972"},{"name":"Italy","code":"IT","callingCode":"39"},{"name":"Jamaica","code":"JM","callingCode":"1876"},{"name":"Jersey","code":"JE","callingCode":"44"},{"name":"Jordan","code":"JO","callingCode":"962"},{"name":"Japan","code":"JP","callingCode":"81"},{"name":"Kazakhstan","code":"KZ","callingCode":"76"},{"name":"Kazakhstan","code":"KZ","callingCode":"77"},{"name":"Kenya","code":"KE","callingCode":"254"},{"name":"Kyrgyzstan","code":"KG","callingCode":"996"},{"name":"Cambodia","code":"KH","callingCode":"855"},{"name":"Kiribati","code":"KI","callingCode":"686"},{"name":"Saint Kitts and Nevis","code":"KN","callingCode":"1869"},{"name":"South Korea","code":"KR","callingCode":"82"},{"name":"Kosovo","code":"XK","callingCode":"383"},{"name":"Kuwait","code":"KW","callingCode":"965"},{"name":"Laos","code":"LA","callingCode":"856"},{"name":"Lebanon","code":"LB","callingCode":"961"},{"name":"Liberia","code":"LR","callingCode":"231"},{"name":"Libya","code":"LY","callingCode":"218"},{"name":"Saint Lucia","code":"LC","callingCode":"1758"},{"name":"Liechtenstein","code":"LI","callingCode":"423"},{"name":"Sri Lanka","code":"LK","callingCode":"94"},{"name":"Lesotho","code":"LS","callingCode":"266"},{"name":"Lithuania","code":"LT","callingCode":"370"},{"name":"Luxembourg","code":"LU","callingCode":"352"},{"name":"Latvia","code":"LV","callingCode":"371"},{"name":"Macau","code":"MO","callingCode":"853"},{"name":"Saint Martin","code":"MF","callingCode":"590"},{"name":"Morocco","code":"MA","callingCode":"212"},{"name":"Monaco","code":"MC","callingCode":"377"},{"name":"Moldova","code":"MD","callingCode":"373"},{"name":"Madagascar","code":"MG","callingCode":"261"},{"name":"Maldives","code":"MV","callingCode":"960"},{"name":"Mexico","code":"MX","callingCode":"52"},{"name":"Marshall Islands","code":"MH","callingCode":"692"},{"name":"Macedonia","code":"MK","callingCode":"389"},{"name":"Mali","code":"ML","callingCode":"223"},{"name":"Malta","code":"MT","callingCode":"356"},{"name":"Myanmar","code":"MM","callingCode":"95"},{"name":"Montenegro","code":"ME","callingCode":"382"},{"name":"Mongolia","code":"MN","callingCode":"976"},{"name":"Northern Mariana Islands","code":"MP","callingCode":"1670"},{"name":"Mozambique","code":"MZ","callingCode":"258"},{"name":"Mauritania","code":"MR","callingCode":"222"},{"name":"Montserrat","code":"MS","callingCode":"1664"},{"name":"Martinique","code":"MQ","callingCode":"596"},{"name":"Mauritius","code":"MU","callingCode":"230"},{"name":"Malawi","code":"MW","callingCode":"265"},{"name":"Malaysia","code":"MY","callingCode":"60"},{"name":"Mayotte","code":"YT","callingCode":"262"},{"name":"Namibia","code":"NA","callingCode":"264"},{"name":"New Caledonia","code":"NC","callingCode":"687"},{"name":"Niger","code":"NE","callingCode":"227"},{"name":"Norfolk Island","code":"NF","callingCode":"672"},{"name":"Nigeria","code":"NG","callingCode":"234"},{"name":"Nicaragua","code":"NI","callingCode":"505"},{"name":"Niue","code":"NU","callingCode":"683"},{"name":"Netherlands","code":"NL","callingCode":"31"},{"name":"Norway","code":"NO","callingCode":"47"},{"name":"Nepal","code":"NP","callingCode":"977"},{"name":"Nauru","code":"NR","callingCode":"674"},{"name":"New Zealand","code":"NZ","callingCode":"64"},{"name":"Oman","code":"OM","callingCode":"968"},{"name":"Pakistan","code":"PK","callingCode":"92"},{"name":"Panama","code":"PA","callingCode":"507"},{"name":"Pitcairn Islands","code":"PN","callingCode":"64"},{"name":"Peru","code":"PE","callingCode":"51"},{"name":"Philippines","code":"PH","callingCode":"63"},{"name":"Palau","code":"PW","callingCode":"680"},{"name":"Papua New Guinea","code":"PG","callingCode":"675"},{"name":"Poland","code":"PL","callingCode":"48"},{"name":"Puerto Rico","code":"PR","callingCode":"1787"},{"name":"Puerto Rico","code":"PR","callingCode":"1939"},{"name":"North Korea","code":"KP","callingCode":"850"},{"name":"Portugal","code":"PT","callingCode":"351"},{"name":"Paraguay","code":"PY","callingCode":"595"},{"name":"Palestine","code":"PS","callingCode":"970"},{"name":"French Polynesia","code":"PF","callingCode":"689"},{"name":"Qatar","code":"QA","callingCode":"974"},{"name":"RÃ©union","code":"RE","callingCode":"262"},{"name":"Romania","code":"RO","callingCode":"40"},{"name":"Russia","code":"RU","callingCode":"7"},{"name":"Rwanda","code":"RW","callingCode":"250"},{"name":"Saudi Arabia","code":"SA","callingCode":"966"},{"name":"Sudan","code":"SD","callingCode":"249"},{"name":"Senegal","code":"SN","callingCode":"221"},{"name":"Singapore","code":"SG","callingCode":"65"},{"name":"South Georgia","code":"GS","callingCode":"500"},{"name":"Svalbard and Jan Mayen","code":"SJ","callingCode":"4779"},{"name":"Solomon Islands","code":"SB","callingCode":"677"},{"name":"Sierra Leone","code":"SL","callingCode":"232"},{"name":"El Salvador","code":"SV","callingCode":"503"},{"name":"San Marino","code":"SM","callingCode":"378"},{"name":"Somalia","code":"SO","callingCode":"252"},{"name":"Saint Pierre and Miquelon","code":"PM","callingCode":"508"},{"name":"Serbia","code":"RS","callingCode":"381"},{"name":"South Sudan","code":"SS","callingCode":"211"},{"name":"SÃ£o TomÃ© and PrÃ­ncipe","code":"ST","callingCode":"239"},{"name":"Suriname","code":"SR","callingCode":"597"},{"name":"Slovakia","code":"SK","callingCode":"421"},{"name":"Slovenia","code":"SI","callingCode":"386"},{"name":"Sweden","code":"SE","callingCode":"46"},{"name":"Swaziland","code":"SZ","callingCode":"268"},{"name":"Sint Maarten","code":"SX","callingCode":"1721"},{"name":"Seychelles","code":"SC","callingCode":"248"},{"name":"Syria","code":"SY","callingCode":"963"},{"name":"Turks and Caicos Islands","code":"TC","callingCode":"1649"},{"name":"Chad","code":"TD","callingCode":"235"},{"name":"Togo","code":"TG","callingCode":"228"},{"name":"Thailand","code":"TH","callingCode":"66"},{"name":"Tajikistan","code":"TJ","callingCode":"992"},{"name":"Tokelau","code":"TK","callingCode":"690"},{"name":"Turkmenistan","code":"TM","callingCode":"993"},{"name":"Timor-Leste","code":"TL","callingCode":"670"},{"name":"Tonga","code":"TO","callingCode":"676"},{"name":"Trinidad and Tobago","code":"TT","callingCode":"1868"},{"name":"Tunisia","code":"TN","callingCode":"216"},{"name":"Turkey","code":"TR","callingCode":"90"},{"name":"Tuvalu","code":"TV","callingCode":"688"},{"name":"Taiwan","code":"TW","callingCode":"886"},{"name":"Tanzania","code":"TZ","callingCode":"255"},{"name":"Uganda","code":"UG","callingCode":"256"},{"name":"Ukraine","code":"UA","callingCode":"380"},{"name":"Uruguay","code":"UY","callingCode":"598"},{"name":"United States","code":"US","callingCode":"1"},{"name":"Uzbekistan","code":"UZ","callingCode":"998"},{"name":"Vatican City","code":"VA","callingCode":"3906698"},{"name":"Vatican City","code":"VA","callingCode":"379"},{"name":"Saint Vincent and the Grenadines","code":"VC","callingCode":"1784"},{"name":"Venezuela","code":"VE","callingCode":"58"},{"name":"British Virgin Islands","code":"VG","callingCode":"1284"},{"name":"United States Virgin Islands","code":"VI","callingCode":"1340"},{"name":"Vietnam","code":"VN","callingCode":"84"},{"name":"Vanuatu","code":"VU","callingCode":"678"},{"name":"Wallis and Futuna","code":"WF","callingCode":"681"},{"name":"Samoa","code":"WS","callingCode":"685"},{"name":"Yemen","code":"YE","callingCode":"967"},{"name":"South Africa","code":"ZA","callingCode":"27"},{"name":"Zambia","code":"ZM","callingCode":"260"},{"name":"Zimbabwe","code":"ZW","callingCode":"263"}]
}

