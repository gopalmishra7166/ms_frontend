import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataSharingService {
    public isProductChange: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
}