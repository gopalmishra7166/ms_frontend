import { AfterContentInit, AfterViewInit, Component, DoCheck, OnChanges, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { MsServices } from './services/ms-services';
import { CookieService } from 'ngx-cookie-service';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import {Http} from '@angular/http';

const Swal = require('sweetalert2');
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit, OnChanges, AfterContentInit, DoCheck, AfterViewInit {
    navbarhideshow = false;
    logoutUserPoint: any;
    logoutResponse: any;
    logoImage = "";
    isLogo = false;
    companyName = '';
    product_type = '';
    uid='';
    isGetMeasured=false;
    isSize2Fit=false;
    isMySize=false;
    email: any;
    name: any;
    token: any;
    role: any;
    publicIP: any;
    physicalIp: any;
    isQuickSize=false;
  
    constructor(public route: Router, public titleService: Title, private cookieService: CookieService,
        private toastr: ToastrService, private spinner: NgxSpinnerService, public msservice: MsServices,private http: Http) {
    }

    ngOnInit() 
    {
        if(JSON.parse(localStorage.getItem('data')))
        {
            var d=JSON.parse(localStorage.getItem('data'));
            this.email=d['emailId'];
            this.token=d['token'];
            this.name=d['companyName'];
            this.role=d['role'];
        }

        this.http.get('https://jsonip.com/').subscribe(data => {
            this.publicIP = data;
            const body = this.publicIP._body;
            this.physicalIp = JSON.parse(body).ip;
        });
        
        this.loadScript();
    }

    ngOnChanges() {
    }

    ngDoCheck() 
    {
        if (localStorage.getItem('data')) {
            var d=JSON.parse(localStorage.getItem('data'));
            this.companyName=d['companyName'];
            this.logoImage =d['companyLogo'];
            if(d['role']=='master')
            {
                this.uid=d['emailId'];
            }
            else
            {
                this.uid=d['subuserId']
            }

            if(d['product'].SIZE2FIT.paymentStatus=='INACTIVE')
            {
                document.getElementById('pays2f').style.display='block';
                document.getElementById('pays2f').classList.add('paybutton')
            }

            if(d['product'].GET_MEASURED.paymentStatus=='INACTIVE')
            {
                document.getElementById('paygm').style.display='block';
            }


            if(d['product'].QUICKSIZE.paymentStatus=='INACTIVE')
            {
                document.getElementById('payqs').style.display='block';
            }


            if(d['product'].MYSIZE.paymentStatus=='INACTIVE')
            {
                document.getElementById('payms').style.display='block';
            }



                if(d['product'].SIZE2FIT)
                {
                    this.isSize2Fit=true;
                    document.getElementById('s2f0').style.display='block';
                    document.getElementById('s2f0').style.position='absolute';
                    document.getElementById('s2f0').style.marginTop='-37px';
                    document.getElementById('s2f0').style.marginLeft='130px';
                    document.getElementById('s2f1').style.display='none';
                }
                else
                {
                    document.getElementById('s2f1').style.display='block';
                    document.getElementById('s2f1').style.position='absolute';
                    document.getElementById('s2f1').style.marginTop='-30px';
                    document.getElementById('s2f1').style.marginLeft='120px';
                    document.getElementById('s2f0').style.display='none';
                }
                
                if(d['product'].GET_MEASURED)
                {
                    this.isGetMeasured=true;
                    document.getElementById('gm0').style.display='block';
                    document.getElementById('gm0').style.position='absolute';
                    document.getElementById('gm0').style.marginTop='-37px';
                    document.getElementById('gm0').style.marginLeft='130px';
                    document.getElementById('gm1').style.display='none';
                   
                }
                else
                {
                    document.getElementById('gm1').style.display='block';
                    document.getElementById('gm1').style.position='absolute';
                    document.getElementById('gm1').style.marginTop='-30px';
                    document.getElementById('gm1').style.marginLeft='120px';
                    document.getElementById('gm0').style.display='none';
                }

                if(d['product'].MYSIZE)
                {
                    this.isMySize=true;
                    document.getElementById('msize0').style.display='block';
                    document.getElementById('Msize-show0').style.display='block';
                    document.getElementById('Msize-show1').style.display='block';
                    document.getElementById('msize0').style.position='absolute';
                    document.getElementById('msize0').style.marginTop='-37px';
                    document.getElementById('msize0').style.marginLeft='130px';
                    document.getElementById('msize1').style.display='none';
                }
                else
                {
                    document.getElementById('msize1').style.display='block';
                    document.getElementById('msize1').style.position='absolute';
                    document.getElementById('msize1').style.marginTop='-30px';
                    document.getElementById('msize1').style.marginLeft='120px';
                    document.getElementById('msize0').style.display='none';
                    document.getElementById('Msize-show0').style.display='none';
                    document.getElementById('Msize-show1').style.display='none';
                }
                if(d['product'].QUICKSIZE)
                {
                    this.isQuickSize=true;
                    document.getElementById('Qsize-show0').style.display='block';
                    document.getElementById('Qsize-show1').style.display='block';
                    document.getElementById('qsize0').style.display='block';
                    document.getElementById('qsize0').style.position='absolute';
                    document.getElementById('qsize0').style.marginTop='-37px';
                    document.getElementById('qsize0').style.marginLeft='130px';
                    document.getElementById('qsize1').style.display='none';
                }
                else
                {
                   
                    document.getElementById('qsize1').style.display='block';
                    document.getElementById('qsize1').style.position='absolute';
                    document.getElementById('qsize1').style.marginTop='-30px';
                    document.getElementById('qsize1').style.marginLeft='120px';
                    document.getElementById('qsize0').style.display='none';
                    document.getElementById('Qsize-show0').style.display='none';
                    document.getElementById('Qsize-show1').style.display='none';
                }
                
           

        }
    }
    
    // for navigation on change
    ngAfterContentInit() 
    {
      
    }

    ngAfterViewInit() 
    {
      
    }

    showSuccess() {
        this.toastr.success('Hello world!', 'Toastr fun!');
    }

    logout() {
        if(this.role=="subuser"){
            this.logoutUserPoint = {
                'emailId': this.uid,
                'token': this.token,
                'role':this.role,
                'physicalIp':this.physicalIp
            };
        }
        else{
            this.logoutUserPoint = {
                'emailId': this.email,
                'token': this.token,
                'role':this.role,
                'physicalIp':this.physicalIp
            };
        }
        
        Swal({
            title: 'Sure, You want to logout?',
            type: 'info',
            showCancelButton: true,
            confirmButtonColor: '',
            cancelButtonColor: '',
            confirmButtonText: 'Yes',
            allowOutsideClick: false,
        }).then((result) => {
            if (result.value) {
                this.msservice.logoutUser(this.logoutUserPoint).subscribe(
                    res => {
                        this.logoutResponse = res;
                    });
                this.cookieService.deleteAll();
                sessionStorage.clear();
                localStorage.clear();
                this.toastr.success('successfull signed out', 'Signed out', {
                    timeOut: 600,
                });
                this.route.navigate(['/']);
                this.msservice.statechange.next(false);
            } else {
                this.toastr.warning('Not signed out', 'Canceled', {
                    timeOut: 800,
                });
            }
        });
    }

    // display()
    // {
    //     var x = document.getElementById("drpContent");
    //     if (x.style.display == "none") 
    //     {
    //         x.style.display = "block";
    //     } 
    //     else 
    //     {
    //         if (x.style.display == "block") 
    //         {
    //             x.style.display = "none";
    //         } 
    //         x.style.display = "none";
    //     }
    // }

    notification()
    {
        var x = document.getElementById("showNotification");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    products()
    {
        var x = document.getElementById("showProducts");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    productSelect(p){
        if(p=='gm'){
            localStorage.setItem('subscribeProduct', 'GET_MEASURED');
            this.route.navigate(['/subscribe'])
        }
        if(p=='sf'){
            localStorage.setItem('subscribeProduct', 'SIZE2FIT');
            
            this.route.navigate(['/subscribe'])
           
        }
        if(p=='ms')
        {
            localStorage.setItem('subscribeProduct', 'MYSIZE');    
            this.route.navigate(['/subscribe'])
        }
        if(p=='qs'){
            localStorage.setItem('subscribeProduct', 'QUICKSIZE');
            this.route.navigate(['/subscribe']);
        }
    }

    public loadScript() {
        var isFound = false;
        var ANS_customer_id="d1896866-4687-448a-877a-4afe81585f55";
        var scripts = document.getElementsByTagName("script")
        for (var i = 0; i < scripts.length; ++i) {
            if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("anetseal")) {
                isFound = true;
            }
        }

        if (!isFound) {
            var dynamicScripts = ["//verify.authorize.net:443/anetseal/seal.js/"];

            for (var i = 0; i < dynamicScripts.length; i++) {
                let node = document.createElement('script');
                node.src = dynamicScripts[i];
                node.id=ANS_customer_id;
                node.type = 'text/javascript';
                node.async = false;
                node.charset = 'utf-8';
                document.getElementsByTagName('head')[0].appendChild(node);
              
            }
        }
    }

}