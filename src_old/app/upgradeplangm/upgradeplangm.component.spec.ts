import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradeplangmComponent } from './upgradeplangm.component';

describe('UpgradeplangmComponent', () => {
  let component: UpgradeplangmComponent;
  let fixture: ComponentFixture<UpgradeplangmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradeplangmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradeplangmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
