import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnChanges, OnInit} from '@angular/core';
import {MsServices} from '../services/ms-services';
import {CookieService} from 'ngx-cookie-service';
import $ from 'jquery';
import {MerchantDetails} from '../model/merchantdetails';
import {Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {Pernt_MerchantSpecificDataContainerArray} from '../SizeToFit/Pernt_MerchantSpecificDataContainerArray';
import {MerchantSizeChartCompositKey} from '../SizeToFit/MerchantSizeChartCompositKey';
import {MerchantSpecificDataContainerArray} from '../SizeToFit/MerchantSpecificDataContainerArray';

import {ToastrService} from 'ngx-toastr';
import {isBoolean} from 'util';


const Swal = require('sweetalert2');


// for side bar you need to add dependencies dom sanitizer and router and jquery with merchannt model

@Component({
    selector: 'app-apparel-detail',
    templateUrl: './apparel-detail.component.html',
    styleUrls: ['./apparel-detail.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApparelDetailComponent implements OnInit, OnChanges {
    email: any;

    constructor(public _sanitizer: DomSanitizer, private toastr: ToastrService, private route: Router, private service: MsServices, private cookieService: CookieService, private cdref: ChangeDetectorRef) {
        this.uid = this.cookieService.get('uid');
        this.name = this.cookieService.get('name');
        this.service.statechange.next(true);

        this.apparelDetail = {
            'brandName': this.cookieService.get('brandName'),
            'gender': this.cookieService.get('gender'),
            'merchatID': this.cookieService.get('email'),
            'apparelName': this.cookieService.get('apparelName'),
            // 'country': this.cookieService.get('country'),
        };
    }


    now = new Date();

    BodyFixed=true;
    BodyRange=false;
    ReadyRange=false;
    ReadyFixed=false;
    public nonbody=true;
    public maxmin: boolean;
    public body = true;
    show=true;
    public showAllowances = false;
    parentDetail: Pernt_MerchantSpecificDataContainerArray;
    mearchantClass: MerchantSizeChartCompositKey;
    merchantSpecificData: MerchantSpecificDataContainerArray[] = [];
    merchantDetails: MerchantDetails;
    cm = true;
    inch = false;
    f = 1;
    minNUmber: any;
    msg = 'centimeters(CM)';
    apparelDetail: any;
    response: any;
    user: any [] = [];
    enable = true;
    data = false;
    arrSize = 0;
    inArrSize: number[] = [];
    testing = [];
    measurementPoints: any[] = [];
    cloned = [];
    arr: any;
    permission: any[][] = [[]]; // for size or min value
    tolerence: any[][] = [[]]; // for tolerence
    allowances: any[][] = [[]]; // for allowances
    permissible: any[][] = [[]]; // for max value
    minSize:any[][]=[[]]
    permit: any[][] = [[]];
    trial: any[] = [];
    perTrial: any[] = [];
    checked: boolean[] = [];
    name: any;
    uid: any;
    sendingData: any;
    onCall: any;
    value2: any;
    type2: any;
    typeArr: any[] = [];
    sizeChartType='';
    selectedChartType='';
    
// logout the session

    /*
        result:[];*/
    deleteData: any;
    maxLengthForPriority: any;
    newResponse: any;

    arrayForTrueFalse: any;
    finalResponse: any;
    valueCheckForBlankAllowances: boolean;
radioThreeButtonClick: boolean;
messageResponseForJson: any;

    // ------New implementation as on 16/2/2019-------//

// -----This will check whether priority is 0 or not-------//
    contains(arr, element) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === element) {
                return true;
            }
        }
        return false;
    }// ----Endeds




// for hidding three forms accourding toi requirement """""Utkarsh""""""
    toggle() {
        if (this.showAllowances) {
            /*alert("on toggle")*/
            this.showAllowances = false;
            this.maxmin = true;
        } else if (this.maxmin) {
            /*alert("in else ifs")*/
            this.maxmin = false;
            this.body = false;
        } else {
            /*alert("in else of toggle")*/
            this.body = true;
            this.maxmin = true;
        }
    }

// for  hidding  three form accourding to requirement
    toggle1() {
        if (!this.body) {
            this.body = true;
            this.showAllowances = true;
            /* alert(showAllowancesclicking")*/
        } else if (this.maxmin) {
            this.maxmin = false;
            this.showAllowances = true;
            /* alert("in else if of toggle 2")*/
        } else {
            this.maxmin = false;
            this.showAllowances = false;
            this.body = false;
            /*alert("in else")*/
        }
    }

    delete(size) {
        this.deleteData = {
            'brandName': this.cookieService.get('brandName'),
            'gender': this.cookieService.get('gender'),
            'merchatID': this.cookieService.get('email'),
            'apparelName': this.cookieService.get('apparelName'),
            'country': this.cookieService.get('country'),
            'size': size.type1,
        };
      
    }
// -------Checking whether  the size is blank------//
    isCherries(fruit) {
        return fruit === '';
    }
    permiTest() 
    {
        this.arrayForTrueFalse = [];
        this.parentDetail = new Pernt_MerchantSpecificDataContainerArray();
        this.mearchantClass = new MerchantSizeChartCompositKey();
        this.mearchantClass.apparelName = this.cookieService.get('apparelName');
        this.mearchantClass.brandName = this.cookieService.get('brandName');
        this.mearchantClass.gender = this.cookieService.get('gender');
        this.mearchantClass.merchatID = this.email;
        this.mearchantClass.contry = this.cookieService.get('country');
        this.parentDetail.sizeChartType=this.sizeChartType;
        this.parentDetail.compositeKey = this.mearchantClass;
        for (let i = 0; i < this.arrSize; i++) {
            this.merchantSpecificData[i] = new MerchantSpecificDataContainerArray();
           
        }
        for (let i = 0; i < this.arrSize; i++) {
            for (let j = 0; j < this.inArrSize[i]; j++) {
                /*this.merchantSpecificData[i].priorities = new Array(this.inArrSize[i]).fill('');*/
                this.merchantSpecificData[i].enteredSize = new Array(this.inArrSize[i]).fill('');
                this.merchantSpecificData[i].min = new Array(this.inArrSize[i]).fill('');
                this.merchantSpecificData[i].max = new Array(this.inArrSize[i]).fill(''); /*
                this.merchantSpecificData[i].tolerance = new Array(this.inArrSize[i]).fill('');*/
                this.merchantSpecificData[i].allowances = new Array(this.inArrSize[i]).fill(''); 
            }
        }

        for (let i = 0; i < this.arrSize; i++) {
            for (let j = 0; j < this.inArrSize[i]; j++) {
                this.merchantSpecificData[i].enteredSize[j] = (this.permission[i][j]);
                this.merchantSpecificData[i].min[j] = (this.minSize[i][j]);                
                this.merchantSpecificData[i].max[j] = (this.permissible[i][j]);
                
                this.merchantSpecificData[i].allowances[j] = (this.allowances[i][j]);
                this.merchantSpecificData[i].selectedSize = this.arr[i].type1; //
                this.parentDetail.measurementPOintsName = this.arr[i].value1;
                this.parentDetail.priorities.push(this.permit[i][j]);
                this.parentDetail.tolerance.push(this.tolerence[i][j]);
               
            }
            
        }
        if (this.cm === true) {
            this.parentDetail.unit = 1;
        } else {
            (this.parentDetail.unit = 2);
        }
        this.parentDetail.child_merchantKey = this.merchantSpecificData;

        /* const valueCheckForAllowancesOne = [];*/
        const valueCheckForAllowancesZero = [];
        const valueCheckForAllowancesNegative = [];

        const valueCheckForPriorityZero = [];
        const valueCheckForPriorityNegative = [];
        // -----Checking for priority and tolerence-----//
        if (this.parentDetail.priorities.findIndex(this.isCherries) === 0 || this.parentDetail.priorities.findIndex(this.isCherries) === 1 || this.parentDetail.tolerance.findIndex(this.isCherries) === 0 || this.parentDetail.tolerance.findIndex(this.isCherries) === 1) {
            /* this.valueCheckForBlankAllowances = true;*/
            /*              || a.max.findIndex(this.isCherries) === 0 || a.max.findIndex(this.isCherries) === 1*/
            valueCheckForPriorityZero.push(this.parentDetail.priorities.findIndex(this.isCherries));
        } else {
            valueCheckForPriorityNegative.push(this.parentDetail.priorities.findIndex(this.isCherries));
        }
       
      

                        this.service.apparelDetailsSave(this.parentDetail).subscribe(res => {
                           
                            this.finalResponse = res;
                            this.messageResponseForJson = this.finalResponse.message;

                            if (this.finalResponse.code == 1) 
                            {
                                this.toastr.success('Size chart save successfully', 'Success', {
                                    timeOut: 1100,
                                });
                                this.route.navigate(['/apparel-table']);
                                this.service.forBlankApiInPriority(this.apparelDetail).subscribe(response => {
                               
                                });
                            } else if (this.finalResponse.message === 'size contain zero') {
                                if (this.radioThreeButtonClick === true) {
                                this.toastr.warning('Minimum cant be zero', 'Minimum Zero', {
                                    timeOut: 1200,
                                }); } else {
                                    this.toastr.warning('Size cant be zero', 'Size Zero', {
                                        timeOut: 1200,
                                    });
                                }
                            } else if (this.finalResponse.message === 'max value contain zero') {
                                this.toastr.warning('Max value cant be zero', 'Zero', {
                                    timeOut: 1000,
                                });
                            } else {
                                this.toastr.warning('Priority could not be same, please fill different priorities', 'Priorities same', {
                                    timeOut: 1300,
                                });
                            }
                        });
            }
        


    cmClick() {
        this.inch = false;
        this.cm = true;
       
        this.msg = '(cm)';
        this.f = 1;
    }

    inchClick() {
       
        this.inch = true;
        this.cm = false;
       
        this.msg = ' (inch)';
        this.f = 2.54;
    }

    public isEmptyObject(onCall) {
        return (onCall && (Object.keys(onCall).length === 0));
    }

    hello() 
    {
        
       
                var data=JSON.parse(localStorage.getItem('apparelData'));
                const user = data;
                this.onCall = user;
                const newObjTest = {};
              
              
                if (this.isEmptyObject(this.onCall) === false) {
                 
                    this.arr = Object.keys(user).map(key => ({type1: key, value1: user[key]}));
                
                    if (this.arr[0].value1 !== 'FAILURE') {
                     
                        this.maxLengthForPriority = this.arr[0].value1.length;
                     
                        this.type2 = this.arr[0].type1;
                        this.value2 = this.arr[0].value1;
                        this.typeArr.push(this.arr[0].type1);
                      
                        this.arrSize = this.arr.length;
                        for (let i = 0; i < this.arrSize; i++) {
                            this.inArrSize[i] = this.arr[i].value1.length;
                           
                        }
                        this.permission = new Array(this.arrSize).fill('');  // --size
                        this.permit = new Array(this.arrSize).fill(''); // priority
                        this.minSize = new Array(this.arrSize).fill('');
                        this.permissible = new Array(this.arrSize).fill(''); // max value
                        this.tolerence = new Array(this.arrSize).fill(''); // tolerence
                        this.allowances = new Array(this.arrSize).fill(''); // tolerence


                        for (let j = 0; j < this.arrSize; j++) {
                            this.permission[j] = new Array(this.inArrSize[j]).fill('');
                           
                        }// for max size
                        for (let k = 0; k < this.arrSize; k++) {
                            this.permit[k] = new Array(this.inArrSize[k]).fill('');
                           
                        }// for priority
                        for (let m = 0; m < this.arrSize; m++) {
                            this.permissible[m] = new Array(this.inArrSize[m]).fill('');
                        
                        }// for min size value

                        for (let m = 0; m < this.arrSize; m++) {
                            this.minSize[m] = new Array(this.inArrSize[m]).fill('');
                    
                        }// for min size value

                        for (let n = 0; n < this.arrSize; n++) {
                            this.tolerence[n] = new Array(this.inArrSize[n]).fill('');
                            
                        }
                        for (let o = 0; o < this.arrSize; o++) {
                            this.allowances[o] = new Array(this.inArrSize[o]).fill('');
                           
                        }// for tolerence calculating on index so that value will not repeat

                  
                        this.data = true;
                      
                        this.cdref.detectChanges();

                    }
                } 
    }

   

    ngOnInit() 
    {
        var d=JSON.parse(localStorage.getItem('data'));
        this.email=d['emailId'];
        
        this.selectedChartType=localStorage.getItem('chartType');
            this.sizeChartType=this.selectedChartType;
            if(this.sizeChartType=='bodyFixed')
            {
                this.BodyFixed=true;
                this.BodyRange=false;
                this.ReadyRange=false;
                this.ReadyFixed=false;
            }
            else if(this.sizeChartType=='bodyRange')
            {
                this.BodyFixed=false;
                this.BodyRange=true;
                this.ReadyRange=false;
                this.ReadyFixed=false; 
            } 
            else if(this.sizeChartType=='readyRange')
            {
                this.BodyFixed=false;
                this.BodyRange=false;
                this.ReadyRange=true;
                this.ReadyFixed=false; 
            }
            else if(this.sizeChartType=='readyFixed')
            {
                this.BodyFixed=false;
                this.BodyRange=false;
                this.ReadyRange=false;
                this.ReadyFixed=true; 
            }
            else
            {
                this.sizeChartType="bodyFixed"
                this.BodyFixed=true;  
            }
        
        this.hello();
        this.apparelDetail = {
            'brandName': this.cookieService.get('brandName'),
            'gender': this.cookieService.get('gender'),
            'merchatID': this.cookieService.get('email'),
            'apparelName': this.cookieService.get('apparelName'),
            'country': this.cookieService.get('country'),
        };

      
      
    }

    ngOnChanges() {
    }

    bodyType(e)
    {
        
      var val=e.target.value;
      this.sizeChartType=val;
      if(val=='bodyFixed'){
        this.BodyFixed=true;
        this.BodyRange=false;
        this.ReadyRange=false;
        this.ReadyFixed=false;
      }  
      else if(val=='bodyRange'){
        this.BodyRange=true;
        this.BodyFixed=false;
        this.ReadyRange=false;
        this.ReadyFixed=false;
      }
      else if(val=='readyRange')
      {
          this.ReadyRange=true;
          this.BodyFixed=false;
          this.BodyRange=false;
          this.ReadyFixed=false;
      }
      else if(val=='readyFixed'){
        this.ReadyRange=false;
        this.BodyFixed=false;
        this.BodyRange=false;
        this.ReadyFixed=true;
      }
    }

}
