import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ActivatedRoute } from '@angular/router';
import { PagerService } from '../services/pagerService';
const Swal = require('sweetalert2');
import {ToastrService} from 'ngx-toastr';
declare var $:any;

@Component({
  selector: 'app-usermysize',
  templateUrl: './usermysize.component.html',
  styleUrls: ['./usermysize.component.css']
})
export class UsermysizeComponent implements OnInit {
  showLoader=false;
  email: any;
  token: any;
  expired= false;
  usersData=[];
  searchKey="email_id";
  searchText="";
  userCount=0;
  pager: any = {};
  pagedItems: any[];
  productName='';

  constructor(private service:MsServices,private toast: ToastrService,private rout:ActivatedRoute,private pagerService:PagerService) { }

  ngOnInit() 
  {
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Users";
    var param = this.rout.snapshot.paramMap.get("id");
  

    var d=JSON.parse(localStorage.getItem('data'));
    this.email=d['emailId'];
    this.token=d['token'];
    this.productName = 'MYSIZE'

    
    if (d['product'].MYSIZE.paymentStatus == 'INACTIVE') 
    {  
      if(d['product'].MYSIZE.remainingDays < 0)
      {
        this.expired = false;
      }
      else
      {
        this.expired = true;
      }
    }
    if(this.expired==false)
    {
      this.getUserListMysize(1);
    }
  }

  getUserListMysize(page)
  {
    this.showLoader=true;
    var obj={"emailId":this.email,"token":this.token,"productName":this.productName,"pageNo":page.toString()}
    this.service.getuserlogs(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        if(response.data)
        {
          this.userCount=response.record_count
          this.usersData=response.data;
          this.pager = this.pagerService.getPager(this.userCount, page);
        }
      }
      else
      {
        this.toast.warning(response.message, 'Message', {timeOut: 2000,}); 
      }
    })
  }

  searchUser()
  {
    this.showLoader=true;
    this.usersData=[];
    var obj={"token":this.token,"emailId":this.email,"productName":'MYSIZE',"key":this.searchKey,"value":this.searchText};
    this.service.searchuserlogs(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        this.usersData=response.data;
        this.userCount=response.record_count
      }
      else
      {
        Swal("Error",response.message,'erro');
      }
    })
  }

  clear()
  {
    this.getUserListMysize(1);
  }

}
