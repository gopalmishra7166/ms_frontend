import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ActivatedRoute } from '@angular/router';
import * as S3 from 'aws-sdk/clients/s3';
import { NgxFileDropEntry, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

declare var $: any;

const Swal = require('sweetalert2');

@Component({
  selector: 'app-sizechart',
  templateUrl: './sizechart.component.html',
  styleUrls: ['./sizechart.component.css']
})
export class SizechartComponent implements OnInit {

  muid='';
  mid='';
  data:any;
  email: any;
  token: any;
  accountId: any;
  timestamp: number;
  upflag = false;
  filedata: any;
  progressbar: boolean;
  pv = 0
  showLoader= false;
  public files: NgxFileDropEntry[] = [];
  product_type: any;
  paid: boolean;

  constructor(private service:MsServices) { }

  ngOnInit() 
  {
    var d=JSON.parse(localStorage.getItem('data'));
    this.email=d['emailId'];
    this.token=d['token'];
    this.accountId=d['accountId'];
    if(d['product'].SIZE2FIT.planType == 'PAID')
    {
        this.paid=true;
        this.getfiles();
    }
  
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Upload size chart";
  }

  uploadToS3()
  {
    this.showLoader=true;
    const contentType = this.data.type;
    const bucket = new S3(
      {
        accessKeyId: 'AKIAIFXOEPN7XBYCUXBA',
        secretAccessKey: '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',
        region: 'us-east-1'
      }
    );

    this.timestamp = Date.now();
     
    const params = {
      Bucket:'miiror-vijay',
      Key:'merchantuploads/'+this.accountId+'/sizecharts/'+this.timestamp+'.zip',
      Body:this.data,
      ContentType: contentType,
    };
    var self=this;
    bucket.putObject(params).on('httpUploadProgress', function(evt) {
      this.pv=(evt.loaded * 100) / evt.total
      // console.log("Uploaded :: " + (evt.loaded * 100) / evt.total+'%');
    }).send(function(err, data) {
        if(!err) {
            self.showLoader=false;
            self.s3fileupload();
            self.getfiles()
            
        } else {
            console.log(err);
        }
    });
    
  }
  
  getfiles(){
    var obj={"emailId":this.email,"token":this.token,"productName":"SIZE2FIT","fileType":"sizechart"}
    this.service.getfiles(obj).subscribe(response=>{
      if(response.code=="1")
      {
        this.filedata=response.data;
      }
      else
      {
        Swal('error',response.message,'error');
      }
    })
  }

  downloadfile(response)
  {
    window.open(response.filePath, '_blank');
  }

  deletefile(d)
  {
    var obj={"emailId":this.email,"token":this.token,"id":d.id}


    Swal({

      title: 'Are you sure, want to delete this file?',
      type: 'info',
      showCancelButton: true,
      confirmButtonColor: '',
      cancelButtonColor: '',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,

    }).then((result) => {

      if (result.value) {
        this.service.removefile(obj).subscribe(response=>{
          if(response.code=="1")
          {
            Swal('Success',response.message,'success');
            this.getfiles()
          }
          else
          {
            Swal('error',response.message,'error');
          }
        })
      }
    });
   
  }

  s3fileupload()
  {
    this.showLoader=true;
    var path = 'https://miiror-vijay.s3.amazonaws.com/merchantuploads/'+this.accountId+'/sizecharts/'+this.timestamp+'.zip'
    var obj={"emailId":this.email,"token":this.token,"productName":"SIZE2FIT","filePath":path,"fileType":"sizechart"}
    this.service.fileUpload(obj).subscribe(response=>{
      if(response.code=="1")
      {
        this.showLoader=false;
        Swal('Success',response.message,'success');
        this.getfiles();
        this.createTicket(path);
        this.data='';
      }
      else
      {
        Swal('error',response.message,'error');
      }
    })
    console.log("Success");
  }

  createTicket(path)
  {
    this.showLoader=true;
    var obj=
    { emailId:this.email,
      token:this.token,
      content:"Review and deploy size chart on the mirrorsize platform, sizechart path "+path,
      subject:"New Size chart Upload request"
    }
    this.service.raiseTicket(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        Swal('success',response.message,'success');
      }
      else
      {
        Swal('error',response.message,'error');
      }
    })
  }

public dropped(files: NgxFileDropEntry[]) {
  this.files = files;
  for (const droppedFile of files) {

    // Is it a file?
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        this.data=file;
      });
    } else {
      const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      console.log(droppedFile.relativePath, fileEntry);
    }
  }
}

public fileOver(event){
  console.log(event);
}

public fileLeave(event){
  console.log(event);
}


}
