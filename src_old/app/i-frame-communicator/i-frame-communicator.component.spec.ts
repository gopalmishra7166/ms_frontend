import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IFrameCommunicatorComponent } from './i-frame-communicator.component';

describe('IFrameCommunicatorComponent', () => {
  let component: IFrameCommunicatorComponent;
  let fixture: ComponentFixture<IFrameCommunicatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IFrameCommunicatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IFrameCommunicatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
