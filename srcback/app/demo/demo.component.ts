import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MsServices } from '../services/ms-services';
const Swal = require('sweetalert2');
declare var $:any;

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {
  mForm: FormGroup;
  email: any;
  merchantId: any;
  bodyShape = "";
  showLoader = false;
  isChecked = false;
  isWeight = false;
  sizes=[];
  isMale=false;
  gender='selectGen';
  jsonData:any;
  brandName='select';
  apparel='selectapparel';
  genderArr=[];
  apparelType=[];
  fitType='fittype';
  fit=[];
  countryName='country';
  countries=[];
  brandData=[];
  apparelName="";
  sizeType=[];
  measurement=[];
  constructor(private service: MsServices) {
    this.mForm = new FormGroup({
      heightInInch: new FormControl(''),
      heightInCM: new FormControl(''),
      heightInFeet: new FormControl(''),
      age: new FormControl(''),
      // gender: new FormControl(''),
      weightInKg: new FormControl(''),
      weightInPound: new FormControl(''),

    })
  }

  ngOnInit() {
    
  
    var d = JSON.parse(localStorage.getItem('data'));
    if(d)
    {
      this.email = d['emailId'];
      this.merchantId = d['accountId']

    }

    if(this.gender=='male')
    {
      document.getElementById('male').style.background="var(--blue)";
      document.getElementById('male').style.color="#fff";
    }

    this.getBrnadList();

  }

  selectBrand(e)
  {
    this.brandName=e.target.value;
  }

  selectFitType(e)
  {
    this.fitType=e.target.fitType;
  }

  selectSize(id,v,m) 
  {
   
    this.measurement=[]
    var mdata=this.sizeType.filter(item=>item.pointName==m)[0].data
      this.measurement = Object.keys(mdata).map(function (key) {
            return ({ "key": (key), "value": mdata[key] });
        });

  //  var idd:any=document.getElementById("s"+id);
  //  var i:any=v+id; 
  //   if (idd.id==i) {
  //     document.getElementById(idd.id).style.background = '#011b2f';
  //     document.getElementById(idd.id).style.color = '#fff';
  //   }
  //   else
  //   {
  //     document.getElementById(idd.id).style.background = '#fff';
  //     document.getElementById(idd.id).style.color = '#333';
  //   }

  
  }

  getMeasurementQuickSize() {
 
      var height = 0;
      var weight = 0;

      if (this.isChecked) {
        height = (this.mForm.value.heightInCM) * 10
      }
      else {
        if (!this.mForm.value.heightInFeet) {
          var inc = (parseInt(this.mForm.value.heightInInch) * 2.54) * 10
          height = inc;
        }
        else if (!this.mForm.value.heightInInch) {
          var f = parseInt(this.mForm.value.heightInFeet) * 12;
          var ff = (f * 2.54) * 10;
          height = ff;
        }
        else {
          var inc = (parseInt(this.mForm.value.heightInInch) * 2.54) * 10
          var f = parseInt(this.mForm.value.heightInFeet) * 12
          var ff = (f * 2.54) * 10;
          height = ff + inc;
        }
      }

        if (this.isWeight) {
          if ((Number(this.mForm.value.weightInKg))>=30 && (Number(this.mForm.value.weightInKg))<=150) {
            weight = this.mForm.value.weightInKg
          }
          else
          {
            Swal('info!', 'weight must be in 30 to 150 kg', 'info')
            weight=0;
          }
        }
        else {
          if((Number(this.mForm.value.weightInPound))>=66 && (Number(this.mForm.value.weightInPound))<=330) 
          {
            weight = (parseInt(this.mForm.value.weightInPound)) * 2.10
          }
          else
          {
            Swal('info!', 'weight must be in 66 to 330lbs kg', 'info');
            weight=0;
          }
        }
      

      if(this.mForm.value.age<"18" || this.mForm.value.age>"80")
      {
        Swal('info!', 'age must be between 18 to 80 year', 'info');
      }
      

      var obj = {
        // "userId"     : "passionategopal001@gmail.com",
        "height": Math.round(height),
        "weight": Math.round(weight),
        "age": this.mForm.value.age,
        "gender": this.gender,
        "userType": "merchant",
        "merchantId": this.email,
        "productName": "QUICKSIZE",
        "stomachShape": this.bodyShape, 
        "apparelName":this.apparelName,
        "fitType":this.fitType,
        "brandName":this.brandName,
        "country":this.countryName,
        "recommendationType":"regular"
      };

      this.showLoader=true;
      this.service.getMeasurementQuickSize(obj).subscribe(response => {
        this.showLoader=false;
        if (response.code == "1") 
        {
          this.sizes=response.data.sizes
          // Swal('success', response.message, 'success');
          document.getElementById('myModal2').style.display = 'none';
          this.mForm.reset();
          document.getElementById('average').style.backgroundColor = 'rgb(2, 19, 81)';
          document.getElementById('flat').style.backgroundColor = 'rgb(2, 19, 81)';
          document.getElementById('belly').style.backgroundColor = 'rgb(2, 19, 81)';
          document.getElementById('size').style.display='block';
          document.getElementById('size').style.opacity='1';
        
        }
        else 
        {
          document.getElementById('headerShow').style.zIndex='99999';
          document.getElementById('outerMain').style.zIndex="0"; 
          document.getElementById('myModal2').style.display = 'none';
          Swal('Oops!', response.message, 'info');
        }
      })
  
  }

  getBodyShape(id) {
    if (id == 'flat') {
      this.bodyShape = id;
      document.getElementById(id).style.backgroundColor = '#ecd00a';
      document.getElementById('average').style.backgroundColor = 'rgb(2, 19, 81)';
      document.getElementById('belly').style.backgroundColor = 'rgb(2, 19, 81)';
    }
    else if (id == 'average') {
      this.bodyShape = id;
      document.getElementById(id).style.backgroundColor = '#ecd00a';
      document.getElementById('flat').style.backgroundColor = 'rgb(2, 19, 81)';
      document.getElementById('belly').style.backgroundColor = 'rgb(2, 19, 81)';
    }
    else if (id == 'belly') {
      this.bodyShape = id;
      document.getElementById(id).style.backgroundColor = '#ecd00a';
      document.getElementById('average').style.backgroundColor = 'rgb(2, 19, 81)';
      document.getElementById('flat').style.backgroundColor = 'rgb(2, 19, 81)';
    }
    else {
      this.bodyShape = "";
    }

  }

  switchValue(e) {
    if (e.target.checked) {
      this.isChecked = true;
    }
    else {
      this.isChecked = false;
    }
  }

  switchWeight(e) {
    if (e.target.checked) {
      this.isWeight = true;
    }
    else {
      this.isWeight = false;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  addClass()
  {
      document.getElementById('headerShow').style.zIndex='1';
      document.getElementById('outerMain').style.zIndex="9999"; 
      document.getElementById('size').style.background='rgba(0, 0, 0, 0.61)';
  }

  closeModal()
  {
      document.getElementById('headerShow').style.zIndex='99999';
      document.getElementById('outerMain').style.zIndex="0"; 
      document.getElementById('size').style.display='none';
  }

  selectGender(e)
  {
    this.gender=e.target.value;
  }

  getBrnadList()
  {
    this.service.getBrnadList({emailId:this.email}).subscribe(response=>{
      if(response.code=="1")
      { 

        for(var i in response.data)
        {
          this.countries.push({country:i,data:response.data[i]});
        }
      }
      else
      {
        Swal('Oops!',response.message,'info')
      }
    })
  }

  getBrands(e)
  {
    this.countryName=e.target.value;
    this.brandData=[];
    this.genderArr=[];
    var data=this.countries.filter(item=>item.country==e.target.value)[0].data
    this.brandData=data.brandName;
    this.genderArr=data.gender;
  }

  getChart(e)
  {
    this.gender=e.target.value;
    this.getQuickSizeChart();
  }

  getQuickSizeChart()
  {
    this.apparelType=[];
    var obj={"emailId":this.email,"country":this.countryName,"brandName":this.brandName,"gender":this.gender}
    this.service.getSizeChart(obj).subscribe(response=>{
      if(response.code=="1")
      {
        for(var i in response.data)
        {
          this.apparelType.push({apparelName:i,data:response.data[i]});
        }
      }
      else
      {
        Swal('Oops!',response.message,'info');
      }
    })
  }

  getFitType(e)
  {
    this.fitType="fittype"
    var app=e.target.value.split("-");
    this.apparelName=app[1];
    this.fit=[];
    var data=this.apparelType.filter(item=>item.apparelName==e.target.value)[0].data
    for(var i in data)
      {
        this.fit.push({fitType:i,data:data[i]});
      }

      console.log(this.fit);
  }

  getMeasurement(e)
  {
    this.sizeType=[];
    var d=this.fit.filter(item=>item.fitType==e.target.value)[0].data.measurementpoints;
    for(var i in d)
    {
      this.sizeType.push({pointName:i,data:d[i]});
    }
  }

}
