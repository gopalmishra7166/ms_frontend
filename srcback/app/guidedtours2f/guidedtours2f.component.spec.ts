import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Guidedtours2fComponent } from './guidedtours2f.component';

describe('Guidedtours2fComponent', () => {
  let component: Guidedtours2fComponent;
  let fixture: ComponentFixture<Guidedtours2fComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Guidedtours2fComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Guidedtours2fComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
