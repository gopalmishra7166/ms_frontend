import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ActivatedRoute, Router } from '@angular/router';
import { PagerService } from '../services/pagerService';
const Swal = require('sweetalert2');
import {ToastrService} from 'ngx-toastr';
declare var $:any;

@Component({
  selector: 'app-userquicksize',
  templateUrl: './userquicksize.component.html',
  styleUrls: ['./userquicksize.component.css']
})
export class UserquicksizeComponent implements OnInit {

  showLoader=false;
  email: any;
  token: any;
  expired= false;
  usersData=[];
  searchKey="processId";
  searchText="";
  userCount=0;
  pager: any = {};
  pagedItems: any[];
  productName='';
  type="all";

  constructor(private router:Router,private service:MsServices,private toast: ToastrService,private rout:ActivatedRoute,private pagerService:PagerService) { }

  ngOnInit() 
  {
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Users";
    var param = this.rout.snapshot.paramMap.get("id");

    var d=JSON.parse(localStorage.getItem('data'));
    this.email=d['emailId'];
    this.token=d['token'];
    this.productName = 'QUICKSIZE'

    
    if (d['product'].QUICKSIZE.paymentStatus == 'INACTIVE') {
      if(d['product'].QUICKSIZE.remainingDays < 0)
      {
        this.expired = false;
      }
      else
      {
        this.expired = true;
      }
    }
    if(this.expired==false)
    {
      this.getUserListQuicksize(1);
    }
  }

  getUserListQuicksize(page)
  {
    this.showLoader=true;
    var obj={"emailId":this.email,"token":this.token,"productName":this.productName,"pageNo":page.toString()}
    this.service.getUserList(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        this.userCount=response.count
        this.usersData=response.data;
        this.pager = this.pagerService.getPager(this.userCount, page);
      }
      else
      {
        this.toast.warning(response.message, 'Message', {timeOut: 2000,}); 
      }
    })
  }

  setPage(page: number) 
  {
      this.pager = this.pagerService.getPager(this.userCount,page);
      this.pagedItems = this.usersData.slice(this.pager.startIndex, this.pager.endIndex + 1);
      this.getUserListQuicksize(page);
  }

  searchUser()
  {
    this.showLoader=true;
    this.userCount=0;
    this.usersData=[];
    var obj={"token":this.token,"emailId":this.email,"productName":'QUICKSIZE',"key":this.searchKey,"value":this.searchText};
    this.service.searchuserlistqs(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        this.usersData=response.data;
        this.userCount=parseInt(response.record_count)
        this.pager = this.pagerService.getPager(this.userCount, 1);
      }
      else
      {
        Swal("Error",response.message,'erro');
      }
    })
  }

  clear()
  {
    this.getUserListQuicksize(1);
  }


  gotoSubscription()
  { 
    localStorage.setItem('subscribeProduct','QUICKSIZE');
    this.router.navigate(['/subscribe'])
     
  }


  genderWise(e)
  {
    var data=this.usersData;
    if(e.target.value=='all')
    {
      this.getUserListQuicksize(1)
    }
    else
    {
      this.showLoader=true;
      this.userCount=0;
      this.usersData=[];
      var obj={"token":this.token,"emailId":this.email,"productName":'QUICKSIZE',"key":"gender","value":e.target.value};
      this.service.searchuserlistqs(obj).subscribe(response=>{
        this.showLoader=false;
        if(response.code=="1")
        {
          this.usersData=response.data;
          this.userCount=parseInt(response.record_count)
          this.pager = this.pagerService.getPager(this.userCount, 1);
        }
        else
        {
          Swal("Oops !",response.message,'error');
        }
      })
    }
  }
  

}
