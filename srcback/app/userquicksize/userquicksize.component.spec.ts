import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserquicksizeComponent } from './userquicksize.component';

describe('UserquicksizeComponent', () => {
  let component: UserquicksizeComponent;
  let fixture: ComponentFixture<UserquicksizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserquicksizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserquicksizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
