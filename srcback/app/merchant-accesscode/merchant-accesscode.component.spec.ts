import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantAccesscodeComponent } from './merchant-accesscode.component';

describe('MerchantAccesscodeComponent', () => {
  let component: MerchantAccesscodeComponent;
  let fixture: ComponentFixture<MerchantAccesscodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MerchantAccesscodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantAccesscodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
