import {postDataOfMerchantInfo} from './../model/postDataOfMerchantInfo';
import {MerchantAdress} from './../model/merchant-adress';
import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {MsServices} from './../services/ms-services';
import {CookieService} from 'ngx-cookie-service';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import {MerchantDetails} from './../model/merchantdetails';

const Swal = require('sweetalert2');

@Component({
    selector: 'app-merchant-accesscode',
    templateUrl: './merchant-accesscode.component.html',
    styleUrls: ['./merchant-accesscode.component.css']
})
export class MerchantAccesscodeComponent implements OnInit {
   
    now = new Date();
    accessCode: string = undefined;
    imageSrc: any = '/assets/img/logo-preview.jpg';
    new_logo: string;
    files: FileList;
    minNUmber: string;
    filestring: string;
    show = false;
    sh = true;
    merchantDetails: MerchantDetails;
    updateMerchantInfo: postDataOfMerchantInfo;
    updateMerchantAddress: MerchantAdress;
    getAcceessCodeDetail;
    uid: string;
    name: string;
    fileType;
    prdDiv = false;
    email: any;
    token: any;
    showLoader=false;
    expired=false;

    ngOnInit() 
    {
        var d=JSON.parse(localStorage.getItem('data'));
        this.email=d['emailId'];
        this.token=d['token'];
        this.name=d['companyName']
        this.accessCode=d['product'].GET_MEASURED.accessCode;
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="User Login Details";
        
        if (d['product'].GET_MEASURED.paymentStatus == 'INACTIVE') {
           
            if(d['product'].GET_MEASURED.remainingDays < 0)
            {
              this.expired = false;
            }
            else
            {
              this.expired =true;
            }
          }
          if(this.expired==false)
          {
            this.getAccessCodeHistory();
          }

        
    }

    constructor(private route: Router, private ser: MsServices, private cookieService: CookieService, public _sanitizer: DomSanitizer) 
    {
        this.merchantDetails = new MerchantDetails();
        this.updateMerchantInfo = new postDataOfMerchantInfo();
        this.updateMerchantAddress = new MerchantAdress();
        var d=JSON.parse(localStorage.getItem('data'));
        this.email=d['emailId'];
    }


    getAccessCodeHistory()
    {   
       var obj={emailId:this.email,token:this.token,productName:'GET_MEASURED'}
        this.ser.getAccessCodeHistory(obj).subscribe(response=>{
            if(response.code=="1")
            {
                this.getAcceessCodeDetail=response.data;
            }
            else{
                Swal('error',response.message,'error')
            }
        })
    }

    updateAccessCode() 
    {
            this.showLoader=true;
            var obj={"token":this.token,"accessCode":this.accessCode,"emailId":this.email,"productName":'GET_MEASURED',}
          
                Swal({
                    
                    title: 'Are you sure, want to update?',
                    type: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '',
                    cancelButtonColor: '',
                    confirmButtonText: 'Yes',
                    allowOutsideClick: false,
    
                }).then((result) => {
    
                    if (result.value) 
                    {
                        this.ser.updateAccessCode(obj).subscribe(res => 
                        {
                            this.showLoader=false;
                            if(res.code=="1")
                            {
                                this.accessCode = res.accessCode;
                                  this.ser.getAccessCodeHistory(obj).subscribe(response=>{
                               
                                  if(response.code=="1")
                                  {
                                      this.getAcceessCodeDetail=response.data;
                                  }
                                  else{
                                      Swal('error',response.message,'error')
                                  }
                              });
                            }
                            else
                            {
                                Swal('error',res.message,'error')  
                            }
                        });
                    }
                    else
                    {
                        this.showLoader=false;
                    }
                });
            }
        
}