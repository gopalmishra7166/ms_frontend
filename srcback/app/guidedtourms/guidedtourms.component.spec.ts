import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidedtourmsComponent } from './guidedtourms.component';

describe('GuidedtourmsComponent', () => {
  let component: GuidedtourmsComponent;
  let fixture: ComponentFixture<GuidedtourmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidedtourmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidedtourmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
