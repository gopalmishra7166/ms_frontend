import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
const Swal = require('sweetalert2');
declare var $: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {


  now = new Date();
  data: any;
  email: any;
  token: any;
  getpaymenthistorydata: any;
  getpaymenthistoryactive: any;
  showLoader: any;
  authorizetoken: any;
  getpaymenthistoryexpired: any;
  constructor(private service: MsServices, private ser: MsServices, private toastr: ToastrService, private spinner: NgxSpinnerService) {

  }

  ngOnInit() {
    var d = JSON.parse(localStorage.getItem('data'));
    this.email = d['emailId'];
    this.token = d['token'];
    this.getplanhistorydata();
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Payment";
  }

  getplanhistorydata() {
    this.showLoader = true;
    var obj = { emailId: this.email, token: this.token }
    this.ser.getpaymenthistorydata(obj).subscribe(response => {
      this.showLoader = false;
      if (response.code == "1") {
        this.getpaymenthistoryactive = response.data.activeHistory;
        this.getpaymenthistoryexpired = response.data.expiredHistory;
      }
      else {
        Swal('error', response.message, 'error')
      }
    })
  }

  pay(i) {
    var d = JSON.parse(localStorage.getItem('data'))
    var data =
    {
      "getHostedPaymentPageRequest": {
        "merchantAuthentication": {
          "name": "9863nDF2VGfG",
          "transactionKey": "4Vv4T8h8Z5vc9Wjn"
        },
        "transactionRequest": {
          "transactionType": "authCaptureTransaction",
          "amount": i.paidAmount,
          "customer": {
            "email": d['emailId']
          },
          "billTo": {
            "firstName": d['contactPerson'].cpName,
            "lastName": '',
            "company": d['companyProfile'].name,
            "address": d['companyProfile'].companyAddress,
            "city": d['companyProfile'].city,
            "state": d['companyProfile'].state,
            "zip": d['companyProfile'].pinCode,
            "country": d['companyProfile'].country,
          }
        },
        "hostedPaymentSettings": {
          "setting": [
            {
              "settingName": "hostedPaymentReturnOptions",
              "settingValue": "{\"showReceipt\": true, \"url\": \"https://services.mirrorsize.com/#/paymentconfirmation\", \"urlText\": \"Continue\", \"cancelUrl\": \"https://services.mirrorsize.com/#/graph\", \"cancelUrlText\": \"Cancel\"}"
            },
            {
              "settingName": "hostedPaymentButtonOptions",
              "settingValue": "{\"text\": \"Pay\"}"
            },
            {
              "settingName": "hostedPaymentStyleOptions",
              "settingValue": "{\"bgColor\": \"blue\"}"
            },
            {
              "settingName": "hostedPaymentPaymentOptions",
              "settingValue": "{\"cardCodeRequired\": true, \"showCreditCard\": true, \"showBankAccount\": true}"
            },
            {
              "settingName": "hostedPaymentSecurityOptions",
              "settingValue": "{\"captcha\": false}"
            },
            {
              "settingName": "hostedPaymentShippingAddressOptions",
              "settingValue": "{\"show\": false, \"required\": false}"
            },
            {
              "settingName": "hostedPaymentBillingAddressOptions",
              "settingValue": "{\"show\": true, \"required\": false}"
            },
            {
              "settingName": "hostedPaymentCustomerOptions",
              "settingValue": "{\"showEmail\": false, \"requiredEmail\": false, \"addPaymentProfile\": true}"
            },
            {
              "settingName": "hostedPaymentOrderOptions",
              "settingValue": "{\"show\": true, \"merchantName\": \"Mirrorsize US Inc\"}"
            },
            {
              "settingName": "hostedPaymentIFrameCommunicatorUrl",
              "settingValue": "{\"url\": \"http://services.mirrorsize.com\"}"
            }
          ]
        }
      }
    }
    this.showLoader = true;
    this.service.makePayment(data).subscribe(response => {
      this.showLoader = false;
      if (response.messages.resultCode == 'Ok') {
        this.authorizetoken = response.token;
        document.getElementById('headerShow').style.zIndex = '1';
        document.getElementById('outerMain').style.zIndex = "9999";
        document.getElementById('paymentmodal').style.background = 'rgba(0, 0, 0, 0.35)';
        $('#paymentmodal').modal('show');
        this.loadScript();
      }
      else {
        Swal('Oops!', response.messages.message.text, 'error');
      }
    })
    console.log("amount Due")
  }

  public loadScript() {
    var isFound = false;
    var scripts = document.getElementsByTagName("script")
    for (var i = 0; i < scripts.length; ++i) {
      if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("AcceptUI")) {
        isFound = true;
      }
    }
    if (!isFound) {
      var dynamicScripts = ["https://js.authorize.net/v3/AcceptUI.js"];
      for (var i = 0; i < dynamicScripts.length; i++) {
        let node = document.createElement('script');
        node.src = dynamicScripts[i];
        node.type = 'text/javascript';
        node.async = false;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
      }
    }
  }
}
