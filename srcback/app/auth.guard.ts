import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {MsServices} from './services/ms-services'
@Injectable()
export class AuthGuard implements CanActivate {


  constructor(private router : Router,public service:MsServices){}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if(this.service.isLoggedIn())
    {
      console.log("true");
      return true
    }
    else
    {
      console.log("false");
      return false;
    }
  }

  verifyLogin(url) : boolean{
    if(!this.isLoggedIn()){
      this.router.navigate(['/login']);
      return false;
    }
    else if(this.isLoggedIn()){
      return true;
    }
  }
  public isLoggedIn(): boolean{
    let status = false;
    if( localStorage.getItem('isLoggedIn') == "true"){
      status = true;
    }
    else{
      status = false;
    }
    return status;
  }
}
