import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
import {CookieService} from 'ngx-cookie-service';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { MerchantDetails } from './../model/merchantdetails';
import { MsServices } from '../services/ms-services';
const Swal = require('sweetalert2');


@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {
 
    now = new Date();

  merchantDetails: MerchantDetails;
  minNUmber: string;
  uid: string;
  name: string;
  constructor(private route: Router, private ser: MsServices, private cookieService: CookieService, public _sanitizer: DomSanitizer) {
    // -----------------------
    this.merchantDetails = new MerchantDetails();
    }

  ngOnInit() 
  {
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Privacy Policy";
  }
}
