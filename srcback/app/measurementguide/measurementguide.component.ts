import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-measurementguide',
  templateUrl: './measurementguide.component.html',
  styleUrls: ['./measurementguide.component.css']
})
export class MeasurementguideComponent implements OnInit {

  token;
  gender='male';
  data=[];
  showLoader=false;

  constructor(private service:MsServices) { }

  ngOnInit() 
  {
    var d=JSON.parse(localStorage.getItem('data'));
    this.token=d['token'];
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Measurement Guide";
    this.getMeasurementGuide();
  }

  getMeasurementGuide()
  {
    this.showLoader=true;
    var obj={gender:this.gender,token:this.token}
    this.service.getMesurementGuide(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
       this.data=response.data;
      }
      else
      Swal('Oops !',response.message,'error')
    })
  }

  getGraph(val)
  {
    this.gender=val;
    this.getMeasurementGuide();
  }


}
