import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MysizelistingComponent } from './mysizelisting.component';

describe('MysizelistingComponent', () => {
  let component: MysizelistingComponent;
  let fixture: ComponentFixture<MysizelistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MysizelistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MysizelistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
