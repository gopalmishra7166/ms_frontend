import {Url} from 'url';
export class URL_api
{
    // public static host = 'http://localhost:8090';
      public static host = 'http://192.168.0.143:8090'; // tenda
    // public static host = 'http://192.168.1.7:8090'; // mirrorsizes
    // public static host = 'http://192.168.1.21:8080';
    // public static host = 'https://api.services.mirrorsize.com';
    // public static host = 'http://localhost:8090';

    // ------for admin disabling the users--20/02/2019---//
    public static merchantDisable = URL_api.host + '/api/UserManagementController/delete';
    // ---For total number of clicks and failed click as on18/02/2019-----//
    public static totalClicksForGraph = URL_api.host + '/api/GraphController/getNUmberOfClick';
    // ---For total number of clicks and failed click as on18/02/2019-----//
    public static onlineApiKeyGeneration = URL_api.host + '/api/merchantController/getnerateApiKey';

    // ---For total number of clicks and failed click as on18/02/2019-----//
    public static gettingAllMerchant = URL_api.host + '/api/UserManagementController/getAll';
    // ------after filling clear the whole page value in datatbase-------
    public static blankApiForData = URL_api.host + '/api/brandReadyToMeasureController/getsimplejsonRemove/';

    // -------billing pdf as on 13/feb/2019-------//
    public static billingPdf = URL_api.host + '/api/pdfController/billingPdf';  
    // for graph clicks
    public static graphDataForClicks = URL_api.host + '/api/GraphController/getClickGraph';

    // for merchant info
    public static merchantInfo = URL_api.host + '/api/merchant/info/';
    // for graph
    public static graphData = URL_api.host + '/api/GraphController/getgraph';

    // ----only for recommendation on size to fit-------//
    public static size2FitRecommendaion = URL_api.host + '/api/RecommendationController/getRecommendationPagination';
    // for logout the user
    public static logout = URL_api.host + '/api/merchant/logout';

    // -----for all kind of store and online measurement of size 2 fit and get measured-------//
    public static allKindOfData = URL_api.host + '/api/widgetControlle/getdynamictype/?mid=';
    // --------all kind of data ended-----//
    // for updating sizes on table for rendering
    public static updateSizesSize2Fit = URL_api.host + '/api/MMPIController/updateSize';

    // for  table generation apparel detail
    public static apparelTable = URL_api.host + '/api/MerchantSpecificSize2FitController/getSize2fit';
    public static storeSizeToFit = URL_api.host + '/api/widgetControlle/getmyCustomer/';
    // check duration
    public static freeTrial = URL_api.host + '/api/merchantCtrl/checkduration?merchantid=';

    // navigation response api
    public static navigation = URL_api.host + '/api/merchantCtrl/getmerchantType?merchantid=';
    public static apparelDetailSave = URL_api.host + '/api/MerchantSpecificSize2FitController/s2fdata';
    public static getSize = URL_api.host + '/api/merchantCommonController/token/getbrandsize?type=';
    public static sizeUpdate = URL_api.host + '/api/merchantCommonController/jwheiury8o73/save';
    public static apparelCompleteDeatil = URL_api.host + '/api/brandReadyToMeasureController/getdata/';
    public static apparelDetail = URL_api.host + '/api/brandReadyToMeasureController/getsimplejson/';
    public static sizeChart = URL_api.host + '/api/brandReadyToMeasureController/merchantsize/';


    // testing with local server
    public static recommendation = URL_api.host + '/api/merchantController/myproduct';
    public static deleteMerchant = URL_api.host + '/api/data/removeapparel/';
    public static storeUserWithOneZero = URL_api.host + '/api/storeOnlineuserPagenation/';
    public static getMerchantDetails = URL_api.host + '/api/merchant/';
    public static resetPassword = URL_api.host + '/api/reset/merchant/';
    public static getCspDetails = URL_api.host + '/api/csp/csp@mirrorsize.com/merchant';

    // Downloading pdf on click
    public static downloadFile = URL_api.host + '/api/measurementController/getpdf/?';
    // getMeasured API
    public static getMissingInfo = URL_api.host + '/api/merchant/getMissingInfo';

    public static loginUser = URL_api.host + '/api/merchant/login/';
    public static signupUser = URL_api.host + '/api/merchant/signup/';
    public static apiDetails = URL_api.host + '/api/merchant/get-measured/generate/';
    public static updatMerchant = URL_api.host + '/api/merchant/update/';
    public static productMapping = URL_api.host + '/api/data/addapparel/';
    public static contactUs = URL_api.host + '/api/contactus/save/';
    public static getMeasured = URL_api.host + '/api/merchant/getmeasured/mapped/';

    // for data division for size to fit and get measured
    public static divisionSize = URL_api.host + '/mmpicontroller/getbyTypePagination/';
    public static getSizeToFitDetail = URL_api.host + '/api/MMPIController/withsize/';
    public static getMeasuredUpdate = URL_api.host + '/api/merchant/getmeasured/mapped/update/';
// -----for  getting measuremenet point accourding  to gender-----//
    public static allMeasurementPoints = URL_api.host + '/api/merchantCtrl/pointsNamewithImage';
// ------ for getting edit tolerence & points --------//
    public static editAllTolerence = URL_api.host + '/api/MerchantSpecificSize2FitController/getJsonForEdit';
// ------posting edited tolerences  and other value---------//
    public static postUpdateAllTolerence = URL_api.host + '/api/MerchantSpecificSize2FitController/updatesizeChart';

// -----for  displaying filtered data in getmeasured store--------//
    public static gmMeasurementPointsWithBrand = URL_api.host + '/api/merchantCtrl/gmpointsWithBrandPagination'
// --------for pdf accourding to specific date-----------//
    public static pdfAccourdingToDate = URL_api.host + '/api/pdfController/makeMyuserPdf';

    // ---for download specific pdf store gm------//

    public static pdfStoreGMDownload = URL_api.host + '/api/pdfController/limitedpointPdf';

// -------download complete table for specific s2f sizechart measurement points with brand---------//
    public static pdfS2fCompletlyDownload = URL_api.host + '/api/pdfController/size2fitpdf';

// -------download complete table for specific getmeasured mapped apparel measurement points with brand---------//
    public static pdfGmCompletlyDownload = URL_api.host + '/api/pdfController/get_measured';
// ------notificationForFreeTrialAndBuyNow-------------
    public static notificationForFreeTrialAndBuyNow = URL_api.host + '/api/notificationController/save';

    // ------------purchase save -----------//
    public static purchaseYear = URL_api.host + '/api/merchantController/savePayment';

    // -------For checking size to fit product map or not---------//
    public static checkForProductMapSize2Fit = URL_api.host + '/api/MMPIController/checkAvailability';
    // --------------For removing s2f sizes 7/2/2019---------//
    public static removeSizeSize2Fit = URL_api.host + '/api/MerchantSpecificSize2FitController/deleteSizes';
}