import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsermysizeComponent } from './usermysize.component';

describe('UsermysizeComponent', () => {
  let component: UsermysizeComponent;
  let fixture: ComponentFixture<UsermysizeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsermysizeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsermysizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
