import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MsServices } from './../services/ms-services';
import { AuthroizeNet} from '../services/authorizeNet.service'
import { Router } from '@angular/router';
import { window } from 'rxjs/operators';
declare var AuthorizeNetIFrame:any
const Swal = require('sweetalert2');
declare var $: any;

@Component({
    selector: 'app-subscription',
    templateUrl: './subscription.component.html',
    styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {

    companyAgreementForm: FormGroup;
    oneYear = true;
    twoYear = false;
    threeYear = false;
    year=1;
    authorizetoken;
    showLoader = false;
    pdfSrc = '';
    plan;
    planChecked=false;
    ammount;
    account = false;
    email: any;
    token: any;
    name: any;
    mid: any;
    d: any;
    subproductName: any;
    planType: any;
    newProductName: any;
    cpName: any;
    city: any;
    state: any;
    pinCode: any;
    country: any;
    productName: string;
    regDate: string;
    cpMobileNo: any;
    cpEmailId: any;
    cpDesignation: any;
    companyAddress: any;
    ftrial=false;
    SIZE2FIT=false;
    GET_MEASURED=false;
    paid=false;
    monthly: boolean;
    yearly: boolean;
    billPeriod: string;
    MYSIZE=false;
    QUICKSIZE=false;
    showPaymentIframe = false;
    getPayment = false;
    emailId:any;

    constructor(public service: MsServices, public router: Router,private authorizeNetScriptLoader: AuthroizeNet) {
        this.companyAgreementForm = new FormGroup({
            ftrial:new FormControl(''),
            oneYear: new FormControl(''),
            twoYear: new FormControl(''),
            threeYear: new FormControl('')
        })
    }

    ngOnInit() 
    {
        this.planType = 'PAID';
        this.getcommondata()
        var d = JSON.parse(localStorage.getItem('data'))
        this.emailId=d['emailId'];
        this.cpName= d['contactPerson'].cpName;
        this.cpMobileNo=d['contactPerson'].cpMobileNo;
        this.cpEmailId=d['contactPerson'].cpEmailId;
        this.cpDesignation=d['contactPerson'].cpDesignation;
        this.name= d['companyProfile'].name;
        this.companyAddress= d['companyProfile'].companyAddress;
        this.city= d['companyProfile'].city;
        this.state= d['companyProfile'].state;
        this.pinCode= d['companyProfile'].pinCode;
        this.country= d['companyProfile'].country;

        this.productName = localStorage.getItem('subscribeProduct');
        // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Subscribe "+this.productName;

        if (this.productName == 'GET_MEASURED') {
            this.GET_MEASURED = true;
        }
        else if(this.productName == 'SIZE2FIT'){
            this.SIZE2FIT = true;
        }
        else if(this.productName == 'MYSIZE'){
            this.MYSIZE = true;
        }
        else if(this.productName == 'QUICKSIZE'){
            this.QUICKSIZE = true;
        }

        console.log("hehe", this.MYSIZE, this.paid )

        if (this.planType === 'PAID') {
            this.paid = true;
            this.ftrial = false;
        } else {
            this.ammount = 1;
            this.plan = 'FREETRIAL';
            this.ftrial = true;
        }
        if(!this.MYSIZE) {
            this.ammount = 1
            this.year = 1
            this.monthly=false;
            this.yearly=true;
        }

        if(localStorage.getItem('size'))
        {
            this.MYSIZE=true;
            this.QUICKSIZE=false;
        }

        this.loadScripts();

    }

    ngAfterViewInit() 
    {
       var can=document.getElementById('cancelBtn');
       can.onclick = function() 
       {
        console.log("cancle button clicked");
        };
        window['CommunicationHandler'] = {};
        window['CommunicationHandler'].onReceiveCommunication = (argument) => {
            var qrstr = argument.substring(argument.indexOf('#') + 1, argument.length);
            console.log("iFrame argument: " + JSON.stringify(argument));
            // Get the parameters from the argument.
            var params = this.parseQueryString(qrstr);
            console.log(params);
            switch (params['action']) {
                case "resizeWindow":
                    //We can use this to resize the window, if needed. Just log it for now.
                    var w = parseInt(params["width"]);
                    var h = parseInt(params["height"]);
                    var ifrm = document.getElementById("add_payment");
                    ifrm.style.width = w.toString() + "px";
                    ifrm.style.height = h.toString() + "px";

                    let elements = document.getElementsByClassName('textLabel');
                    for (let i = 0; i < elements.length; i++) {
                        let element = elements[i];
                        element['style'].fontSize = '18pt';
                        element['style'].fontFamily = 'san-serif';

                    }
                    // {"qstr":"action=resizeWindow&width=551&height=825","parent":"https://test.authorize.net/customer/addPayment"}
                    break;

                case "successfulSave":
                    // {"qstr":"action=successfulSave","parent":"https://test.authorize.net/customer/addPayment"}
                    console.log(`[successfulSave]`, params)
                    const reqObj = {
                        emailId: this.email,
                        productName: this.productName,
                        paymentStatus: 'ACTIVE',
                    }
                    
                    this.service.updatePaymentStatus(reqObj).subscribe(response => {
                        console.log(`[response]`, response);
                        if (response.code == 1) {
                            this.router.navigate(['/login']);
                        } else {
                            alert('Unable to update your payment as ACTIVE. Please contact the support team.');
                            this.router.navigate(['/login']);
                        }
                    });
                    this.router.navigate(['/login']);
                    break;
                case "cancel":
                    this.showPaymentIframe=false;
                    // {"qstr":"action=cancel","parent":"https://test.authorize.net/customer/addPayment"}
                    console.log(`[reqObj]`, reqObj);
                    console.log("cancle payment");
                    this.showPaymentIframe=false;
                    this.router.navigate(['/home']);
                    break;
                case "transactResponse":
                    var ifrm = document.getElementById("add_payment");
                    ifrm.style.display = 'none';
                    console.log(`[transact response]`, params);
                    this.router.navigate(['/login']);
                    break;
                default:
                    console.log('Unknown action from iframe. Action: ' + params['action'])
                //     break;
            }
        }

    }

    parseQueryString(str) {
        var vars = [];
        console.log(str);
        var arr = str.split('&');
        var pair;
        for (var i = 0; i < arr.length; i++) {
            pair = arr[i].split('=');
            //vars[pair[0]] = unescape(pair[1]);
            vars[pair[0]] = pair[1];
        }

        console.log(vars);
        return vars;
    }


    ngDoCheck()
    {
        this.productName = localStorage.getItem('subscribeProduct');
        if (this.productName == 'GET_MEASURED') {
            this.GET_MEASURED = true;
            // this.oneYear=true;
            this.SIZE2FIT = false;
            this.MYSIZE = false;
            this.QUICKSIZE = false;
         
        }
        else if(this.productName == 'SIZE2FIT'){
            this.SIZE2FIT = true;
            // this.oneYear=true;
            this.GET_MEASURED =false;
            this.QUICKSIZE = false;
            this.MYSIZE = false;
        }
        else if(this.productName == 'MYSIZE'){
            this.MYSIZE = true;
            // this.oneYear=true;
            this.GET_MEASURED =false;
            this.QUICKSIZE = false;
            this.SIZE2FIT = false;
        }
        else if(this.productName == 'QUICKSIZE'){
            this.QUICKSIZE = true;
            // this.oneYear=true;
            this.MYSIZE = false;
            this.GET_MEASURED =false;
            this.SIZE2FIT = false;
        }

    }

    getcommondata(){
        var today = new Date();
        var dd: any = today.getDate();
        var mm: any = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        this.regDate = dd + '/' + mm + '/' + yyyy;
    }

    changeBillingPeriod(val)
    {
        if(this.QUICKSIZE){
            this.monthly=true;
            this.yearly=false;
            if(this.oneYear)
            {
                if(this.plan =='small'){
                    this.ammount = 1000;
                }
                else if(this.plan =='medium'){
                    this.ammount = 2000;
                }
            }
            else if(this.twoYear)
            {
                if(this.plan =='small'){
                    this.ammount = 800;
                }
                else if(this.plan =='medium'){
                    this.ammount = 1625;
                }
            }
            else if(this.threeYear)
            {
                if(this.plan =='small'){
                    this.ammount = 600;
                }
                else if(this.plan =='medium'){
                    this.ammount = 1250;
                }
            }
        }
        else{
            if(val=='m'){
                this.monthly=true;
                this.yearly=false;
                if(this.oneYear)
                {
                    if(this.plan =='small'){
                        this.ammount = 119;
                    }
                    else if(this.plan =='medium'){
                        this.ammount = 536;
                    }
                }
                else if(this.twoYear)
                {
                    if(this.plan =='small'){
                        this.ammount = 208;
                    }
                    else if(this.plan =='medium'){
                        this.ammount = 975;
                    }
                }
                else if(this.threeYear)
                {
                    if(this.plan =='small'){
                        this.ammount = 785;
                    }
                    else if(this.plan =='medium'){
                        this.ammount = 2800;
                    }
                }
            }
            else if(val=='y'){
                this.monthly=false;
                this.yearly=true;
                if(this.oneYear)
                {
                    if(this.plan =='small'){
                        this.ammount = 1200;
                    }
                    else if(this.plan =='medium'){
                        this.ammount = 5400;
                    }
                }
                else if(this.twoYear)
                {
                    if(this.plan =='small'){
                        this.ammount = 1920;
                    }
                    else if(this.plan =='medium'){
                        this.ammount = 9000;
                    }
                }
                else if(this.threeYear)
                {
                    if(this.plan =='small'){
                        this.ammount = 6500;
                    }
                    else if(this.plan =='medium'){
                        this.ammount = 24000;
                    }
                }
            }
        }
    }
        
    freetrial(){
        this.planChecked=false;
        this.planType = 'FREETRIAL';
        this.plan = 'FREETRIAL'
        this.year = 1
        this.ftrial=true;
        this.paid=false;
        this.oneYear = false;
        this.twoYear = false;
        this.threeYear = false;
        this.companyAgreementForm.patchValue({ oneYear: false });
        this.companyAgreementForm.patchValue({ twoYear: false });
        this.companyAgreementForm.patchValue({ threeYear: false });
    }

    buyForOneYear() {
        if(this.monthly){
            this.changeBillingPeriod('m');
        }
        else{
            this.changeBillingPeriod('y');
        }
        this.planChecked=false;
        this.planType = 'PAID';
        this.paid=true;
        this.year = 1
        this.oneYear = true;
        this.twoYear = false;
        this.threeYear = false;
        this.ftrial=false;
        this.companyAgreementForm.patchValue({ ftrial: false });
        this.companyAgreementForm.patchValue({ twoYear: false });
        this.companyAgreementForm.patchValue({ threeYear: false });
    }

    buyForTwoYear() {
        if(this.monthly){
            this.changeBillingPeriod('m');
        }
        else{
            this.changeBillingPeriod('y');
        }
        this.planChecked=false;
        this.year = 2
        this.planType = 'PAID';
        this.paid=true;
        this.twoYear = true;
        this.oneYear = false;
        this.threeYear = false;
        this.ftrial=false;
        this.companyAgreementForm.patchValue({ ftrial: false });
        this.companyAgreementForm.patchValue({ oneYear: false });
        this.companyAgreementForm.patchValue({ threeYear: false });
    }

    buyForThreeYear() {
        if(this.monthly){
            this.changeBillingPeriod('m');
        }
        else{
            this.changeBillingPeriod('y');
        }
        this.planChecked=false;
        this.year = 3
        this.planType = 'PAID';
        this.paid=true;
        this.oneYear = false;
        this.twoYear = false;
        this.threeYear = true;
        this.ftrial=false;
        this.companyAgreementForm.patchValue({ ftrial: false });
        this.companyAgreementForm.patchValue({ oneYear: false });
        this.companyAgreementForm.patchValue({ twoYear: false });
    }

    buyProduct(val, am) {
        this.planChecked=true;
        this.plan = val;
        console.log(this.ftrial)
        if(this.monthly){
            this.changeBillingPeriod('m');
        }
        else{
            this.changeBillingPeriod('y');
        }
        
        
    }

    select(val) {
        if (val == 'one') {
            document.getElementById('one').style.backgroundColor = 'transparent';
            document.getElementById('one').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('one').style.color = '#fff';
            document.getElementById('one').style.zIndex = '9999';
            document.getElementById('one').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed1').style.color = '#fff';

            document.getElementById('two').style.backgroundColor = '#fff';
            document.getElementById('two').style.backgroundImage = 'none';
            document.getElementById('two').style.color = '#333';
            document.getElementById('two').style.zIndex = '0';
            document.getElementById('two').style.boxShadow = 'none';
            document.getElementById('hed2').style.color = '#333';

        }
        else if (val == 'two') {
            document.getElementById('one').style.backgroundColor = '#fff';
            document.getElementById('one').style.backgroundImage = 'none';
            document.getElementById('one').style.color = '#333';
            document.getElementById('one').style.zIndex = '0';
            document.getElementById('one').style.boxShadow = 'none';
            document.getElementById('hed1').style.color = '#333';

            document.getElementById('two').style.backgroundColor = 'transparent';
            document.getElementById('two').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('two').style.color = '#fff';
            document.getElementById('two').style.zIndex = '9999';
            document.getElementById('two').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed2').style.color = '#fff';

        }
        else if (val == 'three') {
            document.getElementById('three').style.backgroundColor = 'transparent';
            document.getElementById('three').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('three').style.color = '#fff';
            document.getElementById('three').style.zIndex = '9999';
            document.getElementById('three').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed3').style.color = '#fff';


            document.getElementById('four').style.backgroundColor = '#fff';
            document.getElementById('four').style.backgroundImage = 'none';
            document.getElementById('four').style.color = '#333';
            document.getElementById('four').style.zIndex = '0';
            document.getElementById('four').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed4').style.color = '#333';

        }
        else if (val == 'four') {
            document.getElementById('four').style.backgroundColor = 'transparent';
            document.getElementById('four').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('four').style.color = '#fff';
            document.getElementById('four').style.zIndex = '9999';
            document.getElementById('four').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed4').style.color = '#fff';

            document.getElementById('three').style.backgroundColor = '#fff';
            document.getElementById('three').style.backgroundImage = 'none';
            document.getElementById('three').style.color = '#333';
            document.getElementById('three').style.zIndex = '0';
            document.getElementById('three').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed3').style.color = '#333';
        }
        else if (val == 'five') {
            document.getElementById('five').style.backgroundColor = 'transparent';
            document.getElementById('five').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('five').style.color = '#fff';
            document.getElementById('five').style.zIndex = '9999';
            document.getElementById('five').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed5').style.color = '#fff';

            document.getElementById('six').style.backgroundColor = '#fff';
            document.getElementById('six').style.backgroundImage = 'none';
            document.getElementById('six').style.color = '#333';
            document.getElementById('six').style.zIndex = '0';
            document.getElementById('six').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed6').style.color = '#333';

        }
        else if (val == 'six') {
            document.getElementById('six').style.backgroundColor = 'transparent';
            document.getElementById('six').style.backgroundImage = 'linear-gradient(45deg, #6e0270 0%, #4054b2 100%)';
            document.getElementById('six').style.color = '#fff';
            document.getElementById('six').style.zIndex = '9999';
            document.getElementById('six').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed6').style.color = '#fff';


            document.getElementById('five').style.backgroundColor = '#fff';
            document.getElementById('five').style.backgroundImage = 'none';
            document.getElementById('five').style.color = '#333';
            document.getElementById('five').style.zIndex = '0';
            document.getElementById('five').style.boxShadow = '10px 10px 10px 10px #f8f9fa';
            document.getElementById('hed5').style.color = '#333';

        }
    }

    addClass() {
        document.getElementById('headerShow').style.zIndex = '1';
        document.getElementById('outerMain').style.zIndex = "9999";
        document.getElementById('agreementModal').style.background = 'rgba(0, 0, 0, 0.35)';
        document.getElementById('agreementModalFT').style.background = 'rgba(0, 0, 0, 0.35)';
        // document.getElementById('paymentmodal').style.background = 'rgba(0, 0, 0, 0.35)';
    }

    closeModal() {
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
    }

    public loadScript() {
        var isFound = false;
        var scripts = document.getElementsByTagName("script")
        for (var i = 0; i < scripts.length; ++i) {
            if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("AcceptUI")) {
                isFound = true;
            }
        }

        if (!isFound) {
            var dynamicScripts = ["https://js.authorize.net/v3/AcceptUI.js"];

            for (var i = 0; i < dynamicScripts.length; i++) {
                let node = document.createElement('script');
                node.src = dynamicScripts[i];
                node.type = 'text/javascript';
                node.async = false;
                node.charset = 'utf-8';
                document.getElementsByTagName('head')[0].appendChild(node);
            }
        }
    }



    private loadScripts() {
        let ANS_customer_id = "d1896866-4687-448a-877a-4afe81585f55";
        this.authorizeNetScriptLoader.load('sealJs').then(data => {
            console.log('seal.js script loaded successfully');
        }).catch(error => {
            console.error(error)
        })
    }


    makePayment() {
        var d = JSON.parse(localStorage.getItem('data'));
        
        if(this.monthly){
            this.billPeriod = 'monthly';
        }
        else if (this.yearly){
            this.billPeriod = 'annual';
        }
 
        var data =
        {
            planType: this.planType,
            productName: this.productName,
            planPeriod: this.year.toString(),
            plan: this.plan,
            paidAmount: this.ammount,
            billPeriod:this.billPeriod
        }
        var obj = { companyProfile: d['companyProfile'], contactPerson: d['contactPerson'], planDetail: data, emailId: d['emailId'], accountId: d['accountId'],logo: d['companyLogo'], profilePic: d['contactPerson'].cpProfilePic, registrationType:"exist"}

        this.service.merchantRegistration(obj).subscribe(response => {
            if (response.code == 1) 
            {

                this.service.saveAgreement({emailId:this.emailId,productName:this.productName}).subscribe(response=>{
                    if(response.code=="1")
                    {
                        console.log('agrement saved');
                    }
                    else
                    {
                        console.log('agrement not saved'+response.message)
                    }
                })


                d['product'].GET_MEASURED=response.data.GET_MEASURED;
                d['product'].SIZE2FIT=response.data.SIZE2FIT;
                d['product'].QUICKSIZE=response.data.QUICKSIZE;
                localStorage.setItem('data',JSON.stringify(d));


                var data =
                {
                    "getHostedPaymentPageRequest": {
                        "merchantAuthentication": {
                            "name": "9863nDF2VGfG",
                            "transactionKey": "4Vv4T8h8Z5vc9Wjn"
                        },
                        "transactionRequest": {
                            "transactionType": "authCaptureTransaction",
                            "amount": this.ammount,
                            "customer": {
                                "email": d['emailId']
                            },
                            "billTo": {
                                "firstName": d['contactPerson'].cpName,
                                "lastName": '',
                                "company": d['companyProfile'].name,
                                "address": d['companyProfile'].companyAddress,
                                "city": d['companyProfile'].city,
                                "state": d['companyProfile'].state,
                                "zip": d['companyProfile'].pinCode,
                                "country": d['companyProfile'].country,
                            }
                        },
                        "hostedPaymentSettings": {
                            "setting": [
                                {
                                    "settingName": "hostedPaymentReturnOptions",
                                    "settingValue": "{\"showReceipt\": true, \"url\": \"https://services.mirrorsize.com/#/paymentconfirmation\", \"urlText\": \"Continue\", \"cancelUrl\": \"https://services.mirrorsize.com/#/signup\", \"cancelUrlText\": \"Cancel\"}"
                                },
                                {
                                    "settingName": "hostedPaymentButtonOptions",
                                    "settingValue": "{\"text\": \"Pay\"}"
                                },
                                {
                                    "settingName": "hostedPaymentStyleOptions",
                                    "settingValue": "{\"bgColor\": \"blue\"}"
                                },
                                {
                                    "settingName": "hostedPaymentPaymentOptions",
                                    "settingValue": "{\"cardCodeRequired\": true, \"showCreditCard\": true, \"showBankAccount\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentSecurityOptions",
                                    "settingValue": "{\"captcha\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentShippingAddressOptions",
                                    "settingValue": "{\"show\": false, \"required\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentBillingAddressOptions",
                                    "settingValue": "{\"show\": true, \"required\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentCustomerOptions",
                                    "settingValue": "{\"showEmail\": false, \"requiredEmail\": false, \"addPaymentProfile\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentOrderOptions",
                                    "settingValue": "{\"show\": true, \"merchantName\": \"Mirrorsize US Inc\"}"
                                },
                                {
                                    // "settingName": "hostedPaymentIFrameCommunicatorUrl",
                                    // // "settingValue": "{\"url\": \"https://services.mirrorsize.com/#/graph\"}"
                                    // "settingValue": `{"url": "https://${location.host}/#/graph"}`
                                }
                            ]
                        }
                    }
                }
                this.showLoader = true;
                this.service.makePayment(data).subscribe(response => {
                    this.showLoader = false;
                    if (response.messages.resultCode == 'Ok') {
                        this.authorizetoken = response.token;
                        // $('#paymentModal').show();
                          this.getPayment = true;
                            this.showPaymentIframe = true;
                            // this.loadScript();
                    }
                    else {
                        Swal('Oops!', response.messages.message.text, 'error');
                    }
                })
            }
            else {
                Swal("Oops!", response.message, 'error')
            }
        })
    }

    makePaymentMysize() 
    {
        var d = JSON.parse(localStorage.getItem('data'));
        this.showLoader=true;
        this.billPeriod = 'monthly';
        this.ammount =this.ammount;
        this.plan = 'small';
        this.planType='PAID'

        var data =
        {
            planType: this.planType,
            productName: this.productName,
            planPeriod: "1",
            plan: this.plan,
            paidAmount: this.ammount,
            billPeriod:this.billPeriod
        }

        var obj = { companyProfile: d['companyProfile'], contactPerson: d['contactPerson'], planDetail: data, emailId: d['emailId'], accountId: d['accountId'],logo: d['companyLogo'], profilePic: d['contactPerson'].cpProfilePic ,registrationType:"exist"}
        this.service.merchantRegistration(obj).subscribe(response => {
            this.showLoader=false;

            if (response.code == 1)             
            {
                this.service.saveAgreement({emailId:this.emailId,productName:this.productName}).subscribe(response=>{
                    if(response.code=="1")
                    {
                        console.log('agrement saved');
                    }
                    else
                    {
                        console.log('agrement not saved'+response.message)
                    }
                })

                d['product'].MYSIZE=response.data.MYSIZE;
                localStorage.setItem('data',JSON.stringify(d));

                var data =
                {
                    "getHostedPaymentPageRequest": {
                        "merchantAuthentication": {
                            "name": "9863nDF2VGfG",
                            "transactionKey": "4Vv4T8h8Z5vc9Wjn"
                        },
                        "transactionRequest": {
                            "transactionType": "authCaptureTransaction",
                            "amount": this.ammount,
                            "customer": {
                                "email": d['emailId']
                            },
                            "billTo": {
                                "firstName": d['contactPerson'].cpName,
                                "lastName": '',
                                "company": d['companyProfile'].name,
                                "address": d['companyProfile'].companyAddress,
                                "city": d['companyProfile'].city,
                                "state": d['companyProfile'].state,
                                "zip": d['companyProfile'].pinCode,
                                "country": d['companyProfile'].country,
                            }
                        },
                        "hostedPaymentSettings": {
                            "setting": [
                                {
                                    "settingName": "hostedPaymentReturnOptions",
                                    "settingValue": "{\"showReceipt\": true, \"url\": \"https://services.mirrorsize.com/#/paymentconfirmation\", \"urlText\": \"Continue\", \"cancelUrl\": \"https://services.mirrorsize.com/#/signup\", \"cancelUrlText\": \"Cancel\"}"
                                },
                                {
                                    "settingName": "hostedPaymentButtonOptions",
                                    "settingValue": "{\"text\": \"Pay\"}"
                                },
                                {
                                    "settingName": "hostedPaymentStyleOptions",
                                    "settingValue": "{\"bgColor\": \"blue\"}"
                                },
                                {
                                    "settingName": "hostedPaymentPaymentOptions",
                                    "settingValue": "{\"cardCodeRequired\": true, \"showCreditCard\": true, \"showBankAccount\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentSecurityOptions",
                                    "settingValue": "{\"captcha\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentShippingAddressOptions",
                                    "settingValue": "{\"show\": false, \"required\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentBillingAddressOptions",
                                    "settingValue": "{\"show\": true, \"required\": false}"
                                },
                                {
                                    "settingName": "hostedPaymentCustomerOptions",
                                    "settingValue": "{\"showEmail\": false, \"requiredEmail\": false, \"addPaymentProfile\": true}"
                                },
                                {
                                    "settingName": "hostedPaymentOrderOptions",
                                    "settingValue": "{\"show\": true, \"merchantName\": \"Mirrorsize US Inc\"}"
                                },
                                {
                                    // "settingName": "hostedPaymentIFrameCommunicatorUrl",
                                    // // "settingValue": "{\"url\": \"https://services.mirrorsize.com/#/graph\"}"
                                    // "settingValue": `{"url": "https://${location.host}/#/graph"}`
                                }
                            ]
                        }
                    }
                }
                
                this.service.makePayment(data).subscribe(response => {
                    this.showLoader = false;
                    if (response.messages.resultCode == 'Ok') {
                        this.authorizetoken = response.token;
                          this.getPayment = true;
                            this.showPaymentIframe = true;
                            // this.loadScript();
                    }
                    else {
                        Swal('Oops!', response.messages.message.text, 'error');
                    }
                })
            }
            else {
                Swal("Oops!", response.message, 'error')
            }
        })
    }

    acceptTerms() {
        $('#agreementModal').modal('hide');
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
        this.makePayment();
    }

    acceptTermsFt() {
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
        $('#agreementModalFT').modal('hide');
        this.makePayment();
    }

    acceptTermsMysize() {
        $('#agreementModalMysize').modal('hide');
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
        this.makePaymentMysize();
    }

    acceptTermsQuicksize() {
        $('#agreementModalQuicksize').modal('hide');
        document.getElementById('headerShow').style.zIndex = '99999';
        document.getElementById('outerMain').style.zIndex = "0";
        this.makePayment();
    }
}


