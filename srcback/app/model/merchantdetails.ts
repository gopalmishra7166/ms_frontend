export class MerchantDetails {
  name: string;
  logo = 'NA';
  minNUmber: string;
  email: string;
  countryCode: number;
  mobileNumber: string;
  street:   string;
  city: string;
  state: string;
  country: string;
  pincode: string;
  areaCode = 0;
  industriesType: string;

}
