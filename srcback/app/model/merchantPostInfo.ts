import { AddressInfoV2 } from './AddressInfoV2';

export class MerchantPostInfo {
  companyName: string;
  logo: string;
  industriesType: string;
  landlineNumber: number;
  addressInfo: AddressInfoV2;
  email_id: string;
  contactPersonName: string;
  mobile_num: string;
  designationOfContactPerson: string;
  emailOfContactPerson: string;
  altEmailOfContactPerson: string;
  altMobileOfContactPerson: string;
  profilePic: string;
  landlineOfContactPerson: number;
}
