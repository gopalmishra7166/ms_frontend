import { Component, OnInit } from '@angular/core';
import { MsServices } from '../services/ms-services';
import { PagerService } from '../services/pagerService';
const Swal = require('sweetalert2');
declare var $:any;
@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit {

  subject='';
  content='';
  email='';
  token='';
  tid;
  ticketsData=[];
  status='PENDING'
  showLoader=false;
  ticketCount=0;
  pager: any = {};
  pagedItems: any[];

  constructor(private service:MsServices,private pagerService:PagerService) { }

  ngOnInit() 
  {

    var d=JSON.parse(localStorage.getItem('data'));
    this.email=d['emailId'];
    this.token=d['token'];
    this.getTickets(1);
    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML="Support";
  
  }


  createTicket()
  {
    this.showLoader=true;
    var obj={emailId:this.email,token:this.token,content:this.content,subject:this.subject}
    this.service.raiseTicket(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        Swal('success',response.message,'success');
        this.getTickets(1);
        document.getElementById('createTicket').style.display='none';
      
      }
      else
      {
        Swal('error',response.message,'error');
      }
    })
  }

  getTickets(page)
  {
    this.showLoader=true;
    var obj={emailId:this.email,token:this.token,pageNo:page}
    this.service.getTickets(obj).subscribe(response=>{
      this.showLoader=false;
      if(response.code=="1")
      {
        this.ticketCount=response.count;
        this.pager = this.pagerService.getPager(this.ticketCount, page);
        this.ticketsData=response.data;
      }
      else
      {
        Swal('error',response.message,'error');
      }
    })
  }

  getStatus(e,tid)
  {
    this.status=e.target.value;
    this.tid=tid;
    this.updateTickets();
  }

  updateTickets()
  {
    this.showLoader=true;
      var obj={ticketId:this.tid,token:this.token,emailId:this.email,status:this.status}
      this.service.updateTicketStatus(obj).subscribe(response=>{
        this.showLoader=false;
        if(response.code=="1")
        {
          Swal('success',response.message,'success');
          this.getTickets(1);
        }
        else
        {
          Swal('error',response.message,'error');
        }
      })
  }

  setPage(page: number) 
  {
      this.pager = this.pagerService.getPager(this.ticketCount,page);
      this.pagedItems = this.ticketsData.slice(this.pager.startIndex, this.pager.endIndex + 1);
      this.getTickets(page);
  }

  addClass()
  {
      document.getElementById('headerShow').style.zIndex='1';
      document.getElementById('outerMain').style.zIndex="9999"; 
      document.getElementById('createTicket').style.background='rgba(0, 0, 0, 0.61)';
      
  }

  closeModal()
  {
      document.getElementById('headerShow').style.zIndex='99999';
      document.getElementById('outerMain').style.zIndex="0"; 
  }


}
