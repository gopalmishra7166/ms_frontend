import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuidedtourqsComponent } from './guidedtourqs.component';

describe('GuidedtourqsComponent', () => {
  let component: GuidedtourqsComponent;
  let fixture: ComponentFixture<GuidedtourqsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidedtourqsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuidedtourqsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
