import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { MsServices } from '../services/ms-services';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
// @ts-ignore
import ApexCharts from 'apexcharts';
declare var AmCharts: any

const Swal = require('sweetalert2');
@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  time: any;
  selectedVal = "last30days";
  errormsg: any;
  pendingTicketCount: any;
  apparelCount: any;
  isMySize=false;
  activeRewards: any;
  totalUserCount: any;

  constructor(private route: Router, private cookie: CookieService, private Service: MsServices, public titleService: Title) {
    this.titleService.setTitle("Dashboard");
    this.title = 'List of user visited our website';
  }

  now = new Date();
  title: string;
  dataSource: Object;

  // for pie chart
  width = 600;
  height = 400;
  type = 'pie3d';
  dataFormat = 'json';
  dataSource1: Object; // for pie chart ended
  type1: any;
  response: any;
  dataSource2: Object;
  dataSource3: Object;
  day: any;
  week: any;
  merchantUser: any;
  months: any;
  quaterly: any;
  halfYearly: any;
  isGetMeasured=false;
  isSize2Fit=false;
  productName: any;
  isBoth: boolean;
  displayName: any = "GetMeasured";
  clickResponse: any;
  responseForGMClick: {};
  responseForSMClick: {};
  size2FitClicks: any;
  responseForGaugeGetMeasured: any;
  responseForGaugeSize2Fit: any;
  dataSourceGetMeasured: any;
  dataSourceSize2FitGauge: any;
  product_type = '';
  graphType = "Get_Measured";
  graph1 = false;
  token;
  uid;
  totalAccount = '';
  totalVisitor = '';
  approval = '';
  visitorsData = [];
  genderData = [];
  isQuickSize=false;

  ngOnInit() {
    var d = JSON.parse(localStorage.getItem('data'));
    this.token = d['token'];
    this.uid = d['emailId'];

    if (d['product'].GET_MEASURED) {
      this.isGetMeasured = true;
    }
    if (d['product'].SIZE2FIT) {
      this.isSize2Fit = true;
    }
    if (d['product'].MYSIZE) {
      this.isMySize = true;
    }
    if (d['product'].QUICKSIZE) {
      this.isQuickSize = true;
    }
    if(d['product'].GET_MEASURED){
      this.graphType='GET_MEASURED';
    }
    else if(d['product'].SIZE2FIT){
      this.graphType='SIZE2FIT';
    }
    else if(d['product'].MYSIZE){
      this.graphType='MYSIZE';
    }
    else if(d['product'].QUICKSIZE){
      this.graphType='QUICKSIZE';
    }

    this.getGraphData(this.graphType);

    // (<HTMLHeadingElement>document.getElementById('headingTitle')).innerHTML = "Dashboard";
  }

  getGraph(val) {
    this.graphType = val;
    this.getGraphData(this.graphType);
  }

  getChart(val) {
    this.selectedVal = val.target.value;
    this.getGraphData(this.graphType);
  }

  getGraphData(productName) {
    this.genderData = [];
    this.visitorsData = [];

    var obj = { emailId: this.uid, token: this.token, productName: productName,searchType:this.selectedVal }
    this.Service.graphData(obj).subscribe(response => {
      if (response.code == 1) {
        var d = response.data.last30days.visitors;

        for (var i = 0; i < d.date.length; i++) {
          this.visitorsData.push({ date: d.date[i], count: d.count[i] })
        }

        this.genderData.push(
          { Gender: "Male", count: response.data.last30days.genderWise.male }, 
          { Gender: "Female", count: response.data.last30days.genderWise.female })
        this.genderWise(this.genderData);
        this.visitors(this.visitorsData);
        this.totalAccount = response.data.accounts.subuserAccounts;
        this.totalVisitor = response.data.last30days.totalVisitor;
        this.approval = response.data.mpPendingApproval;
        this.apparelCount = response.data.apparelCount;
        this.pendingTicketCount = response.data.pendingTicketCount;
        this.activeRewards = response.data.activeRewards;
        this.totalUserCount = response.data.totalUserCount;
      }
      else {
        Swal.fire('Oops !', response.message, 'error');
      }

    })
  }

  visitors(data) {
    var chart = AmCharts.makeChart("chartdiv1", {
      "hideCredits": true,
      "type": "serial",
      "theme": "none",
      "dataProvider": data,
      "depth3D": 20,
      "angle": 30,
      "titles": [{
        "text": "Monthly Visitors Count"
      }],
      "valueAxes": [{
        "gridColor": "#FFFFFF",
        "gridAlpha": 0.2,
        "dashLength": 0
      }],
      "gridAboveGraphs": true,
      "startDuration": 1,
      "graphs": [{
        "balloonText": "[[date]]: <b>[[count]]</b>",
        "fillAlphas": 0.8,
        "lineAlpha": 0.2,
        "type": "column",
        "valueField": "count"
      }],
      "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
      },
      "categoryField": "date",
      "categoryAxis": {
        "gridPosition": "start",
        "gridAlpha": 0,
        "tickPosition": "start",
        "tickLength": 20
      },
      "export": {
        "enabled": true
      }

    });

  }

  genderWise(data) {
    var chart = AmCharts.makeChart("chartdiv2", {
      "hideCredits": true,
      "type": "pie",
      "theme": "none",
      "dataProvider": data,
      "titleField": "Gender",
      "valueField": "count",
      "labelRadius": 5,
      "titles": [{
        "text": "Gender Wise Count"
      }],
      "radius": "42%",
      "innerRadius": "60%",
      "labelText": "[[Gender]]",
      "export": {
        "enabled": true
      }
    });
  }
}