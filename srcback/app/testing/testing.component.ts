import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MsServices } from '../services/ms-services';
const Swal = require('sweetalert2');
declare var $:any;

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {

  mForm: FormGroup;
  email: any;
  merchantId: any;
  bodyShape = "";
  showLoader = false;
  isChecked = false;
  isWeight = false;
  sizes=[];
  isMale=false;
  gender='male';
  jsonData:any;
  countriesList=[];

  constructor(private service: MsServices) {
    this.mForm = new FormGroup({
      heightInInch: new FormControl(''),
      heightInCM: new FormControl(''),
      heightInFeet: new FormControl(''),
      age: new FormControl(''),
      // gender: new FormControl(''),
      weightInKg: new FormControl(''),
      weightInPound: new FormControl(''),

    })
  }

  ngOnInit() {
    var d = JSON.parse(localStorage.getItem('data'));
    if(d)
    {
      this.email = d['emailId'];
      this.merchantId = d['accountId']

    }

    if(this.gender=='male')
    {
      document.getElementById('male').style.background="var(--blue)";
      document.getElementById('male').style.color="#fff";
    }

    this.jsonData={"male": {"tops-shirts": {"regular": {"chart-type": "range", "measurementType": "body", "units": "inches", "measurementpoints": {"XXS": {"m-chest": "30-32", "m-stomach": "25-27"}, "XS": {"m-chest": "32-33.98", "m-stomach": "27-29"}, "S": {"m-chest": "35-36", "m-stomach": "30-32"}, "M": {"m-chest": "37.52-38.98", "m-stomach": "33-35.04"}, "L": {"m-chest": "40.94-42.52", "m-stomach": "36-38"}, "XL": {"m-chest": "43-44.49", "m-stomach": "39-40"}, "XXL": {"m-chest": "45-46", "m-stomach": "41-42"}}, "priority": {"chest": 2, "stomach": 3}}}, "bottoms-bottoms": {"regular": {"chart-type": "range", "measurementType": "body", "units": "inches", "measurementpoints": {"XXS": {"m-waist": "25-27", "m-hip": "30-32", "m-inseam": "31-31"}, "XS": {"m-waist": "27-29", "m-hip": "32-34", "m-inseam": "31.89-31.89"}, "S": {"m-waist": "30-32", "m-hip": "35.04-36", "m-inseam": "32.1-32.1"}, "M": {"m-waist": "33-35.04", "m-hip": "36-40", "m-inseam": "32.3-32.3"}, "L": {"m-waist": "36-38", "m-hip": "40-42.13", "m-inseam": "32.5-32.5"}, "XL": {"m-waist": "39-40", "m-hip": "43-44.1", "m-inseam": "32.68-32.68"}, "XXL": {"m-waist": "41-42", "m-hip": "45-46", "m-inseam": "32.8-32.8"}}, "priority": {"waist": 1, "hip": 3, "inseam": 4}}}}, "female": {"tops-shirts": {"regular": {"chart-type": "range", "measurementType": "body", "units": "inches", "measurementpoints": {"XXS": {"m-chest": "28.7-29.89", "m-stomach": "22.4-23.58"}, "XS": {"m-chest": "30-32", "m-stomach": "24-26"}, "S": {"m-chest": "33-35", "m-stomach": "27-28"}, "M": {"m-chest": "36-37", "m-stomach": "29-31"}, "L": {"m-chest": "35-40", "m-stomach": "32-33"}, "XL": {"m-chest": "41-43", "m-stomach": "35-37"}, "XXL": {"m-chest": "44-46", "m-stomach": "39-41"}}, "priority": {"chest": 2, "stomach": 4}}}, "bottoms-bottoms": {"regular": {"chart-type": "range", "measurementType": "body", "units": "inches", "measurementpoints": {"XXS": {"m-waist": "22.4-23.58", "m-hip": "32.28-33.46", "m-inseam": "30.5-30.5"}, "XS": {"m-waist": "24-26", "m-hip": "33.98-36", "m-inseam": "30.71-30.71"}, "S": {"m-waist": "27-28", "m-hip": "37-38", "m-inseam": "30.91-30.91"}, "M": {"m-waist": "29-31", "m-hip": "39-41", "m-inseam": "31.1-31.1"}, "L": {"m-waist": "32-33", "m-hip": "42-43", "m-inseam": "31.3-31.3"}, "XL": {"m-waist": "35-37", "m-hip": "44-46", "m-inseam": "31.5-31.5"}, "XXL": {"m-waist": "39-41", "m-hip": "47.1-49", "m-inseam": "31.7-31.7"}}, "priority": {"waist": 1, "hip": 3, "inseam": 4}}}}}

    this.countriesList=this.service.countriesData;
  
  }


  selectSize(id) {
    if (id == 's1') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s2').style.background = '#fff';
      document.getElementById('s2').style.color = '#333';
      document.getElementById('s3').style.background = '#fff';
      document.getElementById('s3').style.color = '#333';
      document.getElementById('s4').style.background = '#fff';
      document.getElementById('s4').style.color = '#333';
      document.getElementById('s5').style.background = '#fff';
      document.getElementById('s5').style.color = '#333';
      document.getElementById('s6').style.background = '#fff';
      document.getElementById('s6').style.color = '#333';
    }
    else if (id == 's2') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s1').style.background = '#fff';
      document.getElementById('s1').style.color = '#333';
      document.getElementById('s3').style.background = '#fff';
      document.getElementById('s3').style.color = '#333';
      document.getElementById('s4').style.background = '#fff';
      document.getElementById('s4').style.color = '#333';
      document.getElementById('s5').style.background = '#fff';
      document.getElementById('s5').style.color = '#333';
      document.getElementById('s6').style.background = '#fff';
      document.getElementById('s6').style.color = '#333';
    }

    else if (id == 's3') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s2').style.background = '#fff';
      document.getElementById('s2').style.color = '#333';
      document.getElementById('s1').style.background = '#fff';
      document.getElementById('s1').style.color = '#333';
      document.getElementById('s4').style.background = '#fff';
      document.getElementById('s4').style.color = '#333';
      document.getElementById('s5').style.background = '#fff';
      document.getElementById('s5').style.color = '#333';
      document.getElementById('s6').style.background = '#fff';
      document.getElementById('s6').style.color = '#333';
    }
    else if (id == 's4') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s2').style.background = '#fff';
      document.getElementById('s2').style.color = '#333';
      document.getElementById('s3').style.background = '#fff';
      document.getElementById('s3').style.color = '#333';
      document.getElementById('s1').style.background = '#fff';
      document.getElementById('s1').style.color = '#333';
      document.getElementById('s5').style.background = '#fff';
      document.getElementById('s5').style.color = '#333';
      document.getElementById('s6').style.background = '#fff';
      document.getElementById('s6').style.color = '#333';
    }
    else if (id == 's5') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s1').style.background = '#fff';
      document.getElementById('s1').style.color = '#333';
      document.getElementById('s3').style.background = '#fff';
      document.getElementById('s3').style.color = '#333';
      document.getElementById('s4').style.background = '#fff';
      document.getElementById('s4').style.color = '#333';
      document.getElementById('s2').style.background = '#fff';
      document.getElementById('s2').style.color = '#333';
      document.getElementById('s6').style.background = '#fff';
      document.getElementById('s6').style.color = '#333';
    }
    else if (id == 's6') {
      document.getElementById(id).style.background = '#011b2f';
      document.getElementById(id).style.color = '#fff';
      document.getElementById('s1').style.background = '#fff';
      document.getElementById('s1').style.color = '#333';
      document.getElementById('s3').style.background = '#fff';
      document.getElementById('s3').style.color = '#333';
      document.getElementById('s4').style.background = '#fff';
      document.getElementById('s4').style.color = '#333';
      document.getElementById('s2').style.background = '#fff';
      document.getElementById('s2').style.color = '#333';
      document.getElementById('s5').style.background = '#fff';
      document.getElementById('s5').style.color = '#333';
    }

  }

  getMeasurementQuickSize() {
 
      var height = 0;
      var weight = 0;

      if (this.isChecked) {
        height = (this.mForm.value.heightInCM) * 10
      }
      else {
        if (!this.mForm.value.heightInFeet) {
          var inc = (parseInt(this.mForm.value.heightInInch) * 2.54) * 10
          height = inc;
        }
        else if (!this.mForm.value.heightInInch) {
          var f = parseInt(this.mForm.value.heightInFeet) * 12;
          var ff = (f * 2.54) * 10;
          height = ff;
        }
        else {
          var inc = (parseInt(this.mForm.value.heightInInch) * 2.54) * 10
          var f = parseInt(this.mForm.value.heightInFeet) * 12
          var ff = (f * 2.54) * 10;
          height = ff + inc;
        }
      }

        if (this.isWeight) {
          if ((Number(this.mForm.value.weightInKg))>=30 && (Number(this.mForm.value.weightInKg))<=150) {
            weight = this.mForm.value.weightInKg
          }
          else
          {
            Swal('info!', 'weight must be in 30 to 150 kg', 'info')
            weight=0;
          }
        }
        else {
          if((Number(this.mForm.value.weightInPound))>=66 && (Number(this.mForm.value.weightInPound))<=330) 
          {
            weight = (parseInt(this.mForm.value.weightInPound)) * 2.10
          }
          else
          {
            Swal('info!', 'weight must be in 66 to 330lbs kg', 'info');
            weight=0;
          }
        }
      

      if(this.mForm.value.age<"18" || this.mForm.value.age>"80")
      {
        Swal('info!', 'age must be between 18 to 80 year', 'info');
      }
      

      var obj = {
        // "userId"     : "passionategopal001@gmail.com",
        "height": Math.round(height),
        "weight": Math.round(weight),
        "age": this.mForm.value.age,
        "gender": this.gender,
        "userType": "merchant",
        "merchantId":localStorage.getItem('demoemail'),
        "productName": "QUICKSIZE",
        "stomachShape": this.bodyShape,
        "apparelName": "shirts",
        "fitType": "regular",
        "recommendationType":"demo"
      };

      this.showLoader=true;
     
      this.service.getMeasurementQuickSize(obj).subscribe(response => {
        this.showLoader=false;
       
        if (response.code == "1") 
        {
          this.sizes=response.data.sizes
          // Swal('success', response.message, 'success');
          document.getElementById('myModal2').style.display = 'none';
          this.mForm.reset();
          document.getElementById('average').style.backgroundColor = 'rgb(2, 19, 81)';
          document.getElementById('flat').style.backgroundColor = 'rgb(2, 19, 81)';
          document.getElementById('belly').style.backgroundColor = 'rgb(2, 19, 81)';
          document.getElementById('size').style.display='block';
          document.getElementById('size').style.opacity='1';
        
        }
        else {
          Swal('Oops!', response.message, 'info');
          document.getElementById('myModal2').style.display = 'none';
        }
      })
  
  }

  getBodyShape(id) {
    if (id == 'flat') {
      this.bodyShape = id;
      document.getElementById(id).style.backgroundColor = '#ecd00a';
      document.getElementById('average').style.backgroundColor = 'rgb(2, 19, 81)';
      document.getElementById('belly').style.backgroundColor = 'rgb(2, 19, 81)';
    }
    else if (id == 'average') {
      this.bodyShape = id;
      document.getElementById(id).style.backgroundColor = '#ecd00a';
      document.getElementById('flat').style.backgroundColor = 'rgb(2, 19, 81)';
      document.getElementById('belly').style.backgroundColor = 'rgb(2, 19, 81)';
    }
    else if (id == 'belly') {
      this.bodyShape = id;
      document.getElementById(id).style.backgroundColor = '#ecd00a';
      document.getElementById('average').style.backgroundColor = 'rgb(2, 19, 81)';
      document.getElementById('flat').style.backgroundColor = 'rgb(2, 19, 81)';
    }
    else {
      this.bodyShape = "";
    }

  }

  switchValue(e) {
    if (e.target.checked) {
      this.isChecked = true;
    }
    else {
      this.isChecked = false;
    }
  }

  switchWeight(e) {
    if (e.target.checked) {
      this.isWeight = true;
    }
    else {
      this.isWeight = false;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  addClass()
  {
      document.getElementById('headerShow').style.zIndex='1';
      document.getElementById('outerMain').style.zIndex="9999"; 
      document.getElementById('size').style.background='rgba(0, 0, 0, 0.61)';
  }

  closeModal()
  {
      document.getElementById('headerShow').style.zIndex='99999';
      document.getElementById('outerMain').style.zIndex="0"; 
      document.getElementById('size').style.display='none';
  }

  selectGender(val)
  {
    
    if(val=='m')
    {
      this.sizes=[];
      this.gender='male';
      document.getElementById('male').style.background="var(--blue)";
      document.getElementById('male').style.color="#fff";
      document.getElementById('female').style.background="var(--gray)";
    }
    else
    {
      this.sizes=[];
      this.gender='female';
      document.getElementById('female').style.background="var(--pink)";
      document.getElementById('female').style.color="#fff";
      document.getElementById('male').style.background="var(--gray)";
    }
  }


}
